-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2017 at 04:44 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eagle`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_type`
--

CREATE TABLE `account_type` (
  `id` int(11) NOT NULL,
  `account_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `account_type`
--

INSERT INTO `account_type` (`id`, `account_type`, `account_status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Stock', 0, '2017-06-03 05:39:13', '2017-06-03 05:39:13', 106, NULL),
(2, 'Cost Of Goods Sold', 1, '2017-06-03 05:39:29', '2017-06-03 05:39:29', 106, NULL),
(3, '	Expense', 1, '2017-06-03 05:39:35', '2017-06-03 05:39:35', 106, NULL),
(4, 'Income', 0, '2017-07-06 03:32:18', '2017-07-06 03:32:18', 106, NULL),
(5, 'Other Liability', 1, '2017-07-06 03:45:13', '2017-07-06 03:45:13', 106, NULL),
(6, 'Accounts Payable', 0, '2017-07-06 03:45:40', '2017-07-06 03:45:40', 106, NULL),
(7, 'Accounts Receivable', 1, '2017-07-06 03:54:08', '2017-07-06 03:54:08', 106, NULL),
(8, 'Cash', 0, '2017-07-06 03:55:11', '2017-07-06 03:55:11', 106, NULL),
(9, 'Other Current Asset', 0, '2017-07-06 04:03:10', '2017-07-06 04:03:10', 106, NULL),
(10, 'Equity', 1, '2017-07-06 04:11:29', '2017-07-06 04:11:29', 106, NULL),
(11, 'Other Current Liability', 1, '2017-07-06 04:29:44', '2017-07-06 04:29:44', 106, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `basic_info`
--

CREATE TABLE `basic_info` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fathers_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mothers_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alternate_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alternate_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `nid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary` float(7,2) DEFAULT NULL,
  `employee_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital_status` int(11) DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `present_address` longtext COLLATE utf8_unicode_ci,
  `permanent_address` longtext COLLATE utf8_unicode_ci,
  `contract_type_id` int(11) DEFAULT NULL,
  `contract_base_id` int(11) DEFAULT NULL,
  `remarks` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `basic_info`
--

INSERT INTO `basic_info` (`id`, `first_name`, `last_name`, `photo`, `fathers_name`, `mothers_name`, `email`, `alternate_email`, `mobile`, `alternate_mobile`, `department_id`, `designation_id`, `level_id`, `nid`, `salary`, `employee_code`, `passport_no`, `birth`, `gender`, `religion`, `marital_status`, `nationality`, `present_address`, `permanent_address`, `contract_type_id`, `contract_base_id`, `remarks`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(10, 'hh', NULL, '1494833839.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'h', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-05-15 01:37:19', '2017-05-15 01:37:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` int(11) NOT NULL,
  `bill_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `order_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bill_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `amount_due` float(10,2) DEFAULT NULL,
  `tax_status` int(11) DEFAULT NULL,
  `note` longtext COLLATE utf8_unicode_ci,
  `total` float(10,2) DEFAULT NULL,
  `bill_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Unpaid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bills`
--

INSERT INTO `bills` (`id`, `bill_no`, `vendor_id`, `order_no`, `bill_date`, `due_date`, `amount_due`, `tax_status`, `note`, `total`, `bill_status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, 'Bill 3', 1, '3', '2017-06-08', '2017-06-15', 4500.00, NULL, NULL, 8150.00, 'Partially Paid', '2017-06-08 01:22:46', '2017-06-13 06:03:23', NULL, NULL),
(3, 'Bill 1', 1, '2', '2017-06-14', '2017-06-20', 0.00, NULL, NULL, 200.00, 'Paid', '2017-06-10 02:56:32', '2017-06-13 02:51:18', NULL, NULL),
(4, 'bill 2', 2, 'order 2', '2017-06-11', '2017-06-17', 13.00, NULL, NULL, 26.00, 'Partially Paid', '2017-06-11 03:41:16', '2017-06-12 04:08:19', NULL, NULL),
(5, 'Bill 2 Vendor 2.1', 2, 'Order 2 Vendor 2.1', '2017-06-14', '2017-06-24', 1450744.00, NULL, NULL, 1456745.00, 'Partially Paid', '2017-06-14 02:58:19', '2017-06-14 03:11:57', NULL, NULL),
(6, 'Bill 2 Vendor 2.2', 2, 'Order 2 Vendor 2.1', '2017-06-15', '2017-06-23', 2597781.00, NULL, NULL, 2601781.00, 'Partially Paid', '2017-06-14 02:59:27', '2017-06-14 03:09:50', NULL, NULL),
(15, 'y5h6', 3, 'jh76ujh7', '2017-06-10', '2017-06-24', 605.50, NULL, NULL, 605.50, 'Open', '2017-06-15 06:14:29', '2017-06-15 06:14:29', NULL, NULL),
(16, NULL, 2, 'yjyj', NULL, NULL, 5849.00, NULL, NULL, 5849.00, 'Open', '2017-06-19 02:13:31', '2017-06-19 02:13:31', NULL, NULL),
(17, NULL, 1, 'aaa', NULL, NULL, 0.00, NULL, NULL, 361.00, 'Paid', '2017-06-19 03:50:07', '2017-07-02 05:42:07', NULL, NULL),
(18, 'uyj', 1, 'uyjy', '2017-06-08', '2017-06-22', 599.00, NULL, NULL, 599.00, 'Open', '2017-06-19 04:08:41', '2017-06-19 04:08:41', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bill_accounts`
--

CREATE TABLE `bill_accounts` (
  `id` int(11) NOT NULL,
  `bill_id` int(11) DEFAULT NULL,
  `acc_id` int(11) DEFAULT NULL,
  `acc_description` longtext COLLATE utf8_unicode_ci,
  `quantity` float(10,2) DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bill_accounts`
--

INSERT INTO `bill_accounts` (`id`, `bill_id`, `acc_id`, `acc_description`, `quantity`, `rate`, `tax_id`, `amount`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 15, 3, NULL, 1.00, 77.00, 77, 77, '2017-06-15 06:14:29', '2017-06-15 06:14:29', NULL, NULL),
(2, 15, 2, NULL, 1.00, 3.50, 4, 3.5, '2017-06-15 06:14:29', '2017-06-15 06:14:29', NULL, NULL),
(3, 15, 3, NULL, 1.00, 525.00, 525, 525, '2017-06-15 06:14:29', '2017-06-15 06:14:29', NULL, NULL),
(4, 16, NULL, NULL, 1.00, 425.00, 425, 425, '2017-06-19 02:13:31', '2017-06-19 02:13:31', 106, NULL),
(5, 16, NULL, NULL, 1.00, 5424.00, 5424, 5424, '2017-06-19 02:13:31', '2017-06-19 02:13:31', 106, NULL),
(6, 17, 1, NULL, 1.00, 210.00, 210, 210, '2017-06-19 03:50:07', '2017-06-19 03:50:07', 106, NULL),
(7, 17, 3, NULL, 1.00, 151.00, 151, 151, '2017-06-19 03:50:07', '2017-06-19 03:50:07', 106, NULL),
(8, 18, 1, NULL, 1.00, 54.00, 54, 54, '2017-06-19 04:08:41', '2017-06-19 04:08:41', 106, NULL),
(9, 18, 5, NULL, 1.00, 545.00, 545, 545, '2017-06-19 04:08:41', '2017-06-19 04:08:41', 106, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `brand_name` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Brand 1', '2017-05-29 02:30:06', '2017-05-29 02:30:06', NULL, NULL),
(2, 'Nokia', '2017-06-20 01:06:50', '2017-06-20 01:06:50', NULL, NULL),
(3, 'abcd', '2017-07-10 23:48:19', '2017-07-10 23:48:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contract_base`
--

CREATE TABLE `contract_base` (
  `id` int(11) NOT NULL,
  `basename` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contract_base`
--

INSERT INTO `contract_base` (`id`, `basename`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'hourly', '2017-05-14 02:21:13', '2017-05-15 01:53:22', NULL, NULL),
(4, 'Weekly', '2017-05-14 03:06:32', '2017-05-14 07:25:04', NULL, NULL),
(5, 'Monthly', '2017-05-14 07:25:16', '2017-05-14 07:25:16', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contract_type`
--

CREATE TABLE `contract_type` (
  `id` int(11) NOT NULL,
  `typename` longtext COLLATE utf8_unicode_ci,
  `base_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contract_type`
--

INSERT INTO `contract_type` (`id`, `typename`, `base_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Permanent', 5, '2017-05-14 02:49:26', '2017-05-14 07:26:02', NULL, NULL),
(4, 'New type 1', NULL, '2017-05-15 23:45:41', '2017-05-15 23:45:51', NULL, NULL),
(6, 'Contractual', NULL, '2017-07-31 07:29:38', '2017-07-31 07:29:38', NULL, NULL),
(7, 'Daily', NULL, '2017-07-31 07:31:10', '2017-07-31 07:31:10', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `creditnotes`
--

CREATE TABLE `creditnotes` (
  `id` int(11) NOT NULL,
  `credit_note_no` longtext COLLATE utf8_unicode_ci,
  `customer_id` int(11) DEFAULT NULL,
  `reference_no` longtext COLLATE utf8_unicode_ci,
  `creditnote_date` date DEFAULT NULL,
  `creditnote_amount` float(10,2) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `creditnotes`
--

INSERT INTO `creditnotes` (`id`, `credit_note_no`, `customer_id`, `reference_no`, `creditnote_date`, `creditnote_amount`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'note1', 1, 'ref1', NULL, 57995.00, 'Draft', '2017-07-03 06:59:17', '2017-07-03 06:59:17', 106, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `creditnote_items`
--

CREATE TABLE `creditnote_items` (
  `id` int(11) NOT NULL,
  `creditnote_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `qty` float(10,2) DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `creditnote_items`
--

INSERT INTO `creditnote_items` (`id`, `creditnote_id`, `item_id`, `qty`, `rate`, `amount`, `tax_id`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 1, 9, 1.00, 52723.00, 52723.00, NULL, NULL, '2017-07-03 06:59:17', '2017-07-03 06:59:17', NULL, NULL),
(2, 1, 1, 1.00, 5272.00, 5272.00, NULL, NULL, '2017-07-03 06:59:17', '2017-07-03 06:59:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `foreign_currency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exchange_rate` float(10,2) DEFAULT NULL,
  `exchange_date` datetime DEFAULT NULL,
  `currency_note` longtext COLLATE utf8_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `foreign_currency_code`, `exchange_rate`, `exchange_date`, `currency_note`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'USD', 81.00, '2017-06-05 00:00:00', 'jkasbhau iudhlu iuefdebhc uicj', NULL, NULL, '2017-06-05 03:42:58', '2017-06-05 03:42:58'),
(2, 'EUR', 59.00, NULL, NULL, NULL, NULL, '2017-06-05 03:48:50', '2017-06-05 03:48:50'),
(3, 'INR', 30.00, NULL, NULL, NULL, NULL, '2017-06-08 00:13:46', '2017-06-08 00:13:46'),
(4, 'USD', 88.00, NULL, '5yjt', NULL, NULL, '2017-07-12 00:56:38', '2017-07-12 00:56:38');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `salutation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_department` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_payment_terms_id` int(11) DEFAULT NULL,
  `customer_portal_language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_attention` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_address` longtext COLLATE utf8_unicode_ci,
  `customer_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_zip_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `salutation`, `customer_first_name`, `customer_last_name`, `company_name`, `customer_email`, `customer_mobile`, `customer_designation`, `customer_department`, `customer_website`, `customer_currency`, `customer_payment_terms_id`, `customer_portal_language`, `customer_attention`, `customer_address`, `customer_city`, `customer_state`, `customer_zip_code`, `customer_country`, `customer_fax`, `remarks`, `created_at`, `updated_at`, `created_by`, `update_by`) VALUES
(1, NULL, 'Thomes', 'Albert', 'Company 11', NULL, '00125874', NULL, NULL, NULL, '2', NULL, '1', 'Attention', '18, Banani, Dhaka', 'NY', 'USA', '1111', 'usa', NULL, 'nhgn gnhytjyt tyjyt', '2017-05-15 05:03:56', '2017-07-10 06:00:31', NULL, NULL),
(2, NULL, 'New', 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, '1', NULL, '8, Nikunja 2, Khilkhet', NULL, NULL, NULL, 'Spain', NULL, NULL, '2017-06-13 04:15:25', '2017-07-10 05:54:32', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `short_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department_name` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `short_code`, `department_name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(5, 'HR', 'Human Resource', '2017-05-14 00:30:55', '2017-05-14 07:22:22', NULL, NULL),
(6, 'NEWd.', 'New Dept', '2017-05-29 23:32:48', '2017-07-31 07:24:45', NULL, NULL),
(7, 'Dev', 'Development', '2017-07-31 07:25:27', '2017-07-31 07:25:27', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(11) NOT NULL,
  `designation_name` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation_name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(3, 'Asst. manager', '2017-05-14 01:41:47', '2017-07-31 07:27:29', NULL, NULL),
(4, 'Chairman', '2017-05-14 01:41:55', '2017-05-14 07:24:18', NULL, NULL),
(5, 'Manager', '2017-05-14 02:20:08', '2017-05-14 07:23:52', NULL, NULL),
(6, 'Senior Officer', '2017-07-31 07:27:44', '2017-07-31 07:27:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fathers_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mothers_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alternate_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alternate_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `nid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary` float(7,2) DEFAULT NULL,
  `employee_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital_status` int(11) DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `present_address` longtext COLLATE utf8_unicode_ci,
  `permanent_address` longtext COLLATE utf8_unicode_ci,
  `contract_type_id` int(11) DEFAULT NULL,
  `contract_base_id` int(11) DEFAULT NULL,
  `remarks` longtext COLLATE utf8_unicode_ci,
  `ot_rate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `joining_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `photo`, `fathers_name`, `mothers_name`, `email`, `alternate_email`, `mobile`, `alternate_mobile`, `department_id`, `designation_id`, `level_id`, `nid`, `salary`, `employee_code`, `passport_no`, `birth`, `gender`, `religion`, `marital_status`, `nationality`, `present_address`, `permanent_address`, `contract_type_id`, `contract_base_id`, `remarks`, `ot_rate`, `joining_date`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(12, 'John', 'Doe', '', NULL, NULL, NULL, NULL, NULL, NULL, 4, 3, 1, NULL, 6566.00, 'Emp-002', NULL, NULL, 0, '0', 1, NULL, NULL, NULL, 2, 1, NULL, '534', '5 May, 2017', '2017-05-29 10:56:20', '2017-05-16 07:27:35', NULL, NULL),
(13, 'New', 'Employee', '', 'hg76', 'h7uh', NULL, NULL, NULL, NULL, 5, 5, 1, NULL, 30000.00, '121', NULL, NULL, 1, '0', 1, NULL, NULL, NULL, 1, 5, NULL, '12', '2017-01-01', '2017-05-29 10:56:07', '2017-05-29 04:31:15', NULL, NULL),
(15, 'Dominic', 'letto', '', 'MD.', NULL, NULL, NULL, NULL, NULL, 5, 5, 4, NULL, 2778.00, '003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 2, 1, NULL, '5', '2018-07-13', '2017-07-31 06:36:40', '2017-07-31 06:36:40', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `estimates`
--

CREATE TABLE `estimates` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `estimate_no` longtext COLLATE utf8_unicode_ci,
  `reference_no` longtext COLLATE utf8_unicode_ci,
  `status` text COLLATE utf8_unicode_ci,
  `estimate_date` date DEFAULT NULL,
  `estimate_expiry_date` date DEFAULT NULL,
  `estimate_amount` float(10,2) DEFAULT NULL,
  `due_amount` float(10,2) DEFAULT NULL,
  `sales_person_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `estimates`
--

INSERT INTO `estimates` (`id`, `customer_id`, `estimate_no`, `reference_no`, `status`, `estimate_date`, `estimate_expiry_date`, `estimate_amount`, `due_amount`, `sales_person_id`, `project_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(4, 1, 'EST-001', 'vcvf', 'Draft', '2017-07-02', NULL, 8137.00, 8137.00, 2, NULL, '2017-07-02 02:19:15', '2017-07-02 02:19:15', 106, NULL),
(5, 2, 'EST-002', 'ref-est-2', 'Draft', '2017-07-11', '2017-07-18', 4707.00, 4707.00, 2, NULL, '2017-07-10 22:38:42', '2017-07-10 22:38:42', 106, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `estimate_items`
--

CREATE TABLE `estimate_items` (
  `id` int(11) NOT NULL,
  `estimate_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `qty` float(10,2) DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `estimate_items`
--

INSERT INTO `estimate_items` (`id`, `estimate_id`, `item_id`, `qty`, `rate`, `tax_id`, `status`, `amount`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 4, 1, 1.00, 5285.00, NULL, NULL, 5285.00, '2017-07-02 02:19:15', '2017-07-02 02:19:15', NULL, NULL),
(2, 4, 9, 1.00, 2852.00, NULL, NULL, 2852.00, '2017-07-02 02:19:15', '2017-07-02 02:19:15', NULL, NULL),
(3, 5, 1, 1.00, 252.00, NULL, NULL, 252.00, '2017-07-10 22:38:42', '2017-07-10 22:38:42', NULL, NULL),
(4, 5, 2, 1.00, 4455.00, NULL, NULL, 4455.00, '2017-07-10 22:38:42', '2017-07-10 22:38:42', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `expances`
--

CREATE TABLE `expances` (
  `id` int(11) NOT NULL,
  `expence_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `claimant_id` int(11) DEFAULT NULL,
  `milage_calculate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `distance` float(10,2) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `paid_through` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` longtext COLLATE utf8_unicode_ci,
  `expanse_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `expances`
--

INSERT INTO `expances` (`id`, `expence_date`, `claimant_id`, `milage_calculate`, `distance`, `account_id`, `currency_id`, `amount`, `tax_id`, `paid_through`, `vendor_id`, `reference`, `note`, `expanse_image`, `customer_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(7, '2017-06-10', NULL, NULL, NULL, 1, 1, 122, 1, 1, 1, 'Ref 1', 'note 1', '1497077621.jpg', NULL, '2017-06-10 00:53:41', '2017-06-10 00:53:41', 106, NULL),
(8, '2017-06-11', 1, NULL, 55.00, NULL, NULL, 105, 2, 2, 2, 'Ref 2', 'Note 2', '1497077949.jpg', NULL, '2017-06-10 00:59:09', '2017-06-10 00:59:09', 106, NULL),
(9, NULL, NULL, NULL, NULL, 2, 1, 300, NULL, 5, 3, NULL, '3', NULL, NULL, '2017-06-10 01:50:38', '2017-06-10 01:50:38', 106, NULL),
(10, NULL, NULL, NULL, NULL, 3, 2, 400, NULL, 1, 4, NULL, '3', NULL, NULL, '2017-06-10 01:50:38', '2017-06-10 01:50:38', 106, NULL),
(11, '2017-06-15', NULL, NULL, NULL, 2, 1, 555, NULL, 1, 3, NULL, '5555', NULL, NULL, '2017-06-10 01:59:49', '2017-06-10 01:59:49', 106, NULL),
(12, '2017-06-16', NULL, NULL, NULL, 3, 2, 666, NULL, 2, 2, NULL, '6666', NULL, NULL, '2017-06-10 01:59:49', '2017-06-10 01:59:49', 106, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` int(11) NOT NULL,
  `start_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `holiday_name` longtext COLLATE utf8_unicode_ci,
  `reason` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`id`, `start_date`, `end_date`, `holiday_name`, `reason`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, '16 May, 2017', '20 May, 2017', 'Holiday 01', 'Test Holiday 01', '2017-05-16 02:56:25', '2017-05-16 02:56:25', NULL, NULL),
(4, '2017-05-16', '2017-05-27', 'new', 'wdw', '2017-05-30 00:13:15', '2017-05-30 00:15:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `sales_person_id` int(11) DEFAULT NULL,
  `term_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_amount` float(10,2) DEFAULT NULL,
  `due_amount` float(10,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `customer_id`, `invoice_no`, `order_no`, `invoice_date`, `due_date`, `sales_person_id`, `term_id`, `status`, `invoice_amount`, `due_amount`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(7, 1, 'INV-001', '4', '2017-07-02', '2017-07-09', 2, 2, 'Overdue by 1 Day(s)', 53458736.00, 40000000.00, 106, NULL, '2017-07-02 05:48:55', '2017-07-10 12:04:16'),
(8, 2, 'inv 2', 'order 2', '2017-07-10', '2017-07-12', 2, 2, 'Paid', 253540.00, 0.00, 106, NULL, '2017-07-10 12:54:55', '2017-07-12 07:28:51'),
(9, 2, 'Inv 3', 'order 3', '2017-07-11', '2017-07-12', 2, 1, 'Paid', 28345280.00, 0.00, 106, NULL, '2017-07-11 05:11:02', '2017-07-12 07:27:12'),
(10, 2, 'Inv 3', 'order 2', '2017-07-12', '2017-07-20', 2, 1, 'Draft', 140.00, 140.00, 106, NULL, '2017-07-12 07:16:27', '2017-07-12 07:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE `invoice_items` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `qty` float(10,2) DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoice_items`
--

INSERT INTO `invoice_items` (`id`, `invoice_id`, `item_id`, `qty`, `rate`, `tax_id`, `status`, `amount`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 7, 7, 1.00, 53454232.00, 1, NULL, 53454232.00, '2017-07-01 23:48:55', '2017-07-01 23:48:55', NULL, NULL),
(2, 7, 9, 1.00, 4500.00, NULL, NULL, 4500.00, '2017-07-01 23:48:55', '2017-07-01 23:48:55', NULL, NULL),
(3, 8, 1, 41.00, 5636.00, NULL, NULL, 231076.00, '2017-07-10 06:54:55', '2017-07-10 06:54:55', NULL, NULL),
(4, 8, 2, 52.00, 432.00, NULL, NULL, 22464.00, '2017-07-10 06:54:55', '2017-07-10 06:54:55', NULL, NULL),
(5, 9, 3, 456.00, 6556.00, 5, NULL, 2989536.00, '2017-07-10 23:11:02', '2017-07-10 23:11:02', NULL, NULL),
(6, 9, 2, 464.00, 54646.00, NULL, NULL, 25355744.00, '2017-07-10 23:11:02', '2017-07-10 23:11:02', NULL, NULL),
(7, 10, 1, 1.00, 7.00, 7, NULL, 7.00, '2017-07-12 01:16:28', '2017-07-12 01:16:28', NULL, NULL),
(8, 10, 3, 1.00, 33.00, 1, NULL, 33.00, '2017-07-12 01:16:28', '2017-07-12 01:16:28', NULL, NULL),
(9, 10, NULL, 1.00, 44.00, 1, NULL, 44.00, '2017-07-12 01:16:28', '2017-07-12 01:16:28', NULL, NULL),
(10, 10, NULL, 1.00, 56.00, NULL, NULL, 56.00, '2017-07-12 01:16:28', '2017-07-12 01:16:28', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `item_name` longtext COLLATE utf8_unicode_ci,
  `item_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sku` longtext COLLATE utf8_unicode_ci,
  `unit` longtext COLLATE utf8_unicode_ci,
  `manufacturer` longtext COLLATE utf8_unicode_ci,
  `upc` longtext COLLATE utf8_unicode_ci,
  `ean` longtext COLLATE utf8_unicode_ci,
  `brand` longtext COLLATE utf8_unicode_ci,
  `mpn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isbn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selling_price` float(10,2) DEFAULT NULL,
  `selling_account` longtext COLLATE utf8_unicode_ci,
  `selling_description` longtext COLLATE utf8_unicode_ci,
  `tax` int(11) DEFAULT NULL,
  `purchase_price` float(10,2) DEFAULT NULL,
  `purchase_account` longtext COLLATE utf8_unicode_ci,
  `purchase_description` longtext COLLATE utf8_unicode_ci,
  `preferred_vendor` longtext COLLATE utf8_unicode_ci,
  `tracking_account` longtext COLLATE utf8_unicode_ci,
  `reorder_level` longtext COLLATE utf8_unicode_ci,
  `opening_stock` longtext COLLATE utf8_unicode_ci,
  `opening_stock_value` float(10,2) DEFAULT NULL,
  `item_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_name`, `item_image`, `sku`, `unit`, `manufacturer`, `upc`, `ean`, `brand`, `mpn`, `isbn`, `selling_price`, `selling_account`, `selling_description`, `tax`, `purchase_price`, `purchase_account`, `purchase_description`, `preferred_vendor`, `tracking_account`, `reorder_level`, `opening_stock`, `opening_stock_value`, `item_status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'item 1', NULL, 'sku1', NULL, '3r34r3', 'grg', 'eer', 'New Brand', 'vggv', 'bbgf', 534.00, NULL, NULL, 2, 3434.00, NULL, 'rgrgrgr ttt4rt ttrter', 'Audi', NULL, '12', '100', 100.00, NULL, '2017-05-29 01:50:14', '2017-05-29 01:50:14', NULL, NULL),
(2, 'uyj67u', NULL, 'uj6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Alfa Romeo', NULL, NULL, NULL, NULL, NULL, '2017-05-29 01:53:54', '2017-05-29 01:53:54', NULL, NULL),
(3, 'like', '1496044579.gif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Alfa Romeo', NULL, NULL, NULL, NULL, NULL, '2017-05-29 01:56:19', '2017-05-29 01:56:19', NULL, NULL),
(7, 'Brand Item', NULL, NULL, NULL, NULL, NULL, NULL, 'Brand 1', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Alfa Romeo', NULL, NULL, NULL, NULL, NULL, '2017-05-29 02:31:13', '2017-05-29 02:31:13', NULL, NULL),
(8, 'new', '1496577507.jpg', 'ewe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-04 05:58:28', '2017-06-04 05:58:28', NULL, NULL),
(9, 'Nokia 3310', '1497942410.jpg', 'sku-1', NULL, 'HM Global', '323', '123', 'Nokia', '2334', '1212', 4000.00, NULL, NULL, 1, 4500.00, NULL, 'featured Phone', 'Alfa Romeo', NULL, '2', 'Fifty', 50.00, NULL, '2017-06-20 01:06:50', '2017-06-20 01:06:50', NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-21 03:19:47', '2017-06-21 03:19:47', NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abcd', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-10 23:48:19', '2017-07-10 23:48:19', NULL, NULL),
(12, 'Subaru mountineers', '1501509453.jpg', 'sksoaj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', '2', '22', 22.00, NULL, '2017-07-31 07:57:33', '2017-07-31 07:57:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `journals`
--

CREATE TABLE `journals` (
  `id` int(11) NOT NULL,
  `journal_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `journal_reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `journal_note` longtext COLLATE utf8_unicode_ci,
  `journal_currency` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `journals`
--

INSERT INTO `journals` (`id`, `journal_date`, `journal_reference`, `journal_note`, `journal_currency`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(9, '2017-06-04', '1', 'ergerbh  5nu76 65b6hu5', 1, '2017-06-04 05:29:31', '2017-06-04 05:29:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `journal_accounts`
--

CREATE TABLE `journal_accounts` (
  `id` int(11) NOT NULL,
  `journal_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `contact` longtext COLLATE utf8_unicode_ci,
  `tax_id` int(11) DEFAULT NULL,
  `debit` float(10,2) DEFAULT NULL,
  `credit` float(10,2) DEFAULT NULL,
  `total` float(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `journal_accounts`
--

INSERT INTO `journal_accounts` (`id`, `journal_id`, `account_id`, `description`, `contact`, `tax_id`, `debit`, `credit`, `total`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 9, 5, 'b5t ythnhty', 'ytbhyt ytj ny', NULL, 12.00, 12.00, NULL, '2017-06-04 05:29:31', '2017-06-04 05:29:31', NULL, NULL),
(2, 9, 2, 'yt hjn76', 'bn6h6', NULL, 13.00, 13.00, NULL, '2017-06-04 05:29:32', '2017-06-04 05:29:32', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `start_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reason` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leaves`
--

INSERT INTO `leaves` (`id`, `employee_id`, `start_date`, `end_date`, `reason`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(4, 12, '2017-05-10', '2017-05-12', '1012', '2017-05-30 00:30:33', '2017-05-30 00:36:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leave_planners`
--

CREATE TABLE `leave_planners` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `leave_plan` longtext COLLATE utf8_unicode_ci,
  `start_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reason` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leave_planners`
--

INSERT INTO `leave_planners` (`id`, `employee_id`, `leave_plan`, `start_date`, `end_date`, `reason`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(6, 15, 'really sick', '2017-08-17', '2017-07-01', 'hyg-0', '2017-07-31 06:45:22', '2017-08-01 04:53:14', NULL, NULL),
(7, 15, 'uuupupu', '2017-08-03', '2017-08-26', 'no one knows', '2017-08-01 04:53:52', '2017-08-01 04:53:52', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `level` longtext COLLATE utf8_unicode_ci,
  `remarks` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `level`, `remarks`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, 'new level', 'n-l', '2017-05-29 23:35:31', '2017-05-29 23:37:33', NULL, NULL),
(3, 'newest', 'working!', '2017-07-31 06:30:59', '2017-07-31 06:30:59', NULL, NULL),
(4, 'exclusive-1', 'earkly', '2017-07-31 06:31:35', '2017-07-31 06:33:06', NULL, NULL),
(8, 'Intermediate', 'Experienced', '2017-07-31 07:26:45', '2017-07-31 07:26:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `payment_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference` longtext COLLATE utf8_unicode_ci,
  `payment_mode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid_through` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bills` text COLLATE utf8_unicode_ci,
  `bill_ids` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unused_amount` float(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `vendor_id`, `amount`, `payment_date`, `reference`, `payment_mode`, `paid_through`, `bills`, `bill_ids`, `unused_amount`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, 1, 1010, NULL, 'Ref 1', 'Cash', 'Card', 'Bill 3,Bill 1', '2,3', 0.00, '2017-06-13 02:51:18', '2017-06-13 02:51:18', NULL, NULL),
(3, 1, 500, NULL, NULL, NULL, NULL, 'Bill 3', '2', NULL, '2017-06-13 06:01:43', '2017-06-13 06:01:43', NULL, NULL),
(4, 1, 500, '2017-06-13', 'ref 500', 'Cash', 'Card', 'Bill 3', '2', NULL, '2017-06-13 06:03:23', '2017-06-13 06:03:23', NULL, NULL),
(6, 2, 5000, '2017-06-15', 'Ref-ven-2', 'Cash', 'Cash', 'Bill 2 Vendor 2.1,Bill 2 Vendor 2.2', '5,6', NULL, '2017-06-14 03:09:50', '2017-06-14 03:09:50', NULL, NULL),
(7, 2, 4048539, NULL, NULL, NULL, NULL, 'Bill 2 Vendor 2.1', '5', NULL, '2017-06-14 03:11:58', '2017-06-14 03:11:58', NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '2017-06-20 05:57:14', '2017-06-20 05:57:14', NULL, NULL),
(9, 1, 5460, NULL, NULL, NULL, NULL, '', '17', NULL, '2017-07-02 05:42:07', '2017-07-02 05:42:07', NULL, NULL),
(10, NULL, 1000, '2017-07-11', NULL, '1', 'Cash', '', '', NULL, '2017-07-11 00:41:06', '2017-07-11 00:41:06', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payments_recieved`
--

CREATE TABLE `payments_recieved` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `unused_amount` float(10,2) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_mode` int(11) DEFAULT NULL,
  `deposit_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `paid_through` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payments_recieved`
--

INSERT INTO `payments_recieved` (`id`, `customer_id`, `amount`, `unused_amount`, `payment_date`, `reference`, `payment_mode`, `deposit_to`, `status`, `paid_through`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 1, 3458736.00, 0.00, '2017-07-10 00:00:00', 'reference-1', 2, NULL, NULL, NULL, '2017-07-10 04:26:49', '2017-07-10 04:26:49', NULL, NULL),
(3, 1, 10000000.00, 0.00, '2017-07-11 00:00:00', 'y', NULL, NULL, NULL, NULL, '2017-07-10 06:04:16', '2017-07-10 06:04:16', NULL, NULL),
(4, 2, 345280.00, 0.00, '2017-07-11 00:00:00', 'Record Payment 2', 2, NULL, NULL, NULL, '2017-07-10 23:38:21', '2017-07-10 23:38:21', NULL, NULL),
(9, 2, 3540.00, 0.00, '2017-07-11 00:00:00', 'yhyth', 3, NULL, NULL, NULL, '2017-07-11 04:56:58', '2017-07-11 04:56:58', NULL, NULL),
(10, 2, 28000000.00, 0.00, '2017-07-28 00:00:00', 'hyhty', 3, NULL, NULL, NULL, '2017-07-12 01:27:12', '2017-07-12 01:27:12', NULL, NULL),
(11, 2, 25000000.00, 24750000.00, '2017-07-12 00:00:00', 'h', 2, NULL, NULL, NULL, '2017-07-12 01:28:51', '2017-07-12 01:28:51', NULL, NULL),
(12, 1, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, '2017-07-25 08:15:40', '2017-07-25 08:15:40', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_modes`
--

CREATE TABLE `payment_modes` (
  `id` int(11) NOT NULL,
  `mode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_modes`
--

INSERT INTO `payment_modes` (`id`, `mode`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Cash', NULL, '2017-07-09 06:24:19', '2017-07-09 06:24:19', 106, NULL),
(2, 'Petty Cash', NULL, '2017-07-09 06:24:57', '2017-07-09 06:24:57', 106, NULL),
(3, 'Bank Transfer', NULL, '2017-07-09 06:25:20', '2017-07-09 06:25:20', 106, NULL),
(4, 'Credit Card', NULL, '2017-07-09 06:25:28', '2017-07-09 06:25:28', 106, NULL),
(5, 'check', NULL, '2017-07-09 06:25:52', '2017-07-09 06:25:52', 106, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payrolls`
--

CREATE TABLE `payrolls` (
  `id` int(11) NOT NULL,
  `payroll_month` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `salaried_qty` float(10,2) DEFAULT NULL,
  `ot_hour` float(10,2) DEFAULT NULL,
  `estimated_salary` float(10,2) DEFAULT NULL,
  `estimated_ot_bill` float(10,2) DEFAULT NULL,
  `paid_salary` float(10,2) DEFAULT NULL,
  `paid_ot_bill` float(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payrolls`
--

INSERT INTO `payrolls` (`id`, `payroll_month`, `employee_id`, `salaried_qty`, `ot_hour`, `estimated_salary`, `estimated_ot_bill`, `paid_salary`, `paid_ot_bill`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(11, '2016-12', 11, NULL, 50.00, 5235.00, 20000.00, 15000.00, 5000.00, '2017-05-17 07:06:10', '2017-05-19 23:57:44', NULL, NULL),
(12, '2017-04', 11, NULL, 575.00, 757.00, 575.00, 757.00, 757.00, '2017-05-22 04:24:55', '2017-05-23 06:13:14', NULL, NULL),
(14, '2017-05', 11, NULL, 1.00, NULL, NULL, NULL, NULL, '2017-05-22 04:44:23', '2017-05-28 00:07:24', NULL, NULL),
(15, '2017-05', 12, NULL, NULL, NULL, NULL, NULL, NULL, '2017-05-22 04:44:24', '2017-05-22 04:44:24', NULL, NULL),
(18, '2017-01', 11, NULL, NULL, NULL, NULL, 50000.00, NULL, '2017-05-23 05:08:39', '2017-07-08 07:07:09', NULL, NULL),
(19, '2017-01', 12, NULL, NULL, NULL, NULL, 25000.00, NULL, '2017-05-23 05:08:39', '2017-07-08 07:07:20', NULL, NULL),
(20, '2017-06', 11, NULL, 785.00, 8757.00, 857.00, 8755.00, 2828989.00, '2017-05-23 05:37:13', '2017-06-01 00:42:20', NULL, NULL),
(21, '2017-06', 12, NULL, 2557575.00, NULL, NULL, NULL, 112222.00, '2017-05-23 05:37:13', '2017-06-01 03:11:42', NULL, NULL),
(22, '2017-07', 11, NULL, NULL, NULL, NULL, 5000.00, 1000.00, '2017-05-23 05:38:22', '2017-07-07 09:35:44', NULL, NULL),
(23, '2017-07', 12, NULL, NULL, NULL, NULL, 153135.00, NULL, '2017-05-23 05:38:22', '2017-06-01 04:31:50', NULL, NULL),
(24, '2017-08', 11, NULL, 466.00, 6446.00, 46.00, 46.00, 464.00, '2017-05-27 23:54:09', '2017-06-01 00:15:23', NULL, NULL),
(25, '2017-08', 12, NULL, 232.00, 435435.00, 65656.00, 7657.00, 4646.00, '2017-05-27 23:54:09', '2017-06-01 00:15:16', NULL, NULL),
(26, '2016-10', 11, NULL, NULL, NULL, NULL, NULL, NULL, '2017-05-27 23:59:58', '2017-05-27 23:59:58', NULL, NULL),
(27, '2016-10', 12, NULL, NULL, NULL, NULL, NULL, NULL, '2017-05-27 23:59:58', '2017-05-27 23:59:58', NULL, NULL),
(31, '2017-11', 11, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-01 00:50:02', '2017-06-01 00:50:02', NULL, NULL),
(32, '2017-11', 12, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-01 00:50:02', '2017-06-01 00:50:02', NULL, NULL),
(33, '2017-11', 13, NULL, 353.00, 353.00, 353.00, 345.00, 564.00, '2017-06-01 00:50:02', '2017-06-01 00:50:41', NULL, NULL),
(34, '2017-09', 11, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-10 23:55:54', '2017-07-10 23:55:54', NULL, NULL),
(35, '2017-09', 12, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-10 23:55:54', '2017-07-10 23:55:54', NULL, NULL),
(36, '2017-09', 13, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-10 23:55:54', '2017-07-10 23:55:54', NULL, NULL),
(37, '2017-09', 14, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-10 23:55:54', '2017-07-10 23:55:54', NULL, NULL),
(38, '275760-02', 11, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:26:29', '2017-07-31 06:26:29', NULL, NULL),
(39, '275760-02', 12, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:26:29', '2017-07-31 06:26:29', NULL, NULL),
(40, '275760-02', 13, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:26:29', '2017-07-31 06:26:29', NULL, NULL),
(41, '275760-02', 14, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:26:29', '2017-07-31 06:26:29', NULL, NULL),
(42, '2020-04', 11, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:26:57', '2017-07-31 06:26:57', NULL, NULL),
(43, '2020-04', 12, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:26:57', '2017-07-31 06:26:57', NULL, NULL),
(44, '2020-04', 13, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:26:57', '2017-07-31 06:26:57', NULL, NULL),
(45, '2020-04', 14, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:26:57', '2017-07-31 06:26:57', NULL, NULL),
(46, '2012-02', 11, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:50:03', '2017-07-31 06:50:03', NULL, NULL),
(47, '2012-02', 12, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:50:03', '2017-07-31 06:50:03', NULL, NULL),
(48, '2012-02', 13, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:50:03', '2017-07-31 06:50:03', NULL, NULL),
(49, '2012-02', 14, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:50:03', '2017-07-31 06:50:03', NULL, NULL),
(50, '2012-02', 15, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:50:03', '2017-07-31 06:50:03', NULL, NULL),
(51, '2012-02', 16, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 06:50:03', '2017-07-31 06:50:03', NULL, NULL),
(52, '2020-09', 11, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 07:47:14', '2017-07-31 07:47:14', NULL, NULL),
(53, '2020-09', 12, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 07:47:14', '2017-07-31 07:47:14', NULL, NULL),
(54, '2020-09', 13, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 07:47:14', '2017-07-31 07:47:14', NULL, NULL),
(55, '2020-09', 14, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 07:47:14', '2017-07-31 07:47:14', NULL, NULL),
(56, '2020-09', 15, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 07:47:14', '2017-07-31 07:47:14', NULL, NULL),
(57, '2020-09', 16, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 07:47:14', '2017-07-31 07:47:14', NULL, NULL),
(58, '2020-09', 17, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31 07:47:14', '2017-07-31 07:47:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'viewuser', '2016-12-15 02:35:21', '2016-12-14 20:35:21'),
(2, 'removeuser', '2016-12-04 23:51:19', '0000-00-00 00:00:00'),
(3, 'createUser', '2017-06-22 10:29:59', '2017-06-22 04:29:59'),
(4, 'edituser', '2016-12-04 23:51:19', '0000-00-00 00:00:00'),
(5, 'changerole', '2016-12-04 23:51:19', '0000-00-00 00:00:00'),
(11, 'test', '2016-12-15 04:16:32', '2016-12-14 22:16:32'),
(13, 'createrole', '2016-12-05 05:25:31', '2016-12-05 05:25:31'),
(14, 'viewpermission', '2016-12-04 21:17:52', '2016-12-04 21:17:52'),
(15, 'allcompany', '2016-12-04 21:43:01', '2016-12-04 21:43:01'),
(16, 'viewcompany', '2016-12-04 22:19:26', '2016-12-04 22:19:26'),
(18, 'addpermission', '2016-12-04 22:26:32', '2016-12-04 22:26:32'),
(19, 'accessdashboard', '2016-12-20 01:54:57', '2016-12-19 19:54:57'),
(20, 'viewrole', '2016-12-05 00:21:56', '2016-12-05 00:21:56'),
(21, 'updaterole', '2016-12-06 17:36:42', '2016-12-06 17:36:42'),
(22, 'assignpage', '2016-12-14 18:37:13', '2016-12-14 18:37:13'),
(23, 'assignpermission', '2016-12-14 18:39:43', '2016-12-14 18:39:43'),
(24, 'viewallpage', '2016-12-14 18:46:12', '2016-12-14 18:46:12'),
(25, 'allpagelist', '2016-12-14 18:49:13', '2016-12-14 18:49:13'),
(26, 'deleterole', '2016-12-14 19:44:03', '2016-12-14 19:44:03'),
(27, 'editrole', '2016-12-14 19:59:16', '2016-12-14 19:59:16'),
(28, 'editpermission', '2016-12-14 20:24:21', '2016-12-14 20:24:21'),
(29, 'assigncompany', '2016-12-16 17:52:06', '2016-12-16 17:52:06'),
(30, 'createsuperadmin', '2016-12-16 18:04:51', '2016-12-16 18:04:51'),
(31, 'removecompany', '2016-12-16 21:15:18', '2016-12-16 21:15:18'),
(32, 'editcompany', '2016-12-16 21:46:21', '2016-12-16 21:46:21'),
(35, 'deletepermission', '2016-12-19 19:24:51', '2016-12-19 19:24:51'),
(36, 'deletepage', '2017-01-01 16:48:45', '2017-01-01 16:48:45'),
(37, 'viewpage', '2017-01-01 16:53:55', '2017-01-01 16:53:55'),
(38, 'userlimit', '2017-01-01 17:52:54', '2017-01-01 17:52:54'),
(39, 'usermanagement', '2017-01-11 20:22:07', '2017-01-11 20:22:07'),
(40, 'settings', '2017-01-11 20:22:26', '2017-01-11 20:22:26'),
(41, 'role', '2017-01-11 20:24:10', '2017-01-11 20:24:10'),
(42, 'appinfo', '2017-01-11 20:24:38', '2017-01-11 20:24:38'),
(43, 'page', '2017-01-11 20:24:51', '2017-01-11 20:24:51'),
(44, 'company', '2017-01-11 20:24:58', '2017-01-11 20:24:58'),
(45, 'permission', '2017-01-11 21:39:26', '2017-01-11 21:39:26'),
(46, 'nolimit', '2017-07-05 06:07:22', '2017-07-05 06:07:22'),
(47, 'storepaymentmode', '2017-07-12 03:13:53', '2017-07-12 03:13:53');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alternate_mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alternative_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_description` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_account`
--

CREATE TABLE `purchase_account` (
  `id` int(11) NOT NULL,
  `purchase_account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_account_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_account_description` text COLLATE utf8_unicode_ci,
  `purchase_account_type` int(11) DEFAULT NULL,
  `purchase_account_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `roleName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `roleName`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '2017-07-31 13:38:30', '2017-07-31 07:38:30'),
(8, 'User', '2017-06-22 04:23:00', '2017-06-22 04:23:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_wise_permission`
--

CREATE TABLE `role_wise_permission` (
  `id` int(11) NOT NULL,
  `permission_id` int(100) NOT NULL,
  `role_id` int(100) NOT NULL,
  `status` int(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_wise_permission`
--

INSERT INTO `role_wise_permission` (`id`, `permission_id`, `role_id`, `status`, `created_at`, `updated_at`) VALUES
(1258, 1, 1, NULL, '2017-07-12 03:19:57', '2017-07-12 09:19:57'),
(1259, 2, 1, NULL, '2017-07-12 03:19:57', '2017-07-12 09:19:57'),
(1260, 3, 1, NULL, '2017-07-12 03:19:57', '2017-07-12 09:19:57'),
(1261, 4, 1, NULL, '2017-07-12 03:19:57', '2017-07-12 09:19:57'),
(1262, 5, 1, NULL, '2017-07-12 03:19:57', '2017-07-12 09:19:57'),
(1263, 13, 1, NULL, '2017-07-12 03:19:57', '2017-07-12 09:19:57'),
(1264, 14, 1, NULL, '2017-07-12 03:19:57', '2017-07-12 09:19:57'),
(1265, 18, 1, NULL, '2017-07-12 03:19:57', '2017-07-12 09:19:57'),
(1266, 19, 1, NULL, '2017-07-12 03:19:58', '2017-07-12 09:19:58'),
(1267, 20, 1, NULL, '2017-07-12 03:19:58', '2017-07-12 09:19:58'),
(1268, 21, 1, NULL, '2017-07-12 03:19:58', '2017-07-12 09:19:58'),
(1269, 26, 1, NULL, '2017-07-12 03:19:58', '2017-07-12 09:19:58'),
(1270, 27, 1, NULL, '2017-07-12 03:19:58', '2017-07-12 09:19:58'),
(1271, 30, 1, NULL, '2017-07-12 03:19:58', '2017-07-12 09:19:58'),
(1272, 40, 1, NULL, '2017-07-12 03:19:58', '2017-07-12 09:19:58'),
(1273, 45, 1, NULL, '2017-07-12 03:19:58', '2017-07-12 09:19:58'),
(1274, 46, 1, NULL, '2017-07-12 03:19:58', '2017-07-12 09:19:58'),
(1275, 47, 1, NULL, '2017-07-12 03:19:58', '2017-07-12 09:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `sales_account`
--

CREATE TABLE `sales_account` (
  `id` int(11) NOT NULL,
  `sales_account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_account_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_account_description` text COLLATE utf8_unicode_ci,
  `sales_account_type` int(11) DEFAULT NULL,
  `sales_account_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sales_account`
--

INSERT INTO `sales_account` (`id`, `sales_account_name`, `sales_account_code`, `sales_account_description`, `sales_account_type`, `sales_account_status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Generel Income', 'gen', '3f3f 4f4f ', 1, 1, NULL, NULL, NULL, NULL),
(2, 'Sales', 'sal', '3f3f 4f4f ', 2, 1, NULL, NULL, NULL, NULL),
(3, 'Inventory Asset', 'INV', 'Inventory Asset', 1, NULL, '2017-06-03 02:35:05', '2017-07-06 05:11:23', NULL, NULL),
(4, 'Exchange Gain or Loss', 'Exc', 'Exchange Gain or Loss', 2, NULL, '2017-06-03 02:36:59', '2017-07-06 05:12:05', NULL, NULL),
(5, 'Automobile Expense', 'gd', 'brgbrb', 3, NULL, '2017-06-03 05:42:59', '2017-06-03 05:42:59', NULL, NULL),
(6, 'Other Expenses', 'OTH', 'Other Expenses', 3, NULL, '2017-06-03 06:06:11', '2017-07-06 05:12:26', NULL, NULL),
(7, 'Lodging', 'LOD', '4f4rgf4g', 3, NULL, '2017-07-06 04:06:29', '2017-07-06 04:06:29', NULL, NULL),
(8, 'Fuel/Mileage Expenses', 'Mil', 'grtgreg', 3, NULL, '2017-07-06 04:07:15', '2017-07-06 04:07:15', NULL, NULL),
(9, 'Drawings', 'Dra', 'Drawings', 10, NULL, '2017-07-06 04:11:54', '2017-07-06 04:11:54', NULL, NULL),
(10, 'Tax Payable', 'Tax', 'Tax Payable', 11, NULL, '2017-07-06 04:30:01', '2017-07-06 04:30:01', NULL, NULL),
(11, 'Tag Adjustments', 'TAG', 'Tag Adjustments', 5, NULL, '2017-07-06 05:27:33', '2017-07-06 05:27:33', NULL, NULL),
(12, 'Office Supplies', NULL, 'Office Supplies', 3, NULL, '2017-07-08 05:44:54', '2017-07-08 05:44:54', NULL, NULL),
(13, 'Advertising And Marketing', NULL, 'Advertising And Marketing', 3, NULL, '2017-07-08 05:45:25', '2017-07-08 05:45:25', NULL, NULL),
(14, 'Janitorial Expense', NULL, 'Janitorial Expense', 3, NULL, '2017-07-08 05:45:41', '2017-07-08 05:45:41', NULL, NULL),
(15, 'Bank Fees and Charges', NULL, 'Bank Fees and Charges', 3, NULL, '2017-07-08 05:46:05', '2017-07-08 05:46:05', NULL, NULL),
(16, 'Postage', NULL, 'Postage', 3, NULL, '2017-07-08 05:46:29', '2017-07-08 05:46:29', NULL, NULL),
(17, 'Lodging', NULL, 'Lodging', 3, NULL, '2017-07-08 05:46:46', '2017-07-08 05:46:46', NULL, NULL),
(18, 'Bad Debt', NULL, 'Bad Debt', 3, NULL, '2017-07-08 05:47:15', '2017-07-08 05:47:15', NULL, NULL),
(19, 'Repairs and Maintenance', NULL, 'Repairs and Maintenance', 3, NULL, '2017-07-08 05:47:35', '2017-07-08 05:47:35', NULL, NULL),
(20, 'Consultant Expense', NULL, 'Consultant Expense', 3, NULL, '2017-07-08 05:47:55', '2017-07-08 05:47:55', NULL, NULL),
(21, 'Rent Expense', NULL, 'Rent Expense', 3, NULL, '2017-07-08 05:48:12', '2017-07-08 05:48:12', NULL, NULL),
(22, 'Depreciation Expense', NULL, 'Depreciation Expense', 3, NULL, '2017-07-08 05:48:27', '2017-07-08 05:48:27', NULL, NULL),
(23, 'Salaries and Employee Wages', NULL, 'Salaries and Employee Wages', 3, NULL, '2017-07-08 05:48:48', '2017-07-08 05:48:48', NULL, NULL),
(24, 'Printing and Stationery', NULL, 'Printing and Stationery', 3, NULL, '2017-07-08 05:49:05', '2017-07-08 05:49:05', NULL, NULL),
(25, 'IT and Internet Expenses', NULL, 'IT and Internet Expenses', 3, NULL, '2017-07-08 05:49:25', '2017-07-08 05:49:25', NULL, NULL),
(26, 'Shipping Charge', NULL, 'Shipping Charge', 3, NULL, '2017-07-08 05:49:44', '2017-07-08 05:49:44', NULL, NULL),
(27, 'Interest Income', NULL, 'Interest Income', 3, NULL, '2017-07-08 05:50:01', '2017-07-08 05:50:01', NULL, NULL),
(28, 'Other Charges', NULL, 'Other Charges', 3, NULL, '2017-07-08 05:50:18', '2017-07-08 05:50:18', NULL, NULL),
(29, 'Late Fee Income', NULL, 'Late Fee Income', 4, NULL, '2017-07-08 05:51:47', '2017-07-08 05:58:34', NULL, NULL),
(30, 'Advance Tax', NULL, 'Advance Tax', 9, NULL, '2017-07-08 05:52:25', '2017-07-08 05:52:25', NULL, NULL),
(31, 'Sales to Customers (Cash)', NULL, 'Sales to Customers (Cash)', 9, NULL, '2017-07-08 05:53:08', '2017-07-08 05:53:08', NULL, NULL),
(32, 'Petty Cash', NULL, 'Petty Cash', 8, NULL, '2017-07-08 05:53:31', '2017-07-08 05:53:31', NULL, NULL),
(33, 'Undeposited Funds', NULL, 'Undeposited Funds', 8, NULL, '2017-07-08 05:53:59', '2017-07-08 05:53:59', NULL, NULL),
(34, 'Accounts Receivable', NULL, 'Accounts Receivable', 7, NULL, '2017-07-08 05:54:15', '2017-07-08 05:54:15', NULL, NULL),
(35, 'Tax Payable', NULL, 'Tax Payable', 11, NULL, '2017-07-08 05:54:49', '2017-07-08 05:54:49', NULL, NULL),
(36, 'Unearned Revenue', NULL, 'Unearned Revenue', 11, NULL, '2017-07-08 05:55:04', '2017-07-08 05:55:04', NULL, NULL),
(37, 'Opening Balance Adjustments', NULL, 'Opening Balance Adjustments', 11, NULL, '2017-07-08 05:55:21', '2017-07-08 05:55:21', NULL, NULL),
(38, 'Employee Reimbursements', NULL, 'Employee Reimbursements', 11, NULL, '2017-07-08 05:55:41', '2017-07-08 05:55:41', NULL, NULL),
(39, 'Accounts Payable', NULL, 'Accounts Payable', 6, NULL, '2017-07-08 05:56:00', '2017-07-08 05:56:00', NULL, NULL),
(40, 'Tag Adjustments', NULL, 'Tag Adjustments', 5, NULL, '2017-07-08 05:56:15', '2017-07-08 05:56:15', NULL, NULL),
(41, 'Opening Balance Offset', NULL, 'Opening Balance Offset', 10, NULL, '2017-07-08 05:56:39', '2017-07-08 05:56:39', NULL, NULL),
(42, 'Owner''s Equity', NULL, 'Owner''s Equity', 10, NULL, '2017-07-08 05:56:55', '2017-07-08 05:56:55', NULL, NULL),
(43, 'Retained Earnings', NULL, 'Retained Earnings', 10, NULL, '2017-07-08 05:57:13', '2017-07-08 05:57:13', NULL, NULL),
(44, 'Drawings', NULL, 'Drawings', 10, NULL, '2017-07-08 05:57:30', '2017-07-08 05:57:30', NULL, NULL),
(45, 'Discount', NULL, 'Discount', 4, NULL, '2017-07-08 05:57:43', '2017-07-08 05:57:43', NULL, NULL),
(46, 'Shipping Charge', NULL, 'Shipping Charge', 4, NULL, '2017-07-08 05:59:13', '2017-07-08 05:59:13', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(11) NOT NULL,
  `tax_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_amount` float(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`id`, `tax_name`, `tax_amount`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'VAT', 5.00, '2017-05-29 00:13:28', '2017-05-29 00:13:28', NULL, NULL),
(2, 'discount', 10.00, '2017-05-29 00:15:17', '2017-05-29 00:15:17', NULL, NULL),
(3, 'yu67', 54.00, '2017-05-29 00:15:40', '2017-05-29 00:15:40', NULL, NULL),
(4, 'New Tax123', 20.00, '2017-07-02 02:40:45', '2017-07-02 02:40:45', NULL, NULL),
(5, 'g6y', 66.00, '2017-07-02 03:01:47', '2017-07-02 03:01:47', NULL, NULL),
(6, 'eee', 11.00, '2017-07-02 03:02:44', '2017-07-02 03:02:44', NULL, NULL),
(7, 'ere', 23.00, '2017-07-02 03:06:08', '2017-07-02 03:06:08', NULL, NULL),
(8, 'mjh,', 54.00, '2017-07-02 03:06:18', '2017-07-02 03:06:18', NULL, NULL),
(9, NULL, NULL, '2017-07-09 05:50:41', '2017-07-09 05:50:41', NULL, NULL),
(10, NULL, NULL, '2017-07-09 05:52:45', '2017-07-09 05:52:45', NULL, NULL),
(11, NULL, NULL, '2017-07-09 05:53:06', '2017-07-09 05:53:06', NULL, NULL),
(12, NULL, NULL, '2017-07-09 06:08:07', '2017-07-09 06:08:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `transaction_type` int(11) DEFAULT NULL,
  `account_status` int(11) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `unit_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `unit_name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Kg', NULL, NULL, NULL, NULL),
(2, 'litre', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `create_user_quota` int(11) DEFAULT NULL,
  `remaining` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `email`, `password`, `status`, `role_id`, `created_by`, `update_by`, `create_user_quota`, `remaining`, `remember_token`, `created_at`, `updated_at`) VALUES
(106, 'Binary Logic', '1498124901.png', 'admin@binarylogic.co', '$2y$10$PTkC9M.9L97sOVwyIVv7kuYv/OW0jjgbMRW4mzCAyFdyN0kx8d4Au', 0, 1, NULL, NULL, NULL, NULL, 'UAjbFHoiZpozENwbaaWbIRPEht6fFjXXFzqHRm5t2jI9rUp8xyatJuNILi1H', '2016-12-27 17:03:22', '2017-06-22 03:53:39'),
(211, 'Akash Akram', '1498123433.png', 'akash@binarylogic.co', '$2y$10$MGJM5s4sXcj.4OL3y/AnF.K6TFJ4ceNyAxWo1L7NE9gdelmPRvzCq', 0, 8, 106, NULL, NULL, NULL, NULL, '2017-06-22 01:46:06', '2017-06-22 03:23:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `roleid` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `userid`, `roleid`, `created_at`, `updated_at`) VALUES
(78, 105, 1, '2016-12-27 16:36:44', '2016-12-27 16:36:44'),
(79, 106, 1, '2017-01-02 02:33:07', '2017-01-01 20:33:07'),
(158, 198, 2, '2017-01-07 23:02:53', '2017-01-07 23:02:53'),
(164, 204, 3, '2017-01-11 23:39:24', '2017-01-11 23:39:24'),
(167, 207, 3, '2017-02-11 06:37:07', '2017-02-11 06:37:07'),
(168, 208, 3, '2017-03-06 06:07:53', '2017-03-06 06:07:53'),
(169, 209, 3, '2017-05-20 23:58:57', '2017-05-20 23:58:57'),
(170, 210, 3, '2017-05-21 00:21:48', '2017-05-21 00:21:48'),
(171, 212, 1, '2017-06-22 01:48:43', '2017-06-22 01:48:43');

-- --------------------------------------------------------

--
-- Table structure for table `vcredits`
--

CREATE TABLE `vcredits` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `credit_note` longtext COLLATE utf8_unicode_ci,
  `order_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vcredit_date` date DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  `total` float(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vcredits`
--

INSERT INTO `vcredits` (`id`, `vendor_id`, `credit_note`, `order_no`, `vcredit_date`, `tax`, `total`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 1, 'My Note', 'My Order', '2017-06-19', NULL, 339200.00, '2017-06-19 04:46:57', '2017-06-19 04:46:57', NULL, NULL),
(4, 2, 'Note 2', 'new Order', '2017-06-20', NULL, 65291.00, '2017-06-19 05:17:10', '2017-06-19 05:17:10', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vcredit_accounts`
--

CREATE TABLE `vcredit_accounts` (
  `id` int(11) NOT NULL,
  `vcredit_id` int(11) DEFAULT NULL,
  `acc_id` int(11) DEFAULT NULL,
  `acc_description` longtext COLLATE utf8_unicode_ci,
  `quantity` float(10,2) DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vcredit_accounts`
--

INSERT INTO `vcredit_accounts` (`id`, `vcredit_id`, `acc_id`, `acc_description`, `quantity`, `rate`, `tax_id`, `amount`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(10, 3, 1, 'def', 3.00, 4646.00, 2, 13938, '2017-06-19 04:49:48', '2017-06-19 04:49:48', 106, NULL),
(11, 3, 2, 'jtr tjkj jukujku', 7.00, 46466.00, NULL, 325262, '2017-06-19 04:49:48', '2017-06-19 04:49:48', 106, NULL),
(12, 4, 1, 'btrfg hnty', 1.00, 645.00, 1, 645, '2017-06-19 05:17:10', '2017-06-19 05:17:10', 106, NULL),
(13, 4, 3, 'gnn hnt', 1.00, 64646.00, NULL, 64646, '2017-06-19 05:17:10', '2017-06-19 05:17:10', 106, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `vendor_name` longtext COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `vendor_name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Vendor 1', NULL, NULL, NULL, NULL),
(2, 'Vendor 2', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `salutation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_department` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_payment_terms_id` int(11) DEFAULT NULL,
  `vendor_portal_language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_attention` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_address` longtext COLLATE utf8_unicode_ci,
  `vendor_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_zip_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `salutation`, `vendor_first_name`, `vendor_last_name`, `vendor_name`, `vendor_email`, `vendor_mobile`, `vendor_designation`, `vendor_department`, `vendor_website`, `company_name`, `vendor_currency`, `vendor_payment_terms_id`, `vendor_portal_language`, `vendor_attention`, `vendor_address`, `vendor_city`, `vendor_state`, `vendor_zip_code`, `vendor_country`, `vendor_fax`, `remarks`, `created_at`, `updated_at`, `created_by`, `update_by`) VALUES
(1, 'Mr.', 'akash', 'akram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nhgn gnhytjyt tyjyt', '2017-05-15 05:03:56', '2017-05-15 05:34:30', NULL, NULL),
(2, 'Mrs.', 'bnubjh', 'byhb', NULL, 'byt', 'by', '5', NULL, 'byhb', 'byh', '1', 1, '1', 'bghb', 'bghb', 'bgth', 'bgb', NULL, NULL, NULL, 'gb g', '2017-06-19 03:05:41', '2017-06-19 03:05:41', NULL, NULL),
(3, 'Mr.', 'Vendor', '1', NULL, 'v1@eagle.com', '23244', '5', NULL, 'www', 'com 1', '1', 1, '0', 'fvdf', 'vdv', 'vdvd', 'dvdv', 'vdv', 'vdv', NULL, 'dvdv', '2017-06-19 03:08:07', '2017-06-19 03:08:07', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_type`
--
ALTER TABLE `account_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `basic_info`
--
ALTER TABLE `basic_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_accounts`
--
ALTER TABLE `bill_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contract_base`
--
ALTER TABLE `contract_base`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contract_type`
--
ALTER TABLE `contract_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `creditnotes`
--
ALTER TABLE `creditnotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `creditnote_items`
--
ALTER TABLE `creditnote_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimates`
--
ALTER TABLE `estimates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimate_items`
--
ALTER TABLE `estimate_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expances`
--
ALTER TABLE `expances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journals`
--
ALTER TABLE `journals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journal_accounts`
--
ALTER TABLE `journal_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_planners`
--
ALTER TABLE `leave_planners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments_recieved`
--
ALTER TABLE `payments_recieved`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_modes`
--
ALTER TABLE `payment_modes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payrolls`
--
ALTER TABLE `payrolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_account`
--
ALTER TABLE `purchase_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_wise_permission`
--
ALTER TABLE `role_wise_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_account`
--
ALTER TABLE `sales_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vcredits`
--
ALTER TABLE `vcredits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vcredit_accounts`
--
ALTER TABLE `vcredit_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_type`
--
ALTER TABLE `account_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `basic_info`
--
ALTER TABLE `basic_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `bill_accounts`
--
ALTER TABLE `bill_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contract_base`
--
ALTER TABLE `contract_base`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contract_type`
--
ALTER TABLE `contract_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `creditnotes`
--
ALTER TABLE `creditnotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `creditnote_items`
--
ALTER TABLE `creditnote_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `estimates`
--
ALTER TABLE `estimates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `estimate_items`
--
ALTER TABLE `estimate_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `expances`
--
ALTER TABLE `expances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `journals`
--
ALTER TABLE `journals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `journal_accounts`
--
ALTER TABLE `journal_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `leave_planners`
--
ALTER TABLE `leave_planners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `payments_recieved`
--
ALTER TABLE `payments_recieved`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `payment_modes`
--
ALTER TABLE `payment_modes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `payrolls`
--
ALTER TABLE `payrolls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_account`
--
ALTER TABLE `purchase_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `role_wise_permission`
--
ALTER TABLE `role_wise_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1276;
--
-- AUTO_INCREMENT for table `sales_account`
--
ALTER TABLE `sales_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;
--
-- AUTO_INCREMENT for table `vcredits`
--
ALTER TABLE `vcredits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vcredit_accounts`
--
ALTER TABLE `vcredit_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
