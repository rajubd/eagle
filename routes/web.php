<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*					Settings Route 					 */

Route::get('/settings', array(
    'as'    => 'settings',
    'uses'  => 'SettingsController@viewSettings'
));

Route::post('/post-settings', array(
    'as'    => 'post-settings',
    'uses'  => 'SettingsController@postSettings'
));

Route::get('/home', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' =>'auth'], function(){

Route::get('/', 'DashboardController@index');

Route::get('/itemkit', 'ItemkitController@index');
Route::get('/suppliers', 'SupplierController@index');
Route::get('/reports', 'ReportController@index');
Route::get('/sales', 'SaleController@index');
Route::get('/pricerules', 'PriceController@index');

Route::get('/receivings', 'ReceivingController@index');


// Department Controller

Route::get('departments/', 'DepartmentController@index');
Route::get('createdepartment/', 'DepartmentController@create');
Route::post('createdepartment/', 'DepartmentController@store');
Route::get('editdepartment/{id}', 'DepartmentController@edit');
Route::post('editdepartment/{id}', 'DepartmentController@update');
Route::get('deletedepartment/{id}', 'DepartmentController@destroy');


// Level Controller

Route::get('levels/', 'LevelController@index');
Route::get('createlevel/', 'LevelController@create');
Route::post('createlevel/', 'LevelController@store');
Route::get('editlevel/{id}', 'LevelController@edit');
Route::post('editlevel/{id}', 'LevelController@update');
Route::get('deletelevel/{id}', 'LevelController@destroy');

// Designation Controller

Route::get('designations/', 'DesignationController@index');
Route::get('createdesignation/', 'DesignationController@create');
Route::post('createdesignation/', 'DesignationController@store');
Route::get('editdesignation/{id}', 'DesignationController@edit');
Route::post('editdesignation/{id}', 'DesignationController@update');
Route::get('deletedesignation/{id}', 'DesignationController@destroy');

// ContractBase Controller

Route::get('contractbases/', 'ContractBaseController@index');
Route::get('createcontractbase/', 'ContractBaseController@create');
Route::post('createcontractbase/', 'ContractBaseController@store');
Route::get('editcontractbase/{id}', 'ContractBaseController@edit');
Route::post('editcontractbase/{id}', 'ContractBaseController@update');
Route::get('deletecontractbase/{id}', 'ContractBaseController@destroy');

// Contract contractbase Controller

Route::get('contracttypes/', 'ContractTypeController@index');
Route::get('createcontracttype/', 'ContractTypeController@create');
Route::post('createcontracttype/', 'ContractTypeController@store');
Route::get('editcontracttype/{id}', 'ContractTypeController@edit');
Route::post('editcontracttype/{id}', 'ContractTypeController@update');
Route::get('deletecontracttype/{id}', 'ContractTypeController@destroy');

// Employee Controller

Route::get('employees/', 'EmployeeController@index');
Route::get('createemployee/', 'EmployeeController@create');
Route::post('createemployee/', 'EmployeeController@store');
Route::get('editemployee/{id}', 'EmployeeController@edit');
Route::post('editemployee/{id}', 'EmployeeController@update');
Route::get('deleteemployee/{id}', 'EmployeeController@destroy');

// Customer Controller

Route::get('/customers', 'CustomerController@index');
Route::get('/createcustomer', 'CustomerController@create');
Route::post('/createcustomer', 'CustomerController@store');
Route::get('/editcustomer/{id}', 'CustomerController@edit');
Route::get('/viewcustomer/{id}', 'CustomerController@view');
Route::post('/editcustomer/{id}', 'CustomerController@update');
Route::get('/deletecustomer/{id}', 'CustomerController@destroy');


Route::get('/loadcustomeraddress/{id}', 'CustomerController@loadcustomeraddress');


// leave Controller

Route::get('/leaves', 'LeaveController@index');
Route::get('/createleave', 'LeaveController@create');
Route::post('/createleave', 'LeaveController@store');
Route::get('/editleave/{id}', 'LeaveController@edit');
Route::post('/editleave/{id}', 'LeaveController@update');
Route::get('/deleteleave/{id}', 'LeaveController@destroy');

// leave planner Controller

Route::get('/leaveplanners', 'LeavePlannerController@index');
Route::get('/createleaveplanner', 'LeavePlannerController@create');
Route::post('/createleaveplanner', 'LeavePlannerController@store');
Route::get('/editleaveplanner/{id}', 'LeavePlannerController@edit');
Route::post('/editleaveplanner/{id}', 'LeavePlannerController@update');
Route::get('/deleteleaveplanner/{id}', 'LeavePlannerController@destroy');

// Holiday Controller

Route::get('/holidays', 'HolidayController@index');
Route::get('/createholiday', 'HolidayController@create');
Route::post('/createholiday', 'HolidayController@store');
Route::get('/editholiday/{id}', 'HolidayController@edit');
Route::post('/editholiday/{id}', 'HolidayController@update');
Route::get('/deleteholiday/{id}', 'HolidayController@destroy');

// payroll Controller

//Route::get('/payrolls', 'PayrollController@index');
//Route::get('/createpayroll', 'PayrollController@create');
//Route::post('/createpayroll', 'PayrollController@store');
//Route::get('/editpayroll/{id}', 'PayrollController@edit');
//Route::post('/editpayroll/{id}', 'PayrollController@update');
//Route::get('/deletepayroll/{id}', 'PayrollController@destroy');


Route::get('/payrolls', 'PayrollController@index');
Route::post('/payrolls', 'PayrollController@index');
Route::get('/createpayroll', 'PayrollController@create');
//Route::post('/createpayroll', 'PayrollController@store');
Route::post('/createpayroll', 'PayrollController@add');
Route::post('/createpayroll2/{value}', 'PayrollController@add2');
Route::get('/editpayroll/{id}', 'PayrollController@edit');
Route::post('/updatepayroll', 'PayrollController@update');
Route::get('/deletepayroll/{id}', 'PayrollController@destroy');


Route::get('/loadpayroll/{value}', 'PayrollController@loadpayroll');

Route::get('/loadpayrolldata/{value}', 'PayrollController@loadpayrolldata');




Route::get('/getemployeedata/{id}', 'PayrollController@getemployeedata');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

//  Item Controller


Route::get('/items', 'ItemController@index');
Route::get('/createitem', 'ItemController@create');
Route::post('/createitem', 'ItemController@store');
//Route::get('/createitem', 'ItemController@create');


Route::post('/createtax', 'TaxController@store');
Route::get('/createiteeeeefm', 'ItemController@create');
Route::post('/createitevfvfvm', 'ItemController@store');
//Route::get('/createitem', 'ItemController@create');




Route::get('/itemadjustments', 'ItemAdjController@index');
Route::get('/createitemadjustment', 'ItemAdjController@create');
Route::post('/createitemadjustment', 'ItemAdjController@store');
//Route::get('/createitem', 'ItemController@create');



Route::get('/chartofaccounts', 'AccountController@index');
Route::post('/createaccount', 'AccountController@store');
Route::post('/editaccount/{id}', 'AccountController@update');

Route::post('/createaccounttype', 'AccountController@storeaccounttype');



Route::get('/journals', 'JournalController@index');
Route::get('/createjournal', 'JournalController@create');
Route::post('/createjournal', 'JournalController@store');
Route::get('/editjournal/{id}', 'JournalController@edit');
Route::post('/editjournal/{id}', 'JournalController@update');
Route::get('/deletejournal/{id}', 'JournalController@delete');




Route::get('/currencyadjustments', 'CurrencyController@index');
Route::post('/createcurrencyadjustment', 'CurrencyController@store');



Route::get('/bills', 'BillController@index');
Route::get('/createbill', 'BillController@create');
Route::post('/createbill', 'BillController@store');


Route::get('/billdetails','BillController@billdetails');
Route::get('/loadbilldetails/{id}','BillController@loadbilldetails');





//  Expenses Controller

Route::get('/expenses', 'ExpenceController@index');
Route::get('/createexpense', 'ExpenceController@create');
Route::post('/createexpense', 'ExpenceController@storeexpense');
Route::post('/createbulkexpense', 'ExpenceController@storebulkexpense');
Route::post('/createmilage', 'ExpenceController@storemilage');




// Payment Controller

Route::get('/payments','PaymentController@index');
Route::get('/paymentmade','PaymentController@createpayment');
Route::get('/paymentdetails','PaymentController@paymentdetails');
Route::post('/paymentmade','PaymentController@storepayment');


Route::get('/loadpaymentdata/{id}','PaymentController@loadpaymentdata');

Route::get('/loadvendorbills/{id}','PaymentController@loadvendorbills');
Route::get('/loadbilldata/{id}','PaymentController@loadbilldata');



//Route::get('/loadpaymentdata/{id}','PaymentController@loadpaymentdata');

Route::get('/loadcustomerinvoice/{id}','PaymentController@loadcustomerinvoice');
Route::get('/loadinvoicedata/{id}','PaymentController@loadinvoicedata');


//Route::get('/createpaymentmode','PaymentController@createpaymentmode');
Route::post('/createpaymentmode','PaymentController@storepaymentmode');

Route::get('/payments_received','PaymentController@payments_received');
Route::get('/createpaymentsreceived','PaymentController@createpaymentsreceived');
Route::post('/createpaymentsreceived','PaymentController@storepaymentsreceived');



// Vecdor Credit Controller


Route::get('/vendor_credits', 'VcreditController@index');
Route::get('/createvendorcredit', 'VcreditController@create');
Route::post('/createvendorcredit', 'VcreditController@store');


Route::get('/vcreditdetails','VcreditController@vcreditdetails');
Route::get('/loadvcreditdata/{id}','VcreditController@loadvcreditdata');
Route::get('/loadvcreditdata1','VcreditController@loadvcreditdata1');




// vendor Controller

Route::get('/vendors', 'VendorController@index');
Route::get('/createvendor', 'VendorController@create');
Route::post('/createvendor', 'VendorController@store');
Route::get('/editvendor/{id}', 'VendorController@edit');
Route::post('/editvendor/{id}', 'VendorController@update');
Route::get('/deletevendor/{id}', 'VendorController@destroy');






// Invoice Controller


Route::get('/invoices', 'InvoiceController@index');
Route::get('/createinvoice', 'InvoiceController@create');
Route::post('/createinvoice', 'InvoiceController@store');
Route::get('/invoicedetails','InvoiceController@invoicedetails');


Route::get('/loadinvoicedata1','InvoiceController@loadinvoicedata1');
Route::get('/loadinvoicedata/{id}','InvoiceController@loadinvoicedata');

Route::get('/loadrecordpayment','InvoiceController@loadrecordpayment');

Route::get('/recordpayment/{id}','InvoiceController@createrecordpayment');
Route::post('/recordpayment/{id}','InvoiceController@storerecordpayment');




// Estimates Controller


Route::get('/estimates', 'EstimateController@index');
Route::get('/createestimate', 'EstimateController@create');
Route::post('/createestimate', 'EstimateController@store');

Route::get('/estimatedetails','EstimateController@estimatedetails');


Route::get('/loadestimatedata1','EstimateController@loadestimatedata1');
Route::get('/loadestimatedata/{id}','EstimateController@loadestimatedata');





Route::get('/credit_notes', 'CreditNoteController@index');
Route::get('/createcreditnote', 'CreditNoteController@create');
Route::post('/createcreditnote', 'CreditNoteController@store');

Route::get('/creditnoteedetails','CreditNoteController@creditnoteedetails');


Route::get('/loadcreditnote1','CreditNoteController@loadcreditnote1');
Route::get('/loadcreditnote/{id}','CreditNoteController@loadcreditnote');















Route::get('/test', function () {
    return view('test');
});










/*                 UserController                   */

Route::get('/users', 'UserController@index');
Route::get('/createuser', 'UserController@CreateUser');
Route::post('/storeuser' , 'UserController@StoreUser');
Route::get('/removeuser/{id}' , 'UserController@RemoveSingleUser');
Route::get('/editsingleuser/{id}' , 'UserController@EditSingleUser');
Route::post('/updatesingleuser/{id}', 'UserController@UpdateSingleUser');
Route::get('/viewsingleuser/{id}' , 'UserController@ViewSingleUser');

/*                 /UserController                   */






//                  Role Access Area



/*               /RoleController              */

Route::get('/roles', 'RoleController@ViewRole');
Route::get('/editrole/{id}', 'RoleController@EditRole');
Route::post('/updaterole/{id}', 'RoleController@UpdateRole');
Route::get('/deleterole/{id}', 'RoleController@DeleteRole');
Route::get('/createrole', 'RoleController@CreateRole');
Route::post('/storerole', 'RoleController@StoreRole');
/*Route::get('/viewsinglerole/{id}', 'RoleWisePermissionController@GetAllPermissionsByRoleId');*/

/*               RoleController              */






/*                Permission             */

Route::get('/permissions', 'PermissionController@ViewAllPermissions');
Route::get('/addpermission', 'PermissionController@AddPermission');
Route::get('/editpermission/{id}', 'PermissionController@EditPermission');
Route::post('/updatepermission/{id}', 'PermissionController@UpdatePermission');
Route::get('/deletepermission/{id}', 'PermissionController@DeletePermission');
Route::post('/storepermission', 'PermissionController@StorePermission');

/*                /Permission             */




/*             RoleWisePermission Controller          */

Route::get('/assignpermission', 'RoleWisePermissionController@AssignPermission');
Route::get('loadPermission/{value}', 'RoleWisePermissionController@GetRoleWisePermissions');
Route::post('updaterolepermission', 'RoleWisePermissionController@UpdateRolePermission');

Route::get('/viewsinglerole/{id}', 'RoleWisePermissionController@GetAllPermissionsByRoleId');


/*             /RoleWisePermission Controller          */

















});