<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	 protected $table = 'items';
     protected $fillable = [
        'item_name','sku','unit','item_image','manufacturer','upc','ean','brand','mpn','isbn','selling_price','selling_account','selling_description','tax','purchase_price','purchase_account','purchase_description','preferred_vendor','tracking_account','reorder_level','opening_stock','opening_stock_value','item_status','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
