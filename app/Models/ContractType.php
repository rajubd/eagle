<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractType extends Model
{
    protected $table = 'contract_type';
    protected $fillable = [
        'typename', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];
}
