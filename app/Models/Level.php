<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'levels';
    protected $fillable = [
        'level', 'remarks', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

}
