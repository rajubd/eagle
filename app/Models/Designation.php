<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $table = 'designations';
    protected $fillable = [
        'designation_name', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

}
