<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vcredit_account extends Model
{
	 protected $table = 'vcredit_accounts';
     protected $fillable = [
        'vcredit_id','acc_id','acc_description','quantity','rate','tax_id','amount','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
