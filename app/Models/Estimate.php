<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estimate extends Model
{
	 protected $table = 'estimates';
     protected $fillable = [
        'estimate_no','customer_id','reference_no','estimate_date','estimate_expiry_date','sales_person_id','due_amount','status','estimate_amount','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
