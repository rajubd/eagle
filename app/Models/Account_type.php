<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account_type extends Model
{
	 protected $table = 'account_type';
     protected $fillable = [
        'account_type','account_status','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
