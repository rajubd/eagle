<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Journal_account extends Model
{
	 protected $table = 'journal_accounts';
     protected $fillable = [
        'journal_id','account_id','description','contact','tax_id','debit','credit','total',
        'created_at','updated_at', 'created_by', 'updated_by'
    ];
}
