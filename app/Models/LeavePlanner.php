<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeavePlanner extends Model
{
	 protected $table = 'leave_Planners';
     protected $fillable = [
        'employee_id', 'leave_plan','start_date', 'end_date','reason','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
