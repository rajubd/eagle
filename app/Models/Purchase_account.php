<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase_account extends Model
{
	 protected $table = 'purchase_account';
     protected $fillable = [
        'purchase_account_name','purchase_account_code','purchase_account_description','purchase_account_type','purchase_account_status','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
