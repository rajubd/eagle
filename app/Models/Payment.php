<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	 protected $table = 'payments';
     protected $fillable = [
        'vendor_id','amount','payment_date','reference','payment_mode','paid_through','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
