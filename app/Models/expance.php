<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expance extends Model
{
	 protected $table = 'expances';
     protected $fillable = [
        'expence_date','claimant_id','milage_calculate','distance','vendor_id','account_id','currency_id','tax_id','note','amount','paid_through','reference','expanse_image','customer_id','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
