<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
	 protected $table = 'invoices';
     protected $fillable = [
        'invoice_no','customer_id','order_no','invoice_date','due_date','sales_person_id','due_amount','tax_status','status','term_id','invoice_amount','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
