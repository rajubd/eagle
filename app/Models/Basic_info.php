<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Basic_info extends Model
{
    protected $table = 'employees';
    protected $fillable = [
        'first_name', 'last_name', 'fathers_name','email','nid','employee_code','passport_no','birth','joining_date','gender','religion','marital_status','nationality','present_address','permanent_address','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
