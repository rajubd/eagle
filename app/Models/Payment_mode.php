<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment_mode extends Model
{
	 protected $table = 'payment_modes';
     protected $fillable = [
        'mode','status','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
