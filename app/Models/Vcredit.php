<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vcredit extends Model
{
	 protected $table = 'vcredits';
     protected $fillable = [
        'vendor_id','credit_note','order_no','vcredit_date','tax','total','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
