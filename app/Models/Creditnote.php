<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Creditnote extends Model
{
	 protected $table = 'creditnotes';
     protected $fillable = [
        'credit_note_no','customer_id','reference_no','creditnote_date','creditnote_amount','status','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
