<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    protected $table = 'payrolls';
    protected $fillable = [
        'payroll_month', 'employee_id','ot_hour', 'estimated_salary', 'estimated_ot_bill', 'paid_salary', 'paid_ot_bill', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

}
