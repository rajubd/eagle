<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Creditnote_item extends Model
{
	 protected $table = 'creditnote_items';
     protected $fillable = [
        'creditnote_id','item_id','qty','rate','amount','tax_id','status','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
