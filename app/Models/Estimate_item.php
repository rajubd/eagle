<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estimate_item extends Model
{
	 protected $table = 'estimate_items';
     protected $fillable = [
        'estimate_id','item_id','qty','rate','amount','tax_id','status','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
