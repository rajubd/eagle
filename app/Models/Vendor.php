<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{

    protected $table = 'vendors';
    protected $fillable = [
        'salutation','vendor_first_name', 'vendor_last_name', 'company_name','vendor_email','vendor_mobile','vendor_designation','vendor_department','vendor_website','vendor_currency','vendor_payment_terms_id','vendor_attention','vendor_portal_language','vendor_address','vendor_city','vendor_state','vendor_zip_code','vendor_country','vendor_fax','remarks','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
