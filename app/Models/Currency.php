<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
	 protected $table = 'currency';
     protected $fillable = [
        'foreign_currency_code','exchange_rate','exchange_date','currency_note','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
