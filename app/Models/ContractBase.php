<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractBase extends Model
{
    protected $table = 'contract_base';
    protected $fillable = [
        'basename', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];
}
