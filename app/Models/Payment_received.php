<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment_received extends Model
{
	 protected $table = 'payments_recieved';
     protected $fillable = [
        'customer_id','amount','unused_amount','payment_date','reference','payment_mode','deposit_to','status','paid_through','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
