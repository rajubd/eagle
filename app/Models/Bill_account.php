<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill_account extends Model
{
	 protected $table = 'bill_accounts';
     protected $fillable = [
        'bill_id','acc_id','acc_description','quantity','rate','tax_id','amount','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
