<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
	 protected $table = 'bills';
     protected $fillable = [
        'bill_no','vendor_id','order_no','bill_date','due_date','amount_due','tax_status','bill_status','note','total','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
