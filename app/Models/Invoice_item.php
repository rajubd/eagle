<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice_item extends Model
{
	 protected $table = 'invoice_items';
     protected $fillable = [
        'invoice_id','item_id','qty','rate','amount','tax_id','status','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
