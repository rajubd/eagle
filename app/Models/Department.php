<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	protected $table = 'departments';
    protected $fillable = [
        'short_code', 'department_name', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

}
