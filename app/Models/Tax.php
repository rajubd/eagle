<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
	 protected $table = 'taxes';
     protected $fillable = [
        'tax_name', 'tax_amount','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
