<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
	 protected $table = 'brands';
     protected $fillable = [
        'brand_name','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
