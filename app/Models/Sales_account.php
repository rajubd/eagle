<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sales_account extends Model
{
	 protected $table = 'sales_account';
     protected $fillable = [
        'sales_account_name','sales_account_code','sales_account_description','sales_account_type','sales_account_status','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
