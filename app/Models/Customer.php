<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $table = 'customers';
    protected $fillable = [
        'salutation','customer_first_name', 'customer_last_name', 'company_name','customer_email','customer_mobile','customer_designation','customer_department','customer_website','customer_currency','customer_payment_terms_id','customer_attention','customer_portal_language','customer_address','customer_city','customer_state','customer_zip_code','customer_country','customer_fax','remarks','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
