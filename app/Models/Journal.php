<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
	 protected $table = 'journals';
     protected $fillable = [
        'journal_reference','journal_note','journal_date','journal_currency','created_at','updated_at', 'created_by', 'updated_by'
    ];
}
