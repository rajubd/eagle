<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleWisePermission extends Model
{
    protected $table = 'role_wise_permission';    
      
	protected $fillable = [
        'permission_id','role_id',
    ];

    public static function checkExistsRole($id,$permissionID){
    	$result= DB::table('role_wise_permission')
    	->where('role_id', '=', $id)
    	->where('permission_id', '=', $permissionID)
    	->get();
    	if($result){
    		return true;
    	}
    	return false;
    }
    public static function createRoleWisePermission($id,$permissionID){
    	$result= DB::table('role_wise_permission')
    	->insert([
    		'role_id'=>$id,
    		'permission_id'=>$permissionID,
    		'status'=>1,
    		'created_at'=>date('Y-m-d h:i:s')
    		]);
    	if($result){
    		return true;
    	}
    	return false;
    }

    public static function checkExistsPermission($id,$permissionID){
        $result= DB::table('role_wise_permission')
        ->where('permission_id', '=', $permissionID)
        ->get();
        return $result;
    }

    public static function selectMenu(){
    	return DB::table('role_wise_permission')->get();
    }
    public static function deleteRolePermission($id,$permission){
        //print_r($permission);exit;
        
        for ($i=0; $i <count($permission) ; $i++) {
        $result = DB::table('role_wise_permission')
            ->where('permission_id', $permission[$i])  // find your user by their id
            ->delete();
        }
    }
}
