<?php

namespace App\PL;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Pagelimit;
use App\User_Role;
use App\User_company;

use Auth;
class ParsingManager
{
    public function SetUser($input){
//return $input;

         $user = new User();
         
            $user->name = $input->name;
            $user->photo = $input->photo;
            $user->email = $input->email;
            $user->password = bcrypt($input->password);
            $user->role_id  = $input->role_id;
            $user->created_by = Auth::User()->id;
            


       return $user;
   }

    public function SetUserForUpdate()
      {

       /* else{
            $data = array(
            'name'  => $input->name,
            'email' => $input->email,
             );

        }*/
       

    }

    public function SetUserLimit($input, $id)
    {
        $userLimit = new Pagelimit();
        $userLimit->userid = $id;
        $userLimit->user_limit = $input->max_user_limit;
        $userLimit->added_by = Auth::User()->id;
        return $userLimit;
    }

  

    public function SetUserRole($input, $id)
    {
        $userRole = new User_Role();
        $userRole->userid = $id;
        $userRole->roleid = $input->role_id;
        return $userRole;


    }
    
    public function SetUserCompany($input, $id)
    {
        
        $userCompany = new User_company();
        $userCompany->userid = $id;
        $userCompany->roleid = $input->role_id;
        $userCompany->companyid = $input->companyid;
        return $userCompany;
    }

    public function SetProfile($request)
    {
        //return $request;
        if($request->input('password')){
             $data = array(
            'name'  => $request->input('username'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
             );
        }
        else{
            $data = array(
            'name'  => $request->input('username'),
            'email' => $request->input('email')
            
             );

        }
       
        return $data;
    }

    public function SetUserLimitForUpdate($input, $id)
    {
        $userLimit = new Pagelimit();
        $userLimit->userid = $id;
        $userLimit->user_limit = $input->userlimit;
        $userLimit->updated_by = Auth::User()->id;
        return $userLimit;
    }


}
