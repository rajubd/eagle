<?php



namespace App\UCL;
use Illuminate\Http\Request;

use ParsingManager;

use App\User;
use App\User_Role;
use App\Role;
use App\User_Company;
use App\Pagelimit;
use ACL;
use Auth;

use CompanyManager;
use RoleManager;
use UserRoleManager;
use UserLimitManager;
use UserCompanyManager;
use UserManager;
use DB;
use App\UCL\ViewModel\UserRoleCompanyLimitViewModel;

class UserDataUclManager  
{


    public function CreateUser($input)
    {
        

        //return $input;
        try{
            //return $input;
            //DB::beginTransaction();

            //$userRoleCompanyLimit = new UserRoleCompanyLimitViewModel();

           /* $userRoleCompanyLimit->name       = $input->name;
            $userRoleCompanyLimit->email      = $input->email;
            $userRoleCompanyLimit->password   = $input->password;
            $userRoleCompanyLimit->created_by = Auth::user()->id;
            $userRoleCompanyLimit->roleid     = $input->role_id;
            */
          /*  if(empty($input->max_user_limit)){
                $userRoleCompanyLimit->user_limit = -1;
            }
            else
            {
                $userRoleCompanyLimit->user_limit = $input->max_user_limit;
            }
*/

           /* if(empty($input->companyid)){
                $userCompanyManager = new UserCompanyManager();
                $userCompany = $userCompanyManager->GetUserCompanyByUserId(Auth::User()->id);
                $userRoleCompanyLimit->companyid = $userCompany->companyid;
                //return $userRoleCompanyLimit->companyid;
            }
            else
            {
                $userRoleCompanyLimit->companyid  = $input->companyid;
            }*/



           /* $user = new User();
            $user->name = $input->name;
            $user->email = $input->email;
            $user->password = bcrypt($input->password);
            $user->role_id  = $input->role_id;
            $user->created_by = Auth::User()->id;*/

        //return $user;
           

            $parsingManager = new ParsingManager();





            $user = $parsingManager->SetUser($input);

            $userManager = new UserManager();

            $newUser = $userManager->InsertUser($user);
           
          
                /*if(!$newUser)
                {
                    DB::rollBack();
                    return "Error in create User";
                    
                }
                else{*/
                  /*  $userRole = new User_Role();
                    $userRole->userid = $newUser->id;
                    $userRole->roleid = $input->role_id;*/

                    $userRole = $parsingManager->SetUserRole($input, $newUser->id);
                    $userRoleManager = new UserRoleManager();
                    
                    $userRoleManager->InsertUserRole($userRole);
               // }
                    

               /* if (!$userRoleManager) {
                     DB::rollBack();
                    return "Error in User Role";
                }
                else{*/
                   /*

                    $userCompany = new User_company();
                    $userCompany->userid = $newUser->id;
                    $userCompany->roleid = $input->role_id;

                    if(empty($input->companyid)){
                        $userCompanyManager = new UserCompanyManager();
                        $company = $userCompanyManager->GetUserCompanyByUserId(Auth::User()->id);
                        $userCompany->companyid = $company->id;
                        //return $userRoleCompanyLimit->companyid;
                    }
                    else
                    {
                        $userCompany->companyid = $input->companyid;
                    }

                    */

                    //$userCompany->companyid = $userRoleCompanyLimit->companyid;
                    $userCompany = $parsingManager->SetUserCompany($input, $newUser->id);

                    $userCompanyManager = new UserCompanyManager();
                    
                    $userCompanyManager->InsertUserCompany($userCompany);
                //}
                  
                  
                /*if (!$userCompanyManager) {
                    DB::rollBack();
                    return "Error in User Company";
                }
                else{*/

                  /*  $userLimit = new Pagelimit();
                    $userLimit->userid = $newUser->id;
                      if(empty($input->max_user_limit)){
                            $userLimit->user_limit  = -1;
                        }
                    else
                        {
                            $userLimit->user_limit = $input->max_user_limit;
                        }

                    //$userLimit->user_limit = $userRoleCompanyLimit->user_limit;
                    $userLimit->added_by = Auth::User()->id;
*/
                    $userLimit = $parsingManager->SetUserLimit($input, $newUser->id);
                    
                    $userLimitManager = new UserLimitManager();
                    $userLimitManager->InsertUserLimit($userLimit);

                //}
                
                /*if (!$userLimitManager) {
                    DB::rollBack();
                    return "Error in User Limit";
                }
                else{*/

                    $userLimitManager->DecreaseUserLimit();
                //}

                    
                
                return true;
                }
                catch (\Exception $e) {
                    return false;
                }

    }
    public function ViewSingleUser($id)
    {
        
        $userManager = new UserManager();
        $user = $userManager->GetUserById($id);

        $userLimitManager = new UserLimitManager();
        $limit = $userLimitManager->GetUserLimit($id); 

        $userRoleManager = new UserRoleManager();
        $userRole = $userRoleManager->GetUserRoleByUserId($id);

        $roleManager = new RoleManager();
        $role = $roleManager->GetRoleById($userRole->roleid);

        //return $role;

        $userRoleCompanyLimitViewModel = new UserRoleCompanyLimitViewModel();
        $userRoleCompanyLimitViewModel->username   = $user->name;
        $userRoleCompanyLimitViewModel->email      = $user->email;

        $userRoleCompanyLimitViewModel->user_limit = $limit->user_limit;
        $userRoleCompanyLimitViewModel->rolename   = $role->name;
        return $userRoleCompanyLimitViewModel;
    }

    public function UpdateSingleUser($input)
    {
        //return $input->name;
        try{
            /*if ($input->password) {
                $user->password = bcrypt($input->password);
            }
            */
          /*  $userRoleCompanyLimit = new UserRoleCompanyLimitViewModel();
            $userRoleCompanyLimit->userid     = $request->input('userid');
            $userRoleCompanyLimit->name       = $request->input('name');
            $userRoleCompanyLimit->email      = $request->input('email');
            $userRoleCompanyLimit->roleid     = $request->input('role_id');

            if(empty($request->input('userlimit')))
            {
                $userRoleCompanyLimit->user_limit = -1;
            }
            else
            {
                $userRoleCompanyLimit->user_limit = $request->input('userlimit');                
            }
            $userRoleCompanyLimit->password = bcrypt($request->input('password'));

*/
            //$userId = $userRoleCompanyLimit->userid;
      

            //return $existingUser->id;
  
           /* $newUser = new User(); 
         
            $newUser->name = $userRoleCompanyLimit->name;
            $newUser->email = $userRoleCompanyLimit->email; 
            $newUser->password = bcrypt($userRoleCompanyLimit->password); 
            $newUser->role_id = $userRoleCompanyLimit->roleid;*/
      /*

            $newUser = $parsingManager->SetUserForUpdate($input);*/
            $parsingManager = new ParsingManager();


            $userManager = new UserManager();
            
            $existingUser = $userManager->GetUserById($input->userid);
            //return $existingUser->id;
            $userManager->UpdateUser($existingUser);

/*
            $newUser = array(
            'name' => $userRoleCompanyLimit->name, 
            'email' => $userRoleCompanyLimit->email, 
            'password' => $userRoleCompanyLimit->password, 
            'role_id' => $userRoleCompanyLimit->roleid, 
            );
*/

 


            //$newUser = $UserManager->SetUpdatedUser($userRoleCompanyLimit);
            //$existingUser->update($newUser);

  /*          $userRole = new User_Role();
            $userRole->userid = $input->userid;
            $userRole->roleid = $input->role_id;
*/
            //return "Hy";
           /* $userRoleManager = new UserRoleManager();
            $userRole = $parsingManager->SetUserRole($input, $existingUser->id);
            $userRoleManager->UpdateUserRole($userRole, $existingUser->id);*/

/*            $userLimit = new Pagelimit();
            $userLimit->userid = $input->userid;
            $userLimit->user_limit = $input->user_limit;*/
//return $userLimit;
            $userLimitManager = new UserLimitManager();
            $userLimit = $parsingManager->SetUserLimitForUpdate($input, $existingUser->id);
            $userLimitManager->UpdateUserLimit($userLimit);


            return true;
            }
            catch (Exception $e) {
                return false;
            }
    }



    public function RemoveSingleUser($id)
    {
        try{
            $UserManager = new UserManager();
            $existingUser = $UserManager->GetUserById($id);
            \File::delete(public_path('public/uploads/userPhoto/' . $existingUser->photo));
            $existingUser->delete();

            $userRoleManager = new UserRoleManager();
            $userRoleManager->RemoveUserRole($id);

            $userLimitManager = new UserLimitManager();
            $userLimitManager->RemoveAddUserLimit($id);

            $userCompanyManager = new UserCompanyManager();
            $userCompanyManager->RemoveUserCompany($id);
 
            //return $newUser;
                return true;
                }
                catch (Exception $e) {
                    return false;
                }
    }























/*

    public function GetUserByEmail($user){ 
        return User::where('email',$user->email)->first();
    }*/
/*
    public function InsertUserRole($user, $userRoleCompanyLimit)
    {
        
        try{
            $role = User_Role::create(
                    [
                        'userid' => $user->id,
                        'roleid' => $userRoleCompanyLimit->roleid
                    ]
                );
            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }*/
/*
    public function UpdateUserRole($id, $userRoleCompanyLimit)
    {
        //return $userRoleCompanyLimit->roleid;
        try{
            $oldRole = User_Role::where('userid',$id)->first();
            $newRole = array('roleid' => $userRoleCompanyLimit->roleid, );
            $oldRole->update($newRole);
            //return $old = User_Role::where('userid',$id)->first();
           
            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }*/

  

   /* public function SetUpdatedUser($userRoleCompanyLimit){

        $user = array(
            'name' => $userRoleCompanyLimit->name, 
            'email' => $userRoleCompanyLimit->email, 
            'password' => $userRoleCompanyLimit->password, 
            'role_id' => $userRoleCompanyLimit->roleid, 
             
            );
            
        return $user;
    }
*/
   /* public function InsertUserCompany($user, $userRoleCompanyLimit)
    {
        try{
            if( $userRoleCompanyLimit->companyid == ''){
                $companyManager = new CompanyManager();
                $companyId = $companyManager->GetCompanyIdByUserId(Auth::User()->id);
                $userRoleCompanyLimit->companyid = $companyId;
            }

            $userCompany = User_Company::create(
                    [
                    'userid' => $user->id,
                    'roleid' => $userRoleCompanyLimit->roleid,
                    'companyid' => $userRoleCompanyLimit->companyid
                    ]
                );
            //return $userCompany;
        }
        catch(Exception $e) {
            return false;
        }
    }*/

/*
    public function InsertAddUserLimit($user, $userRoleCompanyLimit)
    {
        try{
            if( $userRoleCompanyLimit->user_limit == ''){
                 $userRoleCompanyLimit->user_limit = -1;
            }
            
            $limit = Pagelimit::create(
                [
                    'userid'     => $user->id, 
                    'user_limit' => $userRoleCompanyLimit->user_limit, 
                    'added_by'   => Auth::User()->id
                ]
            );
            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }
*/
/*
 public function UpdateAddUserLimit($id, $userRoleCompanyLimit)
    {
        try{
            if( $userRoleCompanyLimit->user_limit == ''){
                $userRoleCompanyLimit->user_limit = -1;
            }
            $existinglimit = Pagelimit::where('userid',$id)->first();
            $newLimit = array('user_limit' => $userRoleCompanyLimit->user_limit );
            $existinglimit->update($newLimit);

            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }
*/

 /*   public function GetAddUserLimit()
    {
        try{

            $limit = Pagelimit::where('userid',Auth::User()->id)->first();
            return $limit;
        }
        catch(Exception $e) {
            return false;
        }
    }*/
    /*public function GetAddUserLimitByUserId($id)
    {
        try{

            $limit = Pagelimit::where('userid',$id)->first();

            return $limit;
        }
        catch(Exception $e) {
            return false;
        }
    }*/
   /* public function GetUserRoleByUserId($id)
    {
        try{

            $role = User_Role::where('userid',$id)->first();

            return $role;
        }
        catch(Exception $e) {
            return false;
        }
    }*/
/*
    public function GetRoleById($id)
    {
        try{

            $role = Role::find($id);

            return $role;
        }
        catch(Exception $e) {
            return false;
        }
    }
*/

/*
    public function DecreaseAddUserLimit()
    {
        try{
            $myAddUserLimit = $this->GetAddUserLimit();
            $myAddUserLimit->user_limit = $myAddUserLimit->user_limit-1;
            $myAddUserLimit->save();

            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }*/

/*
public function RemoveUserRole($id)
{
    try{
        $userRole = User_Role::where('userid',$id)->first();
        $userRole->delete();
        return true;
    }
    catch(Exception $e) {
            return false;
        }

}*/

/*
public function RemoveAddUserLimit($id)
{
    try{
        $limit = Pagelimit::where('userid',$id)->first();
        $limit->delete();
        return true;
    }
    catch(Exception $e) {
            return false;
        }

}
*/
/*  public function SetUser($userRoleCompanyLimit){
        $user = new User();
            $user->name = $userRoleCompanyLimit->name;
            $user->email = $userRoleCompanyLimit->email;
            $user->password = bcrypt($userRoleCompanyLimit->password);
            $user->role_id = $userRoleCompanyLimit->roleid;
        return $user;
    }

*/
/*public function RemoveUserCompany($id)
{
    try{
        User_Company::where('userid',$id)->delete();
       
        return true;
    }
    catch(Exception $e) {
            return false;
        }

}*/

















/*

    public function UpdateUser($user,$id)
    {
        $userGateway = new UserGateway();
        $existingUser = $this->GetUserById($id);
        //$oldRole = new Role();
        //$oldRole->name = $existingRole->name;
        return $userGateway->UpdateUser($existingUser,$user);
    }

    public function DeleteUser($id)
    {
        $userGateway = new UserGateway();
        $user = $this->GetUserById($id);
        return $userGateway->DeleteRole($user,$id);
    }   
*/
  

}
