<?php

namespace App\DAL;
use Illuminate\Http\Request;

use App\User;
use App\Models\User_Role;
use App\Models\Role;
use ACL;
use Auth;


class UserRoleGateway  
{
/*
    public function SetUserRole($input, $id)
    {
        $userRole = new User_Role();
        $userRole->userid = $id;
        $userRole->roleid = $input->role_id;
        return $userRole;
    }
    */

    public function GetUserRoleByUserId($id){ 
        return  User_Role::where('userid',$id)->first();
    }


     public function GetUserRoleByRoleId($id){ 
    	return  User_Role::where('roleid',$id)->all();
    }
   /*  public function GetUserRoleByUserId($id)
    {
        try{

            $role = User_Role::where('userid',$id)->first();

            return $role;
        }
        catch(Exception $e) {
            return false;
        }
    }*/


    public function InsertUserRole($userRole)
    {
        
        try{
            $userRole->save();
            //return $userRole;
            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }



    
    public function UpdateUserRole($userRole,$id)
    {
        try{
            User_Role::where('userid',$id)->update([
                'roleid' => $userRole->roleid
                ]);
            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }




public function RemoveUserRole($userRole)
{
    try{
        $userRole->delete();
        return true;
    }
    catch(Exception $e) {
            return false;
        }

}
  public function DeleteUserRoleByRoleId($userRole)
    {
        try{
        $userRole->delete();
        return true;
    }
    catch(Exception $e) {
            return false;
        }
    }






}
