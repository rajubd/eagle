<?php

namespace App\DAL;
use DB;
use Illuminate\Http\Request;

use App\Models\Role;
use App\Models\RoleWisePermission;


//use DLL\AppInfoGateway;
class RoleGateway
{
	public function GetAllRoles(){        
        return Role::all();
    }

    public function GetRoleByName($name)
    {
        try{
            return Role::where('roleName',$name)->first();
        }
        catch (Exception $e) {
                    return false;
                }
    }

    public function GetRoleById($id)
    {
        try{
            return Role::find($id);
        }
        catch (Exception $e) {
                    return false;
                }
    }

    public function CreateRole($role)
    {
        
         try{
                $role->save();
                return true;
                }
                catch (Exception $e) {
                    return false;
                }
    }

    public function UpdateRole($existingRole)
    {
        try{
            $existingRole->save();
            return true;
        }
        catch (Exception $e) {
                    return false;
                }
        
    }

 public function DeleteRole($role)
    {
        try{
            $role->delete();  
        }
        catch (Exception $e) {
                    return false;
                }

    }



}
