<?php

namespace App\DAL;


use App\Models\Permission;

class PermissionGateway
{
	public function GetAllPermissions(){        
        return Permission::all();
    }




    public function GetPermissionByName($name)
    {
        try{
            return Permission::where('name',$name)->first();
        }
        catch (Exception $e) {
                    return false;
                }
        
    }



    public function GetPermissionById($id)
    {
        try{
            return Permission::find($id);
        }
        catch (Exception $e) {
                    return false;
                }
    }



    public function CreatePermission($permission)
    {
        
         try{
                $permission->save();
                return true;
                }
                catch (Exception $e) {
                    return false;
                }
    }

   

    public function UpdatePermission($currentPermission,$updatedPermission)
    {
        try{
            return $currentPermission->update($updatedPermission);
        }
        catch (Exception $e) {
                    return false;
                }
        
    }





 public function DeletePermission($currentPermission)
    {
        try{
            $currentPermission->delete();  
        }
        catch (Exception $e) {
                    return false;
                }

    }


 /*public function GetPermissionPermissionById($id)
    {
        try{

        }
         catch (Exception $e) {
                    return false;
                }
        //return $PermissionGateway->GetPermissionPermissionById($id);
    }*/









	

}
