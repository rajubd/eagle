<?php

namespace App\DAl;
use Illuminate\Http\Request;

use App\User;
use App\Models\User_Role;
use App\Models\Role;
use App\Models\RoleWisePermission;
use ACL;
use Auth;

class RoleWisePermissionGateway  
{

    public function GetRolePermissionByRoleId($id)
    { 
        return RoleWisePermission::where('role_id',$id)->get();
    }



    public function DeleteRoleWisePermissionByRoleId($roleWisePermission,$roleid)
    { 
        try{
        	foreach ($roleWisePermission as $rolewise)
	        {
	            RoleWisePermission::where('role_id',$roleid)->delete();
	        }
        }
       catch(Exception $e) {
                    return false;
                }
    }

    public function AddRoleWisePermission($permissions,$roleid)
    { 
        try
        {
	        foreach ($permissions as $permission)
	        {            
	            RoleWisePermission::create(
	             array(
	                'permission_id' => $permission, 
	                'role_id' => $roleid, 
	                ));
	        }
        }
        catch(Exception $e) {
                    return false;
                }
        
    }

}
