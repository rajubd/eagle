<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Level;

class LevelController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levels = Level::all();

        return view('eagle.levels.levels',compact('levels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eagle.levels.createLevel');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Level = new Level();
        $Level->level = $request->level;
        $Level->remarks = $request->remarks;
//return $Level;
        $Level->save();

        return redirect('/levels')->with('success','Level Added Successfully');

        //return $request->all();
    }



    public function edit($id)
    {
        $level = Level::find($id);
        return view('eagle.levels.editLevel',compact('level'));
    }



    public function update(Request $request, $id)
    {
        $Level = Level::find($id);
        $Level->level = $request->level;
        $Level->remarks = $request->remarks;
//return $Level;
        $Level->save();

         return redirect('/levels')->with('success','Level Updated Successfully');
    }



    public function destroy($id)
    {
         $Level = Level::find($id);
         $Level->delete();
          return redirect('/levels')->with('success','Level Deleted Successfully');
    }
}
