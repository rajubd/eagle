<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;
use App\Models\Unit;
use App\Models\Sales_account;
use App\Models\Purchase_account;
use App\Models\Item;
use App\Models\Brand;
use App\Models\Journal;
use App\Models\Journal_account;
use DB;

class JournalController extends Controller
{
      public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $journals = Journal::all();
        $accounts = Sales_account::all();
        return view('eagle.journals.journals',compact('accounts','journals'));
    }

    public function create()
    {
        $taxes = Tax::all();
        $accounts = Sales_account::all();
        return view('eagle.journals.createjournal',compact('accounts','taxes'));
    }


    public function store(Request $request)
    {
        //return Input::all();

        $journal = new Journal();

        $journal->journal_date = Input::get('journal_date');
        $journal->journal_reference = Input::get('journal_reference');
        $journal->journal_note =  Input::get('journal_note');
        $journal->journal_currency =  Input::get('journal_currency');

        $journal->save();

        $id = $journal->id;
        $account = Input::get('account_id');
        $description = Input::get('description');
        $contact = Input::get('contact');
        $tax = Input::get('tax_id');
        $debit = Input::get('debit');
        $credit = Input::get('credit');

        for ($i=0; $i < sizeof($account); $i++) {
             if(!empty($account[$i]))
             {
                $journal_account = new journal_account();

                $journal_account->journal_id  =  $id;
                $journal_account->account_id  =  $account[$i];
                $journal_account->description =  $description[$i];
                $journal_account->contact =  $contact[$i];
                $journal_account->tax_id =  $tax[$i];
                $journal_account->debit =  $debit[$i];
                $journal_account->credit =  $credit[$i];

                $journal_account->save();
     
                //print_r($acc);
            }
        }
       // exit;

        



        return redirect('journals')->with('success','New Journal created Successfully');
      
        //return $item->item_name;
        //return view('eagle.items.items');
    }

    public function edit($id)
    {
        //return $id;
        $journal = Journal::find($id);
        $journal_accounts =  DB::table('journal_accounts')->where('journal_id', $id)
            ->join('sales_account','journal_accounts.account_id', '=', 'sales_account.id')
            ->select('journal_accounts.*', 'sales_account.sales_account_name')
            ->get();
        $taxes = Tax::all();
        $accounts = Sales_account::all();

        //return $journal_accounts;
        return view('eagle.journals.editjournal',compact('accounts','taxes','journal','journal_accounts'));
    }
     public function update($id)
     {
        return Input::all();
     }
    

    public function delete($id)
     {
        $journal = Journal::find($id);
        $journal->delete();
        return redirect('journals')->with('success','Journal removed Successfully');

     }
}
