<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Image;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Level;
use App\Models\Leave;
use App\Models\ContractType;
use App\Models\ContractBase;
use DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$Employees = Employee::all();
        $Employees = DB::table('employees')
            ->leftJoin('departments', 'employees.department_id', '=', 'departments.id')
            ->leftJoin('designations', 'employees.designation_id', '=', 'designations.id')
            ->leftJoin('contract_type', 'employees.contract_type_id', '=', 'contract_type.id')
            ->leftJoin('contract_base', 'employees.contract_base_id', '=', 'contract_base.id')

            ->select('employees.*', 'departments.department_name', 'designations.designation_name', 'contract_type.typename', 'contract_base.basename')
            ->get();;

        return view('eagle.employees.employees',compact('Employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$departments = Department::all();
    	$designations = Designation::all();
    	$levels = Level::all();
    	$contractTypes = ContractType::all();
    	$contractBases = ContractBase::all();

        return view('eagle.Employees.createEmployee',compact('departments','designations','levels','contractTypes','contractBases'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename = '';
        if(Input::file('photo'))
        {
            $image = Input::file('photo');
            //return $image;
            $filename  = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('images/employees/' . $filename);
            Image::make($image->getRealPath())->save($path);
           // \File::delete(public_path('frontend/doctorImages/' . $existingDoctor->doctorImage));
            //$existingDoctor->doctorImage = $filename;
        }

        //return $request->all();
        $Employee = new Employee();
        $Employee->first_name = $request->first_name;
        $Employee->last_name = $request->last_name;
        $Employee->ot_rate = $request->ot_rate;
        $Employee->photo = $filename;
        $Employee->fathers_name = $request->fathers_name;
        $Employee->mothers_name = $request->mothers_name;
        $Employee->department_id = $request->department_id;
        $Employee->designation_id = $request->designation_id;
        $Employee->level_id = $request->level_id;
        $Employee->contract_type_id = $request->contract_type_id;
        $Employee->contract_base_id = $request->contract_base_id;
        $Employee->email = $request->email;
        $Employee->alternate_email = $request->alternate_email;
        $Employee->mobile = $request->mobile;
        $Employee->alternate_mobile = $request->alternate_mobile;
        $Employee->nid = $request->nid;
        $Employee->employee_code = $request->employee_code;
        $Employee->marital_status = $request->marital_status;
        $Employee->present_address = $request->present_address;
        $Employee->permanent_address = $request->permanent_address;
        $Employee->nationality = $request->nationality;
        $Employee->birth = $request->birth;
        $Employee->joining_date = $request->joining_date;
        $Employee->religion = $request->religion;
        $Employee->gender = $request->gender;
        $Employee->passport_no = $request->passport_no;
        $Employee->salary = $request->salary;
        $Employee->remarks = $request->remarks;


//return $Employee;
        $Employee->save();

        return redirect('/employees')->with('success','Employee Added Successfully');

        //return $request->all();
    }



    public function edit($id)
    {
        $employee = Employee::find($id);
        $departments = Department::all();
        $designations = Designation::all();
        $levels = Level::all();
        $contractTypes = ContractType::all();
        $contractBases = ContractBase::all();
        return view('eagle.Employees.editEmployee',compact('employee','departments','designations','levels','contractTypes','contractBases'));
    }



    public function update(Request $request, $id)
    {
        $Employee = Employee::find($id);

        if(Input::file('photo'))
        {
            $image = Input::file('photo');
            //return $image;
            $filename  = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('images/employees/' . $filename);
            Image::make($image->getRealPath())->save($path);
            \File::delete(public_path('images/employees/' . $Employee->photo));
            $Employee->photo = $filename;
        }


        $Employee->first_name = $request->first_name;
        $Employee->last_name = $request->last_name;
        $Employee->ot_rate = $request->ot_rate;

        $Employee->fathers_name = $request->fathers_name;
        $Employee->mothers_name = $request->mothers_name;
        $Employee->department_id = $request->department_id;
        $Employee->designation_id = $request->designation_id;
        $Employee->level_id = $request->level_id;
        $Employee->contract_type_id = $request->contract_type_id;
        $Employee->contract_base_id = $request->contract_base_id;
        $Employee->email = $request->email;
        $Employee->alternate_email = $request->alternate_email;
        $Employee->mobile = $request->mobile;
        $Employee->alternate_mobile = $request->alternate_mobile;
        $Employee->nid = $request->nid;
        $Employee->employee_code = $request->employee_code;
        $Employee->marital_status = $request->marital_status;
        $Employee->present_address = $request->present_address;
        $Employee->permanent_address = $request->permanent_address;
        $Employee->nationality = $request->nationality;
        $Employee->birth = $request->birth;
        $Employee->religion = $request->religion;
        $Employee->gender = $request->gender;
        $Employee->passport_no = $request->passport_no;
        $Employee->salary = $request->salary;
        
        $Employee->save();


        return redirect('/employees')->with('success','Employee Updated Successfully');
    }



    public function destroy($id)
    {
        $Employee = Employee::find($id);
        $leave = Leave::where('employee_id', $Employee->id);
        $leave->delete();
        //$Employee = Employee::find($id);
        \File::delete(public_path('images/employees/' . $Employee->photo));

        $Employee->delete();

        return redirect('/employees')->with('success','Employee Deleted Successfully');
    }
}
