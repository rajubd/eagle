<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;

use App\Models\Sales_account;

use App\Models\Currency;

use App\Models\Payment;
use App\Models\Invoice;
use App\Models\Invoice_item;
use App\Models\Bill;
use App\Models\Vendor;
use App\Models\Customer;
use App\Models\Item;
use App\Models\Payment_mode;
use App\Models\Payment_received;

use DB;
use Auth;

class InvoiceController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $invoices = Invoice::all();
        return view('eagle.invoices.invoices',compact('invoices'));
    }

    public function create()
    {
        //$accounts = Sales_account::all();
        $items = Item::all();
        $customers = Customer::all();
        $taxes = Tax::all();
      return view('eagle.invoices.createinvoice',compact('items','taxes','customers'));
    }

   
    public function invoicedetails()
    {
        $invoices = Invoice::all();
    	return view('eagle.invoices.invoicedetails',compact('invoices'));
    }

   

    public function store(Request $request)
    {

      //return sizeof();
        $items = Input::get('item');
       
       $qty = Input::get('quantity');
       $rate = Input::get('rate');
       $amount = Input::get('amount');
       $tax = Input::get('tax_id');



        $invoice = new Invoice();

        $invoice->customer_id = Input::get('customer_id');
        $invoice->invoice_no = Input::get('invoice_number');
        $invoice->order_no = Input::get('order_no');
        $invoice->invoice_date = Input::get('invoice_date');
        $invoice->due_date = Input::get('invoice_due_date');
        $invoice->sales_person_id = Input::get('sales_person_id');
        $invoice->term_id = Input::get('term_id');
        $invoice->status = 'Draft';
        $invoice->invoice_amount = Input::get('total');
        $invoice->due_amount = Input::get('total');
        $invoice->created_by = Auth::id();

        $invoice->save();



        $invoice_id = $invoice->id;
       
        // $bill_id = $bill->id;

          $limit = count($items)-1 ;

        // //return $limit;
         for ($i=0; $i < $limit; $i++) { 

            $inv_acc = new Invoice_item();

            $inv_acc->invoice_id = $invoice_id;

            $inv_acc->item_id = $items[$i];
           
            
            $inv_acc->qty = $qty[$i];
            $inv_acc->rate = $rate[$i];
            $inv_acc->amount = $amount[$i];
            $inv_acc->tax_id = $tax[$i];
 
            $inv_acc->save();

         }
      //return $bill_ac;



       
    


        return redirect('invoices')->with('success','New Invoice Stored Successfully');
      

    }






















 public function loadinvoicedata($id)
    {
        
        $invoice = Invoice::find($id);
        $inv_items = Invoice_item::where('invoice_id',$invoice->id)->get();
        return view('eagle.invoices.loadinvoicedata',compact('invoice','inv_items'));
    }



 public function loadinvoicedata1()
    {
        $invoice = Invoice::first();
        $inv_items = Invoice_item::where('invoice_id',$invoice->id)->get();
        return view('eagle.invoices.loadinvoicedata',compact('invoice','inv_items'));

    }


 public function loadrecordpayment()
    {
        $bills = Bill::where('vendor_id',$id)->get();
        $total=0;
        foreach ($bills as $bill) {
            $total += $bill->amount_due;
        }
        return $total;
        //return view('eagle.Payrolls.demo2',compact('bills'));
    }




 public function createrecordpayment($id)
    {
        $invoice = Invoice::find($id);
        $modes = Payment_mode::all();
        return view('eagle.invoices.createrecordpayment',compact('invoice','modes'));
    }



 public function storerecordpayment(Request $request, $id)
    {
        
        //return $request->all();
    $invoice = Invoice::find($id);
    $due = $invoice->due_amount - Input::get('payment_made');

    $pay_date = date(Input::get('payment_date'));
    $due_date = date($invoice->due_date);

    // if($pay_date < $due_date)
    //     return $pay_date;
    // else{
    //     return$due_date;
    // }
    if($due == 0){
        $unused = 0;
        $invoice->status = 'Paid';
        $invoice->due_amount = $due;
    }
    elseif($due > 0){
        if($pay_date > $due_date)
        {
            $day = $pay_date - $due_date;
            $invoice->status = 'Overdue by '.$day.' Day(s)';
        }
        else
        {

            $invoice->status = 'Partially paid';
        }
        $invoice->due_amount = $due;
        $unused = 0;
    }
    else{
        $invoice->status = 'Paid';
        $unused = abs($due);
        $invoice->due_amount = 0;
    }
    $invoice->save();



    $payment_received = new Payment_received();

    $payment_received->customer_id = $invoice->customer_id;
    $payment_received->amount = Input::get('payment_made');
    $payment_received->payment_date = Input::get('payment_date');
    $payment_received->payment_mode = Input::get('payment_mode');
    $payment_received->reference = Input::get('reference_no');
    $payment_received->unused_amount = $unused;

    $payment_received->save();
    //return $payment_received;
  
    return redirect('invoices')->with('success','Record added Successfully');
    }

}
