<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;

use App\Models\Sales_account;

use App\Models\Currency;

use App\Models\Payment;
use App\Models\Invoice;
use App\Models\Bill;
use App\Models\Vendor;
use App\Models\Customer;
use App\Models\Item;
use App\Models\Estimate;
use App\Models\Estimate_item;
use App\Models\Creditnote;
use App\Models\Creditnote_item;
use DB;
use Auth;

class CreditNoteController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $creditnotes = Creditnote::all();
        return view('eagle.creditnotes.creditnotes',compact('creditnotes'));
    }

    public function create()
    {
        //$accounts = Sales_account::all();
        $items = Item::all();
        $customers = Customer::all();
        $taxes = Tax::all();
      return view('eagle.creditnotes.createcreditnote',compact('items','taxes','customers'));
    }

   
    public function estimatedetails()
    {
         $creditnotes = Creditnote::all();
    	   return view('eagle.creditnotes.estimatedetails',compact('creditnotes'));
    }

   

public function store(Request $request)
    {

       //return Input::all();
       $items = Input::get('item');
       
       $qty = Input::get('quantity');
       $rate = Input::get('rate');
       $amount = Input::get('amount');
       $tax = Input::get('tax_id');



        $creditnote = new Creditnote();

        $creditnote->customer_id = Input::get('customer_id');
        $creditnote->credit_note_no = Input::get('credit_note_no');
        $creditnote->reference_no = Input::get('reference_no');
        $creditnote->creditnote_date = Input::get('creditnote_date');
       
        $creditnote->status = 'Draft';
        $creditnote->creditnote_amount = Input::get('total');
       
        $creditnote->created_by = Auth::id();

        $creditnote->save();



        $id = $creditnote->id;
       
        // $bill_id = $bill->id;

         $limit = count($items)-1 ;

        // //return $limit;
         for ($i=0; $i < $limit; $i++) { 

            $est_item = new Creditnote_item();

            $est_item->creditnote_id = $id;

            $est_item->item_id = $items[$i];
           
            
            $est_item->qty = $qty[$i];
            $est_item->rate = $rate[$i];
            $est_item->amount = $amount[$i];
            $est_item->tax_id = $tax[$i];
 
            $est_item->save();

         }
      //return $bill_ac;


       
    


        return redirect('credit_notes')->with('success','New Credit Note Stored Successfully');
      

    }






















 public function loadestimatedata1()
    {
        $estimate = Estimate::first();
        $est_items = Estimate_item::where('estimate_id',$estimate->id)->get();
        return view('eagle.estimates.loadestimatedata',compact('estimate','est_items'));
    }


 public function loadestimatedata($id)
    {
        $estimate = Estimate::find($id);
        $est_items = Estimate_item::where('estimate_id',$estimate->id)->get();
        return view('eagle.estimates.loadestimatedata',compact('estimate','est_items'));
    }










}
