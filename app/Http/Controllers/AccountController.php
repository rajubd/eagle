<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;
use App\Models\Unit;
use App\Models\Sales_account;
use App\Models\Purchase_account;
use App\Models\Item;
use App\Models\Brand;
use App\Models\Account_type;
use Auth;
use DB;
class AccountController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $accounts = Sales_account::all();
        $accounts =  DB::table('sales_account')
            ->join('account_type', 'sales_account.sales_account_type', '=', 'account_type.id')
            ->select('sales_account.*', 'account_type.account_type')
            ->get();
        $types = Account_type::all();
        return view('eagle.account.chartofaccounts',compact('accounts','types'));
    }

    // public function create()
    // {
    //     //return "hy";
    //     $items = Item::all();
    //     $taxes = Tax::all();
    //     $units = Unit::all();
    //     $brands = Brand::all();
    //     $sales_accounts = Sales_account::all();
    //     $purchase_accounts = purchase_account::all();
        
    //     return view('eagle.itemAdjustments.createItemAdjustment',compact('taxes','units','sales_accounts','purchase_accounts','brands','items'));
    // }

    public function store(Request $request)
    {
      //return Input::all();
        $account = new Sales_account();

        $account->sales_account_name = Input::get('account_name');
        $account->sales_account_type = Input::get('account_type_id');
        $account->sales_account_code = Input::get('account_code');
        $account->sales_account_description = Input::get('description');
        $account->sales_account_code = Input::get('account_code');

        $account->save();

        return redirect('chartofaccounts')->with('success','New Account created Successfully');
      
        //return $item->item_name;
        //return view('eagle.items.items');
    }


     public function update(Request $request, $id)
    {
      //return Input::all();
        $account = Sales_account::find($id);

        $account->sales_account_name = Input::get('account_name');
        $account->sales_account_type = Input::get('account_type_id');
        $account->sales_account_code = Input::get('account_code');
        $account->sales_account_description = Input::get('description');
        $account->sales_account_code = Input::get('account_code');

        $account->save();

        return redirect('chartofaccounts')->with('success','Account Updated Successfully');
      
        //return $item->item_name;
        //return view('eagle.items.items');
    }





        public function storeaccounttype(Request $request)
    {
      //return Input::all();
        $account = new Account_type();

        $account->account_type = Input::get('account_type');
        $account->account_status = Input::get('account_status');;
        $account->created_by = Auth::user()->id;


        $account->save();

        return redirect('chartofaccounts')->with('success','New Account type created Successfully');
      
        //return $item->item_name;
        //return view('eagle.items.items');
    }

    public function edit()
    {
        return view('eagle.items.items');
    }
}
