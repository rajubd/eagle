<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Vendor;
use App\Models\Level;
use App\Models\Department;
use App\Models\Designation;
use DB;

class VendorController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $vendors = Vendor::all();
        return view('eagle.vendors.vendors',compact('vendors'));
    }

    public function create()
    {
        $departments = Department::all();
        $designations = Designation::all();
        $levels = Level::all();

        /*$departments = Department::all();
        $designations = Designation::all();
        $levels = Level::all();
        $contractTypes = ContractType::all();
        $contractBases = ContractBase::all();*/

        return view('eagle.vendors.createVendor', compact('levels', 'designations', 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vendor = new Vendor();
        $vendor->salutation  =  $request->salutation ;
        $vendor->vendor_first_name  =  $request->vendor_first_name ;
        $vendor->vendor_last_name  =  $request->vendor_last_name ;
        $vendor->company_name  =  $request->company_name ;
        $vendor->vendor_email  =  $request->vendor_email ;
        $vendor->vendor_mobile  =  $request->vendor_mobile ;
        $vendor->vendor_designation  =  $request->vendor_designation ;
        $vendor->vendor_department  =  $request->vendor_department ;
        $vendor->vendor_website  =  $request->vendor_website ;
        $vendor->vendor_currency  =  $request->vendor_currency ;
        $vendor->vendor_payment_terms_id  =  $request->vendor_payment_terms_id ;
        $vendor->vendor_portal_language  =  $request->vendor_portal_language ;
        $vendor->vendor_attention  =  $request->vendor_attention ;
        $vendor->vendor_address  =  $request->vendor_address ;
        $vendor->vendor_city  =  $request->vendor_city ;
        $vendor->vendor_state  =  $request->vendor_state ;
        $vendor->vendor_zip_code  =  $request->vendor_zip_code ;
        $vendor->vendor_country  =  $request->vendor_country ;
        //$vendor->vendor_fax  =  $request->vendor_fax ;
        $vendor->remarks  =  $request->remarks ;

        $vendor->save();


        return redirect('/vendors')->with('success','New Vendor Added Successfully');

        //return $request->all();
    }



    public function edit($id)
    {
        $vendor = Vendor::find($id);


        return view('eagle.vendors.editVendor',compact('vendor'));
    }



    public function update(Request $request, $id)
    {
       $customer = Customer::find($id);
        $customer->salutation  =  $request->salutation ;
        $customer->customer_first_name  =  $request->customer_first_name ;
        $customer->customer_last_name  =  $request->customer_last_name ;
        $customer->company_name  =  $request->company_name ;
        $customer->customer_email  =  $request->customer_email ;
        $customer->customer_mobile  =  $request->customer_mobile ;
        $customer->customer_designation  =  $request->customer_designation ;
        $customer->customer_department  =  $request->customer_department ;
        $customer->customer_website  =  $request->customer_website ;
        $customer->customer_currency  =  $request->customer_currency ;
        $customer->customer_payment_terms_id  =  $request->customer_payment_terms_id ;
        $customer->customer_portal_language  =  $request->customer_portal_language ;
        $customer->customer_attention  =  $request->customer_attention ;
        $customer->customer_address  =  $request->customer_address ;
        $customer->customer_city  =  $request->customer_city ;
        $customer->customer_state  =  $request->customer_state ;
        $customer->customer_zip_code  =  $request->customer_zip_code ;
        $customer->customer_country  =  $request->customer_country ;
        $customer->customer_fax  =  $request->customer_fax ;
        $customer->remarks  =  $request->remarks ;

        $customer->save();


        return redirect('/customers')->with('success','Customer Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Employee = Customer::find($id);
        //\File::delete(public_path('images/employees/' . $Employee->photo));

        $Employee->delete();

        return redirect('/customers')->with('success','Customer removed Successfully');
    }
}




