<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class SettingsController extends Controller
{
    public function viewSettings(){
        $settingsObj    = new Settings();
        $settings       = $settingsObj->findorfail(1);

        return view('Settings.Settings', array(
            'setting'     => $settings
        )); 
    }

    public function postSettings(){

        $settingsObj    = new Settings();
        $settings       = $settingsObj->find(1);
        if (Input::hasFile('photo'))
        {
           

            $image = Input::file('photo');
            $filename  = "logo". '.' . $image->getClientOriginalExtension();
            $path = public_path('images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            
            $settings->logo = $filename;

            $settings->save();

            return redirect()->back();
        }
    }
}
