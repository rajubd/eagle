<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContractBase;

class ContractBaseController extends Controller
{
    

    public function index()
    {
        $contractBase = ContractBase::all();

        return view('eagle.contract_base.contractBase',compact('contractBase'));
    }



    public function create()
    {
        return view('eagle.contract_base.createContractBase');
    }


    public function store(Request $request)
    {
    	//return $request->all();
        $designation = new ContractBase();
        $designation->basename = $request->basename;
 
        $designation->save();

        return redirect('/contractbases')->with('success','New Contract Base Added Successfully');

    }

   

    public function edit($id)
    {
        $contract_base = ContractBase::find($id);
        return view('eagle.contract_base.editContractBase',compact('contract_base'));
    }

   

    public function update(Request $request, $id)
    {
       $contract_base = ContractBase::find($id);
        $contract_base->basename = $request->basename;
     
        $contract_base->save();

         return redirect('/contractbases')->with('success','Contract Base Updated Successfully');
    }

   

    public function destroy($id)
    {
         $designation = ContractBase::find($id);
         $designation->delete();
          return redirect('/contractbases')->with('success','Contract Base Deleted Successfully');
    }
}
