<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LeavePlanner;
use App\Models\Employee;
use DB;


class LeavePlannerController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }



    public function index()
    {
        //$leaves = Leave::all();
        $leaves = DB::table('leave_planners')
        ->leftjoin('employees','leave_planners.employee_id','=','employees.id')
        ->select('leave_planners.*','employees.first_name','employees.last_name','employees.employee_code')
        ->get();
        return view('eagle.leavePlanners.leavePlanners',compact('leaves'));
    }

    public function create()
    {
       $employees = Employee::all();
        return view('eagle.leavePlanners.createLeavePlanner',compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $leave = new LeavePlanner();
        $leave->employee_id = $request->employee_id ;
        $leave->leave_plan = $request->leave_plan ;
        $leave->start_date = $request->start_date ;
        $leave->end_date = $request->end_date ;
        $leave->reason = $request->reason ;

        $leave->save();

        return redirect('/leaveplanners')->with('success','Leave plan Recorded Successfully');

        //return $request->all();
    }



    public function edit($id)
    {
    	$employees = Employee::all();
        $leave = LeavePlanner::find($id);
        $emp = Employee::find($leave->employee_id);
  //      $leave = DB::table('leave_planners')
        
        // ->leftjoin('employees','leave_planners.employee_id','=','employees.id')
        // ->select('leave_planners.*','employees.first_name','employees.last_name','employees.employee_code')

        // ->get();


        return view('eagle.leavePlanners.editLeavePlanner',compact('leave','employees','emp'));
    }



    public function update(Request $request, $id)
    {
        $leave = LeavePlanner::find($id);
        $leave->employee_id = $request->employee_id ;
        $leave->leave_plan = $request->leave_plan ;

        if(!empty($request->start_date))
        {
            $leave->start_date = $request->start_date ;
        }
        if(!empty($request->end_date))
        {
            $leave->start_date = $request->end_date ;
        }

        $leave->reason = $request->reason ;

        $leave->save();

        return redirect('/leaveplanners')->with('success','Leave plan record Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $leave = LeavePlanner::find($id);
        $leave->delete();

        return redirect('/leaveplanners')->with('success','Leave Plan record removed Successfully');
    }
}
