<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;
use App\Models\Unit;

class UnitController extends Controller
{
      public function __construct()
    {
        //$this->middleware('auth');
    }

  

    // public function index()
    // {
    //     return view('eagle.items.items');
    // }

    // public function create()
    // {
    //     return view('eagle.items.createitem');
    // }

    public function store(Request $request)
    {
       $unit = new Unit;
       $unit->unit_name = Input::get('unit_name');
      
       $unit->save();
       return redirect()->back()->with('success', 'New Unit Added Successfully');

       //return $tax;
        //return Input::all();
        //return view('eagle.items.items');
    }

    public function edit()
    {
        return view('eagle.items.items');
    }
}
