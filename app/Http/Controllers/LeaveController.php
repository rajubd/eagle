<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Leave;
use App\Models\Employee;
use DB;


class LeaveController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }



    public function index()
    {
        //$leaves = Leave::all();
        $leaves = DB::table('leaves')
        ->leftjoin('employees','leaves.employee_id','=','employees.id')
        ->select('leaves.*','employees.first_name','employees.last_name','employees.employee_code')
        ->get();
        return view('eagle.leaves.leaves',compact('leaves'));
    }

    public function create()
    {
       $employees = Employee::all();
        return view('eagle.leaves.createLeave',compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $leave = new Leave();
        $leave->employee_id = $request->employee_id ;
        $leave->start_date = $request->start_date ;
        $leave->end_date = $request->end_date ;
        $leave->reason = $request->reason ;

        $leave->save();

        return redirect('/leaves')->with('success','Leave Recorded Successfully');

        //return $request->all();
    }



    public function edit($id)
    {
        $employees = Employee::all();
        $leave = Leave::find($id);
        $emp = Employee::find($leave->employee_id);

        return view('eagle.leaves.editLeave',compact('leave','employees','emp'));
    }



    public function update(Request $request, $id)
    {
        $leave = Leave::find($id);
        $leave->employee_id = $request->employee_id ;

        if(!empty($request->start_date))
        {
            $leave->start_date = $request->start_date ;
        }
        if(!empty($request->end_date))
        {
            $leave->start_date = $request->end_date ;
        }

        $leave->reason = $request->reason ;

        $leave->save();

        return redirect('/leaves')->with('success','Leave record Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $leave = Leave::find($id);
        $leave->delete();

        return redirect('/leaves')->with('success','Leave record removed Successfully');
    }
}
