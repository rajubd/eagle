<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Permission;
use App\Models\RoleWisePermission;
use App\Models\Role;
use App\Models\User_Role;
use PermissionManager;
use RoleManager;
use RoleWisePermissionManager;
//use ACL;

class RoleWisePermissionController extends Controller
{
    
    public function AssignPermission()
    {
        //if(ACL::has('assignpermission')){

            $permissionManager = new PermissionManager();
            $permissions = $permissionManager->GetAllPermissions();
            $roleManager = new RoleManager();
            $roles = $roleManager->GetAllRoles();
            //$roles = Role::all();
        return view('RoleWisePermission.AssignPermission',compact('permissions','roles'));
        //     }
        // else{
        //         return view('Admin.404');
        //     }
    }


    public function GetRoleWisePermissions($roleid)
    {    
        
        $permissionManager = new PermissionManager();
        $permissionDatas = $permissionManager->GetAllPermissions();

        //$rolePermissionUclManager = new RolePermissionUclManager();
        $roleWisePermissionManager = new RoleWisePermissionManager();
        $roleWisePermissions = $roleWisePermissionManager->GetRolePermissionByRoleId($roleid);

        return view('RoleWisePermission.GetRoleWisePermissions', compact('permissionDatas','roleWisePermissions'));
    }


    function array_find($findValue, array $array)
    {
        foreach ($array as $key => $value) {
            if ($value === $findValue) {
               return $key;
            }
        }
        return -1;
    }


    public function UpdateRolePermission(Request $request)
    {
        //return $request::all();
        $roleId=$request->input('roleid');
        $permissions = $request->input('permissions');
        $roleWisePermissionManager = new RoleWisePermissionManager();
        $roleWisePermission = $roleWisePermissionManager->GetRolePermissionByRoleId($roleId);
        $roleWisePermissionManager->DeleteRoleWisePermissionByRoleId($roleWisePermission, $roleId);
        $roleWisePermissionManager->AddRoleWisePermission($permissions,$roleId);

        return redirect()->back();
    }



    public function GetAllPermissionsByRoleId($id)
    {
        $roleWisePermissionManager = new RoleWisePermissionManager();
        $rolePermission = $roleWisePermissionManager->GetRolePermissionByRoleId($id);
        return view('RoleWisePermission.GetAllPermissionsByRoleId',compact('rolePermission'));
    }


}

