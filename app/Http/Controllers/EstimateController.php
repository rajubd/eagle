<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;

use App\Models\Sales_account;

use App\Models\Currency;

use App\Models\Payment;
use App\Models\Invoice;
use App\Models\Bill;
use App\Models\Vendor;
use App\Models\Customer;
use App\Models\Item;
use App\Models\Estimate;
use App\Models\Estimate_item;
use DB;
use Auth;

class EstimateController extends Controller
{
public function __construct()
{
    //$this->middleware('auth');
}



public function index()
{
    $estimates = Estimate::all();
    return view('eagle.estimates.estimates',compact('estimates'));
}

public function create()
{
    //$accounts = Sales_account::all();
    $items = Item::all();
    $customers = Customer::all();
    $taxes = Tax::all();
  return view('eagle.estimates.createestimate',compact('items','taxes','customers'));
}


public function estimatedetails()
{
     $estimates = Estimate::all();
	   return view('eagle.estimates.estimatedetails',compact('estimates'));
}



public function store(Request $request)
{

   //return Input::all();
   $items = Input::get('item');
   
   $qty = Input::get('quantity');
   $rate = Input::get('rate');
   $amount = Input::get('amount');
   $tax = Input::get('tax_id');



    $estimate = new Estimate();

    $estimate->customer_id = Input::get('customer_id');
    $estimate->estimate_no = Input::get('estimate_number');
    $estimate->reference_no = Input::get('reference_no');
    $estimate->estimate_date = Input::get('estimate_date');
    $estimate->estimate_expiry_date = Input::get('estimate_expiry_date');
    $estimate->sales_person_id = Input::get('sales_person_id');
    
    $estimate->status = 'Draft';
    $estimate->estimate_amount = Input::get('total');
    $estimate->due_amount = Input::get('total');
    $estimate->created_by = Auth::id();

    $estimate->save();



    $estimate_id = $estimate->id;
   
    // $bill_id = $bill->id;

     $limit = count($items)-1 ;

    // //return $limit;
     for ($i=0; $i < $limit; $i++) { 

        $est_item = new Estimate_item();

        $est_item->estimate_id = $estimate_id;;

        $est_item->item_id = $items[$i];
       
        
        $est_item->qty = $qty[$i];
        $est_item->rate = $rate[$i];
        $est_item->amount = $amount[$i];
        $est_item->tax_id = $tax[$i];

        $est_item->save();

     }
  //return $bill_ac;


   



    return redirect('estimates')->with('success','New Estimate Stored Successfully');
  

}






















public function loadestimatedata1()
{
    $estimate = Estimate::first();
    $est_items = Estimate_item::where('estimate_id',$estimate->id)->get();
    return view('eagle.estimates.loadestimatedata',compact('estimate','est_items'));
}


public function loadestimatedata($id)
{
    $estimate = Estimate::find($id);
    $est_items = Estimate_item::where('estimate_id',$estimate->id)->get();
    return view('eagle.estimates.loadestimatedata',compact('estimate','est_items'));
}










}
