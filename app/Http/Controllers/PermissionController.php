<?php

namespace App\Http\Controllers;


use AppInfoManager;
use RoleManager;
use PermissionManager;
use CompanyManager;
use UserManager;


use UserDataUclManager;
use UserRoleUclManager;

use UserRoleManager;
use UserLimitManager;

use RoleWisePermissionManager;
use RolePermissionUclManager;


use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use App\Models\Permission;
use App\Models\RoleWisePermission;
use App\Models\Role;
use App\Models\User_Role;
use ACL;






class PermissionController extends Controller
{
    

    /*   Permission Section   */

    public function ViewAllPermissions()
    {
      //  if(ACL::has('viewpermission')){
            $permissionManager = new PermissionManager();
            $permissions = $permissionManager->GetAllPermissions();
        return view('Permission.ViewAllPermissions',compact('permissions'));
        // }
        // else{
        //     return view('Admin.404');
        // }
    }



    public function AddPermission()
    {
        // if(ACL::has('addpermission'))
        // {
            return view('Permission.AddPermission');
        // }
        // else{
        //         return view('Admin.404');
        //     }    
    }





    public function StorePermission(Request $request)
    {
    $v = Validator::make($request->all(), [
            
                'name' => 'unique:permissions|required|max:255',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }

        $permission = new Permission();
        $permission->name = $request->input('name');
        $permission->save();
        return redirect('permissions')->with('success','Permission Created Successfully');
    }




    public function EditPermission($id)
    {
        //if(ACL::has('editpermission')){
            $permissionManager = new PermissionManager();
            $permission = $permissionManager->GetPermissionById($id);
            return view('Permission.EditPermission',compact('permission'));
        // }
        // else{
        //     return view('Admin.404');
        // }
    }







    public function UpdatePermission(Request $request, $id)
    {

        $permission = array(
            'name' => $request->input('name'),
             );
        $permissionManager = new PermissionManager();
        $permissionManager->UpdatePermission($permission,$id);
        return redirect('permissions')->with('success','Permission updated Successfully');

       
    }


    public function DeletePermission($id)
    {
        //if(ACL::has('deletepermission')){
            $permissionManager = new PermissionManager();
            $permissionManager->DeletePermission($id);
            return redirect('permissions')->with('success','Permission Removed Successfully');
        // }
        // else
        // {
        //     return view('Admin.404');
        // }
    }






}
