<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use App\Models\Level;
use App\Models\Payroll;
use App\Models\Employee;
use DB;

class PayrollController extends Controller
{
   

    public function index()
    {
        $levels = Payroll::all();
        $employees = Employee::all();
        $payrolls = DB::table('payrolls')->distinct('payroll_month')->get();
       // echo "<pre>";
//print_r($levels);exit;
        return view('eagle.payrolls.payrolls',compact('levels','employees','payrolls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $employees = Employee::all();
        return view('eagle.payrolls.createPayroll',compact('employees'));
    }

    public function add(Request $request)
    {
        //return $request->all();
        $payrolls = Payroll::where('payroll_month',$request->payroll_month)->first();
if(!empty($payrolls))
{
    //return $payrolls;
    return redirect('/payrolls')->with('success','Payroll Already created for this month');


}
else{
      $employees = Employee::all();
        //return $employees;
        $data = array();
        foreach ($employees as $employee){
            $payroll = new Payroll();

            $payroll->employee_id = $employee->id;
            $payroll->payroll_month = $request->payroll_month;


             $payroll->save();
        }
        
        return redirect('/payrolls')->with('success','Payroll Recorded Successfully');
}
      
    }


    public function add2($value)
    {
        //return $request->all();

        $employees = Employee::all();
        //return $employees;
        $data = array();
        foreach ($employees as $employee){
            $payroll = new Payroll();

            $payroll->employee_id = $employee->id;
            $payroll->payroll_month = $value;


             $payroll->save();
             $data[] = $payroll;
        }
        return $data;
        //return redirect('/payrolls')->with('success','Payroll Recorded Successfully');
    }



    public function store(Request $request)
    {
       $payroll = new Payroll();
        $payroll->payroll_month = $request->paroll_month;
        $payroll->employee_id = $request->employee_id ;
        $payroll->salaried_qty = $request->salaried_qty ;
        $payroll->ot_hour = $request->ot_hour ;
        $payroll->estimated_salary = $request->estimated_salary ;
        $payroll->estimated_ot_bill = $request->estimated_ot_bill ;
        $payroll->paid_salary = $request->paid_salary ;
        $payroll->paid_ot_bill = $request->paid_ot_bill ;

        $payroll->save();

        return redirect('/payrolls')->with('success','Payroll Recorded Successfully');

        //return $request->all();
    }



    public function edit($id)
    {
        return $id;
        $payroll = Payroll::find($id);
        $employees = Employee::all();
        $emp = Employee::find($payroll->employee_id);
        return view('eagle.Payrolls.editpayroll',compact('payroll','employees','emp'));
    }



    //public function update(Request $request)
    public function update()
    {
        $data = Input::all();

        $payroll = Payroll::find(Input::get('id'));

        // if(!empty($request->paroll_month))
        // {
        //     $payroll->payroll_month = $request->payroll_month;
        // }

        //$payroll->employee_id = $request->employee_id ;
        //$payroll->salaried_qty = $request->salaried_qty ;
        $payroll->ot_hour = Input::get('ot_hour') ;
        $payroll->estimated_salary = Input::get('estimated_salary') ;
        $payroll->estimated_ot_bill = Input::get('estimated_ot_bill') ;
        $payroll->paid_salary = Input::get('paid_salary') ;
        $payroll->paid_ot_bill = Input::get('paid_ot_bill') ;

//return $payroll;
        $payroll->save();
        return $payroll;

        //return redirect('/payrolls')->with('success','Item Updated Successfully');
   }



    public function destroy($id)
    {
        $payroll = Payroll::find($id);
        $payroll->delete();
        return redirect('/payrolls')->with('success','Item Removed Successfully');
    }






    public function getemployeedata($id)
    {
        $employee = Employee::find($id);
        return $employee;
    }

    public function loadpayroll($value)
    {
        $payrolls = Payroll::where('payroll_month',$value)->get();
        return view('eagle.Payrolls.demo2',compact('payrolls'));
    }



    public function loadpayrolldata($value)
    {
        $payrolls = Payroll::where('payroll_month',$value)->get();
        //if (!empty($payrolls)) {
            //return $payrolls;
        // }
        // else{
        //     return redirect('createpayroll2')->with('data', $value);
            
        // }
        
        $ar = array();
        $ar['iTotalRecords'] = 6;
        $ar['iTotalDisplayRecords'] = 6;
        $ar['aaData'] = $payrolls;
        //echo "<pre>";
        $jsn = json_encode($ar, JSON_PRETTY_PRINT);
        return $jsn;
    }
}
