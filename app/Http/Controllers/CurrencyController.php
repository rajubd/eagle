<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;
use App\Models\Unit;
use App\Models\Sales_account;
use App\Models\Purchase_account;
use App\Models\Item;
use App\Models\Brand;
use App\Models\Journal;
use App\Models\Journal_account;
use App\Models\Currency;
use DB;
use Auth;

class CurrencyController extends Controller
{
      public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $currency = Currency::all();
        $journals = Journal::all();
        $accounts = Sales_account::all();
        return view('eagle.currency.currencyadjustments',compact('accounts','journals','currency'));
    }

    public function create()
    {
        $taxes = Tax::all();
        $accounts = Sales_account::all();
        return view('eagle.journals.createjournal',compact('accounts','taxes'));
    }


    public function store(Request $request)
    {
        //return Input::all();

        // DB::table('currency')->insert([
        //     'foreign_currency_code' => Input::get('foreign_currency_code'),
        //     'exchange_rate' => Input::get('exchange_rate'),
        //     'exchange_date' => Input::get('exchange_date'),
        //     'exchange_note' => Input::get('exchange_note'),
        //     'created_by' => Auth::User()->id
        // ]);

        $currency = new Currency();

        $currency->foreign_currency_code = Input::get('foreign_currency_code');
        $currency->exchange_rate = Input::get('exchange_rate');
        $currency->exchange_date = Input::get('exchange_date');
        $currency->currency_note = Input::get('currency_note');
        
        //return $currency;

        $currency->save();


       
    


        return redirect()->back()->with('success','New Currency Adjustment Recorded Successfully');
      

    }

    public function edit($id)
    {
        //return $id;
        $journal = Journal::find($id);
        $journal_accounts =  DB::table('journal_accounts')->where('journal_id', $id)
            ->join('sales_account','journal_accounts.account_id', '=', 'sales_account.id')
            ->select('journal_accounts.*', 'sales_account.sales_account_name')
            ->get();
        $taxes = Tax::all();
        $accounts = Sales_account::all();

        //return $journal_accounts;
        return view('eagle.journals.editjournal',compact('accounts','taxes','journal','journal_accounts'));
    }
     public function update($id)
     {
        return Input::all();
     }
}
