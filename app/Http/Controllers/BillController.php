<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;
use App\Models\Unit;
use App\Models\Sales_account;
use App\Models\Purchase_account;
use App\Models\Item;
use App\Models\Brand;
use App\Models\Journal;
use App\Models\Journal_account;
use App\Models\Currency;
use App\Models\Bill;
use App\Models\Vendor;
use App\Models\Bill_account;
use DB;
use Auth;

class BillController extends Controller
{
      public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $bills = Bill::all();
       
        return view('eagle.bills.bills',compact('bills'));
    }

    public function create()
    {
        $taxes = Tax::all();
        $vendors = Vendor::all();
        $accounts = Sales_account::all();
        return view('eagle.bills.createbill',compact('accounts','taxes','vendors'));
    }



    public function billdetails()
    {
         $bills = Bill::all();
         return view('eagle.bills.billdetails',compact('bills'));
    }

   

 public function loadbilldetails($id)
    {
        $bill = Bill::find($id);
        $a = Bill_account::where('bill_id',$bill->id)->get();
        //return $accounts;
        return view('eagle.bills.loadbilldetails',compact('bill','a'));
    }



    public function store(Request $request)
    {

      //return sizeof();
       $accounts = Input::get('acc_id');
       $des = Input::get('acc_description');
       $qty = Input::get('quantity');
       $rate = Input::get('rate');
       $amount = Input::get('amount');
       $tax = Input::get('tax_id');
//return $des[0];

        $bill = new Bill();
        $bill->bill_no = Input::get('bill_no');
        $bill->vendor_id = Input::get('vendor_id');
        $bill->order_no = Input::get('order_no');
        $bill->bill_date = Input::get('bill_date');
        $bill->due_date = Input::get('due_date');
        $bill->total = Input::get('total');
        $bill->amount_due = Input::get('total');
        $bill->bill_status = 'Open';


        $bill->save();
        $bill_id = $bill->id;

        $limit = count($accounts)-1 ;
        //return $limit;
        for ($i=0; $i < $limit; $i++) { 
              $bill_ac = new Bill_account();

              $bill_ac->acc_id = $accounts[$i];
              $bill_ac->bill_id = $bill_id;
              $bill_ac->acc_description = $des[$i];
              $bill_ac->quantity = $qty[$i];
              $bill_ac->rate = $rate[$i];
              $bill_ac->amount = $amount[$i];
              $bill_ac->tax_id = $tax[$i];
              $bill_ac->created_by = Auth::User()->id;
              $bill_ac->save();
              //return $bill_ac;

        }
      //return $bill_ac;


       
    


        return redirect('bills')->with('success','New Bill Stored Successfully');
      

    }

    public function edit($id)
    {
        //return $id;
        $journal = Journal::find($id);
        $journal_accounts =  DB::table('journal_accounts')->where('journal_id', $id)
            ->join('sales_account','journal_accounts.account_id', '=', 'sales_account.id')
            ->select('journal_accounts.*', 'sales_account.sales_account_name')
            ->get();
        $taxes = Tax::all();
        $accounts = Sales_account::all();

        //return $journal_accounts;
        return view('eagle.journals.editjournal',compact('accounts','taxes','journal','journal_accounts'));
    }
     public function update($id)
     {
        return Input::all();
     }
}
