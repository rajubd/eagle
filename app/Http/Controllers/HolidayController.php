<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Leave;
use App\Models\Employee;
use App\Models\Holiday;
use DB;


class HolidayController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }



    public function index()
    {
        $leaves = Holiday::all();
        // $leaves = DB::table('leaves')
        // ->leftjoin('employees','leaves.employee_id','=','employees.id')
        // ->select('leaves.*','employees.first_name','employees.last_name','employees.employee_code')
        // ->get();
        return view('eagle.holidays.holidays',compact('leaves'));
    }

    public function create()
    {
       //$employees = Employee::all();
        return view('eagle.holidays.createHoliday');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $leave = new Holiday();
        $leave->holiday_name = $request->holiday_name ;
        $leave->start_date = $request->start_date ;
        $leave->end_date = $request->end_date ;
        $leave->reason = $request->reason ;

        $leave->save();

        return redirect('/holidays')->with('success','Holiday Recorded Successfully');

        //return $request->all();
    }



    public function edit($id)
    {
        //$employees = Employee::all();
        $leave = Holiday::find($id);
       // $emp = Employee::find($leave->employee_id);

        return view('eagle.holidays.editHoliday',compact('leave'));
    }



    public function update(Request $request, $id)
    {
        $leave = Holiday::find($id);
        $leave->holiday_name = $request->holiday_name ;

        if(!empty($request->start_date))
        {
            $leave->start_date = $request->start_date ;
        }
        if(!empty($request->end_date))
        {
            $leave->start_date = $request->end_date ;
        }

        $leave->reason = $request->reason ;

        $leave->save();

        return redirect('/holidays')->with('success','Holiday record Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $leave = Holiday::find($id);
        $leave->delete();

        return redirect('/holidays')->with('success','Holiday record removed Successfully');
    }
}
