<?php

namespace App\Http\Controllers;

use UserDataUclManager;
use UserRoleUclManager;
use App\UCL\ViewModel\UserRoleCompanyLimitViewModel;
use UserRoleManager;
use RoleManager;
use UserLimitManager;
use UserManager;
use ACL;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

use ParsingManager;

use App\Models\Role;
use App\Models\User_Role;
use App\User;
use Auth;
use DB;

class UserController extends Controller
{
    public function index()
    {
        //$users = User::all();
        //return 
        $users = DB::table('users')
            ->leftjoin('role','users.role_id','=','role.id')
            ->select('users.*','role.roleName')
            ->get();
        return view('User.users',compact('users'));
    }

    public function CreateUser()
    {
        $roles = Role::all();

       
        if(ACL::has('createUser')){
               return view('User.CreateUser',compact('roles')); 
            }
            else{
                return view('Admin.404');
            }
    }


 
    public function StoreUser(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }
        


         $user = new User();


        if (Input::hasFile('photo'))
        {
           

            $image = Input::file('photo');
            $filename  = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('images/users/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $user->photo = $filename;
        }
                
                         

         
            $user->name = Input::get('name');
            $user->status = 1;
            
            $user->email = Input::get('email');
            $user->password = bcrypt(Input::get('password'));
            $user->role_id  = Input::get('role_id');
            $user->created_by = Auth::User()->id;

            $user->save();



            // $userRole = new User_Role();
            // $userRole->userid = $user->id;
            // $userRole->roleid = Input::get('role_id');

            // $userRole->save();
                   
                    

                return redirect('/users')->with('success','User Info Added Successfully.');
            //}
       
//return "fail";
      
    }



    public function ViewSingleUser($id)
    {

       $user = User::find($id);
       
       $role = Role::find($user->role_id);


        return view('User.ViewSingleUser',compact('user','role'));
    }


    public function EditSingleUser($id)
    {
        $user = User::find($id);
        $roles = Role::all();
       
        $thisRole = Role::find($user->role_id);
        return view('User.EditSingleUser',compact('user','thisRole','roles'));
    }



    public function UpdateSingleUser($id,Request $request)
    {
        //$userManager = new UserManager();
        $user = User::find($id);
           
       

        if (Input::file('photo')) {

            $image = Input::file('photo');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('images/users/' . $filename);
            Image::make($image->getRealPath())->save($path);
            \File::delete(public_path('images/users/' . $user->photo));
            //$existingDoctor->doctorImage = $filename;
            $user->photo = $filename;
        }

        $user->name  = Input::get('name');
        $user->email = Input::get('email');
        $user->role_id = Input::get('role_id');

        if(Input::get('password')){
           

            $user->password = bcrypt( Input::get('password'));
        }
        $user->save();

        return redirect('/users')->with('success','User Info Updated Successfully.');
    }


    public function RemoveSingleUser($id)
    {
        $user = User::find($id);
        //return $user;
        $user->delete();
        return redirect('users');
    }







    public function EditProfile()
    {
        $userManager = new UserManager();
        $user = $userManager->GetUserProfile();
        return view('User.Profile',compact('user'));
    }



    public function UpdateProfile(Request $request)
    {
        $parsingManager = new ParsingManager();
        $user = $parsingManager->SetProfile($request);

        $userManager = new UserManager();
        $userManager->UpdateUserProfile();

        return redirect('/');
    }



}
