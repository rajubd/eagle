<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;

use App\Models\Sales_account;

use App\Models\Currency;

use App\Models\Payment;
use App\Models\Payment_mode;
use App\Models\Bill;
use App\Models\Invoice;
use App\Models\Vendor;
use App\Models\Customer;
use App\Models\Payment_received;

use DB;
use Auth;

class PaymentController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $payments = Payment::all();
        return view('eagle.payment.payments',compact('payments'));
    }

    public function createpayment()
    {
        $accounts = Sales_account::all();
        $vendors = Vendor::all();
        $taxes = Tax::all();
      return view('eagle.payment.paymentmade',compact('accounts','taxes','vendors'));
    }

   
    public function paymentdetails()
    {
         $payments = Payment::all();
          $accounts = Sales_account::all();
        $vendors = Vendor::all();
        $taxes = Tax::all();
    	   return view('eagle.payment.paymentdetails',compact('payments','accounts','taxes','vendors'));
    }

   


    

    public function storepayment()
    {
        //return Input::all();
        //return "hy";
        $id = Input::get('vendor_id');
        $payment = Input::get('payment_amount');
        $bills = Bill::where('vendor_id',$id)->get();
        $limit=count($bills);
        $bill_all = '';
        $bill_ids = '';
        $am = array();
        $j=0;
        for ($i=0; $i < $limit; $i++) { 
          if($payment[$i]!='')
           {
               $bill_id = $bills[$i]->id;
               // print_r($bill_id);
               // echo "<br>";

               $mybill = Bill::find($bill_id);
               //return $mybill;
               $due = $mybill->amount_due - $payment[$i];
               $mybill->amount_due = $due;
               if($j>0)
               {
                  $bill_all.=",";
                  $bill_ids.=",";
               }

               $bill_all.= $mybill->bill_no;
               $bill_ids.= $bill_id;

               if($due > 0){
                    $mybill->bill_status = 'Partially Paid';
               }
               else{
                    $mybill->bill_status = 'Paid';
               }

               $mybill->save();
               $j++;
           }
       }

      // exit;
       //return $bill_all;

       $payment = new Payment();
       $payment->vendor_id = Input::get('vendor_id');
       $payment->amount = Input::get('full_amount');
       $payment->payment_date = Input::get('payment_date');
       $payment->reference = Input::get('reference');
       $payment->payment_mode = Input::get('payment_mode');
       $payment->paid_through = Input::get('paid_through');
       $payment->bills = $bill_all;
       $payment->bill_ids = $bill_ids;
       $payment->save();


       
        //return Input::all();
        return redirect('payments')->with('success','New Payment Recorded Successfully');
    }





    public function storepaymentmode()
    {
      if(ACL::has('storepaymentmode'))
      {
         $mode = new Payment_mode();
          $mode->mode = Input::get('payment_mode');
          $mode->created_by = Auth::user()->id;
          $mode->save();
          return redirect()->back()->with('success','New Payment Mode Added Successfully');
      }
    }






    public function payments_received()
    {
      $received = Payment_received::all();
      
      // $mode->mode = Input::get('payment_mode');
      // $mode->created_by = Auth::user()->id;
      // $mode->save();

      return view('eagle.payment.payments_received',compact('received'));
    }





      public function createpaymentsreceived()
      {
        //$received = new Payment_received();
        $customers = Customer::all();
        $modes = Payment_mode::all();
        return view('eagle.payment.createpaymentreceived',compact('customers','modes'));
      }




      public function storepaymentsreceived(Request $request)
      {
        //$received = new Payment_received();
       
    $payment_received = new Payment_received();

    $payment_received->customer_id = Input::get('customer_id');
    $payment_received->amount = Input::get('payment_made');
    $payment_received->payment_date = Input::get('payment_date');
    $payment_received->payment_mode = Input::get('payment_mode');
    $payment_received->reference = Input::get('reference_no');
    

    $payment_received->save();

    return redirect()->back()->with('sucess','New Payment received Added Successfully');
      }






       public function loadpaymentdata($id)
          {
              $payment = Payment::find($id);
              return view('eagle.payment.loadpaymentdata',compact('payment'));
          }



       public function loadvendorbills($id)
          {
              $bills = Bill::where('vendor_id',$id)->get();
              return view('eagle.payment.paymentmadetable',compact('bills'));
          }


       public function loadbilldata($id)
          {
              $bills = Bill::where('vendor_id',$id)->get();
              $total=0;
              foreach ($bills as $bill) {
                  $total += $bill->amount_due;
              }
              return $total;
              //return view('eagle.Payrolls.demo2',compact('bills'));
          }



       public function loadcustomerinvoice($id)
          {
              $invoices = Invoice::where('customer_id',$id)->get();
              return view('eagle.payment.customerinvoicetable',compact('invoices'));
          }


       public function loadinvoicedata($id)
          {
              $bills = Bill::where('vendor_id',$id)->get();
              $total=0;
              foreach ($bills as $bill) {
                  $total += $bill->amount_due;
              }
              return $total;
              //return view('eagle.Payrolls.demo2',compact('bills'));
          }




}
