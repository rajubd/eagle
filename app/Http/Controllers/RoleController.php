<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use RoleManager;
use UserRoleManager;
use RoleWisePermissionManager;
use App\User;
use Validator;
use Auth;
use App\Models\Permission;
use App\Models\RoleWisePermission;
use App\Models\Role;
use App\Models\User_Role;
use ACL;

class RoleController extends Controller
{
    private $roleManager;
    public function __construct()
    {
        $this->middleware('auth');
        //$this->$roleManager = new RoleManager();
    }
/*           Role                               */


    public function ViewRole()
    {
       // if(ACL::has('viewrole')){
            $roleManager = new RoleManager();
            //$roles = $this->$roleManager->GetAllRoles();
            $roles = $roleManager->GetAllRoles();
        return view('Role.ViewRole',compact('roles'));
        // }
        // else{
        //     return view('Admin.404');
        // }
    }





    public function CreateRole()
    {

        // if(ACL::has('createrole'))
        // {
           return view('Role.CreateRole');
        // }
        // else{
        //     return view('Admin.404');
        // }
    }


    public function StoreRole(Request $request)
    {
        $role = new Role();
        $role->roleName = $request->input('roleName');
        $role->save();
       

        return redirect('/roles');

    }




    public function EditRole($id)
    {
        //if(ACL::has('editrole')){
            $roleManager = new RoleManager();
            $role = $roleManager->GetRoleById($id);
            return view('Role.EditRole',compact('role'));
        // }
        // else{
        //     return view('Admin.404');
        // }
    }


 
    public function UpdateRole(Request $request, $id)
    {

        
        $roleManager = new RoleManager();
        $roleManager->UpdateRole($id);
        return redirect('roles');

    }

    public function DeleteRole($id)
    {
        if(ACL::has('deleterole')){
        $role = Role::find($id);
        $role->delete();

       

        $rolePermission = RoleWisePermission::where('role_id',$id)->get();
        if(!empty($rolePermission))
        {

            foreach ($rolePermission as $r) {
                              $r->delete();
                          }              
        }

         return redirect('roles');
        }else{
            return view('Admin.404');
        }
    }

                    
   /* public function ViewSingleRole($id)
    {
       

        $roleWisePermissionManager = new RoleWisePermissionManager();
        $rolePermission = $roleWisePermissionManager->GetRolePermissionByRoleId($id);
        return view('Admin.ViewSingleRole',compact('rolePermission'));
    }

*/



}
