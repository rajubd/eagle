<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;

class TaxController extends Controller
{
      public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        return view('eagle.items.items');
    }

    public function create()
    {
        return view('eagle.items.createitem');
    }

    public function store(Request $request)
    {
       $tax = new Tax;
       $tax->tax_name = Input::get('tax_name');
       $tax->tax_amount = Input::get('tax_amount');
       $tax->save();
       return redirect()->back()->with('success', 'New Tax Added Successfully');

       //return $tax;
        //return Input::all();
        //return view('eagle.items.items');
    }

    public function edit()
    {
        return view('eagle.items.items');
    }
}
