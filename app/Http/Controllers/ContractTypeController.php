<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContractType;
use App\Models\ContractBase;
use DB;
class ContractTypeController extends Controller
{
      public function index()
    {
    	$contractType = ContractType::all();

       /* $contractType = DB::table('contract_type')
            ->join('contract_base', 'contract_type.base_id', '=', 'contract_base.id')
            ->select('contract_type.*', 'contract_base.name')
            ->get();*/

            //return $contractType;

        return view('eagle.contract_type.contractType',compact('contractType'));
    }



    public function create()
    {
    	$contractBase = ContractBase::all();
        return view('eagle.contract_type.createContractType',compact('contractBase'));
    }


    public function store(Request $request)
    {
    	//return $request->all();
        $designation = new ContractType();
        $designation->typename = $request->typename;
       // $designation->base_id = $request->base_id;
 
        $designation->save();

        return redirect('/contracttypes')->with('success','New Contract Type Added Successfully');

    }

   

    public function edit($id)
    {
    	//$contractBase = ContractBase::all();
        $contract_type = ContractType::find($id);
        return view('eagle.contract_type.editContractType',compact('contract_type'));
    }

   

    public function update(Request $request, $id)
    {
        $contract_Type = ContractType::find($id);
        $contract_Type->typename = $request->typename;
        $contract_Type->base_id = $request->base_id;
     
        $contract_Type->save();

         return redirect('/contracttypes')->with('success','Contract Type Updated Successfully');
    }

   

    public function destroy($id)
    {
         $designation = ContractType::find($id);
         $designation->delete();
          return redirect('/contracttypes')->with('success','Contract Type Deleted Successfully');
    }
}
