<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;
use App\Models\Unit;
use App\Models\Sales_account;
use App\Models\Purchase_account;
use App\Models\Item;
use App\Models\Brand;
use App\Models\Vendor;

class ItemController extends Controller
{
      public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $items = Item::all();
        return view('eagle.items.items',compact('items'));
    }

    public function create()
    {
        $taxes = Tax::all();
        $units = Unit::all();
        $vendors = Vendor::all();
        $brands = Brand::all();
        $sales_accounts = Sales_account::all();
        $purchase_accounts = purchase_account::all();
        
        return view('eagle.items.createitem2',compact('taxes','units','sales_accounts','purchase_accounts','brands','vendors'));
    }

    public function store(Request $request)
    {
        //return $request->brand_name;
        if(!empty($request->brand_name))
        {
            $brand = Brand::where('brand_name',$request->brand_name)->first();
            if(empty($brand))
            {
                $newBrand = new Brand();
                $newBrand->brand_name = $request->brand_name;

                $newBrand->save();
            }
        }
        $item = new Item();

        if (Input::file('item_image')) {

            $image = Input::file('item_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('images/item_images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            //\File::delete(public_path('public/uploads/userPhoto/' . $user->photo));
            //$existingDoctor->doctorImage = $filename;
            $item->item_image = $filename;
        }
       //return "hy";
        //return Input::all();


        

        $item->item_name = $request->item_name;
        $item->sku = $request->sku;
        $item->unit = $request->unit;
        $item->manufacturer = $request->manufacturer;
        $item->upc = $request->upc;
        $item->ean = $request->ean;
        $item->brand = $request->brand_name;
        $item->mpn = $request->mpn;
        $item->isbn = $request->isbn;
        $item->selling_price = $request->selling_price;
        $item->selling_account = $request->selling_account;
        $item->selling_description = $request->selling_description;
        $item->tax = $request->tax;
        $item->purchase_price = $request->purchase_price;
        $item->purchase_account = $request->purchase_account;
        $item->purchase_description = $request->purchase_description;
        $item->preferred_vendor = $request->preferred_vendor;
        $item->tracking_account = $request->tracking_account;
        $item->reorder_level = $request->reorder_level;
        $item->opening_stock = $request->opening_stock;
        $item->opening_stock_value = $request->opening_stock_value;

        $item->save();

        return redirect('items')->with('success','New Item Added Successfully');
      
        //return $item->item_name;
        //return view('eagle.items.items');
    }

    public function edit()
    {
        return view('eagle.items.items');
    }
}
