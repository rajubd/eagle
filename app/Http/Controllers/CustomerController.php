<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Currency;
use DB;

class CustomerController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $customers = Customer::all();
        return view('eagle.customers.customers',compact('customers'));
    }

    public function create()
    {
        /*$departments = Department::all();
        $designations = Designation::all();
        $levels = Level::all();
        $contractTypes = ContractType::all();
        $contractBases = ContractBase::all();*/

        return view('eagle.customers.createCustomer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $customer->salutation  =  $request->salutation ;
        $customer->customer_first_name  =  $request->customer_first_name ;
        $customer->customer_last_name  =  $request->customer_last_name ;
        $customer->company_name  =  $request->company_name ;
        $customer->customer_email  =  $request->customer_email ;
        $customer->customer_mobile  =  $request->customer_mobile ;
        $customer->customer_designation  =  $request->customer_designation ;
        $customer->customer_department  =  $request->customer_department ;
        $customer->customer_website  =  $request->customer_website ;
        $customer->customer_currency  =  $request->customer_currency ;
        $customer->customer_payment_terms_id  =  $request->customer_payment_terms_id ;
        $customer->customer_portal_language  =  $request->customer_portal_language ;
        $customer->customer_attention  =  $request->customer_attention ;
        $customer->customer_address  =  $request->customer_address ;
        $customer->customer_city  =  $request->customer_city ;
        $customer->customer_state  =  $request->customer_state ;
        $customer->customer_zip_code  =  $request->customer_zip_code ;
        $customer->customer_country  =  $request->customer_country ;
        $customer->customer_fax  =  $request->customer_fax ;
        $customer->remarks  =  $request->remarks ;

        $customer->save();


        return redirect('/customers')->with('success','Customer Added Successfully');

        //return $request->all();
    }



    public function edit($id)
    {
        $customer = Customer::find($id);
        $currency = Currency::all();

        return view('eagle.customers.editCustomer',compact('customer','currency'));
    }



    public function update(Request $request, $id)
    {
       $customer = Customer::find($id);
        $customer->salutation  =  $request->salutation ;
        $customer->customer_first_name  =  $request->customer_first_name ;
        $customer->customer_last_name  =  $request->customer_last_name ;
        $customer->company_name  =  $request->company_name ;
        $customer->customer_email  =  $request->customer_email ;
        $customer->customer_mobile  =  $request->customer_mobile ;
        $customer->customer_designation  =  $request->customer_designation ;
        $customer->customer_department  =  $request->customer_department ;
        $customer->customer_website  =  $request->customer_website ;
        $customer->customer_currency  =  $request->customer_currency ;
        $customer->customer_payment_terms_id  =  $request->customer_payment_terms_id ;
        $customer->customer_portal_language  =  $request->customer_portal_language ;
        $customer->customer_attention  =  $request->customer_attention ;
        $customer->customer_address  =  $request->customer_address ;
        $customer->customer_city  =  $request->customer_city ;
        $customer->customer_state  =  $request->customer_state ;
        $customer->customer_zip_code  =  $request->customer_zip_code ;
        $customer->customer_country  =  $request->customer_country ;
        $customer->customer_fax  =  $request->customer_fax ;
        $customer->remarks  =  $request->remarks ;

        $customer->save();


        return redirect('/customers')->with('success','Customer Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Employee = Customer::find($id);
        //\File::delete(public_path('images/employees/' . $Employee->photo));

        $Employee->delete();

        return redirect('/customers')->with('success','Customer removed Successfully');
    }





    public function loadcustomeraddress($id)
    {
        $customer = Customer::find($id);
        $cur = Currency::find($customer->customer_currency);
        return view('eagle/invoices/loadcustomerdata',compact('customer','cur'));
    }










}




