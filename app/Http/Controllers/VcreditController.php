<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;
use App\Models\Unit;
use App\Models\Sales_account;
use App\Models\Purchase_account;

use App\Models\Currency;
use App\Models\Bill;
use App\Models\Vendor;
use App\Models\Vcredit;
use App\Models\Vcredit_account;
use App\Models\Bill_account;
//use App\Models\Vcredit;
use DB;
use Auth;

class VcreditController extends Controller
{
      public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        $vcredits_obj = Vcredit::all();
        // $vcredits_obj = DB::table('vcredits')
        //             ->leftJoin('vendors', 'vcredits.vendor_id', '=', 'vendors.id')
        //             ->select('vcredits.*', 'vendors.vendor_first_name')
        //             ->get();
         $vcredits =  $vcredits_obj;
        return view('eagle.vendor_credits.vcredits',compact('vcredits'));
    }

    public function create()
    {
        $vendors = Vendor::all();
        $taxes = Tax::all();
        $accounts = Sales_account::all();
        return view('eagle.vendor_credits.createvcredit',compact('accounts','taxes','vendors'));
    }


    public function store(Request $request)
    {
      //return Input::all();

      //return sizeof();
       $accounts = Input::get('acc_id');
       $des = Input::get('acc_description');
       $qty = Input::get('quantity');
       $rate = Input::get('rate');
       $amount = Input::get('amount');
       $tax = Input::get('tax_id');

//return $des[0];
       $vcredit = new Vcredit();
       
       $vcredit->vendor_id = Input::get('vendor_id');
       $vcredit->order_no = Input::get('order_no');
       $vcredit->credit_note = Input::get('credit_note');
       $vcredit->vcredit_date = Input::get('vcredit_date');
       $vcredit->total = Input::get('total');
       


        $vcredit->save();
        $id = $vcredit->id;



        $limit = count($accounts)-1 ;
        //return $limit;
        for ($i=0; $i < $limit; $i++) { 
              $vcredit_account = new Vcredit_account();

              $vcredit_account->acc_id = $accounts[$i];
              $vcredit_account->vcredit_id = $id;
              $vcredit_account->acc_description = $des[$i];
              $vcredit_account->quantity = $qty[$i];
              $vcredit_account->rate = $rate[$i];
              $vcredit_account->amount = $amount[$i];
              $vcredit_account->tax_id = $tax[$i];
              $vcredit_account->created_by = Auth::User()->id;
              $vcredit_account->save();
              //return $bill_ac;

        }
   

        return redirect('vendor_credits')->with('success','New Vendor Credit Stored Successfully');
      

    }


    public function vcreditdetails()
    {
         $vcredits = Vcredit::all();
         return view('eagle.vendor_credits.vcreditdetails',compact('vcredits'));
    }

   

 public function loadvcreditdata($id)
    {
        $vcredit = Vcredit::find($id);
        $a = Vcredit_account::where('vcredit_id',$vcredit->id)->get();
        return view('eagle.vendor_credits.loadvcreditdata',compact('vcredit','a'));
    }

 public function loadvcreditdata1()
    {
        $vcredit = Vcredit::first();
        $a = Vcredit_account::where('vcredit_id',$vcredit->id)->get();
        return view('eagle.vendor_credits.loadvcreditdata',compact('vcredit','a'));
    }



    


       
    


    public function edit($id)
    {
       
    }
     public function update($id)
     {
        return Input::all();
     }
}
