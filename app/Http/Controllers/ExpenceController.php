<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Models\Tax;

use App\Models\Sales_account;

use App\Models\Currency;

use App\Models\Expance;
use App\Models\Vendor;
use App\Models\Customer;

use DB;
use Auth;

class ExpenceController extends Controller
{
        public function __construct()
    {
        //$this->middleware('auth');
    }

  

    public function index()
    {
        //$expences = Expance::all();
        $expences = DB::table('expances')
        ->leftjoin('vendors','expances.vendor_id','=','vendors.id')
        ->leftjoin('customers','expances.customer_id','=','customers.id')
        ->leftjoin('sales_account','expances.account_id','=','sales_account.id')
        ->get();
        //return $expences;
        return view('eagle.expences.expences',compact('expences'));
    }
    public function create()
    {
        $accounts = Sales_account::all();
        $customers = Customer::all();
        $vendors = Vendor::all();
        $currencies = Currency::all();
        $taxes = Tax::all();
    	return view('eagle.expences.createexpence',compact('accounts','taxes','customers','vendors','currencies'));
    }

    public function storeexpense()
    {
        //return "hy";

        //return Input::file('expanse_image');
        $expance = new Expance();



        if (Input::file('expanse_image')) {
            //return Input::file('expanse_image');

            $image = Input::file('expanse_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            //return $filename;
            $path = public_path('images/expanse_images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            //\File::delete(public_path('public/uploads/userPhoto/' . $user->photo));
            //$existingDoctor->doctorImage = $filename;
            $expance->expanse_image = $filename;
        }



        $expance->expence_date = Input::get('expence_date');
        $expance->account_id = Input::get('account_id');
        $expance->currency_id = Input::get('currency_id');
        $expance->amount = Input::get('amount');
        $expance->tax_id = Input::get('tax_id');
        $expance->paid_through = Input::get('paid_through');
        $expance->vendor_id = Input::get('vendor_id');
        $expance->reference = Input::get('reference');
        $expance->note = Input::get('note');
        $expance->created_by = Auth::user()->id;
        $expance->save();

        //return Input::all();
        return redirect('expenses')->with('success','New Expance Stored Successfully');
    }



     public function storemilage()
    {
        $expance = new Expance();



        if (Input::file('milage_image')) {

            $image = Input::file('milage_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('images/milage_images/' . $filename);
            Image::make($image->getRealPath())->save($path);
            //\File::delete(public_path('public/uploads/userPhoto/' . $user->photo));
            //$existingDoctor->doctorImage = $filename;
            $expance->expanse_image = $filename;
        }



        $expance->expence_date = Input::get('milage_date');
        $expance->claimant_id = Input::get('claimant_id');
        $expance->milage_calculate = Input::get('milage_calculate');
        $expance->distance = Input::get('distance');
        $expance->currency_id = Input::get('currency_id');
        $expance->amount = Input::get('amount');
        $expance->tax_id = Input::get('tax_id');
        $expance->paid_through = Input::get('paid_through');
        $expance->vendor_id = Input::get('vendor_id');
        $expance->reference = Input::get('reference');
        $expance->note = Input::get('note');
        $expance->created_by = Auth::user()->id;
        $expance->save();

        //return Input::all();
        return redirect('expenses')->with('success','New Expance Stored Successfully');
    }

    public function storebulkexpense()
    {
        //return Input::all();
        //return "hy";
        $limit = count(Input::get('account_id'))-1;
       // return $limit;
        $date = Input::get('expanse_date');
        $acc_id = Input::get('account_id');
        $currency_id = Input::get('currency_id');
        $amount = Input::get('amount');
        $tax_id = Input::get('tax_id');
        $paid_through = Input::get('paid_through');
        $vendor = Input::get('vendor_id');
        $ref = Input::get('reference');
        $note = Input::get('note');

        for ($i=0; $i < $limit; $i++) { 
                
            $expance = new Expance();

            
            $expance->expence_date = $date[$i];
            $expance->account_id = $acc_id[$i];
            $expance->currency_id = $currency_id[$i];
            $expance->amount = $amount[$i];
            $expance->tax_id = $tax_id[$i];
            $expance->paid_through = $paid_through[$i];
            $expance->vendor_id = $vendor[$i];
            $expance->reference = $ref[$i];
            $expance->note = $note[$i];
            $expance->created_by = Auth::user()->id;
            $expance->save();

        }
        //return $date;

       
        //return Input::all();
        return redirect('expenses')->with('success','New Expances Stored Successfully');
    }






}
