<?php

namespace App\Http;
use DB;
use Auth;
class ACL  
{
        // Has Permission function

	public static function has($accessName){
        //return "hi";
        $userJoinAlls = DB::table('users')
        ->leftJoin('user_role','users.id','=','user_role.userid')
        ->leftJoin('role','user_role.roleid','=','role.id')
        ->leftJoin('role_wise_permission','role.id','=','role_wise_permission.role_id')
        ->leftJoin('permissions','role_wise_permission.permission_id','=','permissions.id')
        ->orderBy('users.id','asc')
        ->where('users.id',Auth::user()->id)
        ->select('permissions.name')
        ->get();
        //return $userJoinAlls;
        $errFlag=0;
        foreach($userJoinAlls as $val){
        	if($val->name==$accessName){
        		return true;
        	}
                else{
        		$errFlag=1;
        		continue;
        	}
        }
        if($errFlag==1)
        	return false;
	}
}
