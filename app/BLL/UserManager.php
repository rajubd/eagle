<?php

namespace App\BLL;
use Illuminate\Http\Request;
use Auth;
use App\Models\Role;
use ACL;
use UserGateway;
use ParsingManager;

use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
class UserManager  
{



	public function GetAllUsers()
	{ 
		$userGateway = new UserGateway();
		return $userGateway->GetAllUsers();
	}

  
	public function GetUserByName($name){ 
		$userGateway = new UserGateway();
		return $userGateway->GetUserByName($name);
	}

    public function GetUserById($id){ 
        $userGateway = new UserGateway();
        return $userGateway->GetUserById($id);
    }

	public function GetUserProfile(){ 
		$userGateway = new UserGateway();
		return $userGateway->GetUserProfile();
	}

    public function GetUserByEmail($user){ 
        $userGateway = new UserGateway();
        return $userGateway->GetUserByEmail($user);
    }


    public function GetLastUserId(){ 
        $userGateway = new UserGateway();
        return $userGateway->GetLastUserId();
    }
    

    public function UpdateUser($user)
    {
       // $user = new User();

        if (Input::file('photo')) {

            $image = Input::file('photo');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('images/users/' . $filename);
            Image::make($image->getRealPath())->save($path);
            \File::delete(public_path('images/users/' . $user->photo));
            //$existingDoctor->doctorImage = $filename;
            $user->photo = $filename;
        }

        $user->name  = Input::get('name');
        $user->email = Input::get('email');

        if(Input::get('password')){
            /* $data = array(
            'name'  => $input->name,
            'email' => $input->email,
            'password' => bcrypt($input->password),
             );*/
            $user->password = bcrypt( Input::get('password'));
        }

        $userGateway = new UserGateway();
        return $userGateway->UpdateUser($user);
    }



    public function UpdateUserProfile(){
        //return $user;
        $userGateway = new UserGateway();
        /*$parsingManager = new ParsingManager();
        $user = $parsingManager->SetProfile($request);*/
        $user = $this->GetUserById(Auth::User()->id);

        if (Input::file('photo')) {

            $image = Input::file('photo');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('images/users/' . $filename);
            Image::make($image->getRealPath())->save($path);
            \File::delete(public_path('images/users/' . $user->photo));
            //$existingDoctor->doctorImage = $filename;
            $user->photo = $filename;
        }

        $user->name  = Input::get('name');
        $user->email = Input::get('email');

        if(Input::get('password')){
            /* $data = array(
            'name'  => $input->name,
            'email' => $input->email,
            'password' => bcrypt($input->password),
             );*/
            $user->password = bcrypt( Input::get('password'));
        }
        
        
        return $userGateway->UpdateUserProfile($user);
    }


    public function InsertUser($user)
    {
        $userGateway = new UserGateway();
        return $userGateway->InsertUser($user);
    }












/*
	public function CreateUser($user)
    {
    	$userGateway = new UserGateway();
        return $userGateway->CreateUser($user);
		
    }

    public function UpdateUser($user,$id)
    {
        $userGateway = new UserGateway();
        $existingUser = $this->GetUserById($id);
        return $userGateway->UpdateUser($existingUser,$user);
    }

    public function DeleteUser($id)
    {
        $userGateway = new UserGateway();
        $user = $this->GetUserById($id);
        return $userGateway->DeleteRole($user,$id);
    }   
*/
  

}
