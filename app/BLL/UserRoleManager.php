<?php

namespace App\BLL;
use Illuminate\Http\Request;

use App\User;
use App\Models\User_Role;
use App\Models\Role;
use ACL;
use Auth;

use UserRoleGateway;
class UserRoleManager  
{

   /* public function SetUserRole($input, $id)
    {
        //return $input;
        $userRoleGateway = new UserRoleGateway;
        return $userRoleGateway->SetUserRole($input, $id);
    }*/
    
    public function GetUserRoleByUserId($id){ 
        $userRoleGateway = new UserRoleGateway;
        return $userRoleGateway->GetUserRoleByUserId($id);
    }

        public function GetUserRoleByRoleId($id){ 
    	$userRoleGateway = new UserRoleGateway;
    	return $userRoleGateway->GetUserRoleByUserId($id);
    }
   

    public function InsertUserRole($userRole)
    {
    	$userRoleGateway = new UserRoleGateway;
    	return $userRoleGateway->InsertUserRole($userRole);
    }

    public function UpdateUserRole($userRole, $id)
    {
        $userRoleGateway = new UserRoleGateway;
    	return $userRoleGateway->UpdateUserRole($userRole, $id);
    }

    public function RemoveUserRole($id)
    {
        $userRole = $this->GetUserRoleByUserId($id);
        $userRoleGateway = new UserRoleGateway;
        return $userRoleGateway->RemoveUserRole($userRole);
    }

    public function DeleteUserRoleByRoleId($id)
    {
        $userRole = $this->GetUserRoleByRoleId($id);
        return $userRoleGateway->DeleteUserRoleByRoleId($id);
    }

}
