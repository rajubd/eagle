<?php

namespace App\BLL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Role;

use ACL;

use RoleGateway;
class RoleManager  
{

	public function GetAllRoles()
	{ 
		$roleGateway = new RoleGateway();
		return $roleGateway->GetAllRoles();
	}

	public function GetRoleByName($name){ 
		$roleGateway = new RoleGateway();
		return $roleGateway->GetRoleByName($name);
	}

	public function GetRoleById($id){ 
		$roleGateway = new RoleGateway();
		return $roleGateway->GetRoleById($id);
	}

	public function CreateRole($role)
    {
    	$roleGateway = new RoleGateway();
		$isRoleExist = $this->GetRoleByName($role);
		if(empty($isRoleExist))
		{
			$newRole = new Role();
			$newRole->roleName = $role->roleName;
		 	return $roleGateway->CreateRole($newRole);
		}
		return false;
    }

    public function UpdateRole($id)
    {
    	//return $role;
        $roleGateway = new RoleGateway();
        $existingRole = $this->GetRoleById($id);
        
        $existingRole->roleName = Input::get('name');
        return $roleGateway->UpdateRole($existingRole);
    }

    public function DeleteRole($id)
    {
        $roleGateway = new RoleGateway();
        $role = $this->GetRoleById($id);
        
        return $roleGateway->DeleteRole($role);
    }   



}
