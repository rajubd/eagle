<?php

namespace App\BLL;
use Illuminate\Http\Request;

use App\Models\Permission;

use ACL;

use PermissionGateway;
class PermissionManager  
{

	public function GetAllPermissions(){ 
		$permissionGateway = new PermissionGateway();
		return $permissionGateway->GetAllPermissions();
	}


	public function GetPermissionByName($data){ 
		$permissionGateway = new PermissionGateway();
		return $permissionGateway->GetPermissionByName($data);
	}


	public function GetPermissionById($id){ 
		$permissionGateway = new PermissionGateway();
		return $permissionGateway->GetPermissionById($id);
	}


	public function CreatePermission($data)
    {
 		$permissionGateway = new PermissionGateway();
		$isPermissionExist = $this->GetPermissionByName($data);
		if(empty($isPermissionExist))
		{
		 	return $permissionGateway->CreatePermission($data);
		}

		return false;
    }


    public function UpdatePermission($permission,$id)
    {
		$permissionGateway = new PermissionGateway();
        $currentPermission = $this->GetPermissionById($id);
        return $permissionGateway->UpdatePermission($currentPermission,$permission);
    }

/*
    public function EditPermission($id)
    {
        $PermissionGateway = new PermissionGateway();
        return $PermissionGateway->EditPermission($id);

    }
*/

    public function DeletePermission($id)
    {
		$permissionGateway = new PermissionGateway();
        $permission = $this->GetPermissionById($id);
        return $permissionGateway->DeletePermission($permission);
    }
   
   
   /* public function GetPermissionPermissionById($id)
    {
        $PermissionGateway = new PermissionGateway();
        return $PermissionGateway->GetPermissionPermissionById($id);
    }*/
   
   











/*
	public function CreateAppInfo($appInfo)
	{
		$appinfoGateway = new AppinfoGateway();
		return $appinfoGateway->CreateAppInfo($appInfo);
	}




	public function UpdateAppInfo($appInfo)
	{
		 $appinfoGateway = new AppinfoGateway();
		 $existingAppInfo = $this->GetAppInfo();
		 if(empty($existingAppInfo))
		 {
		 	//return $appInfo;
		 	return $this->CreateAppInfo($appInfo);
		 }
		 //return $appInfo;
		return $appinfoGateway->UpdateAppInfo($appInfo, $existingAppInfo);
	}


	
*/
}
