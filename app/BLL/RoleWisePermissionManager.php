<?php

namespace App\BLL;
use Illuminate\Http\Request;

use App\User;
use App\Models\User_Role;
use App\Models\Role;
use App\Models\RoleWisePermission;
use ACL;
use Auth;
use RoleWisePermissionGateway;

class RoleWisePermissionManager 
{

    public function GetRolePermissionByRoleId($id)
    { 
        $rolewisePermissionGateway = new RoleWisePermissionGateway();
        return $rolewisePermissionGateway->GetRolePermissionByRoleId($id);
    }



    public function DeleteRoleWisePermissionByRoleId($roleWisePermission,$roleid)
    { 
       $rolewisePermissionGateway = new RoleWisePermissionGateway();
        return $rolewisePermissionGateway->DeleteRoleWisePermissionByRoleId($roleWisePermission,$roleid);
    }

    public function AddRoleWisePermission($permissions,$roleid)
    { 
        if (!$permissions) {
            return;
        }
        $rolewisePermissionGateway = new RoleWisePermissionGateway();
        return $rolewisePermissionGateway->AddRoleWisePermission($permissions,$roleid);
        
    }

}
