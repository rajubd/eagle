@extends('layouts.main')

@section('content')
<div class="">
    <div class="row"  style="margin-top: 100px">
     <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="header"><label>Role wise Permission</label>
                
                </div>
                <div class="body">
                   <form class="form-horizontal" role="form" method="POST" action="{{ url('/updaterolepermission') }}">
                        {{ csrf_field() }}

                        

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Select Role</label>

                            <div class="col-md-6">
                               <select name="roleid" class="form-control chosen-select" style="width: 100%" onchange="roleWisePermission(this.value)">
                                   <option value="-1"> - Select - </option>
                                   @foreach($roles as $role)
                                   <option value="{{$role->id}}" >{{$role->roleName}}</option>
                                   @endforeach 
                               </select> 
                            </div>
                        </div>

                        <div class="form-group">
                          <label for="name" class="col-md-4 control-label"></label>

                            <div class="col-md-6">
                               <div id="roleWisePermission"></div>
                            </div>
                        </div>
                        
                       
<br>
                 
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input type="submit" id="update" class="btn btn-primary" value="Confirm">
                            </div>
                        </div>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

 <script src="{{ asset('public/js/jquery.min.js') }}" type="text/javascript"></script>
 <script src="{{ asset('public/js/chosen.jquery.js') }}" type="text/javascript"></script>


      
      <script type="text/javascript">
        function roleWisePermission(value){
          //alert(value);
          $("#roleWisePermission").load('{{ URL::to('loadPermission')}}'+'/'+value);
          if(value === -1){
            $("#roleWisePermission").hide();
          }
          //alert(value);
        }
      </script>



@endsection
