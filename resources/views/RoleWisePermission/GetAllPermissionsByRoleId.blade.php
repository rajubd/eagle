@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row"  style="margin-top: 0">
        
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>View Role Permission</h2></div>

                <div class="panel-body">
                    <div class="row">
                        @foreach($rolePermission as $role)
                        <?php 
                        $permission = App\Models\Permission::find($role->permission_id);
                         ?>
                            <div class="col-md-6">
                            <span><?php print_r($permission["name"]) ?></span>
                            </div>
                        @endforeach
                    </div>
                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
