
@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="header">
            <div class="">
                
                <div class="align-right">
                    <a class="btn btn-warning"  href="{{url('/createcontracttype')}}">Add Contract Type</a>
                </div>
            </div>
        </div>
        <div class="body">
           
        
            <div class="row">
                 @include('eagle/contract_type/contractTypeTable')
            </div>
        
       
   
        </div>
    </div>
</div>
@endsection




