<div class="body">
    <!--DataTables example-->
            <div id="table-datatables">
              
              <div class="row">
                <div class="">
                  

                 
                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">


                    <thead>
                        <tr>
                            <th>Name</th>
                            <!-- <th>Base</th>
                            -->
                            <th>Action</th>
                           
                        </tr>
                    </thead>
                 
                 
                 
                    <tbody>
                    @foreach($contractType as $type)
                        <tr>
                            <td>{{ $type->typename }}</td>
                           <!--  <td>{{ $type->name }}</td> -->
                           
                            <td>
                                <a href="{{url('editcontracttype/'.$type->id)}}"><i class="more_edit"></i>Edit</a> | 
                                <a href="{{url('deletecontracttype/'.$type->id)}}"><i class=" material-icons"></i>Delete</a>
                            </td>
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>
                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
</div>