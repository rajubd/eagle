@extends('layouts.addrowlayout')

@section('content')
<style type="text/css">
  label{
    color: #000000;
  }
</style>

<script type="text/javascript">
    
  jQuery(document).ready(function() {
        var id = 0;
      jQuery("#addrow").click(function() {
        id++;           
        var row = jQuery('.samplerow tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id); 
        row.appendTo('#dynamicTable1');        
        return false;
    });        
        
  $('.remove').on("click", function() {
  $(this).parents("tr").remove();
});
});


  </script>

<div class="">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header">New Journal</div>
        <div class="body">
           <form class="form-horizontal" action="{{url('createjournal')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <div class="row clearfix">
                          <!-- <div class="col-md-12">
                             <label align="left">Mode of Adjustment : </label>
                               <input name="group1" type="radio" checked id="test1" />
                                      <label for="test1">Quantity Adjustment</label>
                               <input name="group1" type="radio" id="test2" />
                      <label for="test2">Quantity Adjustment</label>

                          </div> -->
                       </div>
                       <div class="row clearfix">
                         <div class="col-md-6">
                         

                    
                        <div class="col-md-3 form-control-label">
                           <label align="left">Date</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="date" name="journal_date" class="datepicker form-control">
                                </div>
                            </div>
                        </div>

 <div class="col-md-3 form-control-label">
                           <label align="left">Reference#</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="journal_reference" class="form-control">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3 form-control-label">
  

                           <label align="left">Note</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                 <div class="form-line">
                                    <textarea class="form-control" name="journal_note"></textarea>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-3 form-control-label">
                           <label align="left">Currency</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="input-group dropdown">
                            
                             <div class="form-line">
                                <select id="" class="form-control" name="journal_currency">
          <option value="" selected="">Select Currency</option>
          <option value="1" >BDT</option>
          <option value="2" >USD</option>
          <option value="3" >EUR</option>
            
        </select>
                             </div>
                             
                            </div>
                        </div>


                         </div>
                         <div class="col-md-6">
                                                  

                       

                         </div>

                       </div>

                       



      <table id="dynamicTable1"  class="table table-bordered table-hover">
        <thead>
            <th>Account</th>
            <th>Description</th>
            <th>Contact</th>
            <th>Tax</th>
            <th>Debit</th>
            <th>Credit</th>
        </thead>
      <tr id="0">
        <td>
        <select id="fid1" class="form-control" name="account_id[]">
         <option value="" selected="">Select Account</option>
          @foreach($accounts as $account)
          <option value="{{$account->id}}">{{$account->sales_account_name}}</option>
        @endforeach
            
        </select>
        </td>
        <td><input class="form-control"  type="text" id="fld2" name="description[]" /></td>
        <td><input class="form-control"  type="text" id="fld3" name="contact[]" /></td>
        <td>
            <select id="fid4" class="form-control" name="tax[]">
              <option value="" selected="">Select Tax</option>
              @foreach($taxes as $tax)
              <option value="{{$tax->id}}">{{$tax->tax_name}}</option>
            @endforeach
                
            </select>
        </td>
        <td><input class="form-control"  type="text" id="fld5" name="debit[]" /></td>
        <td><input class="form-control"  type="text" id="fld6" name="credit[]" /></td>
        
        
      </tr>
    </table>

  

  <table class="samplerow" style="display:none">
    <tr>
       
        <td>
        <select id="fid1" class="form-control" name="account_id[]">
          <option value="" selected="">Select Account</option>
            @foreach($accounts as $account)
          <option value="{{$account->id}}">{{$account->sales_account_name}}</option>
        @endforeach
        </select>
        </td>
        <td><input class="form-control"  type="text" id="fld2" name="description[]" /></td>
        <td><input class="form-control"  type="text" id="fld3" name="contact[]" /></td>
       <td>
            <select id="fid4" class="form-control" name="tax[]">
              <option value="" selected="">Select Tax</option>
              @foreach($taxes as $tax)
              <option value="{{$tax->id}}">{{$tax->tax_name}}</option>
            @endforeach
                
            </select>
        </td>
        <td><input class="form-control"  type="text" id="fld5" name="debit[]" /></td>
        <td><input class="form-control"  type="text" id="fld6" name="credit[]" /></td>
       <td>
       <a style="color:red; cursor: pointer" ><i class="remove  material-icons">delete</i></a>
       </td>
    </tr>
  </table>
      <div class="row ">

    <div class="body col-md-2">
      <a class="btn btn-primary btn-block" id="addrow"><i class=" large material-icons">add</i>Account</a>
      <!-- <input type="button" id="addrow" value="Add Row" class="btn btn-primary" /> -->
    </div>

  </div>


                     
<div class="row clearfix">
   <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-line" align="right">
                                    <input type="submit" id="" name="submit" class="btn btn-success btn-lg" >
                                </div>
                            </div>
                        </div>
</div>




            </form>
        </div>

      </div>
    </div>
  </div>
</div>


              




<script src="{{asset('js/script_invoice.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function(){
    //alert("document ready");

    $("input[id=test2]").click(function(event){
        $(".row2").hide();
    });
    $("input[id=test1]").click(function(event){
        $(".row2").show();
    });

   $("#test5").click(function () {
    if ($("#test5").is(":checked")) {
        $(".sal").show();
        //$(".tracker").show();
    }
    else {
        $(".sal").hide();
        //$(".tracker").hide();
    }
});
});
</script>




<script type="text/javascript">
  $(function() {
  $('.dropdown-menu a').click(function() {
    console.log($(this).attr('data-value'));
    $(this).closest('.dropdown').find('input.countrycode')
      .val( $(this).attr('data-value') );
  });
});
</script>




@endsection













