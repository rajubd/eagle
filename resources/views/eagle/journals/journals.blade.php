@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-10">
                    <div class="block-header">
                        <h2>Account Journals</h2>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{url('createjournal')}}"  class="btn btn-primary btn-block waves-effect m-r-20"><i class="small material-icons">add</i> New Journal</a>
                </div>
            </div>
        </div>
        <div class="body">
            <div id="table-datatables">
              <div class="row">
                <div class="col-md-12">
                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Reference Number</th>
                            <th>Note</th>
                            <th>Amount</th>
                           <!--  <th>Action</th> -->
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($journals as $journal)
                        <tr>
                            <td>{{ $journal->journal_date }}</td>
                            <td>{{ $journal->journal_reference }}</td>
                            <td>{{ $journal->journal_note }}</td>
                            <td></td>
                          <!--   <td>
                                
                                <a href="{{url('editjournal/'.$journal->id)}}"><i class="material-icons">border_color</i></a> 
                                <a href="{{url('deletejournal/'.$journal->id)}}"><i class=" material-icons">delete</i></a>
                            </td> -->
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>


                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');
    })
</script>

@endsection