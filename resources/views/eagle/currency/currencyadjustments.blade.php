@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-10">
                    <div class="block-header">
                        <h2>Currency Adjustments</h2>
                    </div>
                </div>
               
                <div class="col-md-2">
                    <button type="button" class="btn bg-indigo waves-effect" data-toggle="modal" data-target="#currencyadjustmentmodal"><i class="small material-icons">add</i>  New Adjustment</button>
                </div>



  <!-- Small Size -->
            <div class="modal fade" id="currencyadjustmentmodal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
  <form class="" name="myform" action="{{url('createcurrencyadjustment')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}

                        <div class="modal-header">
                            <h4 class="modal-title" id="smallModalLabel">Currency Adjustment</h4>
                        </div>
                        <div class="modal-body">
                        <div class="row clearfix">
                         <div class="col-md-8 col-md-offset-2">
                            
                           <div class="form-group">
                                <label for="">Currency</label>
                                <select id="changecurrency" data-live-search="true" class="form-control" name="foreign_currency_code">
                                  <option value="" selected="">Select Currency</option>
                                  <option value="USD">USD</option>
                                  <option value="EUR">EUR</option>
                                  
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="">Date of Adjustment</label>
                                <div class="form-line">
                                    
                                     <input type="text" id="date" name="exchange_date" class="datepicker form-control">
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="">Exchange Rate</label>
                                <div class="form-line">
                                     <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                        <div class="input-group-addon" id="currency"></div>
                                        <input type="text" name="exchange_rate" class="form-control" id="" placeholder="">
                                        <div class="input-group-addon">SSD</div>
                                      </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="">Note</label>
                                <div class="form-line">
                                        <textarea name="currency_note" class=" form-control"></textarea>
                                     
                                </div>
                              </div>

                            
                           
                           </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                        <hr>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>

 </form>

                    </div>

                </div>
            </div>


            </div>
        </div>
        <div class="body">
            <div id="table-datatables">
              <div class="row">
                <div class="col-md-12">
                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Currency</th>
                            <th>Exchange Rate (in SSD) </th>
                            <th>Gain Or Loss</th>
                            <th>Action</th>
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($currency as $journal)
                        <tr>
                            <td><?php print_r(substr($journal->exchange_date, 0, 10));?></td>
                            <td>{{ $journal->foreign_currency_code }}</td>
                            <td>{{ $journal->exchange_rate }}</td>
                            <td></td>
                            <td>
                                
                                <a href="{{url('editcurrency/'.$journal->id)}}"><i class="material-icons">border_color</i></a> 
                                <a href="{{url('deletecurrency/'.$journal->id)}}"><i class=" material-icons">delete</i></a>
                            </td>
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>


                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');
    })
</script>

<script type="text/javascript">
    $('#changecurrency').on('change',function () {
        var x = $("#changecurrency option").filter(":selected").text();
       // $("#currency").text() = x;
//$("#currency").text("hy") ;
document.getElementById("currency").innerHTML = "1 "+x+" = ";
    })
</script>

@endsection