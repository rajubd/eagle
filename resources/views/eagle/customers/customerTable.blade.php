
                 
                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                    <thead>
                        <tr>
                            <th>Name</th>
                            <!-- <th>Department</th> -->
                            <th>Country</th>
                            <th>Company</th>
                           <!--  <th>Designation</th> -->
                            <th>Currency</th>
                            <th>Address</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                 
                 
                    <tbody>
                    @foreach($customers as $customer)
                        <tr>
                           <td>
                             <a href="{{url('viewcustomer/'.$customer->id)}}">  {{$customer->salutation}} {{$customer->customer_first_name}} {{$customer->customer_last_name}}</a> 
                         </td>
                          <!--  <td>{{$customer->customer_department}}</td> -->
                           <td>{{$customer->customer_country}}</td>
                           <td>{{$customer->company_name}}</td>
                           <!-- <td>{{$customer->customer_designation}}</td> -->
                           <td>
                           <?php 
                            $currency = App\Models\Currency::find($customer->customer_currency);
                            ?>
                           {{$currency['foreign_currency_code']}}</td>
                           <td>{{$customer->customer_address}}</td>
                           <td>
                          
                           <a href="{{url('editcustomer/'.$customer->id)}}">Edit</a> | 
                           <a href="{{url('deletecustomer/'.$customer->id)}}">Delete</a>
                           </td>
                        </tr>
                    @endforeach
                    </tbody>
                  </table>
                