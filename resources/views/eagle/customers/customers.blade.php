@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
    	

        <div class="">
            <div class="card">
            <div class="body">
            <div class="row">
                <div class="col-md-10">
                    <h4>Customers</h4>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-warning btn-block" href="{{url('/createcustomer')}}">Add Customer</a>
                </div>
            </div>
                
                
            </div>
                <div class="body">
                    @include('../eagle/customers/customerTable')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
