@extends('layouts.main')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" >
                <div class="card">
                        <div class="header">
                            <span>Add New Employee</span>
                        </div>
                    
                        <div class="body">

                      <form action="{{url('editemployee/'.$employee->id)}}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <div class="row">

                            <div class="col-md-6">
                        <div class="col-md-4 form-control-label">
                           <label align="left">Employee Code</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="{{$employee->employee_code}}" required name="employee_code" class="form-control" >
                                </div>
                            </div>
                        </div>

                         <div class="col-md-4 form-control-label">
                           <label align="left">Joining Date</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="date" id="" name="joining_date" class="form-control datepicker" name="item_name">
                                </div>
                            </div>
                        </div>

 <div class="col-md-4 form-control-label">
                           <label align="left">OT Rate</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="{{$employee->ot_rate}}" name="ot_rate" class="form-control" name="item_name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 form-control-label">
                           <label align="left">First Name*</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="{{$employee->first_name}}" name="first_name" class="form-control" name="item_name">
                                </div>
                            </div>
                        </div>
                        
                         <div class="col-md-4 form-control-label">
                           <label align="left">Last Name*</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="{{$employee->last_name}}" name="last_name" class="form-control" name="item_name">
                                </div>
                            </div>
                        </div>
 <div class="col-md-4 form-control-label">
                           <label align="left">Salary</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="{{$employee->salary}}" name="salary" class="form-control" name="item_name">
                                </div>
                            </div>
                        </div>

                      
                            </div>

                            <div class="col-md-6">
                                 <div class="col-md-3 form-control-label">
                                    <label align="left">Department</label>
                                    </div>
                                    <div class="col-md-9 ">
                                        <div class="form-group">
                                            <div class="">
                                                
                                                 <select id="editable-select1" data-live-search="true" class="form-control"  name="department_id">
                                                  <option value="{{$employee->department_id}}"  selected disabled>{{$employee->department_id}}</option>
                                                    @foreach($departments as $type)
                                                        <option value="{{ $type->id }}">{{ $type->department_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-control-label">
                                    <label align="left">Level</label>
                                    </div>
                                    <div class="col-md-9 ">
                                        <div class="form-group">
                                            <div class="">
                                                
                                                 <select id="editable-select1" data-live-search="true" class="form-control"  name="level_id">
                                                  <option  value="{{$employee->level_id}}"  disabled selected>{{$employee->level_id}}</option>
                                                    @foreach($levels as $type)
                                            <option value="{{ $type->id }}">{{ $type->level }}</option>
                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
 <div class="col-md-3 form-control-label">
                                    <label align="left">Contract Type</label>
                                    </div>
                                    <div class="col-md-9 ">
                                        <div class="form-group">
                                            <div class="">
                                                
                                                 <select id="editable-select1" data-live-search="true" class="form-control"  name="contract_type_id">
                                                   <option value="{{$employee->contract_type_id}}" disabled selected>{{$employee->contract_type_id}}</option>
                                        @foreach($contractTypes as $type)
                                            <option value="{{ $type->id }}">{{ $type->typename }}</option>
                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
 <div class="col-md-3 form-control-label">
                                    <label align="left">Contract Base</label>
                                    </div>
                                    <div class="col-md-9 ">
                                        <div class="form-group">
                                            <div class="">
                                                
                                                 <select id="editable-select1" data-live-search="true" class="form-control"  name="contract_base_id">
                                                  <option value="{{$employee->contract_base_id}}" disabled selected>{{$employee->contract_base_id}}</option>
                                                     @foreach($contractBases as $base)
                                            <option value="{{ $base->id }}">{{ $base->basename }}</option>
                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="col-md-3 form-control-label">
                                    <label align="left">Designation</label>
                                    </div>
                                    <div class="col-md-9 ">
                                        <div class="form-group">
                                            <div class="">
                                                
                                                 <select id="editable-select1" data-live-search="true" class="form-control"  name="designation_id">
                                                  <option value="{{$employee->designation_id}}" disabled selected>{{$employee->designation_id}}</option>
                                                    @foreach($designations as $type)
                                                        <option value="{{ $type->id }}">{{ $type->designation_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                            </div>




                            </div>
                          

                          <!-- New Tabs -->

                          <div class="row clearfix">
                              <div class="col-md-12">
                                   <div class="card">
                        <div class="header">
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_1" data-toggle="tab">Other Information</a></li>
                                        <li role="presentation"><a href="#profile_animation_1" data-toggle="tab">Address</a></li>
                                        <li role="presentation"><a href="#messages_animation_1" data-toggle="tab">Contact</a></li>
                                        <li role="presentation"><a href="#settings_animation_1" data-toggle="tab">Remarks</a></li>
                                    </ul>

                           
                        </div>
                        <div class="body">
                            <div class="row ">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                   
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated flipInX active" id="home_animation_1">
                                                                        <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="input-group">
                                       
                                        <div class="form-line">
                                            <input type="text" name="fathers_name" class="form-control date" value="{{$employee->fathers_name}}">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="input-group">
                                       
                                        <div class="form-line">
                                            <input type="text" name="mothers_name" class="form-control date" value="{{$employee->mothers_name}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="passport_no" class="form-control date" value="{{$employee->passport_no}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                       
                                        <div class="form-line">
                                            <input type="text" name="nationality" class="form-control date" value="{{$employee->nationality}}">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="input-group">
                                       
                                        <div class="form-line">
                                            <input type="text" name="nid"  class="form-control date" value="{{$employee->nid}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="date" name="birth"  class="form-control date"value="{{$employee->birth}}">
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-md-4">
                                   
                                    <select class="form-control show-tick" name="gender"  data-live-search="true">
                                        <option value="" disabled="" selected="">Gender</option>
                                        <option value="0">Male</option>
                                        <option value="1">Female</option>
                                    </select>
                                </div> 
                                <div class="col-md-4">
                                   
                                    <select class="form-control show-tick" name="religion"  data-live-search="true">
                                        <option value="" disabled="" selected="">Religion</option>
                                        <option value="1">Muslim</option>
                                        <option value="0">Others</option>
                                    </select>
                                </div> 
                                <div class="col-md-4">
                                   
                                    <select class="form-control show-tick" name="marital_status"  selected="" data-live-search="true">
                                        <option value="" disabled="">Marital Status</option>
                                        <option value="1">married</option>
                                        <option value="0">unmarried</option>
                                    </select>
                                </div>

                            </div>
                                            
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="profile_animation_1">
                                            
                                <div class="col-md-6">
                                    <div class="col-md-4">
                                        <label>Present Address</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="present_address"  class="form-control date" value="{{$employee->present_address}}">
                                        </div>
                                    </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-4">
                                        <label>Permanent Address</label>
                                    </div>
                                    <div class="col-md-8">
                                         <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="permanent_address"  class="form-control date" value="{{$employee->permanent_address}}">
                                        </div>
                                    </div>
                                    </div>
                                   
                                </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="messages_animation_1">
                                           <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="email"  class="form-control date" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="mobile"  class="form-control date" placeholder="Mobile No.">
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="alternate_email"  class="form-control date" placeholder="Alternate Email">
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="alternate_mobile" class="form-control date" placeholder="Alternate Mobile no">
                                        </div>
                                    </div>
                                </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="settings_animation_1">
                                             <div class="col-md-6">
                                                <div class="input-group">
                                                   
                                                    <div class="form-line">
                                                        <input type="text" name="remarks" class="form-control date" placeholder="Remarks Name">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>

                            <div class="" align="right">
                                    <button class="btn btn-primary btn-lg" type="submit" name="action">Submit
                                       
                                    </button>
                                </div>



                        </div>
                    </div>
                              </div>

                              
                          </div>
     

                            


                        </form>

                  </div>

                </div>
            </div>
        </div>
    </div>













































    <div class="container">
        <div class="row">
            <div class="col s12 m10 l10 offset-m1 offset-l1" >
                <div class="card-panel">
                    <span>Edit Employee</span>
                    <div class="">

                        <form action="{{url('editemployee/'.$employee->id)}}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <input id="last_name2" type="text" name="employee_code" value="{{$employee->employee_code}}" required>
                                    <label for="last_name2">Employee Code*</label>
                                </div>
                                <div class="input-field col s12 m6">
                                    <input id="last_name2" type="text" name="ot_rate" value="{{$employee->ot_rate}}" required>
                                    <label for="last_name2">OT Rate</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input  type="text" name="first_name" value="{{$employee}}"  required>
                                    <label for="first_name">First Name*</label>
                                </div>

                                <div class="input-field col m6 s12">
                                    <input id="hth" type="text" name="last_name" value="{{$employee->last_name}}" >
                                    <label for="last_name">Last Name</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col m4 s12">
                                    <select name="department_id" >
                                        <option  value="{{$employee->department_id}}"  disabled selected>{{$employee->department_id}}</option>
                                        @foreach($departments as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                    <label>Department</label>
                                </div>

                                <div class="input-field col m4 s12">
                                    <select name="designation_id" >
                                        <option  value="{{$employee->designation_id}}"  disabled selected>{{$employee->designation_id}}</option>
                                        @foreach($designations as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                    <label>Designation</label>
                                </div>

                                <div class="input-field col m4 s12">
                                    <select name="level_id">
                                        <option  value="{{$employee->level_id}}"  disabled selected>{{$employee->level_id}}</option>
                                        @foreach($levels as $type)
                                            <option value="{{ $type->id }}">{{ $type->level }}</option>
                                        @endforeach
                                    </select>
                                    <label>Level</label>
                                </div>

                            </div>


                            <div class="row">
                                <div class="input-field col m4 s12">
                                    <select name="contract_type_id">
                                        <option  value="{{$employee->contract_type_id}}"  disabled selected>Choose one</option>
                                        @foreach($contractTypes as $type)
                                            <option value="{{ $type->id }}">{{ $type->typename }}</option>
                                        @endforeach
                                    </select>
                                    <label>Contract Type</label>
                                </div>

                                <div class="input-field col m4 s12">
                                    <select name="contract_base_id">
                                        <option  value="{{$employee->contract_base_id}}"  disabled selected>Choose one</option>
                                        @foreach($contractBases as $base)
                                            <option value="{{ $base->id }}">{{ $base->name }}</option>
                                        @endforeach
                                    </select>
                                    <label>Contract Base</label>
                                </div>

                                <div class="input-field col m4 s12">
                                    <input  value="{{$employee->salary}}"  type="text" name="salary">
                                    <label for="">Salary</label>
                                </div>


                            </div>












                            {{--Tabs--}}

                            <div class="row">
                                <div class="divider"></div>
                                <!--Multicolor with icon tabs-->
                                <div id="multi-color-tab" class="section">

                                    <div class="row">

                                        <div class="col s12 m12">
                                            <div class="row">
                                                <div class="col s12">
                                                    <ul class="tabs tab-demo-active z-depth-1">
                                                        <li class="tab col s3"><a class="white-text green darken-1 waves-effect waves-light active" href="#sapien1"><i class="mdi-action-perm-identity"></i> Other Information</a>
                                                        </li>
                                                        <li class="tab col s3"><a class="white-text purple darken-1 waves-effect waves-light " href="#activeone1"><i class="mdi-action-settings-display"></i> Address</a>
                                                        </li>
                                                        <li class="tab col s3"><a class="white-text red darken-1 waves-effect waves-light " href="#new1"><i class="mdi-action-3d-rotation"></i> Contacts</a>
                                                        </li>
                                                        <li class="tab col s3"><a class="white-text light-blue darken-1 waves-effect waves-light" href="#vestibulum1"><i class="mdi-action-speaker-notes"></i> Remarks</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col s12">
                                                    <div id="sapien1" class="col s12  lighten-3">
                                                        <div class="row">
                                                            <label for="">Photo</label>
                                                            <div class="input-field col m12 s12 ">
                                                                <input id="httth" type="file" name="photo">
                                                                <div>
                                                                    <img style="width: 50px" height="50px" src="{{url('public/images/employees/'.$employee->photo)}}" alt="">
                                                                </div>

                                                            </div>


                                                        </div>

                                                        <div class="row">
                                                            <div class="input-field col m4 s12 ">
                                                                <input id="hth" type="text" name="fathers_name" value="{{$employee->fathers_name}}" >
                                                                <label for="first_name">Fathers Name</label>
                                                            </div>

                                                            <div class="input-field col m4 s12">
                                                                <input id="hth" type="text" name="mothers_name" value="{{$employee->mothers_name}}" >
                                                                <label for="last_name">Mothers Name</label>
                                                            </div>

                                                            <div class="input-field col s12 m4">
                                                                <input id="tht" type="text" name="passport_no" value="{{$employee->passport_no}}" >
                                                                <label for="">Passport No.</label>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="input-field col s4">
                                                                <input id="hth" type="text" name="nationality" value="{{$employee->nationality}}" >
                                                                <label for="first_name">Nationality</label>
                                                            </div>

                                                            <div class="input-field col s4">
                                                                <input id="yyt" type="text" name="nid">
                                                                <label for="first_name">National ID</label>
                                                            </div>


                                                            <div class="input-field col s4">
                                                                <input type="date" class="datepicker" name="birth" value="{{$employee->birth}}" >
                                                                <label for="dob">Date of Birth</label>
                                                            </div>


                                                        </div>

                                                        <div class="row">
                                                            <div class="input-field col s4">
                                                                <select name="gender">
                                                                    <option  value="{{$employee->gender}}"  disabled selected>Choose one</option>

                                                                    <option value="1">Male</option>
                                                                    <option value="2">Female</option>

                                                                </select>
                                                                <label>Gender</label>
                                                            </div>
                                                            <div class="input-field col s4">
                                                                <select name="religion">
                                                                    <option  value="{{$employee->religion}}"  disabled selected>Choose one</option>

                                                                    <option value="1">Islam</option>
                                                                    <option value="2">other</option>


                                                                </select>
                                                                <label>Religion</label>
                                                            </div>
                                                            <div class="input-field col s4">
                                                                <select name="marital_status">
                                                                    <option  value="{{$employee->marital_status}}"  disabled selected>Choose one</option>

                                                                    <option value="1">Married</option>
                                                                    <option value="2">Single</option>


                                                                </select>
                                                                <label>Marital Status</label>
                                                            </div>



                                                        </div>
                                                    </div>
                                                    <div id="activeone1" class="col s12  lighten-3">

                                                        <div class="row">


                                                            <div class="input-field col s6">
                                                                <input id="thth" type="text" name="present_address" value="{{$employee->persent_address}}" >
                                                                <label for="last_name">Present Address</label>
                                                            </div>

                                                            <div class="input-field col s6" name="permanent_address" value="{{$employee->permanent_address}}" >
                                                                <input id="thth" type="text">
                                                                <label for="last_name">Permanent Address</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="new1" class="col s12  lighten-3">
                                                        <div class="row">
                                                            <div class="input-field col s6">
                                                                <input id="yhyt" type="email" name="email" value="{{$employee->email}}" >
                                                                <label for="email">Email</label>
                                                            </div>

                                                            <div class="input-field col s6">
                                                                <input id="yhyt" type="email" name="alternate_email" value="{{$employee->alternate_email}}" >
                                                                <label for="email">Alternate Email</label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="input-field col s6">
                                                                <input id="hth" type="text"  name="mobile" value="{{$employee->mobile_no}}" >
                                                                <label for="first_name">Mobile No</label>
                                                            </div>

                                                            <div class="input-field col s6">
                                                                <input id="hth" type="text" name="alternate_mobile" value="{{$employee->alternate_mobile_no}}" >
                                                                <label for="last_name">Alternete Mobile No</label>
                                                            </div>
                                                        </div>
                                                    </div><div id="vestibulum1" class="col s12  lighten-3">
                                                        <div class="input-field col s12">
                                                            <input id="hth" type="text"  name="remarks"  value="{{$employee->remarks}}" >
                                                            <label for="first_name"> Remarks</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                            </div>


                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
