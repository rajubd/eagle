@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-s12">
            <div class="card">
                <div class="body">
                    <div class="row">
                     <div class="col-md-6">
                <h4>Employees</h5>
            </div>
                
                <div class="col-md-6" align="right">
                
                    <a class="btn btn-primary" href="{{url('/createemployee')}}">+ Add Employee</a>
                </div>
                </div>
                </div>
            </div>
        </div>

        <div class="col-s12">
            <div class="card-panel">
                @include('eagle/employees/employeeTable')
            </div>
        </div>
       
    </div>
</div>
@endsection
