<!--DataTables example-->
            <div id="">
              
              <div class="card">
                <div class="body">
                  

                 
                 <!--  <table id="example" class="display nowrap" cellspacing="0" width="100%"> -->
                  <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                    <thead>
                        <tr>
                            <th>Employee Code</th>
                            <th>Name</th>
                            
                            <th>joining Date</th>
                            <th>Department</th>
                            <th>Designation</th>
                            <th>Salary</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($Employees as $employee)
                        <tr>
                        <td>{{ $employee->employee_code }}</td>
                        <td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
                            <td>{{ $employee->joining_date}}</td>
                            <td>{{ $employee->department_name }}</td>
                            <td>{{ $employee->designation_name }}</td>
                            
                            <td>{{ $employee->salary }}</td>
                            <td>
                                
                                <a href="{{url('editemployee/'.$employee->id)}}"><i class=""></i>Edit</a> | 
                                <a href="{{url('deleteemployee/'.$employee->id)}}"><i class=" material-icons"></i>Delete</a>
                            </td>
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>
                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 