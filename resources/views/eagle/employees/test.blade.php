@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s8 m8 l8 offset-s2 offset-m2 offset-l2" >
                <div class="card-panel">
                    <h4 class="header2">New Employee</h4>
                    <div class="row">
                        <form class="col s12" action="{{url('createemployee')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="htht" type="text" name="first_name" required>
                                    <label for="first_name">First Name*</label>
                                </div>

                                <div class="input-field col s6">
                                    <input id="hth" type="text" name="last_name">
                                    <label for="last_name">Last Name</label>
                                </div>
                            </div>


                    <div class="row">
                        <div class="input-field col s4">
                            <select name="department_id" required="">
                                <option value="" disabled selected>Choose one</option>
                                @foreach($departments as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                            <label>Department*</label>
                        </div>

                        <div class="input-field col s4">
                            <select name="designation_id" required>
                                <option value="" disabled selected>Choose one</option>
                                @foreach($designations as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                            <label>Designation*</label>
                        </div>

                        <div class="input-field col s4">
                            <select name="level_id">
                                <option value="" disabled selected>Choose one</option>
                                @foreach($levels as $type)
                                    <option value="{{ $type->id }}">{{ $type->level }}</option>
                                @endforeach
                            </select>
                            <label>Level</label>
                        </div>

                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="hth" type="text" name="salary">
                            <label for="">Salary</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <select name="contract_type_id" required="">
                                <option value="" disabled selected>Choose one</option>
                                @foreach($contractTypes as $type)
                                    <option value="{{ $type->id }}">{{ $type->typename }}</option>
                                @endforeach
                            </select>
                            <label>Contract Type*</label>
                        </div>

                        <div class="input-field col s6">
                            <select name="contract_base_id" required>
                                <option value="" disabled selected>Choose one</option>
                                @foreach($contractBases as $base)
                                    <option value="{{ $base->id }}">{{ $base->name }}</option>
                                @endforeach
                            </select>
                            <label>Contract Base*</label>
                        </div>


                    </div>

                    <div class="row">
                        <div class="input-field col s4">
                            <input id="yyt" type="text" name="nid" required>
                            <label for="first_name">National ID*</label>
                        </div>

                        <div class="input-field col s4">
                            <input id="last_name2" type="text" name="employee_code">
                            <label for="last_name2">Employee Code*</label>
                        </div>

                        <div class="input-field col s4">
                            <input id="htht" type="text" name="marital_status">
                            <label for="first_name">Marital Status</label>
                        </div>


                    </div>



                    <div class="row">


                        <div class="input-field col s6">
                            <input id="thth" type="text" name="present_address">
                            <label for="last_name">Present Address</label>
                        </div>

                        <div class="input-field col s6" name="permanent_address">
                            <input id="thth" type="text">
                            <label for="last_name">Permanent Address</label>
                        </div>
                    </div>



                    <div class="row">
                        <div class="input-field col s4">
                            <input id="hth" type="text" name="nationality">
                            <label for="first_name">Nationality</label>
                        </div>

                        <div class="input-field col s4">
                            <input type="date" class="datepicker" name="birth">
                            <label for="dob">Date of Birth</label>
                        </div>

                        <div class="input-field col s4">
                            <input id="tht" type="text" name="religion">
                            <label for="last_name">Religion</label>
                        </div>
                    </div>


                    <div class="row">
                        <div class="input-field col s4">
                            <select name="gender">
                                <option value="" disabled selected>Choose one</option>

                                <option value="1">Male</option>
                                <option value="2">Female</option>

                            </select>
                            <label>Gender</label>
                        </div>



                        <div class="input-field col s8">
                            <input id="tht" type="text" name="passport_no">
                            <label for="">Passport No.</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="divider"></div>
                        <!--Multicolor with icon tabs-->
                        <div id="multi-color-tab" class="section">

                            <div class="row">

                                <div class="col s12 m12">
                                    <div class="row">
                                        <div class="col s12">
                                            <ul class="tabs tab-demo-active z-depth-1">
                                                <li class="tab col s3"><a class="white-text green darken-1 waves-effect waves-light active" href="#sapien1"><i class="mdi-action-perm-identity"></i> Other Information</a>
                                                </li>
                                                <li class="tab col s3"><a class="white-text purple darken-1 waves-effect waves-light active" href="#activeone1"><i class="mdi-action-settings-display"></i> Contacts</a>
                                                </li>
                                                <li class="tab col s3"><a class="white-text light-blue darken-1 waves-effect waves-light" href="#vestibulum1"><i class="mdi-action-speaker-notes"></i> Remarks</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col s12">
                                            <div id="sapien1" class="col s12   lighten-3">
                                                <div class="row">
                                                    <div class="input-field col s6">
                                                        <input id="hth" type="text" name="fathers_name">
                                                        <label for="first_name">Fathers Name</label>
                                                    </div>

                                                    <div class="input-field col s6">
                                                        <input id="hth" type="text" name="mothers_name">
                                                        <label for="last_name">Mothers Name</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="activeone1" class="col s12  lighten-3">
                                                <div class="row">
                                                    <div class="input-field col s6">
                                                        <input id="yhyt" type="email" name="email">
                                                        <label for="email">Email</label>
                                                    </div>

                                                    <div class="input-field col s6">
                                                        <input id="yhyt" type="email" name="alternate_email">
                                                        <label for="email">Alternate Email</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s6">
                                                        <input id="hth" type="text"  name="mobile" >
                                                        <label for="first_name">Mobile No</label>
                                                    </div>

                                                    <div class="input-field col s6">
                                                        <input id="hth" type="text" name="alternate_mobile">
                                                        <label for="last_name">Alternete Mobile No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="vestibulum1" class="col s12  lighten-3">
                                                <div class="input-field col s6">
                                                    <input id="hth" type="text"  name="mobile" >
                                                    <label for="first_name"> Remarks</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>




                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
