@extends('layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" >
                <div class="card">
                        <div class="header">
                            <span>Add New Employee</span>
                        </div>
                    
                        <div class="body">

                        <form action="{{url('createemployee')}}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <div class="row">

                            <div class="col-md-10 col-md-offset-1">

                            <div class="col-md-6">
                                  <div class="col-md-4 form-control-label">
                           <label align="">Employee Code</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="employee_code" class="form-control" >
                                </div>
                            </div>
                        </div>

                            </div>
                      

                         <div class="col-md-6">
                                
                              <div class="col-md-4 form-control-label">
                           <label align="">Joining Date</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="date" name="joining_date" class="form-control datepicker" name="item_name">
                                </div>
                            </div>
                        </div>
                            </div>
                      

                             <div class="col-md-6">
                                
                        <div class="col-md-4 form-control-label">
                           <label align="">First Name*</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="first_name" class="form-control" name="item_name">
                                </div>
                            </div>
                        </div>
                            </div>


                                                    <div class="col-md-6">
                                 <div class="col-md-4 form-control-label">
                           <label align="">Last Name*</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="last_name" class="form-control" name="item_name">
                                </div>
                            </div>
                        </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col-md-4 form-control-label">
                                   <label align="">Salary Amount</label>
                                </div>
                                <div class="col-md-8 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="" name="salary" class="form-control" name="item_name">
                                        </div>
                                    </div>
                                </div> 
                            </div>

                             <div class="col-md-6">
                                        
                                 <div class="col-md-4 form-control-label">
                                   <label align="">OT Rate</label>
                                </div>
                                <div class="col-md-8 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="" name="ot_rate" class="form-control" name="item_name">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        

                           
                           

                            <div class="col-md-6">
                                <div class="col-md-4 form-control-label">
                                    <label align="">Department</label>
                                    </div>
                                    <div class="col-md-8 ">
                                        <div class="form-group">
                                            <div class="">
                                                
                                                 <select id="editable-select1" data-live-search="true" class="form-control"  name="department_id">
                                                  <option value="" disabled selected>Choose one</option>
                                                    @foreach($departments as $type)
                                                        <option value="{{ $type->id }}">{{ $type->department_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                             <div class="col-md-6">
                                 <div class="col-md-4 form-control-label">
                                    <label align="">Level</label>
                                    </div>
                                    <div class="col-md-8 ">
                                        <div class="form-group">
                                            <div class="">
                                                
                                                 <select id="editable-select1" data-live-search="true" class="form-control"  name="level_id">
                                                  <option value="" disabled selected>Choose one</option>
                                                    @foreach($levels as $type)
                                            <option value="{{ $type->id }}">{{ $type->level }}</option>
                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                            </div>


                            <div class="col-md-6">
                                 <div class="col-md-4 form-control-label">
                                    <label align="">Contract Type</label>
                                    </div>
                                    <div class="col-md-8 ">
                                        <div class="form-group">
                                            <div class="">
                                                
                                                 <select id="editable-select1" data-live-search="true" class="form-control"  name="contract_type_id">
                                                   <option value="" disabled selected>Choose one</option>
                                        @foreach($contractTypes as $type)
                                            <option value="{{ $type->id }}">{{ $type->typename }}</option>
                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                             <div class="col-md-6">
                                
 <div class="col-md-4 form-control-label">
                                    <label align="">Contract Base</label>
                                    </div>
                                    <div class="col-md-8 ">
                                        <div class="form-group">
                                            <div class="">
                                                
                                                 <select id="editable-select1" data-live-search="true" class="form-control"  name="contract_base_id">
                                                  <option value="" disabled selected>Choose one</option>
                                                     @foreach($contractBases as $base)
                                            <option value="{{ $base->id }}">{{ $base->basename }}</option>
                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                            </div>

                             <div class="col-md-6">
                                
                                <div class="col-md-4 form-control-label">
                                    <label align="left">Designation</label>
                                    </div>
                                    <div class="col-md-8 ">
                                        <div class="form-group">
                                            <div class="">
                                                
                                                 <select id="editable-select1" data-live-search="true" class="form-control"  name="designation_id">
                                                  <option value="" disabled selected>Choose one</option>
                                                    @foreach($departments as $type)
                                                        <option value="{{ $type->id }}">{{ $type->department_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                   

                            </div>




                            </div>
                          

                          <!-- New Tabs -->

                          <div class="row clearfix">
                              <div class="col-md-12">
                                   <div class="">
                        <div class="header">
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_1" data-toggle="tab">Other Information</a></li>
                                        <li role="presentation"><a href="#profile_animation_1" data-toggle="tab">Address</a></li>
                                        <li role="presentation"><a href="#messages_animation_1" data-toggle="tab">Contact</a></li>
                                        <li role="presentation"><a href="#settings_animation_1" data-toggle="tab">Remarks</a></li>
                                    </ul>

                           
                        </div>
                        <div class="body">
                            <div class="row ">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                   
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated flipInX active" id="home_animation_1">
                                                                        <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="input-group">
                                       
                                        <div class="form-line">
                                            <input type="text" name="fathers_name" class="form-control date" placeholder="Fathers Name ">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="input-group">
                                       
                                        <div class="form-line">
                                            <input type="text" name="mothers_name" class="form-control date" placeholder="Mothers Name">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="passport_no" class="form-control date" placeholder="Passport No.">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                       
                                        <div class="form-line">
                                            <input type="text" name="nationality" class="form-control date" placeholder="Nationality">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="input-group">
                                       
                                        <div class="form-line">
                                            <input type="text" name="nid"  class="form-control " placeholder="National ID">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="birth"  class="form-control " id="date2" placeholder="Date of Birth">
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-md-4">
                                   
                                    <select class="form-control show-tick" name="gender"  data-live-search="true">
                                        <option value="" disabled="" selected="">Gender</option>
                                        <option>Burger, Shake and a Smile</option>
                                        <option>Sugar, Spice and all things nice</option>
                                    </select>
                                </div> 
                                <div class="col-md-4">
                                   
                                    <select class="form-control show-tick" name="religion"  data-live-search="true">
                                        <option value="" disabled="" selected="">Religion</option>
                                        <option>Muslim</option>
                                        <option>Others</option>
                                    </select>
                                </div> 
                                <div class="col-md-4">
                                   
                                    <select class="form-control show-tick" name="marital_status"  selected="" data-live-search="true">
                                        <option value="" disabled="">Marital Status</option>
                                        <option value="1">married</option>
                                        <option value="0">unmarried</option>
                                    </select>
                                </div>

                            </div>
                                            
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="profile_animation_1">
                                            
                                <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="present_address"  class="form-control date" placeholder="Present Address">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="permanent_address"  class="form-control date" placeholder="Permanent Address">
                                        </div>
                                    </div>
                                </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="messages_animation_1">
                                           <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="email"  class="form-control date" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="mobile"  class="form-control date" placeholder="Mobile No.">
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="alternate_email"  class="form-control date" placeholder="Alternate Email">
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="input-group">
                                        
                                        <div class="form-line">
                                            <input type="text" name="alternate_mobile" class="form-control date" placeholder="Alternate Mobile no">
                                        </div>
                                    </div>
                                </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="settings_animation_1">
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                <label>Remarks</label>
                                                   
                                                    <div class="form-line">
                                                    <textarea name="remarks" class="form-control"></textarea>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>

                            <div class="" align="right">
                                    <button class="btn btn-primary btn-lg" type="submit" name="action">Submit
                                       
                                    </button>
                                </div>



                        </div>
                    </div>
                              </div>

                              
                          </div>
     

                            


                        </form>

                  </div>

                </div>
            </div>
        </div>
    </div>

@endsection
