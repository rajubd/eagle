@extends('layouts.addrowlayout')

@section('content')
<style type="text/css">
  label{
    color: #000000;
  }
</style>


<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
      <?php 
          $customer = App\Models\Customer::find($invoice->customer_id);
       ?>
        <div class="header bg-blue"><h4 align=""> Payment for {{$customer->salutation}} {{$customer->customer_first_name}} {{$customer->customer_last_name}}</h4></div>
        <div class="body">
           <form class="form-horizontal" action="{{url('recordpayment/'.$invoice->id)}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
            
                       <div class="row clearfix">
                         <div class="col-md-8">

                         
                         
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Payment Made</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="" name="payment_made" value="{{$invoice->due_amount}}">
                          </div>
                          </div>
                        </div>

     
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Payment date</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="date" class="form-control" id="" name="payment_date">
                          </div>
                          </div>
                        </div>




                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Payment Mode</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control chosen-select"  data-live-search="true" name="payment_mode" id="">
                                <option value="" selected></option>
                                @foreach($modes as $mode)
                                <option value="{{$mode->id}}">{{$mode->mode}}</option>
                                @endforeach
                              </select>
                            </div>
                                <br>
                            <a href="" target="_blank"  data-toggle="modal" data-target="#myModal"> + Add payment Mode</a>
          
                          </div>
                        </div>


<!-- Modal -->




                          

                       



                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Reference #</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="reference_no" name="reference_no">
                          </div>
                            
                           
                          </div>
                        </div>

      
                       


                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Note </label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="note" name="note">
                          </div>
                            
                          
                          
                          </div>
                        </div>

      
                       




                       


                         </div>
                        
                       </div>

                       


     


 
                     
<div class="row clearfix">
   <div class="col-md-2 col-md-offset-2">
        <div class="form-group">
            <div class="form-line" align="">
            
            <button type="submit" id="" name="submit" class="btn btn-success btn-block" >Submit <i class="small material-icons">input</i> </button>
             
            </div>
        </div>
    </div>
</div>




            </form>
        </div>

      </div>
    </div>
  </div>
</div>














<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payment Mode </h4>
      </div>
      <div class="modal-body">
        <form  id="tax_form" action="{{url('createpaymentmode')}}" method="post" enctype="multipart/form-data">
           <div class="col-md-3 form-control-label">
             <label align="left">New Payment Mode</label>
          </div>
          <div class="col-md-9 ">
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" id="" name="payment_mode" class="form-control" >
                  </div>
              </div>
          </div>
          <div class="col-md-3"></div>
          <div class="col-md-9">
             <button type="submit" id="sub" class="btn btn-primary" form="tax_form" value="Submit">Submit</button>
          </div>

        
         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
      </div>
    </div>

  </div>
</div>























<script type="text/javascript">
  $(document).ready(function () {
    $('#check').hide();
  })
</script>
              


<script type="text/javascript">
  $(function() {
  $('.dropdown-menu a').click(function() {
    console.log($(this).attr('data-value'));
    $(this).closest('.dropdown').find('input.countrycode')
      .val( $(this).attr('data-value') );
  });
});
</script>





<script type="text/javascript">
  
$('#customer').on('change', function(e){
      // e.preventDefault()
        customer_id = $('#customer').val();
       
        $("#customerdetails").load('{{ URL::to('loadcustomeraddress')}}'+'/'+customer_id);
        
       }); 





</script>





<script type="text/javascript">
    
  jQuery(document).ready(function() {
      var id = 0;
      jQuery("#addrow").click(function() {
        id++;           
        var row = jQuery('.samplerow tr').clone(true);
        row.find(".amount").val(0);

        row.find(".amount").attr('id','amount_'+id);
        row.find(".amount").attr('autocomplete',id);

        row.find(".quantity").attr('id','quantity_'+id);
        row.find(".quantity").attr('autocomplete',id);

        row.find(".rate").attr('id','rate_'+id);
        row.find(".rate").attr('autocomplete',id);

        row.find("#delete-btn").attr('autocomplete',id);

        row.attr('id',id);
        row.appendTo('#dynamicTable1');        
        return false;
    });        
        
  $('.remove').on("click", function() {
  $(this).parents("tr").remove();

     var tr = $('tr').length - 2;
    alert(tr);
    document.getElementById("rowcount").value = tr;
    //alert(tr);
    var total =0;
    for (var i = 0; i < tr; i++) {
      //console.log(i);
      var myAmount = $('#amount_'+i).val();
      total +=  parseFloat(myAmount);
    }
    //alert(total);

    document.getElementById("total").value = total;

});
});


  </script>




<script type="text/javascript">
  function UpdateAmount(x) {
    //$(this.)
    //alert(x);
    var i = x.autocomplete;
    //console.log(i);
    var amount = $('#quantity_'+i).val() * $('#rate_'+i).val();
    console.log(i);
    document.getElementById("amount_"+i).value = amount;

    var tr = $('tr').length - 2;
    //$('#rowcount').val() = tr;
    document.getElementById("rowcount").value = tr;
    //alert(tr);
    var total =0;
    //alert($('.amount').val());
    for (var i = 0; i < tr; i++) {
      
      var myAmount = $('#amount_'+i).val();
      //console.log(myAmount);
      total +=  parseFloat(myAmount);
    }
    //alert(total);

    document.getElementById("total").value = total;
    //$( "#total" ).html( "iuhe" );


  }
</script>



@endsection




