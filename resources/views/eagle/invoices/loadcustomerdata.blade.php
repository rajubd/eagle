						<div class="form-group ">
                          <label class="control-label col-sm-3" for="email"></label>
                          <div class="col-sm-9">
                           <label class="control-label" for="email">Address</label>
                            <p>{{$customer->customer_address}}</p>
                          </div>
                          
                        </div>
                        <div class="form-group ">
                          <label class="control-label col-sm-3" for=""></label>
                          <div class="col-sm-9">
                          	<span class="badge bg-green">{{$cur->foreign_currency_code}}</span><br>
                          	As on <?php echo date(" jS \of F Y") . "<br>"; ?>
                          	1 {{$cur->foreign_currency_code}} = {{$cur->exchange_rate}} SSD
                          </div>
                          
                        </div>

                      