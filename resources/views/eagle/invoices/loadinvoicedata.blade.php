<div class="card" >
	<div class="body">
		 <a  class="btn btn-warning btn-lg" target="_blank" href="#" onclick="return xepOnline.Formatter.Format('mycard');">
             <i class="small material-icons">print</i> Print
          </a>
           <a  class="btn btn-primary btn-lg"  href="#" onclick="return xepOnline.Formatter.Format('mycard',{render:'download'});">
             <i class="small material-icons"> description</i> Pdf
          </a>

<a href="{{url('recordpayment/'.$invoice->id)}}" class="btn btn-danger btn-lg"><i class="small material-icons">mode_edit</i> Record Payment</a>
	</div>	
</div>




<div class="card" id="mycard">

	<div class="body">
		
		<div class="body">
			<div class="col-md-4">
				<div class="">
					<h3>LOGO</h3>
				</div>
			</div>
			<div class="col-md-8" align="right">
				<h2 class="text-uppercase" style="color: skyblue" >invoice</h2>
			<h6>Invoice ID #: {{$invoice->invoice_no}}</h6>
			<div >
					Balance Due
			<h3 style="color: teal">$ {{$invoice->due_amount}}</h3>
			</div>
			</div>
			<hr>
		</div>
		<div class="body" align="right">
			
			<div class="row clearfix">
				<div class="col-md-9">Order Number:</div>
				<div class="col-md-3">{{$invoice->order_no}}</div>
			</div>
			<div class="row clearfix">
				<div class="col-md-9">invoice Date:</div>
				<div class="col-md-3">{{$invoice->invoice_date}}</div>
			</div>
			<div class="row clearfix">
				<div class="col-md-9">Due date:</div>
				<div class="col-md-3">{{$invoice->due_date}}</div>
			</div>

		</div>

		<div class="body">	
			
			<?php 
		$customer = App\Models\Customer::find($invoice->customer_id);
		 ?>
			<h4>{{$customer->salutation}} {{$customer->customer_first_name}} {{$customer->customer_last_name}}</h4>
			<address>{{$customer->customer_address}}</address>
			


			<div class="row clearfix">
				<table class="table table-striped">
					<thead>
						<tr class="bg-teal">
							<th> #</th>
							<th> Item & Description</th>
							
							<th>Qty</th>
							<th>Rate</th>
							<th>Amount</th>
							
						</tr>
					</thead>
					<tbody>
					<?php $i=1; ?>
					@foreach($inv_items as $invoice)
					<?php 
						$item = App\Models\Item::find($invoice->item_id);
					 ?>
					<tr>
					<td>{{ $i++ }}</td>
						<td>{{$item->item_name}}<br><small>{{$item->item_description}}</small></td>
						
						<td>{{$invoice->qty}}</td>
						<td>{{$invoice->rate}}</td>
						<td>{{$invoice->amount}}</td>
					</tr>
					@endforeach
					
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>