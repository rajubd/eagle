@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-10">
                    <div class="block-header">
                        <h2>All Invoices</h2>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{url('createinvoice')}}"  class="btn btn-primary btn-block waves-effect m-r-20"><i class="small material-icons">add</i> New Invoice</a>
                </div>
            </div>
        </div>
        <div class="body">
            <div id="table-datatables">
              <div class="row">
                <div class="col-md-12">
                   <table class="table table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>Invoice#</th>
                            <th>Date</th>
                            
                            <th>Order Number</th>
                            
                            <th>Customer Name</th>
                            <th>Status</th>
                            <th>Due Date</th>
                            <th>Amount</th>
                            <th>Balance Due</th>
                           
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($invoices as $inv)
                        <tr  style="cursor: pointer" onclick="GetPayment({{$inv->id}});">
                           <td>{{$inv->invoice_no}}</td>
                           <td>{{$inv->invoice_date}}</td>
                           <td>{{$inv->order_no}}</td>
                           <td>
                           <?php 
                            $customer = App\Models\Customer::find($inv->customer_id);
                            ?>

                           {{$customer->salutation}} {{$customer->customer_first_name}} {{$customer->customer_last_name}}</td>
                           <td>{{$inv->status}}</td>
                           <td>{{$inv->due_date}}</td>
                           <td>{{$inv->invoice_amount}}</td>
                           <td>{{$inv->due_amount}}</td>
                           
                         
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>


                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');
    })
</script>


<script type="text/javascript">
    function GetPayment(id) {
        $('.tr').css( 'cursor', 'pointer' );
        window.location.href = '{{url('invoicedetails')}}';
    }
</script>
@endsection