<div class="">
  
  <ul class="list-group">
   @foreach($invoices as $payment)
   <li id="{{$payment->id}}" class="list-group-item ls" style="cursor: pointer;">
      <div class="row">
           <div class="col-md-6">
               {{$payment->invoice_no}}<br>
               {{$payment->invoice_date}}<br>
               
           </div>
           <div class="col-md-6" align="right">
               {{$payment->due_amount}}<br>
               {{$payment->status}}
           </div>
       </div>
   </li>
  
   @endforeach
  </ul>
</div>

