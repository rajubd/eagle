<div class="">
  
  <ul class="list-group">
   @foreach($estimates as $payment)
   <li id="{{$payment->id}}" class="list-group-item ls" style="cursor: pointer;">
      <div class="row">
           <div class="col-md-6">
               {{$payment->estimate_no}}<br>
               {{$payment->estimate_date}}<br>
               
           </div>
           <div class="col-md-6" align="right">
               {{$payment->due_amount}}<br>
               {{$payment->status}}
           </div>
       </div>
   </li>
  
   @endforeach
  </ul>
</div>

