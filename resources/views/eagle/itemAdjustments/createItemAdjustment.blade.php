@extends('layouts.addrowlayout')

@section('content')
<style type="text/css">
  label{
    color: #000000;
  }
</style>

<script type="text/javascript">
    
  jQuery(document).ready(function() {
        var id = 0;
      jQuery("#addrow").click(function() {
        id++;           
        var row = jQuery('.samplerow tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id); 
        row.appendTo('#dynamicTable1');        
        return false;
    });        
        
  $('.remove').on("click", function() {
  $(this).parents("tr").remove();
});
});


  </script>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header">Item Adjustment</div>
        <div class="body">
           <form class="form-horizontal" action="{{url('createitemadjustment')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <div class="row clearfix">
                          <!-- <div class="col-md-12">
                             <label align="left">Mode of Adjustment : </label>
                               <input name="group1" type="radio" checked id="test1" />
                                      <label for="test1">Quantity Adjustment</label>
                               <input name="group1" type="radio" id="test2" />
                      <label for="test2">Quantity Adjustment</label>

                          </div> -->
                       </div>
                       <div class="row clearfix">
                         <div class="col-md-6">
                         

                    
                        <div class="col-md-3 form-control-label">
                           <label align="left">Reference#</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="item_name" class="form-control" name="reference">
                                </div>
                            </div>
                        </div>

                         <div class="col-md-3 form-control-label">
                           <label align="left">Date</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                 <div class="form-line">
                                    <input type="text" id="date" name="date" class="datepicker form-control" >
                                </div>
                            </div>
                        </div>

                         

                          <div class="col-md-3 form-control-label">
                           <label align="left">Reason</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                 <div class="form-line">
                                    <input type="text" id="" name="reason" class="form-control">
                                </div>
                            </div>
                        </div>


                         </div>
                         <div class="col-md-6">
                                                  <div class="col-md-3 form-control-label">
  

                           <label align="left">Description</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                 <div class="form-line">
                                    <input type="text" id="" name="description" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 form-control-label">
                           <label align="left">Account</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="input-group dropdown">
                            
                             <div class="form-line">
                                <input type="text" name="account" class="form-control countrycode dropdown-toggle" value="">
                             </div>
                              <ul class="dropdown-menu">
                              @foreach($units as $unit)
                                <li><a data-value="{{$unit->unit_name}}">{{$unit->unit_name}}</a></li>
                              @endforeach
                              </ul>
                              <span role="button" class="input-group-addon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                            </div>
                        </div>


                         </div>

                       </div>

                       



      <table id="dynamicTable1"  class="table table-bordered table-hover">
        <thead>
            <th>Item Details</th>
            <th>Quantity Available</th>
            <th>New Quantity on Hand</th>
            <th>Quantity Adjusted</th>
        </thead>
      <tr id="0">
        <td>
        <select id="fid1" class="form-control" name="item[]" data-placeholder="Choose Item" data-live-search="true" >
         <option value="" selected="">Select Item</option>
          @foreach($items as $item)
            <option value="{{$item->id}}">{{$item->item_name}}</option>
          @endforeach
            
        </select>
        </td>
        <td><input class="form-control"  type="number" id="fld2" name="quantity[]" /></td>
        <td><input class="form-control"  type="number" id="fld3" name="onhand[]" /></td>
        <td><input class="form-control"  type="number" id="fld4" name="adjusted[]" /></td>
        
        
      </tr>
    </table>

  

  <table class="samplerow" style="display:none">
    <tr>
       
        <td>
       <select id="fid1" class="form-control" name="item[]" data-placeholder="Choose Item" data-live-search="true" >
          <option value="" selected="">Select Item</option>
             @foreach($items as $item)
              <option value="{{$item->id}}">{{$item->item_name}}</option>
            @endforeach
        </select>
        </td>
        <td><input class="form-control"  type="number" id="fld2" name="quantity[]" /></td>
        <td><input class="form-control"  type="number" id="fld3" name="onhand[]" /></td>
        <td><input class="form-control"  type="number" id="fld4" name="adjusted[]" min="0"/></td>
       <td>
       <a style="color:red; cursor: pointer" ><i class="remove large material-icons">delete</i></a>
       </td>
    </tr>
  </table>
      <div class="row ">

    <div class="body col-md-2">
      <a class="btn btn-primary btn-block" id="addrow"><i class=" large material-icons">add</i>Item</a>
      <!-- <input type="button" id="addrow" value="Add Row" class="btn btn-primary" /> -->
    </div>

  </div>


                     
<div class="row clearfix">
   <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-line" align="right">
                                    <input type="submit" id="" name="submit" class="btn btn-success btn-lg" >
                                </div>
                            </div>
                        </div>
</div>




            </form>
        </div>

      </div>
    </div>
  </div>
</div>


              




<script src="{{asset('js/script_invoice.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function(){
    //alert("document ready");

    $("input[id=test2]").click(function(event){
        $(".row2").hide();
    });
    $("input[id=test1]").click(function(event){
        $(".row2").show();
    });

   $("#test5").click(function () {
    if ($("#test5").is(":checked")) {
        $(".sal").show();
        //$(".tracker").show();
    }
    else {
        $(".sal").hide();
        //$(".tracker").hide();
    }
});
});
</script>




<script type="text/javascript">
  $(function() {
  $('.dropdown-menu a').click(function() {
    console.log($(this).attr('data-value'));
    $(this).closest('.dropdown').find('input.countrycode')
      .val( $(this).attr('data-value') );
  });
});
</script>




@endsection













