@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
       
        <div class="col-md-8 col-md-offset-2 ">
                  <div class="card col-md-8 col-md-offset-2">
                    <h4 class="header">Edit Department</h4>
                    <div class="row body">
                      <form class="col s12" action="{{url('editdepartment/'.$department->id)}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
                        <div class="row">
                         <div class="col-md-3 form-control-label">
                           <label align="left">Department Name</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="department_name" class="form-control" name="item_name" required  value="{{ $department->department_name }}">
                                </div>
                            </div>
                        </div>
                           <div class="col-md-3 form-control-label">
                           <label align="left">Short Code</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="short_code" class="form-control" name="item_name" required value="{{ $department->short_code }}">
                                </div>
                            </div>
                        </div>
                          
                       
                        
                          <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                              <button class="btn btn-success" type="submit" name="action">Submit
                                
                              </button>
                            </div>
                          </div>


                        </div>
                      </form>
                    </div>
                  </div>
                </div>
       
       
    </div>
</div>
@endsection
