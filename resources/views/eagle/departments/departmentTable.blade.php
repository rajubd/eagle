<!--DataTables example-->
            <div id="table-datatables">
              
              <div class="row">
                <div class="col-md-12">
                  

                  
                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Short Code</th>
                            <th>Action</th>
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($departments as $department)
                        <tr>
                            <td>{{ $department->department_name }}</td>
                            <td>{{ $department->short_code }}</td>
                            <td>
                                
                                <a href="{{url('editdepartment/'.$department->id)}}"><i class="more_edit"></i>Edit</a> | 
                                <a href="{{url('deletedepartment/'.$department->id)}}"><i class=" material-icons"></i>Delete</a>
                            </td>
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>
                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 