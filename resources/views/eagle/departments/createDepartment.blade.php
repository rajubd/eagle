@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
       
        <div class="col-md-6 col-md-offset-3 ">
                  <div class="card">
                    <h4 class="header">New Department</h4>
                    <div class="row clearfix body">
                      <form class="col-sm-12" action="{{url('createdepartment')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}


                       <div class="row clearfix">
                         <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="department_name">
                                            <label class="form-label">Department Name</label>
                                        </div>
                                    </div>
                                </div>
                               <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control"  name="short_code">
                                            <label class="form-label">Short Code</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                   <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                                </div>
                       </div>

                        </div>
                      </form>
                    </div>
                  </div>
                </div>
       
       
    </div>
</div>
@endsection
