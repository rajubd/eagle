@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="">
            <div class="card">
            <div class="body">
               <div class="row">
                    <div class="col-md-6">
                    <h4 class="blue-text text-darken-2">Departments</h4>
                </div>
                <div class="col-md-6">
                     <div align="right">
                    <a class=" btn btn-warning" href="{{url('/createdepartment')}}"><i class="material-icons">add</i> Add Department</a>
                </div>
                </div>
               </div>
            </div>
                
               
            </div>
        </div>

        <div class="card">
            <div class="body">
                @include('../eagle/departments/departmentTable')
            </div>
        </div>
       
    </div>
</div>
@endsection
