@extends('layouts.addrowlayout')

@section('content')
<style type="text/css">
  label{
    color: #000000;
  }
</style>


<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header bg-blue"><h4 align=""> New Credit Note</h4></div>
        <div class="body">
           <form class="form-horizontal" action="{{url('createcreditnote')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
            
                       <div class="row clearfix">
                         <div class="col-md-8">

                         
                         
      

                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Customer Name</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control chosen-select"  data-live-search="true" name="customer_id" id="customer">
                                <option value="" selected>Choose customer</option>
                                @foreach($customers as $customer)
                                <option value="{{$customer->id}}">{{$customer->customer_first_name}} {{$customer->customer_last_name}}</option>
                                @endforeach
                              </select>

                            </div>
                            <br>
                            <a href="{{url('createcustomer')}}" target="_blank"> + Add Customer</a>
                          </div>
                        </div>

                       
                          

                       
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="">Credit Note #</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="" name="credit_note_no">
                          </div>
                          </div>
                        </div>




                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Reference Number</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="" name="reference_no">
                          </div>
                         

                          
                          </div>
                        </div>

      
                          
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Credit Note Date</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="date" class="form-control" id="" name="credit_note_date">
                          </div>
                          </div>
                        </div>
   
                      




                         </div>
                         <div class="col-md-4">
                                                  
                        
                       
                        <div id="customerdetails"></div>
                        
                     

                       

                        
                       

                         </div>

                       </div>

                       


      <table id="dynamicTable1"  class="table table-striped table-bordered table-hover">
        <thead>
            <th>Item</th>
            <th>Description</th>
            <th>Quantity</th>
            <th>Rate</th>
            <th>Tax</th>
            <th>Amount</th>
        </thead>
      <tr id="0">
        <td>
        <select id="account_id" class="form-control" name="item[]">
          <option value="" selected="">Choose Item</option>
          @foreach($items as $item)
          <option value="{{$item->id}}">{{$item->item_name}}</option>
          @endforeach
        </select>
        </td>
        <td><input class="form-control "  type="text" placeholder="Description" id="description" name="item_description[]" /></td>
       <td><input class="form-control quantity2"  type="number" id="quantity_0" autocomplete="0" onchange="UpdateAmount(this)"  name="quantity[]" onchange=""  min="0" value="1" step="any" /></td>
         <td><input class="form-control rate2"  type="number" id="rate_0" name="rate[]"  autocomplete="0" onchange="UpdateAmount(this)"  min="0" value="0" step="any" /></td>
        <td>
            <select id="tax" class="form-control" name="tax[]">
              <option value="" selected="">Select Tax</option>
              @foreach($taxes as $tax)
              <option value="{{$tax->id}}">{{$tax->tax_name}}</option>
            @endforeach
                
            </select>
             <a href="" data-toggle='modal' data-target='#MyModal'>Add Tax</a>



        </td>
       
        <td><input class="form-control amount2"  type="number" readonly="" id="amount_0" name="amount[]"  min="0" value="0" step="any" /></td>
        
        
      </tr>
    </table>

  

  <table class="samplerow" style="display:none">
    <tr>
       
        <td>
        <select id="account_id" class="form-control" name="item[]">
          <option value="" selected="">Select Account</option>
           @foreach($items as $item)
          <option value="{{$item->id}}">{{$item->item_name}}</option>
          @endforeach
        </select>
        </td>
        <td><input class="form-control"  type="text" placeholder="Description" id="description" name="item_description[]" /></td>
        <td><input class="form-control quantity"  type="number" id="" name="quantity[]" onchange="UpdateAmount(this)"  min="0" value="1" step="any" /></td>
        <td><input class="form-control rate"  type="number" id="" onchange="UpdateAmount(this)"  name="rate[]"  min="0" value="0" step="any" /></td>
        <td>
            <select id="tax" class="form-control" name="tax_id[]">
              <option value="" selected="">Select Tax</option>
              @foreach($taxes as $tax)
              <option value="{{$tax->id}}">{{$tax->tax_name}}</option>
              @endforeach
                
            </select>
             <a href="" data-toggle='modal' data-target='#MyModal'>Add Tax</a>
        </td>
       
        <td><input class="form-control amount"  type="number" id="amount" name="amount[]"  min="0" value="0" step="any" readonly="" /></td>
        <td>
       <a id="delete-btn" style="cursor: pointer" onclick="UpdateAmount(this)" ><i class="remove  material-icons green-text text-darken-4">delete</i></a>
       </td>
    </tr>
  </table>
  <div class="row clearfix">
    <div class="body col-md-2">
      <a class="btn btn-primary btn-block" id="addrow"><i class=" large material-icons">add</i>Add new row</a>
      <!-- <input type="button" id="addrow" value="Add Row" class="btn btn-primary" /> -->
    </div>

    <div class="col-md-6 col-md-offset-4">
      <div class="panel panel-info">
      <!--  <div class="panel-body">
         
        </div> -->
        <div class="panel-heading">
         <div class="row">
            <div class="col-md-7">Total</div>
            <div class="col-md-5" align="right">
              <input class="form-control" align="right" type="number" name="total" id="total" value="0.00" readonly>
            </div>
          </div>
        </div>
       
      </div>
    </div>
    

  </div>
  <input type="hidden" name="rowcount" id="rowcount" value="">


           


 
                     
<div class="row clearfix">
   <div class="col-md-2 col-md-offset-10">
        <div class="form-group">
            <div class="form-line" align="right">
            
            <button type="submit" id="" name="submit" class="btn btn-success btn-block" >Submit <i class="small material-icons">input</i> </button>
             
            </div>
        </div>
    </div>
</div>




            </form>
        </div>

      </div>
    </div>
  </div>
</div>

















              <!-- Modals -->

              <!-- Tax Modal -->

<div id="MyModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add new</h4>
      </div>
      <div class="modal-body">
        <form  id="tax_form" action="{{url('createtax')}}" method="post" enctype="multipart/form-data">
           <div class="col-md-3 form-control-label">
             <label align="left">Title</label>
          </div>
          <div class="col-md-9 ">
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" id="" name="tax_name" class="form-control" >
                  </div>
              </div>
          </div>
          <div class="col-md-3 form-control-label">
             <label align="left">Amount (%)</label>
          </div>
          <div class="col-md-9 ">
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" id="" name="tax_amount" class="form-control" >
                  </div>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
         <button type="submit" id="sub" class="btn btn-primary" form="tax_form" value="Submit">Submit</button>
      </div>
    </div>

  </div>
</div>
























<script type="text/javascript">
  $(document).ready(function () {
    $('#check').hide();
  })
</script>
              


<script type="text/javascript">
  $(function() {
  $('.dropdown-menu a').click(function() {
    console.log($(this).attr('data-value'));
    $(this).closest('.dropdown').find('input.countrycode')
      .val( $(this).attr('data-value') );
  });
});
</script>





<script type="text/javascript">
  
$('#customer').on('change', function(e){
      // e.preventDefault()
        customer_id = $('#customer').val();
       
        $("#customerdetails").load('{{ URL::to('loadcustomeraddress')}}'+'/'+customer_id);
        
       }); 





</script>





<script type="text/javascript">
    
  jQuery(document).ready(function() {
      var id = 0;
      jQuery("#addrow").click(function() {
        id++;           
        var row = jQuery('.samplerow tr').clone(true);
        row.find(".amount").val(0);

        row.find(".amount").attr('id','amount_'+id);
        row.find(".amount").attr('autocomplete',id);

        row.find(".quantity").attr('id','quantity_'+id);
        row.find(".quantity").attr('autocomplete',id);

        row.find(".rate").attr('id','rate_'+id);
        row.find(".rate").attr('autocomplete',id);

        row.find("#delete-btn").attr('autocomplete',id);

        row.attr('id',id);
        row.appendTo('#dynamicTable1');        
        return false;
    });        
        
  $('.remove').on("click", function() {
  $(this).parents("tr").remove();

     var tr = $('tr').length - 2;
    alert(tr);
    document.getElementById("rowcount").value = tr;
    //alert(tr);
    var total =0;
    for (var i = 0; i < tr; i++) {
      //console.log(i);
      var myAmount = $('#amount_'+i).val();
      total +=  parseFloat(myAmount);
    }
    //alert(total);

    document.getElementById("total").value = total;

});
});


  </script>




<script type="text/javascript">
  function UpdateAmount(x) {
    //$(this.)
    //alert(x);
    var i = x.autocomplete;
    //console.log(i);
    var amount = $('#quantity_'+i).val() * $('#rate_'+i).val();
    console.log(i);
    document.getElementById("amount_"+i).value = amount;

    var tr = $('tr').length - 2;
    //$('#rowcount').val() = tr;
    document.getElementById("rowcount").value = tr;
    //alert(tr);
    var total =0;
    //alert($('.amount').val());
    for (var i = 0; i < tr; i++) {
      
      var myAmount = $('#amount_'+i).val();
      //console.log(myAmount);
      total +=  parseFloat(myAmount);
    }
    //alert(total);

    document.getElementById("total").value = total;
    //$( "#total" ).html( "iuhe" );


  }
</script>

<script type="text/javascript">
  $("#account_id").on('change', function () {
    
  });
</script>



@endsection




