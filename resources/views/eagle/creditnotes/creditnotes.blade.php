@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-10">
                    <div class="block-header">
                        <h2>Credit Notes</h2>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{url('createcreditnote')}}"  class="btn btn-primary btn-block waves-effect m-r-20"><i class="small material-icons">add</i> New Credit Notes</a>
                </div>
            </div>
        </div>
        <div class="body">
            <div id="table-datatables">
              <div class="row">
                <div class="col-md-12">
                   <table class="table table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>Credit Note #</th>
                            <th>Date</th>
                            
                            <th>Reference #</th>
                            
                            <th>Customer Name</th>
                            <th>Status</th>
                            
                            <th>Amount</th>
                           
                           
                           
                        </tr>
                    </thead>
                 
                  
                 
                    <tbody>
                    @foreach($creditnotes as $inv)
                        <tr  style="cursor: pointer" onclick="GetPayment({{$inv->id}});">
                           <td>{{$inv->credit_note_no}}</td>
                           <td>{{$inv->creditnote_date}}</td>
                           <td>{{$inv->reference_no}}</td>
                          <td>
                           <?php 
                            $customer = App\Models\Customer::find($inv->customer_id);
                            ?>

                           {{$customer->salutation}} {{$customer->customer_first_name}} {{$customer->customer_last_name}}</td>
                           <td>{{$inv->status}}</td>
                           
                           <td>{{$inv->creditnote_amount}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                  </table>


                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');
    })
</script>


<script type="text/javascript">
    function GetPayment(id) {
        $('.tr').css( 'cursor', 'pointer' );
        window.location.href = '{{url('estimatedetails')}}';
    }
</script>
@endsection