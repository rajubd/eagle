<!--DataTables example-->
           <div class="">
               <div class="body">
                    <div id="table-datatables">
              
              <div class="row">
                <div class="col-md-12">
                  

                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                    <thead>
                        <tr>
                            <th>Name</th>
                           
                            <th>Action</th>
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($designations as $designation)
                        <tr>
                            <td>{{ $designation->designation_name }}</td>
                           
                            <td>
                                
                                <a href="{{url('editdesignation/'.$designation->id)}}"><i class="more_edit"></i>Edit</a> | 
                                <a href="{{url('deletedesignation/'.$designation->id)}}"><i class=" material-icons"></i>Delete</a>
                            </td>
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>
                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
               </div>
           </div>