

@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="header">
            <div class="">

            <div class="row">
                    <div class="col-md-6">
                    <h4 class="blue-text text-darken-2">Designations</h4>
                </div>
                <div class="col-md-6">
                     <div align="right">
                    <a class=" btn btn-warning" href="{{url('/createdesignation')}}"><i class="material-icons">add</i> Add Designation</a>
                </div>
                </div>
               </div>

                
                
            </div>
        </div>
        <div class="body">
           
        
            <div class="row">
                @include('../eagle/designations/designationTable')
            </div>
        
       
   
        </div>
    </div>
</div>
@endsection






