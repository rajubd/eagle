@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
       
        <div class="col-md-6 col-md-offset-3 ">
                  <div class="card">
                    <h4 class="header">New Level</h4>
                    <div class="row clearfix body">
                      <form class="col s12" action="{{url('createlevel')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}


                       <div class="row clearfix">
                         <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="level">
                                            <label class="form-label">Level Name</label>
                                        </div>
                                    </div>
                                </div>
                               <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control"  name="remarks">
                                            <label class="form-label">Remarks</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                   <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                                </div>
                       </div>

                        </div>
                      </form>
                    </div>
                  </div>
                </div>
       
       
    </div>
</div>
@endsection









