<!--DataTables example-->
            <div id="card">
              
              <div class="body">
                <div class="">
                  

                  
                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                    <thead>
                        <tr>
                            <th>Level Name</th>
                            <th>Remarks</th>
                            <th>Action</th>
                           
                        </tr>
                    </thead>
                 
                  
                 
                    <tbody>
                    @foreach($levels as $level)
                        <tr>
                            <td>{{ $level->level }}</td>
                            <td>{{ $level->remarks }}</td>
                            <td>
                                
                                <a href="{{url('editlevel/'.$level->id)}}"><i class="more_edit"></i>Edit</a> | 
                                <a href="{{url('deletelevel/'.$level->id)}}"><i class=" material-icons"></i>Delete</a>
                            </td>
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>
                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 