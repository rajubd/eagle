@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="header bg-blue">
            <div class="">

            <div class="row">
                    <div class="col-md-6">
                    <h4 class="blue-text text-darken-2">Levels</h4>
                </div>
                <div class="col-md-6">
                     <div align="right">
                    <a class=" btn btn-warning" href="{{url('/createlevel')}}"><i class="material-icons">add</i> Add Level</a>
                </div>
                </div>
               </div>


                
            </div>
        </div>
        <div class="body">
           
        
            <div class="row">
                @include('eagle/levels/levelTable')
            </div>
        
       
   
        </div>
    </div>
</div>
@endsection
