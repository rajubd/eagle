@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-8">
                    <div class="block-header">
                        <h2>Chart Of Accounts</h2>
                    </div>
                </div>
                <div class="col-md-2">
                    <a  data-toggle="modal" data-target="#newaccount" class="btn bg-indigo waves-effect btn-block waves-effect m-r-20"><i class="small material-icons">note_add</i> New Account</a>
                </div>
                 <div class="col-md-2">
                    <a  data-toggle="modal" data-target="#newtype" class="btn bg-teal waves-effect btn-block waves-effect m-r-20"><i class="small material-icons">library_add</i> New Type</a>
                </div>

                    <!-- New Account Modals  -->
                     <!-- Default Size -->
            <div class="modal fade" id="newaccount" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
        <form class="form-horizontal" name="account" action="{{url('createaccount')}}" method="post" enctype="multipart/form-data">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">New Account</h4>
                        </div>
                        <div class="modal-body">
                            <div class="">
                                <div class="body">
                            
                                <div class="row clearfix">
                                    
                                   
                                <div class="col-md-6">
                                    <label>Account Type</label>
                                    <select name="account_type_id" class="form-control " data-live-search="true"  data-toggle="tooltip" >
                                    <option selected value="">Choose One</option>
                                    @foreach($types as $type)
                                        <option value="{{$type->id}}">{{$type->account_type}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <i class="material-icons tool_tip"  data-toggle="tooltip" data-placement="right" title="Track special assets like goodwill and other intangible assets">live_help</i>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="account_name" type="text" class="form-control">
                                            <label class="form-label">Account Name</label>
                                        </div>
                                    </div>
                                </div>
                                   <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="account_code" type="text" class="form-control">
                                            <label class="form-label">Account Code</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="description" type="text" class="form-control">
                                            <label class="form-label">Description</label>
                                        </div>
                                    </div>
                                </div>
                                
                               
                                   
                                </div>
                            
                                </div>
                            </div>                   
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success waves-effect">SAVE</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                        </div>

</form> 

                    </div>
                </div>
            </div>








                                <!-- New Type Modals  -->
                     <!-- Default Size -->
            <div class="modal fade" id="newtype" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
        <form class="form-horizontal" name="account" action="{{url('createaccounttype')}}" method="post" enctype="multipart/form-data">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">New Type</h4>
                        </div>
                        <div class="modal-body">
                            <div class="">
                                <div class="body">
                            
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                   
                               
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="account_type" type="text" class="form-control">
                                            <label class="form-label">Account Type</label>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-12">
                                   <div class="form-group form-float">
                                    <label class="form-label">Account Status</label>
                                    <div class="form-line">
                                        <select name="account_status" class="form-control" data-live-search="true"  data-toggle="tooltip">
                                            <option selected value="">Choose one</option>
                                            <option value="0">Debit</option>
                                            <option value="1">Credit</option>
                                        </select>
                                    </div>
                                   </div>
                                </div>
                                   
                               <!-- 
                               <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="description" type="text" class="form-control">
                                            <label class="form-label">Description</label>
                                        </div>
                                    </div>
                                </div> 
                                -->
                                
                               
                                    </div>
                                </div>
                            
                                </div>
                            </div>                   
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success waves-effect">SAVE</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                        </div>

</form> 

                    </div>
                </div>
            </div>

            </div>
        </div>
        <div class="body">
              <div id="table-datatables">
              
              <div class="row">
                <div class="col-md-12">
                  

                  
                   <table class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>Account Name</th>
                            <th>Account Code</th>
                            <th>Type</th>
                          <!--   <th>Action</th> -->
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($accounts as $account)
                        <tr>
                            <td><a data-toggle="modal" data-target="#acc{{$account->id}}" href="">{{ $account->sales_account_name }}</a></td>


                    <!-- New Account Modals  -->
                     <!-- Default Size -->
            <div class="modal fade" id="acc{{$account->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
        <form class="form-horizontal" name="account" action="{{url('editaccount/'.$account->id)}}" method="post" enctype="multipart/form-data">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">New Account</h4>
                        </div>
                        <div class="modal-body">
                            <div class="">
                                <div class="body">
                            
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                   
                                <div class="col-md-6">
                                    <label>Account Type</label>
                                    <select name="account_type_id" class="form-control " data-live-search="true"  data-toggle="tooltip" >
                                    <?php 
                                    $thistype = App\Models\Account_type::find($account->sales_account_type);
                                     ?>
                                    <option selected value="{{$account->sales_account_type}}">{{$thistype->account_type}}</option>
                                    @foreach($types as $type)

                                        @if($type->id != $account->sales_account_type)
                                        <option value="{{$type->id}}">{{$type->account_type}}</option>
                                        @endif
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <i class="material-icons tool_tip"  data-toggle="tooltip" data-placement="right" title="Track special assets like goodwill and other intangible assets">live_help</i>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="account_name" type="text" class="form-control" value="{{$account->sales_account_name}}">
                                            <label class="form-label">Account Name</label>
                                        </div>
                                    </div>
                                </div>
                                   <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="account_code" type="text" class="form-control" value="{{$account->sales_account_code}}">
                                            <label class="form-label">Account Code</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="description" type="text" class="form-control" value="{{$account->sales_account_description}}">
                                            <label class="form-label">Description</label>
                                        </div>
                                    </div>
                                </div>
                                
                               
                                    </div>
                                </div>
                            
                                </div>
                            </div>                   
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success waves-effect">SAVE</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                        </div>

</form> 

                    </div>
                </div>
            </div>





                            <td>{{ $account->sales_account_code }}</td>
                            <td>{{ $account->account_type }}</td>
                           <!--  
                            <td>
                                
                                <a href="{{url('editaccount/'.$account->id)}}" title="Edit"><i class="material-icons green">border_color</i></a> 
                                <a href="{{url('deleteaccount/'.$account->id)}}" title="Delete"><i class="material-icons red">delete</i></a>
                            </td> -->
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>
                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');
    })
</script>

@endsection