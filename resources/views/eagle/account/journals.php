@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-10">
                    <div class="block-header">
                        <h2>Account Journals</h2>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{url('createjournal')}}"  class="btn btn-primary btn-block waves-effect m-r-20"><i class="small material-icons">add</i> New Journal</a>
                </div>

    
            </div>
        </div>
        <div class="body">
              <div id="table-datatables">
              
              <div class="row">
                <div class="col-md-12">
                  

                  <!--  -->
                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');
    })
</script>

@endsection