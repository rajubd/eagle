
<div class="body" align="right">
	<h2 class="text-uppercase">estimate</h2>
	<h6>estimate ID #: {{$estimate->estimate_no}}</h6>
	Balance Due
	<h4>$ {{$estimate->due_amount}}</h4>
	<hr>
	<div class="row clearfix">
		<div class="col-md-9">Order Number:</div>
		<div class="col-md-3">{{$estimate->order_no}}</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-9">estimate Date:</div>
		<div class="col-md-3">{{$estimate->estimate_date}}</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-9">Due date:</div>
		<div class="col-md-3">{{$estimate->due_date}}</div>
	</div>

</div>

<div class="body">	
	<p>estimate from</p>
	<?php 
$customer = App\Models\Customer::find($estimate->customer_id);
 ?>
	<h4>{{$customer->salutation}} {{$customer->customer_first_name}} {{$customer->customer_last_name}}</h4>
	<address>{{$customer->customer_address}}</address>
	

<?php 
			//	$invoice_acc = App\Models\invoice_account::where('invoice_id',$invoice->id)->get();
			 ?>
	<div class="row clearfix">
		<table class="table table-striped">
			<thead>
				<tr class="bg-teal">
					<th> #</th>
					<th> Item & Description</th>
					
					<th>Qty</th>
					<th>Rate</th>
					<th>Amount</th>
					
				</tr>
			</thead>
			<tbody>
			<?php $i=1; ?>
			@foreach($est_items as $estimate)
			<?php 
				$item = App\Models\Item::find($estimate->item_id);
			 ?>
			<tr>
			<td>{{ $i++ }}</td>
				<td>{{$item->item_name}}<br><small>{{$item->item_description}}</small></td>
				
				<td>{{$estimate->qty}}</td>
				<td>{{$estimate->rate}}</td>
				<td>{{$estimate->amount}}</td>
			</tr>
			@endforeach
			
			</tbody>
		</table>
	</div>
</div>