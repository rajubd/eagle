@extends('layouts.main')

@section('content')

<div class="row clearfix">
    <div class="col-md-4">
        <div class="block-header">
            All Estimates
        </div>

@include('../eagle/estimates/estimate-list')
                   
       
    </div>
    <div class="col-md-8">
        <div class="block-header">Estimate Details</div>
        
        <div class="card">
          <div class="body">
            <a  class="btn btn-warning btn-lg" target="_blank" href="#" onclick="return xepOnline.Formatter.Format('printTable');">
             <i class="small material-icons">print</i> Print
          </a>
           <a  class="btn btn-primary btn-lg"  href="#" onclick="return xepOnline.Formatter.Format('printTable',{render:'download'});">
             <i class="small material-icons"> description</i> Pdf
          </a>
              
           <button class="btn btn-success btn-lg"><i class="small material-icons">mode_edit</i> Edit </button>
           <button class="btn btn-danger btn-lg"><i class="small material-icons">delete</i> Delete </button>
        </div>
        </div>

        <div class="card" id="printTable">
            <div class="body">
               
                <div class="estimate"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    $(".estimate").load('{{ URL::to('loadestimatedata1')}}');
  });
</script>


<script type="text/javascript">
   $('.ls').on('click', function () {
      var id = jQuery(this).attr("id");
     // alert("hy");
     
      $(".estimate").load('{{ URL::to('loadestimatedata')}}'+'/'+id);
      });

   </script>




  

@endsection