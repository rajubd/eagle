@extends('layouts.addrowlayout')

@section('content')
<style type="text/css">
  label{
    color: #000000;
  }
</style>


<div class="">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header bg-blue"><h4 align=""> New Payment</h4></div>
        <div class="body">
           <form class="form-horizontal" action="{{url('paymentmade')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
            
                       <div class="row clearfix">
                         <div class="col-md-6">

                         
                         
      

                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vendor</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control  chosen-select"  data-live-search="true" name="vendor_id" id="vendor">
                                <option value="" selected>Choose Vendor</option>
                                @foreach($vendors as $vendor)
                                <option value="{{$vendor->id}}">{{$vendor->vendor_first_name}} {{$vendor->vendor_last_name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>




                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Amount</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="full_amount" name="full_amount">
                          </div>
                            
                           <div id="check">
                              <input type="checkbox" class="form-control" id="usr">
                            <label for="usr" id="am"></label>
                           </div>
                          
                          </div>
                        </div>

      
                          
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Payment Date</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="date" name="payment_date">
                          </div>
                          </div>
                        </div>




                       


                         </div>
                         <div class="col-md-6">
                                                  
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Reference</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="" name="reference">
                          </div>
                          </div>
                        </div>
                        
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Payment Mode</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control chosen-select" data-live-search="true" name="payment_mode">
                                <option value="">Choose One</option>
                                <option value="Cash">Cash</option>
                                <option value="Card">Card</option> 
                              </select>
                            </div>
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Paid Through</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control  chosen-select" data-live-search="true" name="paid_through">
                                 <option value="">Choose One</option>
                                <option value="Cash">Cash</option>
                                <option value="Card">Card</option> 
                               
                              </select>
                            </div>
                          </div>
                        </div>

                       

                        
                       

                         </div>

                       </div>

                       <div class="row clearfix">
                         <div id="bills"></div>
                       </div>

 
                     
<div class="row clearfix">
   <div class="col-md-2 col-md-offset-10">
        <div class="form-group">
            <div class="form-line" align="right">
            
            <button type="submit" id="" name="submit" class="btn btn-success btn-block" >SUBMIT <i class="material-icons">input</i> </button>
             
            </div>
        </div>
    </div>
</div>




            </form>
        </div>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    $('#check').hide();
  })
</script>
              


<script type="text/javascript">
  $(function() {
  $('.dropdown-menu a').click(function() {
    console.log($(this).attr('data-value'));
    $(this).closest('.dropdown').find('input.countrycode')
      .val( $(this).attr('data-value') );
  });
});
</script>





<script type="text/javascript">
  
$('#vendor').on('change', function(e){
      // e.preventDefault()
        vendor_id = $('#vendor').val();
       
        $("#bills").load('{{ URL::to('loadvendorbills')}}'+'/'+vendor_id);
        

    $.ajax({
    type: "get", // HTTP method POST or GET
    url: "{{ URL::to('loadbilldata')}}"+"/"+vendor_id, 

    success: function(data) {
      $('#check').show();
      //document.getElementById('am').value;
      //document.getelementbyid('am').value = "( "+data+" )";
      $('#am').text("Pay Full Amount ( "+data+" )");

      $('#usr').val(data);
      //$('#hidden_number').val(data);


    
       

    },
    error: function(xhr, ajaxOptions, thrownError) {
        //On error, we alert user
        alert(thrownError);
    },
    complete: function() {
        //alert('update success'); 
    }
});

    }); 





</script>


<script type="text/javascript">
  $('#usr').on('change', function (e) {
    if ( $(this).is(':checked')) {
      var fa = $('#usr').val();  
    }
    else{
      var fa = '';
    }
    
    $('#full_amount').val(fa) ; 
  })
</script>


@endsection













