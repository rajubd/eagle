<div class="">
  
  <ul class="list-group">
   @foreach($payments as $payment)
   <li id="{{$payment->id}}" class="list-group-item ls" style="cursor: pointer;">
      <div class="row">
           <div class="col-md-6">
               {{$payment->vendor_id}}<br>
               {{$payment->payment_date}}<br>
               {{$payment->bills}}
           </div>
           <div class="col-md-6" align="right">
               {{$payment->amount}}<br>
               {{$payment->payment_mode}}
           </div>
       </div>
   </li>
  
   @endforeach
  </ul>
</div>

