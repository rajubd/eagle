@extends('layouts.main')

@section('content')

<div class="row clearfix">
    <div class="col-md-4">
        <div class="block-header">
            All Payments
        </div>

@include('../eagle/payment/payment-list')
                   
       <!--  @foreach($payments as $payment)
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-md-6">
                        {{$payment->vendor_id}}<br>
                        {{$payment->payment_date}}<br>
                        {{$payment->bills}}
                    </div>
                    <div class="col-md-6" align="right">
                        {{$payment->amount}}<br>
                        {{$payment->payment_mode}}
                    </div>
                </div>
            </div>
        </div>
        @endforeach -->
    </div>
    <div class="col-md-8">
        <div class="block-header">Payment Details</div>
        
        <div class="card">
          <div class="body">
          
           <a  class="btn btn-warning btn-lg" target="_blank" href="#" onclick="return xepOnline.Formatter.Format('printTable');">
             <i class="small material-icons">print</i> Print
          </a>
           <a  class="btn btn-primary btn-lg"  href="#" onclick="return xepOnline.Formatter.Format('printTable',{render:'download'});">
             <i class="small material-icons"> description</i> Pdf
          </a>
           <!-- <button class="btn btn-warning btn-lg"><i class="small material-icons">description</i> Pdf </button> -->
           <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#editdetails"><i class="small material-icons">mode_edit</i> Edit </button>
           <button class="btn btn-danger btn-lg"><i class="small material-icons">delete</i> Delete </button>
        </div>
        </div>

        <div class="card" id="printTable">
            <div class="body">
                <div class="payment1">
                <?php 

                $payment = App\Models\Payment::first(); ?>
                    @include('../eagle/payment/loadpaymentdata')
                </div>
                <div class="payment"></div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="payment_id" id="payment_id">




<!-- Modal Edit -->

  <!-- Small Size -->
            <div class="modal fade" id="editdetails" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
  <form class="" name="myform" action="{{url('')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}

                        <div class="modal-header">
                            <h4 class="modal-title" id="smallModalLabel">Edit Payment</h4>
                        </div>
                        <div class="modal-body">
                                  
                       <div class="row clearfix">
                         <div class="col-md-6">

                         
                         
      

                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vendor</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control "  data-live-search="true" name="vendor_id" id="vendor">
                                <option value="" selected>Choose Vendor</option>
                                @foreach($vendors as $vendor)
                                <option value="{{$vendor->id}}">{{$vendor->vendor_first_name}} {{$vendor->vendor_last_name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>




                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Amount</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="full_amount" name="full_amount">
                          </div>
                            
                           <!-- <div id="check">
                              <input type="checkbox" class="form-control" id="usr">
                            <label for="usr" id="am"></label>
                           </div> -->
                          
                          </div>
                        </div>

      
                          
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Payment Date</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="date" name="payment_date">
                          </div>
                          </div>
                        </div>




                       


                         </div>
                         <div class="col-md-6">
                                                  
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Reference</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="" name="reference">
                          </div>
                          </div>
                        </div>
                        
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Payment Mode</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control " data-live-search="true" name="payment_mode">
                                <option value="">Choose One</option>
                                <option value="Cash">Cash</option>
                                <option value="Card">Card</option> 
                              </select>
                            </div>
                          </div>
                        </div>
                        
                         
                       

                        
                       

                         </div>
                         <div class="col-md-6">
                           <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Paid Through</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control " data-live-search="true" name="paid_through">
                                 <option value="">Choose One</option>
                                <option value="Cash">Cash</option>
                                <option value="Card">Card</option> 
                               
                              </select>
                            </div>
                          </div>
                        </div>

                         </div>
                        </div>
                        <div class="modal-footer">
                        <hr>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>

 </form>

                    </div>

                </div>
            </div>




























<script type="text/javascript">
   $('.ls').on('click', function () {
      var id = jQuery(this).attr("id");
      //document.getElementById("payment_id").value() = "hy";
      //alert(id);
      document.getElementById('payment_id').value  = id;
      $(".payment1").hide();
      $(".payment").load('{{ URL::to('loadpaymentdata')}}'+'/'+id);
      });

   </script>

   <!-- 
   <script type="text/javascript">
       $(document).ready(function (e) {
           var id = $( "li" ).first().attr('id');
           console.log(id);
       })
   </script>
     -->


    
@endsection