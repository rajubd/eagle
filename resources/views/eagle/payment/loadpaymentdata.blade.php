<div class="header">
	<h4>{{$payment->vendor_id}}</h4>
</div>
<div class="body" align="center">
	<h4 class="text-uppercase">Payments Made</h4>
</div>

<div class="body">	
	<div class="row clearfix">
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-5">Payment Date</div>
			<div class="col-md-7">{{$payment->payment_date}}</div>
			</div>
			<div class="row">
				<div class="col-md-5">Reference Number</div>
			<div class="col-md-7">{{$payment->reference}}</div>
			</div>
			<div class="row">
				<div class="col-md-5">Payment Date</div>
				<div class="col-md-7">{{$payment->payment_date}}</div>
			</div>
			<div class="row">
				<div class="col-md-5">Paid To</div>
				<div class="col-md-7">{{$payment->vendor_id}}</div>
			</div>
<div class="row">
				<div class="col-md-5">Payment Mode</div>
				<div class="col-md-7">{{$payment->payment_mode}}</div>
			</div>
<div class="row">
				<div class="col-md-5">Paid Through</div>
				<div class="col-md-7">{{$payment->paid_through}}</div>
			</div>


		</div>



		<div class="col-md-4 ">
			<div class="card bg-green">
				<div class="body" align="center">
					<span style="color: #ffffff;">
						Amount Paid <br>
						<span style="font-size: 20px;">
							$ {{$payment->amount}}
						</span>
					</span>
				</div>
			</div>
		</div>
		
	</div>

<?php 
				$bills = explode(",",$payment->bill_ids);
			 ?>
	<div class="row clearfix">
		<table class="table table-striped">
			<thead>
				<tr class="bg-teal">
					<th>Bill #</th>
					<th>Bill Date</th>
					<th>Bill Amount</th>
					<th>Due Amount</th>
					
				</tr>
			</thead>
			<tbody>
			
			 @for($i=0; $i < sizeof($bills);$i++)
				<?php 
					$bill = App\Models\Bill::find($bills[$i]);
				 ?>
				<tr>
					<td>{{ $bill->id }}</td>
					<td>{{ $bill->bill_date }}</td>
					<td>{{ $bill->total }}</td>
					<td>{{ $bill->amount_due }}</td>
					
					
				</tr>
			 @endfor
			</tbody>
		</table>
	</div>
</div>