


<table class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Bill</th>

                            <th>Amount</th>
                            
                            <th>Due Amount</th>
                            <th>Payment</th>
                            
                            
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($bills as $bill)
                        <tr>
                           
                            <td>{{ substr($bill->bill_date,0,10) }}</td>
                            <td>{{ $bill->bill_no }}</td>

                            <td>{{ $bill->total }}</td>
                            
                            <td>{{ $bill->amount_due }}</td>
                            <td>
                              <input class="form-control"  type="number" placeholder="" id="" name="payment_amount[]" onchange="(getAmount();)" />
                            </td>
                     
                            
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>