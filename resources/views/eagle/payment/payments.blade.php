@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-10">
                    <div class="block-header">
                        <h2>All Payments</h2>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{url('paymentmade')}}"  class="btn btn-primary btn-block waves-effect m-r-20"><i class="small material-icons">add</i> Payment Made</a>
                </div>
            </div>
        </div>
        <div class="body">
            <div id="table-datatables">
              <div class="row">
                <div class="col-md-12">
                   <table class="table table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                        <th>Payment #</th>
                            <th>Date</th>
                            <th>Bill</th>
                            
                            <th>Vendor Name</th>
                            <th>reference</th>
                            <th>Payment Mode</th>
                            <th>Amount</th>
                            <th>Unused Amount</th>
                           
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($payments as $payment)
                        <tr  style="cursor: pointer" onclick="GetPayment({{$payment->id}});">
                            <td>{{$payment->id}}</td>
                            <td>{{$payment->payment_date}}</td>
                            <td>{{$payment->bills}}</td>
                            
                            <td>{{$payment->vendor_id}}</td>
                            <td>{{$payment->reference}}</td>
                            <td>{{$payment->payment_mode}}</td>
                            <td>{{$payment->amount}}</td>
                            <td>{{$payment->unused_amount}}</td>
                           
                         
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>


                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');
    })
</script>


<script type="text/javascript">
    function GetPayment(id) {
        $('.tr').css( 'cursor', 'pointer' );
        window.location.href = '{{url('paymentdetails')}}';
    }
</script>
@endsection