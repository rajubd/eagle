@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-10">
                    <div class="block-header">
                        <h2>Payments Received</h2>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{url('createpaymentsreceived')}}"  class="btn btn-primary btn-block waves-effect m-r-20"><i class="small material-icons">add</i> Add New</a>
                </div>
            </div>
        </div>
        <div class="body">
            <div id="table-datatables">
              <div class="row">
                <div class="col-md-12">
                   <table class="table table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                        <th>Payment #</th>
                            <th>Date</th>
                            <th>Type</th>
                            
                            <th>Reference</th>
                            <th>Customer Name</th>
                            <!-- <th>Invoice</th> -->
                            <th>Payment Mode</th>
                            <th>Amount</th>
                            <th>Unused Amount</th>
                           
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                   <!--  -->
                   @foreach($received as $rec)
                   <tr >
                       <td>{{$rec->id}}</td>
                       <td>{{$rec->payment_date}}</td>
                       <td>Invoice Payment</td>
                       <td>{{$rec->reference}}</td>
                       <td>
                       <?php 
                       $name = App\Models\Customer::find($rec->customer_id);
                        ?>
                       {{$name->customer_first_name}} {{$name->customer_last_name}}
                       </td>
                       <!-- <td>---</td> -->
                       <td>
                        <?php 
                       $mode = App\Models\Payment_mode::find($rec->payment_mode);
                        ?>
                       {{$mode['mode']}}</td>
                       <td>{{$rec->amount}}</td>
                       <td>{{$rec->unused_amount}}</td>
                   </tr>
                   @endforeach
                       
                    </tbody>
                  </table>


                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');
    })
</script>


<script type="text/javascript">
    function GetPayment(id) {
        $('.tr').css( 'cursor', 'pointer' );
        window.location.href = '{{url('paymentdetails')}}';
    }
</script>
@endsection