


<table class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Invoice</th>

                            <th>Amount</th>
                            
                            <th>Due Amount</th>
                            <th>Payment</th>
                            
                            
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($invoices as $invoice)
                        <tr>
                           
                            <td>{{ substr($invoice->invoice_date,0,10) }}</td>
                            <td>{{ $invoice->invoice_no }}</td>

                            <td>{{ $invoice->invoice_amount }}</td>
                            
                            <td>{{ $invoice->due_amount }}</td>
                            <td>
                              <input class="form-control"  type="number" placeholder="" id="" name="payment_amount[]" onchange="(getAmount();)" />
                            </td>
                     
                            
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>