<div class="header">
	<h4>{{$bill->vendor_id}}</h4>
</div>
<div class="body" align="right">
	<h2 class="text-uppercase">Bill</h2>
	<h6>Bill #: {{$bill->bill_no}}</h6>
	Balance Due
	<h4>$ {{$bill->total}}</h4>
	<hr>
	<div class="row clearfix">
		<div class="col-md-9">Order Number:</div>
		<div class="col-md-3">{{$bill->order_no}}</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-9">Bill Date:</div>
		<div class="col-md-3">{{$bill->bill_date}}</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-9">Due date:</div>
		<div class="col-md-3">{{$bill->due_date}}</div>
	</div>

</div>

<div class="body">	
	<p>Bill from</p>
	<p>{{$bill->vendor_id}}</p>
	

<?php 
				$bill_acc = App\Models\Bill_account::where('bill_id',$bill->id)->get();
			 ?>
	<div class="row clearfix">
		<table class="table table-striped">
			<thead>
				<tr class="bg-teal">
					<th> #</th>
					<th>Description</th>
					<th>Qty</th>
					<th>Rate</th>
					<th>Amount</th>
					
				</tr>
			</thead>
			<tbody>
			<?php $i=1; ?>
			@foreach($bill_acc as $acc)
				<tr>
					<td>{{$i++}}</td>
					<td>{{$acc->acc_description}}</td>
					<td>{{$acc->quantity}}</td>
					<td>{{$acc->rate}}</td>
					<td>{{$acc->amount}}</td>
				</tr>
			@endforeach
			
			</tbody>
		</table>
	</div>
</div>