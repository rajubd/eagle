<div class="">
  
  <ul class="list-group">
   @foreach($bills as $bill)
   <li id="{{$bill->id}}" class="list-group-item ls" style="cursor: pointer;">
      <div class="row">
           <div class="col-md-6">
               {{$bill->vendor_id}}<br>
               {{$bill->bill_no}}<br>
               {{$bill->bill_date}}
           </div>
           <div class="col-md-6" align="right">
               {{$bill->total}}<br>
               {{$bill->bill_status}}
           </div>
       </div>
   </li>
  
   @endforeach
  </ul>
</div>

