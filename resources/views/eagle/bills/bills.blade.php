@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-10">
                    <div class="block-header">
                        <h2>All Bills</h2>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{url('createbill')}}"  class="btn btn-primary btn-block waves-effect m-r-20"><i class="small material-icons">add</i> New Bill</a>
                </div>
            </div>
        </div>
        <div class="body">
            <div id="table-datatables">
              <div class="row">
                <div class="col-md-12">
                   <table class="table table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr class="bg-teal">
                            <th>Date</th>
                            <th>Bill</th>
                            <th>Order Number</th>
                            <th>Vendor Name</th>
                            <th>Status</th>
                            <th>Due Date</th>
                            <th>Amount</th>
                            <th>Balance Due</th>
                          <!--   <th>Action</th> -->
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($bills as $bill)
                        <tr style="cursor: pointer" onclick="GetPayment({{$bill->id}});">
                           
                            <td>{{ substr($bill->bill_date, 0,10) }}</td>
                            <td>{{ $bill->bill_no }}</td>
                            <td>{{ $bill->order_no }}</td>
                            <td>{{ $bill->vendor_id }}</td>
                            <td>{{ $bill->bill_status }}</td>
                            
                            <td>{{ $bill->due_date }}</td>
                            <td>{{ $bill->total }}</td>
                            <td>{{ $bill->amount_due }}</td>
                            
                        <!--   
                            <td>
                                
                                <a href="{{url('editbill/'.$bill->id)}}"><i class="material-icons">border_color</i></a> 
                                <a href="{{url('deletebill/'.$bill->id)}}"><i class=" material-icons">delete</i></a>
                            </td> 
                        -->
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>


                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');
    })
</script>



<script type="text/javascript">
    function GetPayment(id) {
        $('.tr').css( 'cursor', 'pointer' );
        window.location.href = '{{url('billdetails')}}';
    }
</script>


@endsection