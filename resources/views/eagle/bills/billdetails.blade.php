@extends('layouts.main')

@section('content')

<div class="row clearfix">
    <div class="col-md-4">
        <div class="block-header">
            All Bills
        </div>

@include('../eagle/bills/bill-list')
                   
     
    </div>
    <div class="col-md-8">
        <div class="block-header">Bill Details</div>
        
        <div class="card">
          <div class="body">
            <a  class="btn btn-warning btn-lg" target="_blank" href="#" onclick="return xepOnline.Formatter.Format('printTable');">
             <i class="small material-icons">print</i> Print
          </a>
           <a  class="btn btn-primary btn-lg"  href="#" onclick="return xepOnline.Formatter.Format('printTable',{render:'download'});">
             <i class="small material-icons"> description</i> Pdf
          </a>
              
           <button class="btn btn-success btn-lg"><i class="small material-icons">mode_edit</i> Edit </button>
           <button class="btn btn-danger btn-lg"><i class="small material-icons">delete</i> Delete </button>
        </div>
        </div>

        <div class="card" id="printTable">
            <div class="body">
                <div class="bill1">
                <?php 

                $bill  = App\Models\Bill::first(); ?>
                    @include('../eagle/bills/loadbilldetails')
                </div>
                <div class="bill"></div>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
   $('.ls').on('click', function () {
      var id = jQuery(this).attr("id");
     // alert("hy");
      $(".bill1").hide();
      $(".bill").load('{{ URL::to('loadbilldetails')}}'+'/'+id);
      });

   </script>

   <!-- <script type="text/javascript">
       $(document).ready(function (e) {
           var id = $( "li" ).first().attr('id');
           console.log(id);
       })
   </script>
     -->


     <script type="text/javascript">
       function printData()
        {
           var divToPrint=document.getElementById("printTable");
           newWin= window.open("");
           newWin.document.write(divToPrint.outerHTML);
           newWin.print();
           newWin.close();
        }

        $('button').on('click',function(){
        printData();
        })
     </script>

@endsection