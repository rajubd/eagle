@extends('layouts.main')

@section('content')

    <div class="">
        <div class="container-fluid">
            <div class="card">
               <div class="body">
              
                   
                   <a href="{{url('createitem')}}" 
                    class="btn btn-primary">
                    <i class="material-icons">add</i>New Item</a>
                   
                    
                    
               </div>
            </div>
            <div class="card">
              <div class="header">
                Items
              </div>
              <div class="body">
                
                 <!--  <table id="example" class="display nowrap" cellspacing="0" width="100%"> -->
                  <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                    <thead>
                        <tr>
                           
                            <th>Item Name</th>
                            
                            <th>SKU</th>
                            <th>Opening Stock</th>
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                        <td>{{ $item->item_name }}</td>
                        <td>{{ $item->sku }}</td>
                        <td>{{ $item->opening_stock_value}}</td>
                           
                            
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>
              </div>
            </div>
        </div>
    </div>

@endsection
