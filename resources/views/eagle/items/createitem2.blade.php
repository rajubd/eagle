@extends('layouts.main')

@section('content')
<style type="text/css">
  label{
    color: #000000;
  }
</style>
 <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
     <script src="{{asset('js/jquery-editable-select.js')}}"></script>
     <script type="text/javascript">
        $('#editable-select1').editableSelect();
    </script>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header">New Item</div>
        <div class="body">
           <form class="form-horizontal" action="{{url('createitem')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}


<div class="row">
  <div class="col-md-7">

    <div class="form-group">
       <label class="control-label col-sm-3" for="email">Type</label>
                               <input name="group1" type="radio" checked id="test1" />
                      <label for="test1">Product</label>
                               <input name="group1" type="radio" id="test2" />
                      <label for="test2">Service</label>


    </div>

    <div class="form-group">
      <label class="control-label col-sm-3" for="email">Item Name</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="item_name" required>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3" for="email">SKU</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="sku">
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3" for="email">Unit</label>
      <div class="col-sm-9">
           <div class="">
                            <div class="input-group dropdown">
                            
                             <div class="form-line">
                                <input type="text" name="unit_name" class="form-control countrycode dropdown-toggle" value="">
                             </div>
                              <ul class="dropdown-menu">
                              @foreach($units as $unit)
                                <li><a data-value="{{$unit->unit_name}}">{{$unit->unit_name}}</a></li>
                              @endforeach
                              </ul>
                              <span role="button" class="input-group-addon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                            </div>
                        </div>

      </div>
      
    </div>

  </div>
  <div class="col-md-5">
     <label for="input-file-now">Select Image</label>
                    <input type="file" id="input-file-now" class="dropify" name="item_image"/>
  </div>
</div>
<hr>


<div class="row row2">

   <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">Menufacturer</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="manufacturer">
        </div>
      </div>
    </div>
   </div>
 <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">UPC</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="upc">
        </div>
      </div>
    </div>
   </div>
 <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">EAN</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="ean">
        </div>
      </div>
    </div>
   </div>
 <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="">Brand Name</label>
      <div class="col-sm-9">
           <div class="input-group dropdown">
                            
             <div class="form-line">
                <input type="text" name="brand_name" class="form-control countrycode dropdown-toggle">
             </div>
              <ul class="dropdown-menu">
              @foreach($brands as $brand)
                <li><a data-value="{{$brand->brand_name}}">{{$brand->brand_name}}</a></li>
              @endforeach
              </ul>
              <span role="button" class="input-group-addon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
            </div>
      </div>
      
    </div>

   </div>

   <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">MPN</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="mpn">
        </div>
      </div>
    </div>
   </div>

   <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">ISBN</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="isbn">
        </div>
      </div>
    </div>
   </div>


</div>
<hr>



  <div class="row">

    <div class="col-md-12">
       <div class="demo-checkbox">
                            <input type="checkbox" id="test5" class="filled-in" checked />
                                  <label for="test5">Sales Information </label>
                         </div>
    </div>
    
    <div class="sal">
      <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">Selling Price</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="isbn">
        </div>
      </div>
    </div>
   </div>
    <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">Description</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="sales_description">
        </div>
      </div>
    </div>
   </div>

        <div class="col-md-6">
     
           <div class="form-group">
           <label class="control-label col-sm-3" for="email">Account</label>
             <div class="col-sm-9">
        <div class="">
          <select  data-placeholder="choose one" data-live-search="true" class="form-control purchase chosen-select" name="sales_account">
          <option value="">Choose one</option>
             @foreach($sales_accounts as $sal_acc)
             <option value="{{$sal_acc->id}}">{{$sal_acc->sales_account_name}}</option>
             @endforeach
          </select>
        </div>
      </div>
           </div>
        </div>
 <div class="col-md-6">

           <div class="form-group">
           <label class="control-label col-sm-3" for="email">Tax</label>
             <div class="col-sm-9">
                 <select data-placeholder="Choose one" data-live-search="true" class="form-control chosen-select sales" name="tax">
                 <option value="">Choose one</option>
          @foreach($taxes as $tax)
            <option value="{{ $tax->id }}">{{ $tax->tax_name }}</option>
          @endforeach
          </select>
          <br>
           <a href="" data-toggle='modal' data-target='#MyModal'>Add Tax</a>
             </div>
           </div>
        </div>

    </div>
          
  </div>


  <div class="row">
    <div class="col-md-12">
       <div class="demo-checkbox">
          <input type="checkbox" id="test6" class="filled-in" checked />
          <label for="test6">Purchase Information </label>
       </div>
    </div>
    <div class="pur">

   

     <div class="col-md-6">
     
           <div class="form-group">
           <label class="control-label col-sm-3" for="email">Perchase Account</label>
             <div class="col-sm-9">
        <div class="">
          <select  data-placeholder="Choose Acount" data-live-search="true" class="form-control purchase chosen-select" name="purchase_account">
          <option value="">Choose one</option>
             @foreach($sales_accounts as $sal_acc)
             <option value="{{$sal_acc->id}}">{{$sal_acc->sales_account_name}}</option>
             @endforeach
                               
          </select>
        </div>
      </div>
           </div>
        </div>
 <div class="col-md-6">

           <div class="form-group">
           <label class="control-label col-sm-3" for="email">Preferred Vendor</label>
             <div class="col-sm-9">
                 <select data-placeholder="Choose Vendor" data-live-search="true" class="form-control chosen-select sales" name="tax">
                 <option value="">Choose one</option>
          @foreach($vendors as $vendor)
            <option value="{{ $vendor->id }}">{{ $vendor->salutation }} {{ $vendor->vendor_first_name }} {{ $vendor->vendor_first_name }}</option>
          @endforeach
          </select>
         
             </div>
           </div>
        </div>


        <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">Pershase Price</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="isbn">
        </div>
      </div>
    </div>
   </div>

    <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">Description</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="sales_description">
        </div>
      </div>
    </div>
   </div>


    </div>


  </div>

<hr>

  <div class="row">
    <div class="col-md-12">
      <div class="demo-checkbox">
        <input type="checkbox" id="test7" class="filled-in" checked />
              <label for="test7">Tracking Information </label>
     </div>
    </div>
    <div class="tracker">
      

       <div class="col-md-6">
         <div class="form-group">
           <label class="control-label col-sm-3" for="email">Perchase Account</label>
             <div class="col-sm-9">
        <div class="">
          <select  data-placeholder="Choose Acount" data-live-search="true" class="form-control purchase chosen-select" name="tracking_account">
          <option value="">Choose one</option>
              @foreach($sales_accounts as $sal_acc)
             <option value="{{$sal_acc->id}}">{{$sal_acc->sales_account_name}}</option>
             @endforeach
                               
          </select>
        </div>
      </div>
           </div>
       </div>

            <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">Reorder Level</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="reorder_level">
        </div>
      </div>
    </div>
   </div>

    <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">Opening Stock</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="opening_stock" required>
        </div>
      </div>
    </div>
   </div>

    <div class="col-md-6">
     <div class="form-group">
      <label class="control-label col-sm-3" for="email">Opening Stock Value</label>
      <div class="col-sm-9">
        <div class="form-line">
          <input type="text" class="form-control" id="" name="opening_stock_value">
        </div>
      </div>
    </div>
   </div>

    </div>



  </div>


                     
<div class="row clearfix">
   <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-line" align="right">
                                    <input type="submit" id="" name="submit" class="btn btn-success btn-lg" >
                                </div>
                            </div>
                        </div>
</div>




            </form>
        </div>

      </div>
    </div>
  </div>
</div>


              





              <!-- Modals -->

              <!-- Tax Modal -->

<div id="MyModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add new</h4>
      </div>
      <div class="modal-body">
        <form  id="tax_form" action="{{url('createtax')}}" method="post" enctype="multipart/form-data">
           <div class="col-md-3 form-control-label">
             <label align="left">Title</label>
          </div>
          <div class="col-md-9 ">
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" id="" name="tax_name" class="form-control" >
                  </div>
              </div>
          </div>
          <div class="col-md-3 form-control-label">
             <label align="left">Amount (%)</label>
          </div>
          <div class="col-md-9 ">
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" id="" name="tax_amount" class="form-control" >
                  </div>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
         <button type="submit" id="sub" class="btn btn-primary" form="tax_form" value="Submit">Submit</button>
      </div>
    </div>

  </div>
</div>







<script type="text/javascript">
  $(document).ready(function(){
    //alert("document ready");

    $("input[id=test2]").click(function(event){
        $(".row2").hide();
    });
    $("input[id=test1]").click(function(event){
        $(".row2").show();
    });

   $("#test5").click(function () {
    if ($("#test5").is(":checked")) {
        $(".sal").show();
        //$(".tracker").show();
    }
    else {
        $(".sal").hide();
        //$(".tracker").hide();
    }
});


 $("#test6").click(function () {
    if ($("#test6").is(":checked")) {
        $(".purchase")
            .removeAttr("disabled");
        $(".pur").show();
    }
    else {
        $(".purchase")
            .attr("disabled", "disabled");
            //.css("background-color", "red");
          $(".pur").hide();
    }
});


 $("#test7").click(function () {
    if ($("#test7").is(":checked")) {
          
        $(".tr")
            .removeAttr("disabled");
        $(".tr").show();
    }
    else {
        $(".tr")
            .attr("disabled", "disabled");
            //.css("background-color", "red");
          $(".tr").hide();
    }
});


});
</script>

<script type="text/javascript">
  $(function() {
  $('.dropdown-menu a').click(function() {
    console.log($(this).attr('data-value'));
    $(this).closest('.dropdown').find('input.countrycode')
      .val( $(this).attr('data-value') );
  });
});
</script>
@endsection













