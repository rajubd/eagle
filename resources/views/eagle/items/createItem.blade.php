@extends('layouts.main')

@section('content')
<style type="text/css">
  label{
    color: #000000;
  }
</style>
 <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
     <script src="{{asset('js/jquery-editable-select.js')}}"></script>
     <script type="text/javascript">
        $('#editable-select1').editableSelect();
    </script>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header">New Item</div>
        <div class="body">
           <form class="form-horizontal" action="{{url('createitem')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <div class="row clearfix">
                         <div class="col-md-8">
                            <label for="">TYPE : </label>
                               <input name="group1" type="radio" checked id="test1" />
                      <label for="test1">Product</label>
                               <input name="group1" type="radio" id="test2" />
                      <label for="test2">Service</label>

                      <br>
                        <div class="col-md-3 form-control-label">
                           <label align="left">Item Name</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="item_name" class="form-control" name="item_name">
                                </div>
                            </div>
                        </div>

                         <div class="col-md-3 form-control-label">
                           <label align="left">SKU</label>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group">
                                 <div class="form-line">
                                    <input type="text" id="" name="sku" class="form-control" name="sku">
                                </div>
                            </div>
                        </div>

                         <div class="col-md-3 form-control-label">
                           <label align="left">Unit</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="input-group dropdown">
                            
                             <div class="form-line">
                                <input type="text" name="unit_name" class="form-control countrycode dropdown-toggle" value="">
                             </div>
                              <ul class="dropdown-menu">
                              @foreach($units as $unit)
                                <li><a data-value="{{$unit->unit_name}}">{{$unit->unit_name}}</a></li>
                              @endforeach
                              </ul>
                              <span role="button" class="input-group-addon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                            </div>
                        </div>

                         </div>
                         <div class="col-md-4">
                          <label for="input-file-now">Select Image</label>
                    <input type="file" id="input-file-now" class="dropify" name="item_image"/>
                    <br />
                         </div>

                       </div>

                       <hr>
                       <div class="row clearfix row2">
                         <div class="col-md-12">
                        <div class="col-md-3 form-control-label">
                           <label align="left">Manufacturer</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="manufacturer" class="form-control" >
                                </div>
                            </div>
                        </div>
                       <div class="col-md-3 form-control-label">
                           <label align="left">UPC</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="upc" class="form-control" >
                                </div>
                            </div>
                        </div>
                         <div class="col-md-3 form-control-label">
                           <label align="left">EAN</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="ean" class="form-control" >
                                </div>
                            </div>
                        </div>

                         
                           <div class="col-md-3 form-control-label">
                           <label align="left">Brand</label>
                        </div>
                        <div class="col-md-3 ">
                             <div class="input-group dropdown">
                            
                             <div class="form-line">
                                <input type="text" name="brand_name" class="form-control countrycode dropdown-toggle">
                             </div>
                              <ul class="dropdown-menu">
                              @foreach($brands as $brand)
                                <li><a data-value="{{$brand->brand_name}}">{{$brand->brand_name}}</a></li>
                              @endforeach
                              </ul>
                              <span role="button" class="input-group-addon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                            </div>
                        </div>
                       <div class="col-md-3 form-control-label">
                           <label align="left">MPN</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="mpn" class="form-control" >
                                </div>
                            </div>
                        </div>
                         <div class="col-md-3 form-control-label">
                           <label align="left">ISBN</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="isbn" class="form-control" >
                                </div>
                            </div>
                        </div>
                         </div>
                       </div>

                       
                       <div class="row clearfix">
                         <div class="col-md-12">
                         <div class="demo-checkbox">
                          <input type="checkbox" id="test5" class="filled-in" checked />
                                <label for="test5">Sales Information </label>
                       </div>
                       <hr>
                        <div class="sal">
                          <div class="col-md-3 form-control-label">
                           <label align="left">Selling Price (BDT)</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="selling_price" class="form-control sales" >
                                </div>
                            </div>
                        </div>
                       <div class="col-md-3 form-control-label">
                           <label align="left">Account</label>
                        </div>
                        <div class="col-md-3 ">

                           <div class="form-group">
                                 <select  data-live-search="true" class="form-control purchase" name="sales_account">
                                 @foreach($sales_accounts as $sal_acc)
                                 <option value="{{$sal_acc->id}}">{{$sal_acc->sales_account_name}}</option>
                                 @endforeach
                               
                              </select>
                            </div>
                          </div>
                        


                         <div class="col-md-3 form-control-label">
                           <label align="left">Description</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="sales_description" class="form-control sales" >
                                </div>
                            </div>
                        </div>


                           <div class="col-md-3 form-control-label">
                           <label align="left">Tax</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <select  data-live-search="true" class="form-control sales" name="tax">
                                @foreach($taxes as $tax)
                                  <option value="{{ $tax->id }}">{{ $tax->tax_name }}</option>
                                @endforeach
                                </select>
                            </div>
                            <a href="" data-toggle='modal' data-target='#MyModal'>Add Tax</a>
                            

                        </div>


                        </div>

                         </div>




          <div class="col-md-12">
         <div class="demo-checkbox">
                          <input type="checkbox" id="test6" class="filled-in" checked />
                                <label for="test6">Purchase Information </label>
                       </div>
          <hr>
          <div class="pur">
             
                       <div class="col-md-3 form-control-label">
                           <label align="left">Account</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                 <select  data-live-search="true" class="form-control purchase" name="purchase_account">
                                 @foreach($purchase_accounts as $pur_acc)
                                 <option value="{{$pur_acc->id}}">{{$pur_acc->purchase_account_name}}</option>
                                 @endforeach
                               
                              </select>
                            </div>
                        </div>
                         <div class="col-md-3 form-control-label">
                           <label align="left">Description</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="purchase_description" class="form-control purchase" >
                                </div>
                            </div>
                        </div>
                           <div class="col-md-3 form-control-label">
                           <label align="left">Preferred Vendor</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                 <select data-live-search="true" class="form-control purchase" name="preferred_vendor">
                                <option>Alfa Romeo</option>
                                <option>Audi</option>
                                <option>BMW</option>
                                <option>Citroen</option>
                              </select>
                            </div>
                        </div>
                        <div class="col-md-3 form-control-label">
                           <label align="left">Purchase Price (BDT)</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="purchase_price" class="form-control purchase">
                                </div>
                            </div>
                        </div>
          </div>
                       

                         </div>
                       </div>
                       <div class="demo-checkbox">
                          <input type="checkbox" id="test7" class="filled-in" checked />
                                <label for="test7">Tracking Information </label>
                       </div>
 <hr>
                       <div class="row clearfix tracker">
                         <div class="col-md-12 tr">
                             <div class="col-md-3 form-control-label">
                           <label align="left">Account</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                 <select  data-live-search="true" class="form-control" value="tracking_account">
                                <option>Inventory Asset</option>
                               
                              </select>
                            </div>
                        </div>
                        <div class="col-md-3 form-control-label">
                           <label align="left">Opening Stock</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="opening_stock" class="form-control" >
                                </div>
                            </div>
                        </div>

                        
                            <div class="col-md-3 form-control-label">
                           <label align="left">Reorder Level</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="reorder_level" class="form-control" >
                                </div>
                            </div>
                        </div>

                         <div class="col-md-3 form-control-label">
                           <label align="left">Opening Stock Value</label>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="opening_stock_value" class="form-control" >
                                </div>
                            </div>
                        </div>

                       
                         </div>
                       </div>
                     
<div class="row clearfix">
   <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-line" align="right">
                                    <input type="submit" id="" name="submit" class="btn btn-success btn-lg" >
                                </div>
                            </div>
                        </div>
</div>




            </form>
        </div>

      </div>
    </div>
  </div>
</div>


              





              <!-- Modals -->

              <!-- Tax Modal -->

<div id="MyModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add new</h4>
      </div>
      <div class="modal-body">
        <form  id="tax_form" action="{{url('createtax')}}" method="post" enctype="multipart/form-data">
           <div class="col-md-3 form-control-label">
             <label align="left">Title</label>
          </div>
          <div class="col-md-9 ">
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" id="" name="tax_name" class="form-control" >
                  </div>
              </div>
          </div>
          <div class="col-md-3 form-control-label">
             <label align="left">Amount (%)</label>
          </div>
          <div class="col-md-9 ">
              <div class="form-group">
                  <div class="form-line">
                      <input type="text" id="" name="tax_amount" class="form-control" >
                  </div>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
         <button type="submit" id="sub" class="btn btn-primary" form="tax_form" value="Submit">Submit</button>
      </div>
    </div>

  </div>
</div>







<script type="text/javascript">
  $(document).ready(function(){
    //alert("document ready");

    $("input[id=test2]").click(function(event){
        $(".row2").hide();
    });
    $("input[id=test1]").click(function(event){
        $(".row2").show();
    });

   $("#test5").click(function () {
    if ($("#test5").is(":checked")) {
        $(".sal").show();
        //$(".tracker").show();
    }
    else {
        $(".sal").hide();
        //$(".tracker").hide();
    }
});


 $("#test6").click(function () {
    if ($("#test6").is(":checked")) {
        $(".purchase")
            .removeAttr("disabled");
        $(".pur").show();
    }
    else {
        $(".purchase")
            .attr("disabled", "disabled");
            //.css("background-color", "red");
          $(".pur").hide();
    }
});


 $("#test7").click(function () {
    if ($("#test7").is(":checked")) {
          
        $(".tr")
            .removeAttr("disabled");
        $(".tr").show();
    }
    else {
        $(".tr")
            .attr("disabled", "disabled");
            //.css("background-color", "red");
          $(".tr").hide();
    }
});


});
</script>

<script type="text/javascript">
  $(function() {
  $('.dropdown-menu a').click(function() {
    console.log($(this).attr('data-value'));
    $(this).closest('.dropdown').find('input.countrycode')
      .val( $(this).attr('data-value') );
  });
});
</script>
@endsection













