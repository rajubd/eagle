@extends('layouts.main')

@section('content')
<div class="">
    <div class="card">
        <div class="header">
            
                
                <div class="align-right">
                	<a class="btn btn-warning" href="{{url('/createleaveplanner')}}"><i class="mdi-content-add"></i>New Leave Plan</a>
                </div>
            
        </div>

        <div class="body">
            
                @include('eagle/leaveplanners/leavePlannerTable')
            
        </div>
       
    </div>
</div>
@endsection
