@extends('layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" >

            <div class="card">
                <div class="body">
                    <h4>Add Vendor</h4>
                </div>
                <div class="body">
                    
                        <form action="{{url('createvendor')}}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <div class="row">

                            <div class="col-md-12">

                            <div class="col-md-4">
                                  <div class="col-md-4 form-control-label">
                           <label align="">Salutation</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <select name="salutation" class="form-control chosen-select">
                                        <option value="" disabled selected>Choose one</option>
                                
                                            <option value="Mr.">Mr.</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Miss.">Miss.</option>
                                       
                                    </select>
                                </div>
                            </div>
                        </div>

                            </div>



                      

                       

                             <div class="col-md-4">
                                
                        <div class="col-md-4 form-control-label">
                           <label align="">First Name*</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="vendor_first_name" class="form-control" name="item_name">
                                </div>
                            </div>
                        </div>
                            </div>


                                                    <div class="col-md-4">
                                 <div class="col-md-4 form-control-label">
                           <label align="">Last Name*</label>
                        </div>
                        <div class="col-md-8 ">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="" name="vendor_last_name" class="form-control" name="item_name">
                                </div>
                            </div>
                        </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col-md-4 form-control-label">
                                   <label align="">Company Name</label>
                                </div>
                                <div class="col-md-8 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                             <input class="form-control" id="hth" type="text" name="company_name">
                                        </div>
                                    </div>
                                </div> 
                            </div>

                             <div class="col-md-6">
                                        
                                 <div class="col-md-4 form-control-label">
                                   <label align="">Contact Email</label>
                                </div>
                                <div class="col-md-8 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input class="form-control" id="hth" type="text" name="vendor_email">
                                        </div>
                                    </div>
                                </div>

                            </div>
                                                   <div class="col-md-6">
                                <div class="col-md-4 form-control-label">
                                   <label align="">Mobile no.</label>
                                </div>
                                <div class="col-md-8 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                             <input class="form-control" id="hth" type="text" name="vendor_mobile">
                                        </div>
                                    </div>
                                </div> 
                            </div>

                             <div class="col-md-6">
                                        
                                 <div class="col-md-4 form-control-label">
                                   <label align="">Website</label>
                                </div>
                                <div class="col-md-8 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input class="form-control" id="hth" type="text" name="vendor_website">
                                        </div>
                                    </div>
                                </div>

                            </div>
                         <div class="col-md-6">
                                        
                                 <div class="col-md-4 form-control-label">
                                   <label align="">Department</label>
                                </div>
                                <div class="col-md-8 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                          
                                             <select name="vendor_designation" class="form-control chosen-select">
                                                <option value="" disabled selected>Choose one</option>
                                        
                                                @foreach($departments as $des)
                                                    <option value="{{$des->id}}">{{$des->department_name}}</option>
                                                @endforeach
                                               
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        <div class="col-md-6">
                                        
                                 <div class="col-md-4 form-control-label">
                                   <label align="">Designation</label>
                                </div>
                                <div class="col-md-8 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                          
                                             <select name="vendor_designation"  class="form-control chosen-select">
                                                <option value="" disabled selected>Choose one</option>
                                        
                                                @foreach($designations as $des)
                                                    <option value="{{$des->id}}">{{$des->designation_name}}</option>
                                                @endforeach
                                               
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        

                           
                           



                            </div>
                          

                          <!-- New Tabs -->

                          <div class="row clearfix">
                              <div class="col-md-12">
                                   <div class="">
                        <div class="header">
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_1" data-toggle="tab">Other Information</a></li>
                                        <li role="presentation"><a href="#profile_animation_1" data-toggle="tab">Address</a></li>
                                        <li role="presentation"><a href="#messages_animation_1" data-toggle="tab">Remarks</a></li>
                                        
                                    </ul>

                           
                        </div>
                        <div class="body">
                            <div class="row ">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                   
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated flipInX active" id="home_animation_1">
                                                                        <div class="row clearfix">
                               
                               

                                 <div class="col-md-4">
                                   
                                    <select class="form-control chosen-select" name="vendor_currency"  data-live-search="true">
                                        <option value="" disabled="" selected="">Choose currency</option>
                                        <option value="1">BDT</option>
                                        <option value="2">EUR</option>
                                        
                                    </select>
                                </div> 
                                <div class="col-md-4">
                                   
                                    <select class="form-control chosen-select show-tick" name="vendor_payment_terms_id"  data-live-search="true">
                                        <option value="" disabled="" selected="">Payment Terms</option>
                                        <option value="1">Term 1</option>
                                        <option value="2">Term 2</option>
                                    </select>
                                </div> 
                                <div class="col-md-4">
                                   
                                    <select class="form-control chosen-select" name="vendor_portal_language"  selected="" data-live-search="true">
                                        <option value="" disabled="">Portal Language</option>
                                        <option value="1">Bangla</option>
                                        <option value="0">English</option>
                                    </select>
                                </div>

                            </div>
                                            
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="profile_animation_1">
                                            
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <label>Attention</label>
                                        
                                        <div class="form-line">
                                            <input type="text" name="vendor_attention"  class="form-control date" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <label>Address</label>
                                        
                                        <div class="form-line">
                                            <input type="text" name="vendor_address"  class="form-control date" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="input-group">
                                    <label>City</label>
                                        
                                        <div class="form-line">
                                            <input type="text" name="vendor_city"  class="form-control date" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                  <div class="col-md-6">
                                    <div class="input-group">
                                    <label>State</label>
                                        
                                        <div class="form-line">
                                            <input type="text" name="vendor_state"  class="form-control date" placeholder="">
                                        </div>
                                    </div>
                                </div>  
                                   <div class="col-md-6">
                                    <div class="input-group">
                                    <label>Zip Code</label>
                                        
                                        <div class="form-line">
                                            <input type="text" name="vendor_zip_code"  class="form-control date" placeholder="">
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <label>Country</label>
                                        
                                        <div class="form-line">
                                            <input type="text" name="vendor_country"  class="form-control date" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                </div>

                                        <div role="tabpanel" class="tab-pane animated flipInX" id="messages_animation_1">
                                           <div class="col-md-12">
                                                <div class="form-group">
                                                <label>Remarks</label>
                                                   
                                                    <div class="form-line">
                                                    <textarea name="remarks" class="form-control"></textarea>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                 
                                 
                                        </div>
                                       
                                    </div>
                                </div>
                               
                            </div>

                            <div class="" align="right">
                                    <button class="btn btn-primary btn-lg" type="submit" name="action">Submit
                                       
                                    </button>
                                </div>



                        </div>
                    </div>
                              </div>

                              
                          </div>
     

                            


                        </form>
                </div>
            </div>










































































               
            </div>
        </div>
    </div>

@endsection
