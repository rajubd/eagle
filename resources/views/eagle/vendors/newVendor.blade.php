@extends('layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12 m10 l10 offset-m1 offset-l1" >
                <div class="card">
                    <span>Add New Customer</span>
                    <div class="body">

                        <form action="{{url('createcustomer')}}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            

                            <div class="row">

                            
                                <div class="col-md-6">
                                    <div class="row">

                               <div class="col s12 m8">
                                     <div class="input class="form-control"-field col s12 m4">
                                     <label>Salutation</label>
                                    <select name="salutation">
                                        <option value="" disabled selected>Choose one</option>
                                
                                            <option value="Mr.">Mr.</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Miss.">Miss.</option>
                                       
                                    </select>
                                    
                                </div>
                                <div class="form-group">
                                <label for="first_name">First Name*</label>
                                    <input class="form-control" id="htht" type="text" name="customer_first_name" required>
                                    
                                </div>

                                <div class="form-group">
                                <label for="last_name">Last Name</label>
                                    <input class="form-control" id="hth" type="text" name="customer_last_name">
                                    
                                </div>
                               </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                <label>Company Name</label>
                                      <input class="form-control" id="hth" type="text" name="company_name">
                                    
                                </div>

                                <div class="form-group">
                                <label>Email</label>
                                     <input class="form-control" id="hth" type="text" name="customer_email">
                                    
                                </div>

                              

                            </div>


                            <div class="row">
                              <div class="form-group">
                              <label>Phone</label>
                                      <input class="form-control" id="hth" type="text" name="customer_mobile">
                                      
                                </div>
                                <div class="form-group">
                                <label>Web Site</label>
                                     <input class="form-control" id="hth" type="text" name="customer_website">
                                      
                                </div>

                               

                            </div>

                          <div class="row">
                              <div class="form-group">
                              <label>Designation</label>
                                      <input class="form-control" id="hth" type="text" name="customer_designation">
                                      
                                </div>
                                <div class="form-group">
                                <label>Department</label>
                                     <input class="form-control" id="hth" type="text" name="customer_department">
                                      
                                </div>

                               

                            </div>


                                </div>




                            </div>










                            {{--Tabs--}}

                            <div class="row">
                                <div class="divider"></div>
                                <!--Multicolor with icon tabs-->
                                <div id="multi-color-tab" class="section">

                                    <div class="row">

                                        <div class="col s12 m12">
                                            <div class="row">
                                                <div class="col s12">
                                                    <ul class="tabs tab-demo-active z-depth-1">
                                                        <li class="tab col s3"><a class="white-text green darken-1 waves-effect waves-light active" href="#sapien1"><i class="mdi-action-perm-identity"></i> Other Details</a>
                                                        </li>
                                                        <li class="tab col s3"><a class="white-text purple darken-1 waves-effect waves-light " href="#activeone1"><i class="mdi-action-settings-display"></i> Address</a>
                                                        </li>
                                                       

                                                        <li class="tab col s3"><a class="white-text light-blue darken-1 waves-effect waves-light" href="#vestibulum1"><i class="mdi-action-speaker-notes"></i> Remarks</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col s12">
                                                    <div id="sapien1" class="col s12  lighten-3">
                                                        

                                                       


                                                       

                                                        <div class="row">
                                                            <div class="input class="form-control"-field col s4">
                                                                <select name="customer_currency">
                                                                    <option value="" disabled selected>Choose one</option>

                                                                    <option value="1">Bdt</option>
                                                                    <option value="2">Usd</option>

                                                                </select>
                                                                <label>Currency</label>
                                                            </div>
                                                            <div class="input class="form-control"-field col s4">
                                                                <select name="customer_payment_terms_id">
                                                                    <option value="" disabled selected>Choose one</option>

                                                                    <option value="1">terms 1</option>
                                                                    <option value="2">terms 2</option>


                                                                </select>
                                                                <label>Payment Terms</label>
                                                            </div>
                                                            <div class="input class="form-control"-field col s4">
                                                                <select name="customer_portal_language">
                                                                    <option value="" disabled selected>English</option>

                                                                    <option value="1">Bangla</option>
                                                                    <option value="2">Single</option>


                                                                </select>
                                                                <label>Portal Language</label>
                                                            </div>



                                                        </div>
                                                    </div>
                                                    <div id="activeone1" class="col s12  lighten-3">

                                                        <div class="row">


                                                            <div class="input class="form-control"-field col s6">
                                                                <input class="form-control" id="thth" type="text" name="customer_attention">
                                                                <label for="last_name">Attention</label>
                                                            </div>

                                                            <div class="input class="form-control"-field col s6" name="customer_address">
                                                                <input class="form-control" id="thth" type="text">
                                                                <label for="last_name">Address</label>
                                                            </div>
                                                        </div>
                                                        <div class="row">


                                                            <div class="input class="form-control"-field col s6">
                                                                <input class="form-control" id="thth" type="text" name="customer_city">
                                                                <label for="last_name">City</label>
                                                            </div>

                                                            <div class="input class="form-control"-field col s6" name="customer_state">
                                                                <input class="form-control" id="thth" type="text">
                                                                <label for="last_name">State</label>
                                                            </div>
                                                        </div>

                                                         <div class="row">


                                                            <div class="input class="form-control"-field col s6">
                                                                <input class="form-control" id="thth" type="text" name="customer_zip_code">
                                                                <label for="last_name">Zip Code</label>
                                                            </div>

                                                            <div class="input class="form-control"-field col s6" name="customer_country">
                                                                <input class="form-control" id="thth" type="text">
                                                                <label for="last_name">Country</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    

                                                    <div id="vestibulum1" class="col s12  lighten-3">
                                                        <div class="input class="form-control"-field col s12">
                                                            <input class="form-control" id="hth" type="text"  name="remarks" >
                                                            <label for="first_name"> Remarks</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="input class="form-control"-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                            </div>


                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
