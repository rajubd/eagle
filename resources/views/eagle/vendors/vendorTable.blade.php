
                 
                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Department</th>
                            <th>Country</th>
                            <th>Company</th>
                            <th>Designation</th>
                            <th>Currency</th>
                            <th>Address</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                 
                 
                    <tbody>
                    @foreach($vendors as $vendor)
                        <tr>
                           <td>{{$vendor->salutation}} {{$vendor->vendor_first_name}} {{$vendor->vendor_last_name}}</td>
                           <td>{{$vendor->vendor_department}}</td>
                           <td>{{$vendor->vendor_country}}</td>
                           <td>{{$vendor->company_name}}</td>
                           <td>{{$vendor->vendor_designation}}</td>
                           <td>{{$vendor->vendor_currency}}</td>
                           <td>{{$vendor->vendor_address}}</td>
                           <td>
                           <a href="{{url('editvendor/'.$vendor->id)}}">Edit</a> | 
                           <a href="{{url('deletevendor/'.$vendor->id)}}">Delete</a>
                           </td>
                        </tr>
                    @endforeach
                    </tbody>
                  </table>
                