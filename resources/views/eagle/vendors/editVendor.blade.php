@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12 m10 l10 offset-m1 offset-l1" >
                <div class="card-panel">
                    <span>Edit Customer</span>
                    <div class="">

                        <form action="{{url('editcustomer/'.$customer->id)}}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            

                            <div class="row">

                               <div class="col s12 m8">
                                     <div class="input-field col s12 m4">
                                    <select name="salutation">
                                        <option value="{{$customer->salutation}}"  selected>{{$customer->salutation}}</option>
                                
                                            <option value="Mr.">Mr.</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Miss.">Miss.</option>
                                       
                                    </select>
                                    <label>Salutation</label>
                                </div>
                                <div class="input-field col m4 s12">
                                    <input id="htht" type="text" name="customer_first_name" required value="{{$customer->customer_first_name}}">
                                    <label for="first_name">First Name*</label>
                                </div>

                                <div class="input-field col m4 s12">
                                    <input id="hth" type="text" name="customer_last_name" value="{{$customer->customer_last_name}}">
                                    <label for="last_name">Last Name</label>
                                </div>
                               </div>
                            </div>

                            <div class="row">
                                <div class="input-field col m4 s12">
                                      <input id="hth" type="text" name="company_name" value="{{$customer->company_name}}">
                                    <label>Company Name</label>
                                </div>

                                <div class="input-field col m4 s12">
                                     <input id="hth" type="text" name="customer_email" value="{{$customer->customer_email}}">
                                    <label>Email</label>
                                </div>

                              

                            </div>


                            <div class="row">
                              <div class="input-field col m4 s12">
                                      <input id="hth" type="text" name="customer_mobile" value="{{$customer->customer_mobile}}">
                                      <label>Phone</label>
                                </div>
                                <div class="input-field col m4 s12">
                                     <input id="hth" type="text" name="customer_website" value="{{$customer->customer_website}}">
                                      <label>Web Site</label>
                                </div>

                               

                            </div>

                          <div class="row">
                              <div class="input-field col m4 s12">
                                      <input id="hth" type="text" name="customer_designation" value="{{$customer->customer_designation}}">
                                      <label>Designation</label>
                                </div>
                                <div class="input-field col m4 s12">
                                     <input id="hth" type="text" name="customer_department" value="{{$customer->customer_department}}">
                                      <label>Department</label>
                                </div>

                               

                            </div>












                            {{--Tabs--}}

                            <div class="row">
                                <div class="divider"></div>
                                <!--Multicolor with icon tabs-->
                                <div id="multi-color-tab" class="section">

                                    <div class="row">

                                        <div class="col s12 m12">
                                            <div class="row">
                                                <div class="col s12">
                                                    <ul class="tabs tab-demo-active z-depth-1">
                                                        <li class="tab col s3"><a class="white-text green darken-1 waves-effect waves-light active" href="#sapien1"><i class="mdi-action-perm-identity"></i> Other Details</a>
                                                        </li>
                                                        <li class="tab col s3"><a class="white-text purple darken-1 waves-effect waves-light " href="#activeone1"><i class="mdi-action-settings-display"></i> Address</a>
                                                        </li>
                                                       

                                                        <li class="tab col s3"><a class="white-text light-blue darken-1 waves-effect waves-light" href="#vestibulum1"><i class="mdi-action-speaker-notes"></i> Remarks</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col s12">
                                                    <div id="sapien1" class="col s12  lighten-3">
                                                        

                                                       


                                                       

                                                        <div class="row">
                                                            <div class="input-field col s4">
                                                                <select name="customer_currency">
                                                                    <option value="{{$customer->customer_currency}}" disabled selected>{{$customer->customer_currency}}</option>

                                                                    <option value="1">Bdt</option>
                                                                    <option value="2">Usd</option>

                                                                </select>
                                                                <label>Currency</label>
                                                            </div>
                                                            <div class="input-field col s4">
                                                                <select name="customer_payment_terms_id">
                                                                    <option value="{{$customer->customer_currency}}" disabled selected>{{$customer->customer_currency}}</option>

                                                                    <option value="1">terms 1</option>
                                                                    <option value="2">terms 2</option>


                                                                </select>
                                                                <label>Payment Terms</label>
                                                            </div>
                                                            <div class="input-field col s4">
                                                                <select name="customer_portal_language">
                                                                    <option value="{{$customer->customer_portal_language}}" disabled selected>{{$customer->customer_portal_language}}</option>

                                                                    <option value="1">Bangla</option>
                                                                    <option value="2">Single</option>


                                                                </select>
                                                                <label>Portal Language</label>
                                                            </div>



                                                        </div>
                                                    </div>
                                                    <div id="activeone1" class="col s12  lighten-3">

                                                        <div class="row">


                                                            <div class="input-field col s6">
                                                                <input id="thth" type="text" name="customer_attention" value="{{$customer->customer_attention}}">
                                                                <label for="last_name">Attention</label>
                                                            </div>

                                                            <div class="input-field col s6" name="customer_address">
                                                                <input id="thth" type="text" value="{{$customer->customer_address}}">
                                                                <label for="last_name">Address</label>
                                                            </div>
                                                        </div>
                                                        <div class="row">


                                                            <div class="input-field col s6">
                                                                <input id="thth" type="text" name="customer_city" value="{{$customer->customer_city}}">
                                                                <label for="last_name">City</label>
                                                            </div>

                                                            <div class="input-field col s6" name="customer_state">
                                                                <input id="thth" type="text" value="{{$customer->customer_state}}">
                                                                <label for="last_name">State</label>
                                                            </div>
                                                        </div>

                                                         <div class="row">


                                                            <div class="input-field col s6">
                                                                <input id="thth" type="text" name="customer_zip_code" value="{{$customer->customer_zip_code}}">
                                                                <label for="last_name">Zip Code</label>
                                                            </div>

                                                            <div class="input-field col s6" name="customer_country">
                                                                <input id="thth" type="text"value="{{$customer->customer_country}}">
                                                                <label for="last_name">Country</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    

                                                    <div id="vestibulum1" class="col s12  lighten-3">
                                                        <div class="input-field col s12">
                                                            <input id="hth" type="text"  name="remarks" value="{{$customer->remarks}}">
                                                            <label for="first_name"> Remarks</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                            </div>


                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
