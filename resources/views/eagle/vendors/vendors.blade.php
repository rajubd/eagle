@extends('layouts.main')

@section('content')
<div class="">
    <div class="row">
    	

        <div class="col-md-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4>vendors</h4>
                        </div>
                        <div class="col-md-2">
                            <a class="btn btn-warning btn-block" href="{{url('/createvendor')}}">Add Vendor</a>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="body" style="overflow-x: scroll;">
                    @include('../eagle/vendors/vendorTable')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
