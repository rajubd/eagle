

@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="header">
            <div class="">
                
                <div class="align-right">
                    <a class="btn btn-warning"   href="{{url('/createcontractbase')}}">Add Contract Base</a>
                </div>
            </div>
        </div>
        <div class="body">
           
        
            <div class="row">
                 @include('eagle/contract_base/contractBaseTable')
            </div>
        
       
   
        </div>
    </div>
</div>
@endsection



