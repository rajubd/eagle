




@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
       
        <div class="col-md-6 col-md-offset-3 ">
                  <div class="card">
                    <h4 class="header">Edit Contract Base</h4>
                    <div class="row clearfix body">
                    <form class="col s12" action="{{url('editcontractbase/'.$contract_base->id)}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}

                       <div class="row clearfix">
                         <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="basename"  value="{{ $contract_base->basename }}">
                                            <label class="form-label"> Name</label>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="col-md-8 col-md-offset-2">
                                   <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                                </div>
                       </div>

                        </div>
                      </form>
                    </div>
                  </div>
                </div>
       
       
    </div>
</div>
@endsection








