




@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
       
        <div class="col-md-6 col-md-offset-3 ">
                  <div class="card">
                    <h4 class="header">Add Leave</h4>
                    <div class="row clearfix body">
                       <form class="col s12" action="{{url('editleave/'.$leave->id)}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            
                       <div class="row clearfix">
                                <div class="col-md-8 col-md-offset-2">
                                <label>Employee Id</label>
                                     <select  data-live-search="true" class="form-control " name="employee_id">
                                         @if(!empty($emp))
                                                <option value="{{$leave->employee_id}}"  selected>{{$emp->employee_code}}</option>
                                            @else
                                                <option value="">Not Selected</option>

                                            @endif
                                            @foreach($employees as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_code}}</option>
                                            @endforeach
                                           
                                       
                                    </select>
                                    
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <label class="form-label">Start date</label>
                                            <input type="date" class="form-control" name="start_date">
                                            
                                        
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <label class="form-label">End date</label>
                                            <input type="date" class="form-control" name="end_date">
                                            
                                        
                                </div>
                                 <div class="col-md-8 col-md-offset-2">
                                  
                                        <label class="form-label">Reason</label>
                                            <input type="text" class="form-control" name="reason"  value="{{$leave->reason}}">
                                            
                                       
                                </div>
                               
                                <div class="col-md-8 col-md-offset-2">
                                   <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                                </div>
                       </div>

                        </div>
                      </form>
                    </div>
                  </div>
                </div>
       
       
    </div>
</div>
@endsection













