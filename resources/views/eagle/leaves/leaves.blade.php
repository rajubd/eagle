@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
        
            <div class="card">
                
                <div class="body align-right">
                	<a class="btn btn-warning" href="{{url('/createleave')}}">New Leave</a>
                </div>
            </div>
       
            <div class="card">
                @include('eagle/leaves/leaveTable')
            </div>
        
    </div>
</div>
@endsection
