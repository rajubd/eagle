<!--DataTables example-->
          <div class="body">
                <div id="table-datatables">
              
              <div class="row">
     
                  

                 
                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                    <thead>
                        <tr>
                            <th>Employee Code</th>
                            <th>Employee Name</th>
                            <th>Leave Start</th>
                            <th>Leave End</th>
                            <th>Reason of leave</th>
                            <th>Action</th>
                           
                        </tr>
                    </thead>
                 
                    <tbody>
                    @foreach($leaves as $leave)
                        <tr>
                            <td>{{ $leave->employee_code }}</td>
                            <td>{{ $leave->first_name }} {{ $leave->last_name }}</td>
                            <td>{{ $leave->start_date }}</td>
                            <td>{{ $leave->end_date }}</td>
                            <td>{{ $leave->reason }}</td>
                            <td>
                                
                                <a href="{{url('editleave/'.$leave->id)}}"><i class="more_edit"></i>Edit</a> | 
                                <a href="{{url('deleteleave/'.$leave->id)}}"><i class=" material-icons"></i>Remove</a>
                            </td>
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>
              
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
          </div>