
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script> 


<script type="text/javascript">


 $(document).ready(function() {
    $('#example_plugins').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        
    } );

</script>
   




<!-- 
<script type="text/javascript">
    var startDate = new Date();


$('.from').datepicker({
    autoclose: true,
    minViewMode: 1,
    format: 'yyyy-mm'
}).on('changeDate', function(e){
     e.defaultPrevented();
       
        value = $('.from').val();
       
        $("#payroll").load('{{ URL::to('loadpayroll')}}'+'/'+value);
//$("#month").text('Payroll   '+value);

    $.ajax({
    type: "get", // HTTP method POST or GET
    url: "{{ URL::to('loadpayrolldata')}}"+"/"+value, 

    success: function(data) {

        //$('#manage_user table > tbody:first').append(data);
        x=[]
        $.each(data, function(i,n) {
        x.push(n);});
        v = JSON.stringify(data);
        console.log(data);

        $("#month").text('Payroll ('+value+')');

        
 $(document).ready(function() {
    $('#example_plugins').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
         ajax: ''
    } );
} );
       
    },
    error: function(xhr, ajaxOptions, thrownError) {
        //On error, we alert user
        alert(thrownError);
    },
    complete: function() {
        //alert('update success'); 
    }
});

    }); 


$( document ).ready(function() {
    var d = new Date();
     n = d.getMonth()+1;
     if(n<10)
      n = '0'+n;

    y = d.getFullYear();
    m = y+'-'+n;
    $("#month").text('Payroll ('+m+')');

$("#payroll").load('{{ URL::to('loadpayroll')}}'+'/'+m);
//$("#month").text('Payroll   '+value);

    $.ajax({
    type: "get", // HTTP method POST or GET
    url: "{{ URL::to('loadpayrolldata')}}"+"/"+m, 

    success: function(data) {

        //$('#manage_user table > tbody:first').append(data);
        //x=[]
        //$.each(data, function(i,n) {
        //x.push(n);});
        //v = JSON.stringify(data);
        console.log(data);

        $("#month").text('Payroll ('+data["0"].payroll_month+')');
       
    },
    error: function(xhr, ajaxOptions, thrownError) {
        //On error, we alert user
        alert(thrownError);
    },
    complete: function() {
        //alert('update success'); 
    }
});



});


</script> -->


 <script type="text/javascript">
     //alert('hyy');
     
 </script>
<div class="card-panel">
    <table id="example_plugins" class="display nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Employee Code</th>
            <th>Name</th>
            
            <th style="">Contract Type</th>
            <th>Contract Base</th>
            <th>OT Hour</th>
            <th>Estimated Salary</th>
            <th>Estimated OT Bill</th>
            <th>Paid Salary</th>
            <th>Paid OT Bill</th>
            <th>Action</th>
           
        </tr>
    </thead>
 
    <tfoot>
        <tr>
            <th>Employee Code</th>
            <th>Name</th>
            
            <th>Contract Type</th>
            <th>Contract Base</th>
            <th>OT Hour</th>
            <th>Estimated Salary</th>
            <th>Estimated OT Bill</th>
            <th>Paid Salary</th>
            <th>Paid OT Bill</th>
            <th>Action</th>
           
        </tr>
    </tfoot>
 
    <tbody>
    @foreach($payrolls as $level)
    
          <?php 
          $emp = App\Models\Employee::find($level->employee_id);
          $type = App\Models\ContractType::find($emp->contract_type_id);
          $base = App\Models\ContractBase::find($emp->contract_base_id);
         // echo "<pre>";
          //print_r((array)$type['typename']);
           ?>
        <tr>
            <td>{{ $emp->employee_code }}</td>
           <td>{{ $emp->first_name }} {{ $emp->last_name }}</td>
            <td>
            @if(!empty($type))
                {{ $type->typename }}
            @endif
            </td>
            <td>
            @if(!empty($base))
                {{ $base->basename }}
            @endif
           
            </td>
            <td>{{ $level->ot_hour }}</td>
            <td>{{ $level->estimated_salary }}</td>
            <td>{{ $level->estimated_ot_bill }}</td>
            <td>{{ $level->paid_salary }}</td>
            <td>{{ $level->paid_ot_bill }}</td>
            <td>

                
               <a class=" modal-trigger edit " href="#modal{{$level->id}}"><i class="mdi-editor-mode-edit"></i></a> 
 
                <a class="waves-effect delete" href="{{url('deletepayroll/'.$level->id)}}"><i class="mdi-action-delete" onclick="confirm('Are really want to delete this item ?');"></i></a>
            </td>
           <!--  @include('../eagle/payrolls/editPayrollModal') -->

            
        </tr>

         <div id="modal{{$level->id}}" class="modal">
                  <div class="modal-content ">
                    <div class="container">
                        <div class="row">
                        <h5>Edit Payroll Info</h5>
                            <div class="col m12">
                        <form class="col s12" action="{{url('editpayroll/'.$level->id)}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
                    
                          <div class="row">

                               <div class="input-field col s12 m6">
                               <label align="left">Employee Code</label>
                            <input placeholder="Month" id="" name="employee_code" type="text" required value="{{ $emp->employee_code }}" disabled>
                            
                          </div>

                                 <div class="input-field col s12 m6">
                               <label align="left">Month</label>
                            <input placeholder="Month" id="" name="payroll_month" type="text" required value="{{ $level->payroll_month }}" disabled>
                            
                          </div>

                               
                         
                        </div>
                       
                        
                        <div class="row">
                        
                         

                          <div class="input-field col s12 m6">
                            <input placeholder="" id="email2" name="ot_hour" type="text"  value="{{ $level->ot_hour }}">
                            <label for="email">OT Hour</label>
                          </div>

                        

                           <div class="input-field col s12 m6">
                            <input placeholder="" id="" name="estimated_ot_bill" type="text"  value="{{ $level->estimated_ot_bill }}">
                            <label for="email">Estimated OT Bill</label>
                          </div>
                        </div>

                        <div class="row">
                        
                         
                          <div class="input-field col s12 m4">
                            <input placeholder="" id="email2" name="paid_salary" type="text"  value="{{ $level->paid_salary }}">
                            <label for="email">Paid Salary</label>
                          </div>

                          <div class="input-field col s12 m4">
                            <input placeholder="" id="email2" name="paid_ot_bill" type="text" value="{{ $level->paid_ot_bill }}">
                            <label for="email">Paid OT Bill</label>
                          </div>

                          <div class="input-field col s12 m4">
                            <input placeholder="" id="email2" name="estimated_salary" type="text"  value="{{ $level->estimated_salary }}">
                            <label for="email">Estimated Salary</label>
                          </div>
                          
                        </div>

                      
                        
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                <i class="mdi-content-send right"></i>
                              </button>
                            </div>
                          </div>


                       
                      </form>
                            </div>
                        </div>
                    </div>
                  </div>
                  
                </div>

    @endforeach
       
    </tbody>        
</table>

</div>
 
   
