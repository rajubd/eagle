  <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
          

<style type="text/css">
    
    .saveButton,
[contenteditable] .editButton{
    display: none;
}

.editButton,
[contenteditable] .saveButton {
    display: inline; /* For IE */
    display: inline-block;
}

[contenteditable] {
    background: #f4f1d2;
    /*color:#ffffff;*/
}

</style>


<table id="elencoMezzi" class="itemList table table-bordered"  width="100%">
 <!-- <table id="" class="itemList table table-bordered table-striped table-hover dataTable js-exportable"> -->
    <thead>
     <tr>
                            <td>#</td>
                            <th>Employee Code</th>
                            <th>Name</th>
                            <th>Contract Type</th>
                            <th>Contract Base</th>
                            <th>OT Hour</th>
                            <th>Estimated Salary</th>
                            <th>Estimated OT Bill</th>
                            <th>Paid Salary</th>
                            <th>Paid OT Bill</th>
                            <th>Action</th>
                            <th style="display:none">Hide</th>
                           
                        </tr>
    </thead>
    <tbody>
    <?php  $i=1; ?>
     @foreach($payrolls as $level)
    
          <?php 
          $emp = App\Models\Employee::find($level->employee_id);
          $type = App\Models\ContractType::find($emp->contract_type_id);
          $base = App\Models\ContractBase::find($emp->contract_base_id);
         // echo "<pre>";
          //print_r((array)$type['typename']);
         
           ?>


    <tr data-id="1" class="displayRow">
            <td id="qq">{{ $i }}</td>
            <td id="emp_code{{$i}}">{{ $emp->employee_code }}</td>
           <td>{{ $emp->first_name }} {{ $emp->last_name }}</td>
            <td>
            @if(!empty($type))
                {{ $type->typename }}
            @endif
            </td>
            <td>
            @if(!empty($base))
                {{ $base->basename }}
            @endif
           
            </td>
            <td id="ot_hour{{ $i }}">{{ $level->ot_hour }}</td>
            <td id="estimated_salary{{ $i }}">{{ $level->estimated_salary }}</td>
            <td id="estimated_ot_bill{{ $i }}">{{ $level->estimated_ot_bill }}</td>
            <td id="paid_salary{{ $i }}">{{ $level->paid_salary }}</td>
            <td id="paid_ot_bill{{ $i }}">{{ $level->paid_ot_bill }}</td>
            <td>

                <!-- <a href="" class="editButton" >Edit</a> -->
                <input type="hidden" id="row_id" value="{{$level->id}}">
              <!--  
            <input type="button" value="edit" class="btn btn-info editButton" />
            <input type="button" value="save" class="btn btn-success saveButton" onclick="saveData({{ $i }})" /> -->

            <button  type="button" value="edit" class="btn btn-info editButton"><i class="material-icons Small">mode_edit</i> Edit</button>

            <button  type="button" value="save" class="btn btn-success saveButton Small" onclick="saveData({{ $i }})" ><i class="material-icons">done</i> Save</button>
          
              <!--  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal{{$level->id}}">Edit</button>
 

 
                <a class="btn btn-danger" href="{{url('deletepayroll/'.$level->id)}}"><i class="mdi-action-delete" onclick="confirm('Are really want to delete this item ?');"></i>Delete</a> -->
            </td>
            <td style="display: none" id="id{{ $i }}">{{$level->id}}</td>

      <!--   <td>
            <input type="button" value="edit" class="editButton" />
            <input type="button" value="save" class="saveButton" />
        </td> -->
    </tr>    
    <?php $i++; ?>
      @endforeach
    </tbody>
</table>
<!-- <div id="editRowHolder" style="display:none;">
    <tr class="editRow">
        <td>
            <input type="text" name="field1" />
        </td>
        <td>
            <input type="text" name="field2" />
        </td>
        <td>
            <input type="button" value="save" class="saveButton" />
        </td>
    </tr>
</div>
 -->



<script type="text/javascript">
    $(function () {
    $( 'tr' ).each( function editAndSave( index, tr ){
        var $tr = $( tr );
        
        $tr.find( 'button[type=button]' ).on( 'click', function( e ){            
            var toggle = $( e.target ).is( '.editButton' );
            
            if( toggle ){
                $tr.attr( 'contenteditable', toggle );
            }
            else {
                $tr.removeAttr( 'contenteditable' );
            }
        } )
    });
});






    //$('.saveButton').on('click', );


    function saveData(i) {
        //e.preventDefault();
        var td = $('#td'+i).text();
        var ot_hour = $('#ot_hour'+i).text();
        var estimated_salary = $('#estimated_salary'+i).text();
        var estimated_ot_bill = $('#estimated_ot_bill'+i).text();
        var paid_salary = $('#paid_salary'+i).text();
        var paid_ot_bill = $('#paid_ot_bill'+i).text();
        var id = $('#id'+i).text();

        // var myArray = [];
        // myArray[0] = ot_hour ;
        // myArray[1] = estimated_salary ;
        // myArray[2] = estimated_ot_bill ;
        // myArray[3] = paid_salary ;
        // myArray[4] = paid_ot_bill ;

         var $post = {};

         $post.ot_hour = ot_hour;
         $post.id = id;
         $post.estimated_salary = estimated_salary;
         $post.estimated_ot_bill = estimated_ot_bill;
         $post.paid_salary = paid_salary;
         $post.paid_ot_bill = paid_ot_bill;

        $.ajax({
                url: "updatepayroll",
                type: "post",
                data: $post,
                success: function(d) {
                    console.log(d);
                    alert("Updated Sucessfully");
                }
            });

       //alert(td);
    }


</script>