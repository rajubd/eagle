
             <div id="modal{{$level->id}}"  class="modal fade"  role="dialog"> 
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h5>Edit Payroll Info</h5>
                    </div>
                    <div class="modal-body">
              
                        <div class="row">
                          <div class="col-md-12">

<!-- {{url('updatepayroll/'.$level->id)}} -->
                            
<form action="{{url('updatepayroll')}}" method="post" id="nameform" name="nameform" enctype="multipart/form-data">
 
                            <div class="row clearfix">
                                              <div class="col-md-1 form-control-label">
                                                 <label align="left">Employee Code</label>
                                              </div>
                                              <div class="col-md-4 ">
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                          <input type="text" id="" name="employee_code" class="form-control" value="{{ $emp->employee_code }}" disabled>
                                                      </div>
                                                  </div>
                                              </div>
                                                <div class="col-md-1 form-control-label">
                                                  <label for="text_2">Month</label>
                                              </div>
                                              <div class="col-md-4">
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                          <input type="text" id="payroll_month" class="form-control" name="payroll_month" value="{{ $level->payroll_month }}" >
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row clearfix">
                                             <div class="col-md-1 form-control-label">
                                                  <label for="text_2">OT Hour</label>
                                              </div>
                                              <div class="col-md-4">
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                          <input type="text" id="ot_hour" class="form-control" name="ot_hour" value="{{ $level->ot_hour }}">
                                                      </div>
                                                  </div>
                                              </div>
                                               <div class="col-md-1 form-control-label">
                                                  <label for="text_2">Estimated OT Bill</label>
                                              </div>
                                              <div class="col-md-4">
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                          <input type="text" id="estimated_ot_bill" class="form-control" name="estimated_ot_bill"   value="{{ $level->estimated_ot_bill }}">
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row clearfix">
                                             <div class="col-md-1 form-control-label">
                                                  <label for="text_2">Paid Salary</label>
                                              </div>
                                              <div class="col-md-4">
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                          <input type="text" id="paid_salary" class="form-control" name="paid_salary"   value="{{ $level->paid_salary }}">
                                                      </div>
                                                  </div>
                                              </div>
                                               <div class="col-md-1 form-control-label">
                                                  <label for="text_2">Paid OT Bill</label>
                                              </div>
                                              <div class="col-md-4">
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                          <input type="text" name="paid_ot_bill" id="" class="form-control"   value="{{ $level->paid_ot_bill }}">
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row clearfix">
                                               <div class="col-md-1 form-control-label">
                                                  <label for="text_2">Estimated Salary</label>
                                              </div>
                                              <div class="col-md-4">
                                                  <div class="form-group">
                                                      <div class="form-line">
                                                          <input type="text" id="estimated_salary" class="form-control" name="estimated_salary"  value="{{ $level->estimated_salary }}">
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <input type="hidden" name="id" id="id" value="{{$level->id}}">

                                          
</form>

                          </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      
                      <button type="button" onclick="submitForm();" class="btn btn-default" id="ajax-btn">Ajax Call</button>
                      

                       <input type="submit" id="sub" class="btn btn-primary" name="submit" form="nameform" value="Submit" />
     
                     
                    </div>



                </div>
                  
              </div>



                
                  
              </div>
<?php 

if (isset($_POST['submit1'])) {

$employee_code = $_POST['employee_code'];
$payroll_month = $_POST['payroll_month'];
$ot_hour = $_POST['ot_hour'];
$estimated_ot_bill = $_POST['estimated_ot_bill'];
$paid_salary = $_POST['paid_salary'];
$paid_ot_bill = $_POST['paid_ot_bill'];
$estimated_salary = $_POST['estimated_salary'];
$id = $_POST['id'];


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "eagle";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "UPDATE payrolls SET estimated_salary='".$estimated_salary."' WHERE id='11";

if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();







}

 ?>
              
<!-- 

<script type="text/javascript">
    var frm = $('#nameform');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    });
</script> -->

<script type="text/javascript">
  function submitForm(){
    form = $(document.forms[0]);
    var url = form.attr("action");
    var formData = {};
    $(form).find("input[id]").each(function (index, node) {
        formData[node.name] = node.name;
    });
    $.post('updatepayroll', formData).done(function (data) {
        console.log(formData);
    });
}
</script>











<script type="text/javascript">
  var frm = $('#nameform');
  var estimated_salary = $('#estimated_salary').val();

    $('#ajax-btn1').on('click', function (e) {
      e.preventDefault();
    //var value = $('#nameform').serialize();
    //var value = $('#nameform').serialize();
    var value =  $(document.forms[0]).serialize();
    //$.post('updatepayroll', data);
     // console.log(value);
    $.ajax({
    type: "post", // HTTP method POST or GET
    url: "{{ URL::to('updatepayroll')}}",
    //+"/"+value, 
    data: {"str":value},
      contentType: "application/json; charset=utf-8",
        dataType: "json",

    success: function(data) {

      console.log(data);
    },
    error: function(xhr, ajaxOptions, thrownError) {
        //On error, we alert user
        alert(thrownError);
    },
    complete: function() {
        //alert('update success'); 
    }
});




    });
    //return;




</script>

<!-- 
<script type="text/javascript">
      $.ajax({
    type: "get", // HTTP method POST or GET
    url: "{{ URL::to('loadpayrolldata')}}"+"/"+value, 

    success: function(data) {

        //$('#manage_user table > tbody:first').append(data);
        x=[]
        $.each(data, function(i,n) {
        x.push(n);});
        v = JSON.stringify(data);
        console.log(data);

        $("#month").text('Payroll ('+value+')');

    //     $('#example_plugins').DataTable( {
    //     dom: 'Bfrtip',
    //     buttons: [
    //         'copy', 'csv', 'excel', 'pdf', 'print'
    //     ],
        
    // } );
       
    },
    error: function(xhr, ajaxOptions, thrownError) {
        //On error, we alert user
        alert(thrownError);
    },
    complete: function() {
        //alert('update success'); 
    }
});
</script> -->