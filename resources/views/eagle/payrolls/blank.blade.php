
            <div id="table-datatables">
              
              <div class="row">
                <div class="col s12 m12">
                  

                    <table id="example_plugins" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Employee Code</th>
                            <th>Name</th>
                            <th>Contract Type</th>
                            <th>Contract Base</th>
                            <th>OT Hour</th>
                            <th>Estimated Salary</th>
                            <th>Estimated OT Bill</th>
                            <th>Paid Salary</th>
                            <th>Paid OT Bill</th>
                            <th>Action</th>
                           
                        </tr>
                    </thead>
                 
                    <tfoot>
                        <tr>
                            <th>Employee Code</th>
                            <th>Name</th>
                            <th>Contract Type</th>
                            <th>Contract Base</th>
                            <th>OT Hour</th>
                            <th>Estimated Salary</th>
                            <th>Estimated OT Bill</th>
                            <th>Paid Salary</th>
                            <th>Paid OT Bill</th>
                            <th>Action</th>
                           
                        </tr>
                    </tfoot>
                 
                    <tbody>
                    <?php 

                     ?>
                    @foreach($levels as $level)
                    
                          <?php 
                          $emp = App\Models\Employee::find($level->employee_id);
                          $type = App\Models\ContractType::find($emp->contract_type_id);
                          $base = App\Models\ContractBase::find($emp->contract_base_id);
                         // echo "<pre>";
                          //print_r((array)$type['typename']);
                           ?>
                        <tr>
                            <td>{{ $emp->employee_code }}</td>
                            <td>{{ $emp->first_name }} {{ $emp->last_name }}</td>
                            <td>
                            @if(!empty($type))
                                {{ $type->typename }}
                            @endif
                            </td>
                            <td>
                            @if(!empty($base))
                                {{ $base->basename }}
                            @endif
                           
                            </td>
                            <td>{{ $level->ot_hour }}</td>
                            <td>{{ $level->estimated_salary }}</td>
                            <td>{{ $level->estimated_ot_bill }}</td>
                            <td>{{ $level->paid_salary }}</td>
                            <td>{{ $level->paid_ot_bill }}</td>
                            <td>

                                
                               <a class=" modal-trigger edit " href="#modal{{$level->id}}"><i class="mdi-editor-mode-edit"></i></a> 
                 
                                <a class="waves-effect delete" href="{{url('deletepayroll/'.$level->id)}}"><i class="mdi-action-delete"></i></a>
                            </td>
                            @include('../eagle/payrolls/editPayrollModal')
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>


                </div>
              </div>
            </div> 
            <br>
