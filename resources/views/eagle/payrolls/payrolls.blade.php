
@extends('layouts.main')

@section('content')

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">
     <script type="text/javascript" src="//code.jquery.com/jquery-1.11.2.min.js"></script>
   

<div class="container-fluid">


        <div class="card bg-deep-purple" >
           <div class="body">
             <div class="row">
             <div class="col-md-4">
             
    
<h5 style="color:#ffffff; font-size:20px;"><div id="month"></div></h5>

             </div>
             <div class="col-md-8">
              
  

         <div align="right">
            <!-- <a class="waves-effect waves-light btn " href="{{url('/createpayroll')}}">Add Payroll </a> -->

            <a class="btn btn-success"   data-toggle="modal" data-target="#modal">Add New Payroll</a>

         </div>
             </div>
           </div>
           </div>
                    
                @include('../eagle/payrolls/createPayrollModal')
                   
        </div>




  
    <div class="card">
        <div class="header">
            <div class="row">
        
         

                <div class="col-md-3">
               
                    <input type="text" class="form-control form-control-1 input-md from" placeholder="Select Month" id="month">
                </div>
 
            </div>
        </div>
        <div class="body">

            <div id="payroll"></div>

        </div>
    </div>


</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script> 
 

<script type="text/javascript">
    var startDate = new Date();


$('.from').datepicker({
    autoclose: true,
    minViewMode: 1,
    format: 'yyyy-mm'
}).on('changeDate', function(e){
      // e.preventDefault()
        value = $('.from').val();
       
        $("#payroll").load('{{ URL::to('loadpayroll')}}'+'/'+value);
         $("#month").text('Payroll ('+value+')');

    $.ajax({
    type: "get", // HTTP method POST or GET
    url: "{{ URL::to('loadpayrolldata')}}"+"/"+value, 

    success: function(data) {

        //$('#manage_user table > tbody:first').append(data);
        x=[]
        $.each(data, function(i,n) {
        x.push(n);});
        v = JSON.stringify(data);
        console.log(data);

       
        

    //     $('#example_plugins').DataTable( {
    //     dom: 'Bfrtip',
    //     buttons: [
    //         'copy', 'csv', 'excel', 'pdf', 'print'
    //     ],
        
    // } );
       
    },
    error: function(xhr, ajaxOptions, thrownError) {
        //On error, we alert user
        alert(thrownError);
    },
    complete: function() {
        //alert('update success'); 
    }
});

    }); 







$( document ).ready(function() {
    var d = new Date();
     n = d.getMonth()+1;
     if(n<10)
      n = '0'+n;

    y = d.getFullYear();
    m = y+'-'+n;
    $("#month").text('Payroll ('+m+')');

$("#payroll").load('{{ URL::to('loadpayroll')}}'+'/'+m);
//$("#month").text('Payroll   '+value);

    $.ajax({
    type: "get", // HTTP method POST or GET
    url: "{{ URL::to('loadpayrolldata')}}"+"/"+m, 

    success: function(data) {

        //console.log(data);

        $("#month").text('Payroll ('+m+')');

        
       
    },
    error: function(xhr, ajaxOptions, thrownError) {
        //On error, we alert user
        alert(thrownError);
    },
    complete: function() {
        //alert('update success'); 
    }
});



});


</script>



@endsection
