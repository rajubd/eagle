@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
       
        <div class="col s12 m12  ">
                  <div class="card-panel">
                    <h4 class="header2">Update Payroll</h4>
                    <div class="row">
                      <form class="col s12" action="{{url('editpayroll/'.$payroll->id)}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
                        <div class="row">
                          <div class="row">
                                <div class="input-field col s12 m3">
                                    <select name="employee_id" id="employee_id" required>
                                        <option value="{{$payroll->employee_id}}"  selected>{{$emp->employee_code}}</option>
                                
                                            @foreach($employees as $employee)
                                                <option value="{{$employee->id}}">{{$employee->employee_code}}
                                                </option>
                                            @endforeach
                                           
                                       
                                    </select>
                                    <label>Employee Id</label>
                                </div>

                               
                         
                        </div>
                        <div class="row">

                            <div class="input-field col s12 m6">
                               <label align="left">Month</label>
                            <input placeholder="Month" id="" name="payroll_month" type="month" required value="{{$payroll->payroll_month}}">
                            
                          </div>
                        </div>
                         <div class="row">
                        
                         
                          <div class="input-field col s12 m4">
                            <input placeholder="" id="email2" name="employee_name" type="text" required value="{{$emp->first_name}} {{$emp->last_name}} ">
                            <label for="email">Employee Name</label>
                          </div>

                          <div class="input-field col s12 m4">
                            <input placeholder="" id="email2" name="salary" type="text" required  value="{{$emp->salary}}">
                            <label for="email">Salary</label>
                          </div>

                          <div class="input-field col s12 m4">
                            <input placeholder="" id="email2" name="ot_rate" type="text"  value="{{$emp->ot_rate}}" >
                            <label for="email">OT Rate</label>
                          </div>
                        </div>

                        <div class="row">
                        
                         

                          <div class="input-field col s12 m6">
                            <input placeholder="" id="email2" name="ot_hour" type="text"  value="{{$payroll->ot_hour}} ">
                            <label for="email">OT Hour</label>
                          </div>

                        

                           <div class="input-field col s12 m6">
                            <input placeholder="" id="" name="estimated_ot_bill" type="text"  value="{{$payroll->estimated_ot_bill}} ">
                            <label for="email">Estimated OT Bill</label>
                          </div>
                        </div>

                        <div class="row">
                        
                         
                          <div class="input-field col s12 m4">
                            <input placeholder="" id="email2" name="paid_salary" type="text"  value="{{$payroll->paid_salary}} ">
                            <label for="email">Paid Salary</label>
                          </div>

                          <div class="input-field col s12 m4">
                            <input placeholder="" id="email2" name="paid_ot_bill" type="text" value="{{$payroll->paid_of_bill}} ">
                            <label for="email">Paid ot Bill</label>
                          </div>

                          <div class="input-field col s12 m4">
                            <input placeholder="" id="email2" name="estimated_salary" type="text"  value="{{$payroll->estimated_salary}} ">
                            <label for="email">Estimated Salary</label>
                          </div>
                          
                        </div>

                      
                        
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                <i class="mdi-content-send right"></i>
                              </button>
                            </div>
                          </div>


                        </div>
                      </form>
                    </div>
                  </div>
                </div>

        



<script type="text/javascript">
    $("#employee_id").change(function(){

       // var pageId =  $("#page-id").val();
        var e = document.getElementById("employee_id");
        var e_id = e.options[e.selectedIndex].value;
          //alert('Hy');
        $.ajax({               
                url:"<?php URL::to('') ?>/pos/getemployeedata/"+e_id,

                async:true,
                success: function(result)
                {  
                  //alert(result);
                  //console.log(result.id);
                 
                  $('input[name=employee_name]').val(result.first_name +' '+ result.last_name);
                  $('input[name=salary]').val(result.first_name +' '+ result.salary);

                  $('input[name=ot_rate]').val(result.first_name +' '+ result.ot_rate);
                  


                }/*Success*/
        });/*ajax*/
            


        
    });/*change*/




</script> 


       
    </div>
</div>
@endsection
