           
  <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
          

  <div class="">
    <div class="card">
        <div class="header">
              Payroll
            </div>

             <div class="body">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                   <tr>
                            <th>Employee Code</th>
                            <th>Name</th>
                            <th>Contract Type</th>
                            <th>Contract Base</th>
                            <th>OT Hour</th>
                            <th>Estimated Salary</th>
                            <th>Estimated OT Bill</th>
                            <th>Paid Salary</th>
                            <th>Paid OT Bill</th>
                            <th>Action</th>
                           
                        </tr>
                                </thead>
                                <tbody>
    @foreach($payrolls as $level)
    
          <?php 
          $emp = App\Models\Employee::find($level->employee_id);
          $type = App\Models\ContractType::find($emp->contract_type_id);
          $base = App\Models\ContractBase::find($emp->contract_base_id);
         // echo "<pre>";
          //print_r((array)$type['typename']);
           ?>
        <tr>
            <td>{{ $emp->employee_code }}</td>
           <td>{{ $emp->first_name }} {{ $emp->last_name }}</td>
            <td>
            @if(!empty($type))
                {{ $type->typename }}
            @endif
            </td>
            <td>
            @if(!empty($base))
                {{ $base->basename }}
            @endif
           
            </td>
            <td>{{ $level->ot_hour }}</td>
            <td>{{ $level->estimated_salary }}</td>
            <td>{{ $level->estimated_ot_bill }}</td>
            <td>{{ $level->paid_salary }}</td>
            <td>{{ $level->paid_ot_bill }}</td>
            <td>

                
          
               <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal{{$level->id}}">Edit</button>
 

 
                <a class="btn btn-danger" href="{{url('deletepayroll/'.$level->id)}}"><i class="mdi-action-delete" onclick="confirm('Are really want to delete this item ?');"></i>Delete</a>
            </td>
            @include('../eagle/payrolls/editPayrollModal')




            
        </tr>
          @endforeach
                       
                    </tbody>
                               
                            </table>
                        </div> 
    </div>
  </div>


<!-- 
  <script src="{{asset('js/pages/tables/editable-table.js')}}"></script>
 -->
     <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>