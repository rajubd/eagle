@extends('layouts.addrowlayout')

@section('content')
<style type="text/css">
  label{
    color: #000000;
  }
</style>


<div class="">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header bg-blue"><h4>New Verdor credit</h4></div>
        <div class="body">
           <form class="form-horizontal" action="{{url('createvendorcredit')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
            
                       <div class="row clearfix">
                         <div class="col-md-6">

                         
                         
      

                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vendor</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control chosen-select"  data-live-search="true" name="vendor_id">
                                <option value="1" selected>vendor 1</option>
                                @foreach($vendors as $vendor)
                                <option value="{{$vendor->id}}">{{$vendor->vendor_first_name}} {{$vendor->vendor_last_name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>




                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Credit Note</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="" name="credit_note">
                          </div>
                          </div>
                        </div>


                          
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Order No</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" name="order_no">
                          </div>
                          </div>
                        </div>


                       


                         </div>
                         <div class="col-md-6">
                                                  

                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vendor Credit Date</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="date" id="" class="datepicker form-control" name="vcredit_date">
                          </div>
                          </div>
                        </div>

                      

                        
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Item Rates Are </label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control chosen-select" data-live-search="true" name="tax">
                                <option value="">Choose one</option>
                                <option value="1">Tax Inclusive</option>
                                <option value="2">Tax Exclusive</option>
                               
                              </select>
                            </div>
                          </div>
                        </div>


                       

                         </div>

                       </div>

                       



      <table id="dynamicTable1"  class="table table-striped table-bordered table-hover">
        <thead>
            <th>Account</th>
            <th>Description</th>
            <th>Quantity</th>
            <th>Rate</th>
            <th>Tax</th>
            <th>Amount</th>
        </thead>
      <tr id="0">
        <td>
        <select id="account_id" class="form-control" name="acc_id[]">
          <option value="" selected="">Select Account</option>
          @foreach($accounts as $account)
          <option value="{{$account->id}}">{{$account->sales_account_name}}</option>
          @endforeach
        </select>
        </td>
        <td><input class="form-control"  type="text" placeholder="Description" id="description" name="acc_description[]" /></td>
       <td><input class="form-control quantity2"  type="number" id="quantity_0" autocomplete="0" onchange="UpdateAmount(this)"  name="quantity[]" onchange=""  min="0" value="1" step="any" /></td>
         <td><input class="form-control rate2"  type="number" id="rate_0" name="rate[]"  autocomplete="0" onchange="UpdateAmount(this)"  min="0" value="0" step="any" /></td>
        <td>
            <select id="tax" class="form-control" name="tax[]">
              <option value="" selected="">Select Tax</option>
              @foreach($taxes as $tax)
              <option value="{{$tax->id}}">{{$tax->tax_name}}</option>
            @endforeach
                
            </select>
        </td>
       
        <td><input class="form-control amount2"  type="number" readonly="" id="amount_0" name="amount[]"  min="0" value="0" step="any" /></td>
        
        
      </tr>
    </table>

  

  <table class="samplerow" style="display:none">
    <tr>
       
        <td>
        <select id="account_id" class="form-control" name="acc_id[]">
          <option value="" selected="">Select Account</option>
            @foreach($accounts as $account)
          <option value="{{$account->id}}">{{$account->sales_account_name}}</option>
        @endforeach
        </select>
        </td>
        <td><input class="form-control"  type="text" placeholder="Description" id="description" name="acc_description[]" /></td>
        <td><input class="form-control quantity"  type="number" id="" name="quantity[]" onchange="UpdateAmount(this)"  min="0" value="1" step="any" /></td>
        <td><input class="form-control rate"  type="number" id="" onchange="UpdateAmount(this)"  name="rate[]"  min="0" value="0" step="any" /></td>
        <td>
            <select id="tax" class="form-control" name="tax_id[]">
              <option value="" selected="">Select Tax</option>
              @foreach($taxes as $tax)
              <option value="{{$tax->id}}">{{$tax->tax_name}}</option>
            @endforeach
                
            </select>
        </td>
       
        <td><input class="form-control amount"  type="number" id="amount" name="amount[]"  min="0" value="0" step="any" readonly="" /></td>
        <td>
       <a id="delete-btn" style="cursor: pointer" onclick="UpdateAmount(this)" ><i class="remove  material-icons green-text text-darken-4">delete</i></a>
       </td>
    </tr>
  </table>
  <div class="row clearfix">
    <div class="body col-md-2">
      <a class="btn btn-primary btn-block" id="addrow"><i class=" large material-icons">add</i>Add new row</a>
      <!-- <input type="button" id="addrow" value="Add Row" class="btn btn-primary" /> -->
    </div>

    <div class="col-md-6 col-md-offset-4">
      <div class="panel panel-info">
      <!--  <div class="panel-body">
         
        </div> -->
        <div class="panel-heading">
         <div class="row">
            <div class="col-md-7">Total</div>
            <div class="col-md-5" align="right">
              <input class="form-control" align="right" type="number" name="total" id="total" value="0.00" readonly>
            </div>
          </div>
        </div>
       
      </div>
    </div>
    

  </div>
  <input type="hidden" name="rowcount" id="rowcount" value="">


                     
            <div class="row clearfix">
               <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-line" align="right">
                            <input type="submit" id="" name="submit" class="btn btn-success btn-lg" >
                        </div>
                    </div>
                </div>
            </div>




            </form>
        </div>

      </div>
    </div>
  </div>
</div>


              



<!-- 
<script src="{{asset('js/script_invoice.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function(){
    //alert("document ready");

    $("input[id=test2]").click(function(event){
        $(".row2").hide();
    });
    $("input[id=test1]").click(function(event){
        $(".row2").show();
    });

   $("#test5").click(function () {
    if ($("#test5").is(":checked")) {
        $(".sal").show();
        //$(".tracker").show();
    }
    else {
        $(".sal").hide();
        //$(".tracker").hide();
    }
});
});
</script>

 -->


<script type="text/javascript">
  $(function() {
  $('.dropdown-menu a').click(function() {
    console.log($(this).attr('data-value'));
    $(this).closest('.dropdown').find('input.countrycode')
      .val( $(this).attr('data-value') );
  });
});
</script>



<script type="text/javascript">
    
  jQuery(document).ready(function() {
      var id = 0;
      jQuery("#addrow").click(function() {
        id++;           
        var row = jQuery('.samplerow tr').clone(true);
        row.find(".amount").val(0);

        row.find(".amount").attr('id','amount_'+id);
        row.find(".amount").attr('autocomplete',id);

        row.find(".quantity").attr('id','quantity_'+id);
        row.find(".quantity").attr('autocomplete',id);

        row.find(".rate").attr('id','rate_'+id);
        row.find(".rate").attr('autocomplete',id);

        row.find("#delete-btn").attr('autocomplete',id);

        row.attr('id',id);
        row.appendTo('#dynamicTable1');        
        return false;
    });        
        
  $('.remove').on("click", function() {
  $(this).parents("tr").remove();

     var tr = $('tr').length - 2;
    alert(tr);
    document.getElementById("rowcount").value = tr;
    //alert(tr);
    var total =0;
    for (var i = 0; i < tr; i++) {
      //console.log(i);
      var myAmount = $('#amount_'+i).val();
      total +=  parseFloat(myAmount);
    }
    //alert(total);

    document.getElementById("total").value = total;

});
});


  </script>




<script type="text/javascript">
  function UpdateAmount(x) {
    //$(this.)
    //alert(x);
    var i = x.autocomplete;
    //console.log(i);
    var amount = $('#quantity_'+i).val() * $('#rate_'+i).val();
    console.log(i);
    document.getElementById("amount_"+i).value = amount;

    var tr = $('tr').length - 2;
    //$('#rowcount').val() = tr;
    document.getElementById("rowcount").value = tr;
    //alert(tr);
    var total =0;
    //alert($('.amount').val());
    for (var i = 0; i < tr; i++) {
      
      var myAmount = $('#amount_'+i).val();
      //console.log(myAmount);
      total +=  parseFloat(myAmount);
    }
    //alert(total);

    document.getElementById("total").value = total;
    //$( "#total" ).html( "iuhe" );


  }
</script>


@endsection













