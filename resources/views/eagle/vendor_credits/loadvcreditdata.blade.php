
<div class="body" align="right">
	<h2 class="text-uppercase">Vendor Credit</h2>
	<h6>Credit Note: {{$vcredit->credit_note}}</h6>
	Credit Remaining
	<h4>$ {{$vcredit->total}}</h4>
	<hr>
	<div class="row clearfix">
		<div class="col-md-9">Order Number:</div>
		<div class="col-md-3">{{$vcredit->order_no}}</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-9">Bill Date:</div>
		<div class="col-md-3">{{$vcredit->vcredit_date}}</div>
	</div>
	

</div>

<div class="body">	
	

			<?php 
				$vcredit_acc = App\Models\Vcredit_account::where('vcredit_id',$vcredit->id)->get();
			 ?>
	<div class="row clearfix">
		<table class="table table-striped">
			<thead>
				<tr class="bg-teal">
					<th> #</th>
					<th>Description</th>
					<th>Qty</th>
					<th>Rate</th>
					<th>Amount</th>
					
				</tr>
			</thead>
			<tbody>
			<?php $i=1; ?>
			@foreach($vcredit_acc as $acc)
				<tr>
					<td>{{$i++}}</td>
					<td>{{$acc->acc_description}}</td>
					<td>{{$acc->quantity}}</td>
					<td>{{$acc->rate}}</td>
					<td>{{$acc->amount}}</td>
				</tr>
			@endforeach
			
			</tbody>
		</table>
	</div>
</div>