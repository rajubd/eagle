@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-10">
                    <div class="block-header">
                        <h2>Vendor Credit</h2>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{url('createvendorcredit')}}"  class="btn btn-primary"><i class="small material-icons">add</i> New Vendor Credit</a>
                </div>
            </div>
        </div>
        <div class="body">
            <div id="table-datatables">
              <div class="row">
                <div class="col-md-12">
                   <table class="table table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr class="bg-teal">
                            <th>Date</th>
                            <th>Credit Note</th>
                            <th>Order Number</th>
                            <th>Vendor Name</th>
                           
                            <th>Amount</th>
                            
                            
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($vcredits as $v)
                     <tr style="cursor: pointer" onclick="GetPayment({{$v->id}});">
                        <td>{{$v->vcredit_date}}</td>
                        <td>{{$v->credit_note}}</td>
                        <td>{{$v->order_no}}</td>
                        <td>
                        <?php 
                            $vendor = App\Models\Vendor::find($v->vendor_id);
                            if(!empty($vendor))
                            {
                                print_r($vendor->vendor_first_name." ".$vendor->vendor_last_name);
                            }
                         ?>
                        </td>
                        <td>{{$v->total}}</td>
                        
                    </tr>
                    @endforeach
                   
                       
                    </tbody>
                  </table>


                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');
    })
</script>



<script type="text/javascript">
    function GetPayment(id) {
        $('.tr').css( 'cursor', 'pointer' );
        window.location.href = '{{url('vcreditdetails')}}';
    }
</script>


@endsection