<div class="">
  
  <ul class="list-group">
   @foreach($vcredits as $vcredit)
   <li id="{{$vcredit->id}}" class="list-group-item ls" style="cursor: pointer;">
      <div class="row">
           <div class="col-md-6">
               {{$vcredit->vendor_id}}<br>
               {{$vcredit->order_no}}<br>
               {{$vcredit->vcredit_date}}
           </div>
           <div class="col-md-6" align="right">
               {{$vcredit->total}}<br>
               {{$vcredit->credit_note}}
              
           </div>
       </div>
   </li>
  
   @endforeach
  </ul>
</div>

