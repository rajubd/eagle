
              <div class="body">
                <div class="col-md-12">
                  

                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                    <thead>
                        <tr>
                            
                            <th>Holiday</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Reason of Holiday</th>
                            <th>Action</th>
                           
                        </tr>
                    </thead>
                 
                    
                 
                    <tbody>
                    @foreach($leaves as $leave)
                        <tr>
                            <td>{{ $leave->holiday_name }}</td>
                            
                            <td>{{ $leave->start_date }}</td>
                            <td>{{ $leave->end_date }}</td>
                            <td>{{ $leave->reason }}</td>
                            <td>
                                
                                <a href="{{url('editholiday/'.$leave->id)}}"><i class="more_edit"></i>Edit</a> | 
                                <a href="{{url('deleteholiday/'.$leave->id)}}"><i class=" material-icons"></i>Remove</a>
                            </td>
                            
                        </tr>
                    @endforeach
                       
                    </tbody>
                  </table>
                </div>
              </div>
       
            <div class="divider"></div> 