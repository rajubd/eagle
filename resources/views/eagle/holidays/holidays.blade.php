


@extends('layouts.main')

@section('content')
<div class="">
    <div class="card">
        <div class="header">
            <div class="">
                
                <div class="align-right">
                    <a class="btn btn-warning"  href="{{url('/createholiday')}}">Add Holiday</a>
                </div>
            </div>
        </div>
        <div class="body">
           
        
            <div class="row">
                 @include('eagle/holidays/holidayTable')
            </div>
        
       
   
        </div>
    </div>
</div>
@endsection


