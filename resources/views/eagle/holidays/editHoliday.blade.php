

@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row">
       
        <div class="col-md-6 col-md-offset-3 ">
                  <div class="card">
                    <h4 class="header">Add Holiday</h4>
                    <div class="row clearfix body">
                      <form class="col s12" action="{{url('editholiday/'.$leave->id)}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}

                       <div class="row clearfix">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="holiday_name" value="{{$leave->holiday_name}}">
                                            <label class="form-label">Title</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="date" class="form-control" name="start_date">
                                            <label class="form-label">Start date</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="date" class="form-control" name="end_date">
                                            <label class="form-label">End date</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="reason" value="{{$leave->reason}}">
                                            <label class="form-label">Reason</label>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="col-md-8 col-md-offset-2">
                                   <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                                </div>
                       </div>

                        </div>
                      </form>
                    </div>
                  </div>
                </div>
       
       
    </div>
</div>
@endsection













