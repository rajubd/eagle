@extends('layouts.main')

@section('content')
<div class="">



    <div class="card">
        <div class="header ">
            <div class="row clearfix">
                <div class="col-md-10">
                    <div class="block-header">
                        <h2>All Expenses</h2>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="{{url('createexpense')}}"  class="btn btn-primary btn-block waves-effect m-r-20"><i class="small material-icons">add</i> New Expense</a>
                </div>
            </div>
        </div>
        <div class="body">
            <div id="table-datatables">
              <div class="row">
                <div class="col-md-12">
                   <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Expance Account</th>
                            <th>Reference</th>
                            <th>Vendor Name</th>
                            <th>Paid Through</th>
                            <th>Customer name</th>
                            <th>Status</th>
                            <th>Amount</th>
                            
                           
                        </tr>
                    </thead>
                 
                   
                 
                    <tbody>
                    @foreach($expences as $exp)
                        <tr>
                           
                          <td>{{$exp->expence_date}}</td>
                          <td>{{$exp->sales_account_name}}</td>
                          <td>{{$exp->reference}}</td>
                          <td>{{$exp->salutation}} {{$exp->vendor_first_name}} {{$exp->vendor_last_name}}</td>
                          <td>{{$exp->paid_through}}</td>
                          <td>{{$exp->customer_first_name}} {{$exp->customer_last_name}}</td>
                          <td></td>
                          <td>{{$exp->amount}}</td>
                            
                        
                            
                        </tr>
                    @endforeach
                    </tbody>
                  </table>


                </div>
              </div>
            </div> 
            <br>
            <div class="divider"></div> 
        </div>
    </div>
</div>


@endsection