
              <form class="form-horizontal" action="{{url('createmilage')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
            
                       <div class="row clearfix">
                         <div class="col-md-8">

                         

                          <div class="form-group">
                          <label class="control-label col-sm-3" for="">Date</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="datepicker form-control" id="date2" name="milage_date">
                          </div>
                          </div>
                        </div>
                         
      

                       <div class="form-group" id="expanseSection">
                          <label class="control-label col-sm-3" for="email">Claimant</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control "  data-live-search="true" name="claimant_id">
                                <option value="1">Claimant 1</option>
                                <option value="2">Claimant 2</option>
                                <option value="3">Claimant 3</option>
                                <option value="4">Claimant 4</option>
                                <option value="5">Claimant 5</option>
                              </select>
                            </div>
                          </div>
                        </div>

                       <div class="form-group" id="expanseSection">
                          <label class="control-label col-sm-3" for="email">Calculate mileage using</label>
                          <div class="col-sm-9">
                            <input name="milage_calculate" type="radio" value="0" checked id="test1" />
                      <label for="test1">Distance travelled</label>
                               <input name="milage_calculate" type="radio" value="1" id="test2" />
                      <label for="test2">Odometer reading</label>
                          </div>
                        </div>


                <div class="form-group" id="expanseSection">
                          <label class="control-label col-sm-3" for="email">Distance</label>
                          <div class="col-sm-9">
                           
                            <div class="form-line">
                              
                              <div class="form-group has-primary has-feedback">
                            
                              <div class="input-group">
                                
                                <input type="text" class="form-control" id="inputGroupSuccess4" aria-describedby="inputGroupSuccess4Status" name="distance">
                                <span class="input-group-addon">Kilometer(s)</span>
                              </div>
                             
                            </div>

                            </div>


                          </div>
                        </div>




                       <div class="form-group " id="amountSection">
                          <label class="control-label col-sm-3" for="">Amount</label>
                          <div class="col-sm-5">
                            <div class="form-line">
                            <div class="row">
                              <div class="col-md-6">
                                 <select class="form-control"  data-live-search="true" name="currency_id">
                                <option value="1">USD</option>
                                <option value="2">BDT</option>
                                <option value="3">MRP</option>
                                <option value="4">EUR</option>
                                
                          </select>
                              </div>
                              <div class="col-md-4">
                                 <input type="text" class="form-control" id="" name="amount">
                              </div>
                              
                         
                            </div>
                         
                          </div>
                          </div>
                          <div class="col-md-4">
                            <label>Tax</label>
                            <div class="form-line">
                               <select class="form-control"  data-live-search="true" name="tax_id">
                                <option value="" selected>Choose Tax</option>
                                <option value="1">Tax 1</option>
                                <option value="2">Tax 2</option>
                                <option value="3">Tax 3</option>
                                <option value="4">Tax 4</option>
                                <option value="5">Tax 5</option>
                          </select>
                            </div>
                            
                          </div>
                        </div>


                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Paid Through</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control"  data-live-search="true" name="paid_through">
                                <option value="1">Petty Cash</option>
                                <option value="2">Card</option>
                                
                              </select>
                            </div>
                          </div>
                        </div>


                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vendor</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control"  data-live-search="true" name="vendor_id">
                                <option value="1">vendor 1</option>
                                <option value="2">vendor 2</option>
                                <option value="3">vendor 3</option>
                                <option value="4">vendor 4</option>
                                <option value="5">vendor 5</option>
                              </select>
                            </div>
                          </div>
                        </div>



                          
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Reference</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="" name="reference">
                          </div>
                          </div>
                        </div>

                         
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Notes</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <textarea class="form-control" name="note"></textarea>
                          </div>
                          </div>
                        </div>

                      
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Customer Name</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control"  data-live-search="true" name="customer_id">
                                <option value="1">Customer 1</option>
                                <option value="2">Customer 2</option>
                                <option value="3">Customer 3</option>
                                <option value="4">Customer 4</option>
                                <option value="5">Customer 5</option>
                              </select>
                            </div>
                          </div>
                        </div>




                       


                       


                         </div>
                         <div class="col-md-3">
                                                  
                            <label for="input-file-now">Choose File</label>
                            <input type="file" id="input-file-now" class="dropify" name="milage_image"/>
                      
                         </div>

                       </div>


                     
<div class="row clearfix">
   <div class="col-md-3 col-md-offset-2">
        <div class="form-group">
            <div class="form-line" align="">
                <input type="submit" id="" name="submit" class="btn btn-success btn-block" >
            </div>
        </div>
    </div>
</div>

            </form>
