
              <form class="form-horizontal" action="{{url('createexpense')}}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}
            
                       <div class="row clearfix">
                         <div class="col-md-8">

                         

                          <div class="form-group">
                          <label class="control-label col-sm-3" for="">Date</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="datepicker form-control" id="date" name="expence_date">
                          </div>
                          </div>
                        </div>
                         
      

                       <div class="form-group" id="expanseSection">
                          <label class="control-label col-sm-3" for="email">Expence Account</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control chosen-select"  data-live-search="true" name="account_id">
                                <option value="" selected>Choose Account</option>
                                @foreach($accounts as $acc)
                                <option value="{{$acc->id}}">{{$acc->sales_account_name}}</option>
                                @endforeach
                                
                              </select>
                            </div>
                          </div>
                        </div>




                       <div class="form-group" id="amountSection">
                          <label class="control-label col-sm-3" for="">Amount</label>
                          <div class="col-sm-5">
                            <div class="form-line">
                            <div class="row">
                              <div class="col-md-6">
                                 <select class="form-control"  data-live-search="true" name="currency_id">
                                @foreach($currencies as $currency)
                                <option value="{{$currency->id}}">{{$currency->foreign_currency_code}}</option>
                                @endforeach
                          </select>
                              </div>
                              <div class="col-md-4">
                                 <input type="text" class="form-control" id="" name="amount">
                              </div>
                              
                         
                            </div>
                         
                          </div>
                          </div>
                          <div class="col-md-4">
                            <label>Tax</label>
                            <div class="form-line">
                               <select class="form-control "  data-live-search="true" name="tax_id">
                                <option value="" selected>Choose Tax</option>
                               @foreach($taxes as $tax)
                                <option>{{$tax->foreign_currency_code}}</option>
                                @endforeach
                          </select>
                            </div>
                            
                          </div>
                        </div>



                          
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Reference</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <input type="text" class="form-control" id="" name="reference">
                          </div>
                          </div>
                        </div>

                         
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="">Notes</label>
                          <div class="col-sm-9">
                            <div class="form-line">
    
                          <textarea class="form-control" name="note"></textarea>
                          </div>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Paid Through</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control  chosen-select"  data-live-search="true" name="paid_through">
                              <option value="" selected>Choose one</option>
                                <option value="1">Petty Cash</option>
                                <option value="2">Card</option>
               
                              </select>
                            </div>
                          </div>
                        </div>


                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vendor</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control  chosen-select"  data-live-search="true" name="vendor_id">
                                <option value="" selected>Choose Vendor</option>
                                @foreach($vendors as $vendor)
                                <option value="{{$vendor->id}}">{{$vendor->vendor_first_name}} {{$vendor->vendor_last_name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>




                      
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Customer Name</label>
                          <div class="col-sm-9">
                            <div class="form-line">
                              <select class="form-control  chosen-select"  data-live-search="true" name="customer_id">
                                <option value="" selected="">Choose Customer</option>
                                 @foreach($customers as $customer)
                                <option value="{{$customer->id}}">{{$customer->customer_first_name}} {{$customer->customer_last_name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>




                       


                       


                         </div>
                         <div class="col-md-3">
                                                  
                            <label for="input-file-now">Choose File</label>
                            <input type="file" id="input-file-now" class="dropify" name="expanse_image"/>
                      
                         </div>

                       </div>


                     
<div class="row clearfix">
   <div class="col-md-3 col-md-offset-2">
        <div class="form-group">
            <div class="form-line" align="">
                <input type="submit" id="" name="submit" class="btn btn-success btn-block" >
            </div>
        </div>
    </div>
</div>

            </form>
