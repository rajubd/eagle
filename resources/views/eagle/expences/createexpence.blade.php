@extends('layouts.addrowlayout')

@section('content')
<style type="text/css">
  label{
    color: #000000;
  }
</style>


<div class="">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="header">Create Expence</div>
        <div class="body">

        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   
                       
                        <div class="body">
                            <div class="row clearfix">
                               
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_2" id="expanse" data-toggle="tab">Record Expense</a></li>
                                        <li role="presentation"><a href="#profile_animation_2" data-toggle="tab" id="milage">Record Mileage</a></li>
                                        <li role="presentation"><a href="#messages_animation_2" data-toggle="tab">Bulk Add Expenses</a></li>
                                       
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="home_animation_2">
                                            
                                        
                 @include('eagle/expences/expenseTab')

                                           
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_2">

                 @include('eagle/expences/milageTab')
                 
                                           
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                @include('eagle/expences/bulkexpenseTab')
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
               
                </div>
            </div>






        </div>

      </div>
    </div>
  </div>
</div>


              



<!-- 
<script src="{{asset('js/script_invoice.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function(){
    //alert("document ready");

    $("input[id=test2]").click(function(event){
        $(".row2").hide();
    });
    $("input[id=test1]").click(function(event){
        $(".row2").show();
    });

   $("#test5").click(function () {
    if ($("#test5").is(":checked")) {
        $(".sal").show();
        //$(".tracker").show();
    }
    else {
        $(".sal").hide();
        //$(".tracker").hide();
    }
});
});
</script>

 -->


<script type="text/javascript">
  $(function() {
  $('.dropdown-menu a').click(function() {
    console.log($(this).attr('data-value'));
    $(this).closest('.dropdown').find('input.countrycode')
      .val( $(this).attr('data-value') );
  });
});
</script>


<!-- <script type="text/javascript">
  $('#milage').on('click',function (e) {
    //alert('hy');
    $('#expanseSection').hide();
  })

    $('#expanse').on('click',function (e) {
    //alert('hy');
    $('#expanseSection').show();
  })
</script>


 -->


@endsection













