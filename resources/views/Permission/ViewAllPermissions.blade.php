@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row"  style="margin-top: 0">
     <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="header">All Permission
                <div align="right"><a href="{{url('addpermission')}}" class="btn btn-success">Add Permission</a></div>
                </div>
                <div class="body">
                <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sl</th>
                            <th>Permissions</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                        @foreach($permissions as $per)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$per->name}}</td>
                            <td>
                                <a href="{{asset('editpermission/'.$per->id)}}" class="btn btn-primary">Edit</a>
                                <a href="{{asset('deletepermission/'.$per->id)}}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
