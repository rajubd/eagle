@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row"  style="margin-top: 100px">
        
        <div class="col-md-6 col-md-offset-3">
        
            <div class="panel panel-default">
                <div class="panel-heading"><label for="">Permission Name</label></div>

                <div class="panel-body">
                    <div>
                        <form action="{{asset('updatepermission/'.$permission->id)}}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                
                                <input type="text" class="form-control" value="{{ $permission->name }}" name="name">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <input type="hidden" name="updated_by" value="{{Auth::User()->id}}">
                            <button class="btn btn-success" type="post">Confirm</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
