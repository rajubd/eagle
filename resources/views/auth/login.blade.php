@extends('layouts.login-layout')

@section('content')

<br><br><br><br><br><br><br>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-7">
            <div class="panel panel-default panel-info" style="box-shadow:15px 15px 15px rgba(0,0,0,.3)">
                <div class="panel-heading" align="center">Login</div>
                <div class="panel-body">
                   <div class="col-md-10 col-md-offset-1">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                           

                            <div class="col m8 offset-m2">
                             <!-- <label for="email" class="">E-Mail Address</label> -->
                                <input placeholder="E-Mail Address" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            

                            <div class="col m8 offset-m2">
                            <!-- <label>Password</label> -->
                                <input placeholder="Password" id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <div class="col m8 offset-m2">
                                <button type="submit" class="btn btn-success btn-block">
                                    Login
                                </button>

                               <!--  <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a> -->
                            </div>
                        </div>
                        <div class="row"></div>
                    </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
