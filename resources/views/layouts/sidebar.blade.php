  <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{asset('images/user.png')}}" width="48" height="48" alt="User" />

                </div>
                <div class="info-container">
                 <?php 
                    $user =  \Auth::user()->name;
                    $email = \Auth::user()->email;
                     ?>
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$user}}</div>
                    <div class="email">{{$email}}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i> Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="{{url('/')}}">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                   
                   

                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons col-green">swap_calls</i>
                            <span>Adminstrator</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('payrolls')}}">Payrolls</a>
                            </li>
                  
                            <li>
                                <a href="{{url('employees')}}">Employees</a>
                            </li>

                            <li>
                                <a href="{{url('departments')}}">Departments</a>
                            </li>
                            <li>
                                <a href="{{url('levels')}}">Levels</a>
                            </li>
                            <li>
                                <a href="{{url('designations')}}">Designations</a>
                            </li>
                            <li>
                                <a href="{{url('contracttypes')}}">Contract Type</a>
                            </li>
                            <li>
                                <a href="{{url('contractbases')}}">Contract Base</a>
                            </li>
                            <li>
                                <a href="{{url('holidays')}}">Holiday</a>
                            </li>
                            <li>
                                <a href="{{url('leaves')}}">Leave Records</a>
                            </li>
                           
                            <li>
                                <a href="{{url('leaveplanners')}}">Leave Planner</a>
                            </li>
                          
                        </ul>
                    </li>

                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>Items</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('items')}}">Items</a>
                            </li>
                            <li>
                                <a href="{{url('itemadjustments')}}">Item Adjustments</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons col-red">swap_calls</i>
                            <span>Accountant</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('chartofaccounts')}}">Chart of Accounts</a>
                            </li>
                            <li>
                                <a href="{{url('journals')}}">Manual Journals</a>
                            </li>
                            <li>
                                <a href="{{url('currencyadjustments')}}">Currency Adjustments</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons col-light-blue">swap_calls</i>
                            <span>Sales</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('invoices')}}">Invoices</a>
                            </li>
                            <li>
                                <a href="{{url('estimates')}}">Estimates</a>
                            </li>
                             <li>
                                <a href="{{url('payments_received')}}">Payments Received</a>
                            </li>
                             <li>
                                <a href="{{url('credit_notes')}}">Credit Notes</a>
                            </li>
                        </ul>
                    </li>
                  <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons col-amber">swap_calls</i>
                            <span>Purchases</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('bills')}}">Bills</a>
                            </li>
                            <li>
                                <a href="{{url('expenses')}}">Expenses</a>
                            </li>
                             <li>
                                <a href="{{url('payments')}}">Payments</a>
                            </li>
                             <li>
                                <a href="{{url('vendor_credits')}}">Vendor Credits</a>
                            </li>
                        </ul>
                    </li>
                  


                   
                 
                      <li>
                        <a href="{{url('customers')}}">
                            <i class="material-icons">update</i>
                            <span>Customers</span>
                        </a>
                    </li>
                      <li>
                        <a href="{{url('vendors')}}">
                            <i class="material-icons col-red">donut_large</i>
                            <span>Vendors</span>
                        </a>
                    </li>

                      <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>User Management</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('users')}}">Users</a>
                            </li>
                            <li>
                                <a href="{{url('roles')}}">Roles</a>
                            </li>
                             <li>
                                <a href="{{url('permissions')}}">Permissions</a>
                            </li>
                             <li>
                                <a href="{{url('assignpermission')}}">Assign Permissions</a>
                            </li>
                        </ul>
                    </li>

                    
                  <!-- 
                    <li>
                        <a href="{{url('suppliers')}}">
                            <i class="material-icons col-amber">donut_large</i>
                            <span>Suppliers</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('')}}">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>Reports</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('recievings')}}">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>Recievings</span>
                        </a>
                    </li>
                     <li>
                        <a href="{{url('sales')}}">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>Sales</span>
                        </a>
                    </li>
                     <li>
                        <a href="{{url('expances')}}">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>Expances</span>
                        </a>
                    </li> -->
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="binarylogic.co" target="_blank">Binary Logic</a>.
                </div>
                
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->























































        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                       

                         <li data-theme="indigo" class="active">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>

                        <li data-theme="red" >
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                       
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                       
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                    </ul>
                </div>
               
            </div>
        </aside>