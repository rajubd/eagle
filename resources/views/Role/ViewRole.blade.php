@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row"  style="margin-top: 100px">
     <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">All Roles
                <div align="right"><a href="{{url('createrole')}}" class="btn btn-success">Add Roles</a></div>
                </div>
                <div class="panel-body">
                <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th align="center">Sl</th>
                            <th align="center">Role Type</th>
                            <th align="center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                        @foreach($roles as $role)
                        <tr>
                            <td align="center">{{$i++}}</td>
                            <td align="center">{{$role->roleName}}</td>
                             <td align="center">
                                <a href="{{url('viewsinglerole/'.$role->id)}}" class="btn btn-success">Permissions</a>
                                @if(ACL::has('updaterole'))
                                <a href="{{url('editrole/'.$role->id)}}" class="btn btn-primary">Update</a>
                                @endif
                                @if(ACL::has('deleterole'))
                                <a href="{{url('deleterole/'.$role->id)}}" class="btn btn-danger" onClick="return confirm('Are you absolutely sure you want to delete?')">Delete</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>

   



@endsection
