@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row"  style="margin-top: 100px">
        
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"><label for="">Update Role</label></div>

                <div class="panel-body">
                    <div>
                        <form action="{{asset('updaterole/'.$role->id)}}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" value="{{$role->roleName}}" name="name">
                                </div>
                            </div>
                    

                            <input type="hidden" name="updated_by" value="{{Auth::User()->id}}">
                            <button class="btn btn-success" type="post">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
