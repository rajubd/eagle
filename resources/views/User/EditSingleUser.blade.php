@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row"  style="margin-top: 0">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="header">Edit User Information</div>

                <div class="body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/updatesingleuser') }}/{{$user->id}} " enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                            <div class="form-line">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus>
                               </div>

                               

                                
                            </div>
                        </div>
                        <input type="hidden" name="userid" value="{{ $user->id}}">


                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">User Photo</label>

                            <div class="col-md-6">
                            <div class="form-line">
                                 <input id="photo" type="file" class="form-control" name="photo"
                                       onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                            </div>
                               
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">Photo Preview</label>

                            <div class="col-md-6">
                                <img id="blah" alt="your image" class="img img-thumbnail" style="width: auto; height: 300px;" src="{{asset('images/users/'.$user->photo)}}" />
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                               <div class="form-line">
                                    <input id="email"  type="email" class="form-control" name="email" value="{{ $user->email}}" required>
                               </div>

                            </div>
                        </div>

                      

                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">Role</label>

                            <div class="col-md-6">
                            <div class="form-line">
                                   <select name="role_id" class="form-control chosen-select">
                                        <option value="{{$thisRole->id}}" selected>{{$thisRole->roleName}}</option>
                                        @foreach($roles as $role)
                                           @if($role->id != $thisRole->id)
                                           <option value="{{$role->id}}">{{$role->roleName}}</option>
                                           @endif
                                        @endforeach
                                   </select>

                               </div>
                                
                             
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="photo" class="col-md-4 control-label">Change Password</label>

                            <div class="col-md-6">
                            <div class="form-line">
                                    <input id="password" type="password" value="" class="form-control" name="password" >

                               </div>
                                
                             
                            </div>
                        </div>

                       
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Confirm
                                </button>
                            </div>
                        </div>


    
                      
                 

       
                        
                              



                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>








@endsection
