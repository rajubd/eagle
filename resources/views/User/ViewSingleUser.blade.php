@extends('layouts.main')

@section('content')
<div class="container-fluid">
    <div class="row"  style="margin-top: 0px">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="header">
                <div class="row">
                    <div class="col-md-8">User Frofile</div>
                    <div class="col-md-4" align="right">
                        <a class="btn btn-primary" href="{{url('editsingleuser')}}/{{$user->id}}">Update Informatin</a>
                    </div>
                </div>
                </div>

                <div class="body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/storeuser') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" disabled="" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus>

                                
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Photo</label>

                            <div class="col-md-6">
                                <img class="img-thumbnail" src="{{asset('images/users/'.$user->photo)}}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" disabled="" type="email" class="form-control" name="email" value="{{ $user->email}}" required>

                            </div>
                        </div>




                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Role</label>

                            <div class="col-md-6">
                                <input id="photo" type="text" disabled="" value="{{ $role->roleName }}" class="form-control" name="" >
                            </div>
                        </div> 


    
                  
                        
                              



                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>








@endsection
