@extends('layouts.main')
@section('style')
<style type="text/css">
  tr,th{
    text-align: center;
  }
</style>
@endsection
@section('content')
<div class="container-fluid">
  <div class="">
    <div class="row">
        <div class="col-md-12 ">
        <div class="panel panel-default">
            <div class="panel-heading">All Users
              <div align="right"><a href="{{url('createuser')}}" class="btn btn-success">Add User</a></div>
            </div>
                

                <div class="panel-body">
                  
                      
                      <table id="example" class="table table-striped" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            
                            <th align=""> Role</th>
                            <th align=""> Name</th>
                            <th align=""> Email</th>
                  
                            <th align=""> Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                         @foreach($users as $user)
                            <tr>
                            
                                
                                <td align="">
                                
                                <?php print_r($user->roleName); ?></td>

                                <td align="">{{ $user->name }}</td>
                                <td align="">{{ $user->email }}</td>
                                
                                <td align="">
                          

                                <a class="btn btn-primary" href="viewsingleuser/{{$user->id}}">
                                 <i class="fa fa-search-plus fa-fw fa-lg" style="color: green;font-size: 24px;" aria-hidden="true"></i>View</a>

                                  <a class="btn btn-success" href="editsingleuser/{{$user->id}}">
                                 <i class="fa fa-pencil-square-o fa-fw fa-lg" style="color: teal;font-size: 24px;" aria-hidden="true"></i>Edit</a>


                                
                                <a class="btn btn-danger" href="removeuser/{{ $user->id }}
                                " onClick="return confirm('Are you absolutely sure you want to delete?')"><i class="fa fa-trash-o fa-fw fa-lg" style="color: red;font-size: 24px;"></i>Remove</a>
                               

                                </td>
                            </tr>
                         @endforeach
                        </tbody>
                      </table>
                      
                  
                </div>
        </div>
        </div> 
    </div>
    
</div>
</div>


@endsection

