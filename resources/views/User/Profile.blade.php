@extends('layouts.akash')

@section('content')
<div class="container">
    <div class="row"  style="margin-top: 100px">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
               <!--  <div class="panel-heading">User Details
                    <div align="right">
                        <a class="btn btn-primary" style="color: #ffffff;" href="{{ url('/newPost') }}">
                          <i class="fa fa-plus" aria-hidden="true"></i> Edit
                        </a>
                    </div>
                </div> -->

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/updateprofile/'.$user->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="name" value="{{ $user->name }}" autofocus required>

                               
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}"  required >

                             
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">User Photo</label>

                            <div class="col-md-6">
                                <input id="photo" type="file" class="form-control" name="photo"
                                       onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">Photo Preview</label>

                            <div class="col-md-6">
                                <img id="blah" alt="your image" class="img img-thumbnail" style="width: auto; height: 300px;" src="{{asset('public/uploads/userPhoto/'.$user->photo)}}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Change Password</label>

                            <div class="col-md-6">
                                <input id="password" value="" type="password" class="form-control" name="password" >
                            </div>
                        </div>
                        
                        <input type="hidden" name="id" value="{{$user->id}}">

                 
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input type="submit" id="update" class="btn btn-primary" value="Update">
                                    
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
