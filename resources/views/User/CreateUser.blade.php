@extends('layouts.main')

@section('content')
<div class="">
    <div class="row"  style="margin-top: 10px">
        <div class="col-md-12">
            @if( ACL::has('nolimit') )
            <div class="panel panel-default">
                <div class="panel-heading">Add New User</div>
           
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/storeuser') }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"  autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="" class="col-md-4 control-label">User Photo</label>

                            <div class="col-md-6">
                                <input id="photo" type="file" class="form-control" name="photo"
                                       onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                            </div>
                        </div>

                        

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" >

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                            </div>
                        </div>

                       


                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Role</label>

                            <div class="col-md-6">
                          <!--       <select name="role_id" id="AddrType"  onchange="showTextBox()"  class="form-control"> -->
                          <select class="form-control" name="role_id" id="travel_arriveVia" onchange="showfield(this.options[this.selectedIndex].value)">
                                    <option selected disabled="disabled">Select Role</option>
                                   
                                    @if(ACL::has('createsuperadmin'))
                                    <option class="form-control" value="1" >Super Admin</option>
                                    @endif
                                    @foreach($roles as $role)
                                        @if($role->id != 1)
                                        <option class="form-control" value="{{$role->id}}" >{{$role->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                               
                            </div>
                        </div> 

        
            
                      

                        
                              



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Confirm
                                </button>
                            </div>
                        </div>
                    </form>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <img id="blah" alt="your image" class="img img-thumbnail" style="width: auto; height: 300px;" src="{{asset('default.png')}}" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
           
            @endif
        </div>
    </div>
</div>






@endsection
