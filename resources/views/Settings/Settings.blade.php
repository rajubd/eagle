@extends('layouts.main')

<?php
    if(isset($setting)){
        $logo   = $setting->logo;
        $url    = url('/')."/images/".$logo;
    }
    else{
        $url    = asset('default.png');
    }
    
?>

@section('content')
<div class="">
    <div class="row"  style="margin-top: 10px">
        <div class="col-md-12">
            @if( ACL::has('nolimit') )
            <div class="panel panel-default">
                <div class="panel-heading">Settings</div>
           
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/post-settings') }}"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                         <div class="form-group">
                            <label for="" class="col-md-4 control-label">User Photo</label>

                            <div class="col-md-6">
                                <input id="photo" type="file" class="form-control" name="photo"
                                       onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                            </div>
                        </div>
        
            
                      

                        
                              



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Confirm
                                </button>
                            </div>
                        </div>
                    </form>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <img id="blah" alt="your image" class="img img-thumbnail" style="width: auto; height: 300px;" src="{{ $url }}" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

           
            @endif

        </div>
    </div>
</div>






@endsection
